package alg;

import java.io.*;
import java.util.StringTokenizer;

public class Main {
static int[][] nodes;
static int maxDepth = 0;


public static void main(String[] args) throws IOException {
    BufferedReader br = new BufferedReader(new InputStreamReader(new BufferedInputStream(System.in)));
    StringTokenizer token = new StringTokenizer(br.readLine());
    int nodesTotal = Integer.parseInt(token.nextToken());
    int rootValue = Integer.parseInt(token.nextToken());
    int inputLines = Integer.parseInt(token.nextToken());
    if (inputLines < 1) {
        return;
    }
    nodes = new int[nodesTotal][6];
    
    int index, lValue, rValue;
    
    for (index = 1; index < nodesTotal; ++index) {
        token = new StringTokenizer(br.readLine());
        lValue = Integer.parseInt(token.nextToken());
        rValue = Integer.parseInt(token.nextToken());
        if (nodes[lValue][0] == 0) {
            nodes[lValue][0] = rValue + 1;
        } else if (nodes[lValue][1] == 0) {
            nodes[lValue][1] = rValue + 1;
        } else {
            nodes[lValue][2] = rValue + 1;
        }
        if (nodes[rValue][0] == 0) {
            nodes[rValue][0] = lValue + 1;
        } else if (nodes[rValue][1] == 0) {
            nodes[rValue][1] = lValue + 1;
        } else {
            nodes[rValue][2] = lValue + 1;
        }
    }
    root(rootValue, 0, 1);
    BufferedOutputStream print = new BufferedOutputStream(System.out);
    for (index = 0; index < inputLines; ++index) {
        lValue = Integer.parseInt(br.readLine());
        print.write((((nodes[lValue][4] - 1) + " " + (maxDepth - nodes[lValue][5]) + System.lineSeparator()))
                            .getBytes());
    }
    print.flush();
}

private static int root(int value, int depth, int x) {
    nodes[value][3] = 1;
    nodes[value][5] = depth;
    if (depth > maxDepth) {
        maxDepth = depth;
    }
    if (nodes[value][0] != 0 && nodes[nodes[value][0] - 1][3] != 1) {
        x = nodes[value][4] = root(nodes[value][0] - 1, depth + 1, x);
    }
    if (nodes[value][1] != 0 && nodes[nodes[value][1] - 1][3] != 1) {
        if (nodes[value][4] != 0) {
            return root(nodes[value][1] - 1, depth + 1, x + 1);
        } else {
            x = nodes[value][4] = root(nodes[value][1] - 1, depth + 1, x);
        }
    }
    if (nodes[value][2] != 0 && nodes[nodes[value][2] - 1][3] != 1) {
        if (nodes[value][4] != 0) {
            return root(nodes[value][2] - 1, depth + 1, x + 1);
        } else {
            x = nodes[value][4] = root(nodes[value][2] - 1, depth + 1, x);
            return x + 1;
        }
    }
    nodes[value][4] = x;
    return x + 1;
}
}
