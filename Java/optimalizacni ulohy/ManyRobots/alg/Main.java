package alg;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

/**
 * Reseni algoritmizacni ulohy
 * @see <a href="https://cw.felk.cvut.cz/courses/a4b33alg/task.php?task=manyrobots">Zadani</a>
 */
public class Main {
private static int[][][] field;
private static int[] maxInRow;
private static int[] maxInColumn;
private static int usedRobots = 0;
private static int x, y, rowsCount, columnsCount;

public static void main(String[] args) throws IOException {
    BufferedReader br = new BufferedReader(new InputStreamReader(new BufferedInputStream(System.in)));
    StringTokenizer token = new StringTokenizer(br.readLine());
    rowsCount = Integer.parseInt(token.nextToken());
    columnsCount = Integer.parseInt(token.nextToken());
    /**
     * Plocha kam pokladam roboty a ukladam hodnoty, kolik jich je potreba k pokryti oblasti
     */
    field = new int[2][columnsCount][rowsCount];
    maxInColumn = new int[columnsCount];
    maxInRow = new int[rowsCount];
    int row, column;

    for (row = 0; row < rowsCount; ++row) {
        token = new StringTokenizer(br.readLine(), " ");
        for (column = 0; column < columnsCount; ++column) {
            field[0][column][row] = Integer.parseInt(token.nextToken());
            if (maxInColumn[column] < field[0][column][row]) {
                maxInColumn[column] = field[0][column][row];
            }
            if (maxInRow[row] < field[0][column][row]) {
                maxInRow[row] = field[0][column][row];
            }
        }
    }
    for (row = 0; row < rowsCount; ++row) {
        for (column = 0; column < columnsCount; ++column) {
            if (field[1][column][row] != 1) {
                x = column;
                y = row;
                fillField();
                ++usedRobots;
            }
        }
    }
    System.out.println(usedRobots);
}

private static void fillField() {
    int i;
    int rowMax = maxInRow[y] + field[0][x][y];
    for (i = field[0][x][y]; i <= rowMax && (x - i >= 0 || x + i < columnsCount); ++i) {
        if (x - i >= 0 && field[1][x - i][y] != 1 && field[0][x - i][y] + field[0][x][y] == i) {
            x -= i;
            field[1][x][y] = 1;
            fillField();
            x += i;
        }
        if (x + i < columnsCount && field[1][x + i][y] != 1 && field[0][x + i][y] + field[0][x][y] == i) {
            x += i;
            field[1][x][y] = 1;
            fillField();
            x -= i;
        }
    }
    int columnMax = maxInColumn[x] + field[0][x][y];
    for (i = field[0][x][y]; i <= columnMax && (y - i >= 0 || y + i < rowsCount); ++i) {
        if (y - i >= 0 && field[1][x][y - i] != 1 && field[0][x][y - i] + field[0][x][y] == i) {
            y -= i;
            field[1][x][y] = 1;
            fillField();
            y += i;
        }
        if (y + i < rowsCount && field[1][x][y + i] != 1 && field[0][x][y + i] + field[0][x][y] == i) {
            y += i;
            field[1][x][y] = 1;
            fillField();
            y -= i;
        }
    }
}
}
