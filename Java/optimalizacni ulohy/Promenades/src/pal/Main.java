package pal;

import java.io.IOException;
import java.util.ArrayDeque;
import java.util.Deque;

class Main {

	static int N;
	static int B;
	static int M;

	private static Vertex[] vertices;
	private static Component[] components;

	public static void main(String[] args) throws IOException {
		InputReader in = new InputReader(System.in);
		N = in.readInt();
		B = in.readInt();
		M = in.readInt();

		vertices = new Vertex[N];
		for (int i = 0; i < N; i++) {
			Vertex vertex = new Vertex(i);
			vertices[i] = vertex;
		}

		for (int i = 0; i < B; i++) {
			vertices[in.readInt()].setBlue();
		}

		for (int i = 0; i < M; i++) {
			int from = in.readInt();
			int to = in.readInt();
			vertices[from].addDescendant(to);
		}

		components = new Tarjan(vertices).computeComponents();
		countDistances();
		findBestConnection();

		Component component = components[B - 1];
		System.out.println(component.getBlues() + " " + component.getDistance());
	}


	private static void countDistances() {
		for (int componentIndex = 0; componentIndex < B; componentIndex++) {
			Component component = components[componentIndex];
			Vertex blueVertex = component.getBlueVertex();
			blueVertex.setDistance(0);

			Deque<Vertex> queue = new ArrayDeque<>();
			queue.add(blueVertex);

			for (Vertex vertex = queue.pollFirst(); ; vertex = queue.pollFirst()) {
				int distance = vertex.getDistance() + 1;
				int toComponent = vertex.getComponent();
				if (vertex.isBlue() && toComponent != componentIndex) {
					component.addConnection(new Edge(toComponent, distance - 1));
					if (queue.isEmpty()) {
						break;
					}
					continue;
				}
				for (Integer vertexIndex : vertex.getDescendants()) {
					Vertex v = vertices[vertexIndex];
					if (v.getDistance() == -1 && (toComponent == componentIndex || toComponent == v.getComponent())) {
						v.setDistance(distance);
						queue.add(v);
					}
				}
				if (queue.isEmpty()) {
					break;
				}
			}

			for (Vertex vertex : vertices) {
				vertex.setDistance(-1);
			}
		}
	}

	private static void findBestConnection() {
		for (Component fromComponent : components) {
			for (Edge connection : fromComponent.getConnections()) {
				Component toComponent = components[connection.getTo()];
				int newBlues = fromComponent.getBlues() + 1;
				int newDistance = fromComponent.getDistance() + connection.getLength();
				if (toComponent.getBlues() < newBlues) {
					toComponent.setBlues(newBlues);
					toComponent.setDistance(newDistance);
				} else if (toComponent.getBlues() == newBlues && toComponent.getDistance() > newDistance) {
					toComponent.setDistance(newDistance);
				}
			}
		}
	}
}
