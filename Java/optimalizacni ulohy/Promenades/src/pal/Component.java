package pal;


import java.util.ArrayList;
import java.util.List;

class Component {
	private final List<Edge> connections;
	private final List<Vertex> vertices;
	private Vertex blueVertex;
	private int blues = 1;
	private int distance;

	Component() {
		connections = new ArrayList<>();
		vertices = new ArrayList<>();
	}

	void addVertex(Vertex vertex) {
		if (vertex.isBlue()) {
			blueVertex = vertex;
		}
		vertices.add(vertex);
	}

	List<Vertex> getVertices() {
		return vertices;
	}

	void addConnection(Edge edge) {
		connections.add(edge);
	}

	List<Edge> getConnections() {
		return connections;
	}

	Vertex getBlueVertex() {
		return blueVertex;
	}

	int getBlues() {
		return blues;
	}

	void setBlues(int blues) {
		this.blues = blues;
	}

	int getDistance() {
		return distance;
	}

	void setDistance(int distance) {
		this.distance = distance;
	}
}
