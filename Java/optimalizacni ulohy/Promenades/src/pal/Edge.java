package pal;

class Edge {
	private final int to;
	private final int length;

	Edge(int to, int length) {
		this.to = to;
		this.length = length;
	}

	int getTo() {
		return to;
	}

	int getLength() {
		return length;
	}
}
