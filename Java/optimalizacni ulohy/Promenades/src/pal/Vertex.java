package pal;

import java.util.ArrayList;
import java.util.List;

class Vertex {
	private final int index;
	private final List<Integer> descendants;
	private boolean isBlue;
	private int component;
	private int distance = -1;

	Vertex(int index) {
		this.index = index;
		descendants = new ArrayList<>();
	}

	int getIndex() {
		return index;
	}

	List<Integer> getDescendants() {
		return descendants;
	}

	void addDescendant(int descendant) {
		descendants.add(descendant);
	}

	boolean isBlue() {
		return isBlue;
	}

	void setBlue() {
		isBlue = true;
	}

	int getComponent() {
		return component;
	}

	void setComponent(int component) {
		this.component = component;
	}

	int getDistance() {
		return distance;
	}

	void setDistance(int distance) {
		this.distance = distance;
	}
}