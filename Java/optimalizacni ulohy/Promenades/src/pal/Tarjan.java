package pal;


import java.util.ArrayDeque;
import java.util.Deque;

class Tarjan {

	private final Vertex[] vertices;
	private final int[] lows;
	private final boolean[] visited;
	private final Deque<Vertex> stack;
	private final Component[] components;

	private int preCount;
	private int componentIndex;

	Tarjan(Vertex[] vertices) {
		this.vertices = vertices;
		lows = new int[vertices.length];
		visited = new boolean[vertices.length];
		stack = new ArrayDeque<>();
		components = new Component[Main.B];
	}

	Component[] computeComponents() {
		componentIndex = components.length - 1;
		for (int i = 0; i < vertices.length; i++) {
			if (!visited[i]) {
				dfs(i);
			}
		}
		return components;
	}

	private void dfs(int index) {
		lows[index] = preCount++;
		visited[index] = true;
		Vertex vertex = vertices[index];
		stack.addLast(vertex);

		int min = preCount;
		for (Integer descendant : vertex.getDescendants()) {
			if (!visited[descendant]) {
				dfs(descendant);
			}
			int low = lows[descendant];
			if (low < min) {
				min = low;
			}
		}

		if (min < lows[index]) {
			lows[index] = min;
			return;
		}

		Component component = new Component();
		Vertex v;
		do {
			v = stack.pollLast();
			v.setComponent(componentIndex);
			lows[v.getIndex()] = vertices.length;
			component.addVertex(v);
		} while (!vertex.equals(v));
		components[componentIndex--] = component;
	}
}
