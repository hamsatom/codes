package pal;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

class Main {

	private static final String EMPTY_STRING = "";

	static int T;
	static int N;
	static int M;

	private static Vertex[] tree;

	public static void main(String[] args) throws IOException {
		InputReader in = new InputReader(System.in);
		T = in.readInt();
		N = in.readInt();
		M = in.readInt();

		Map<String, Integer> certificates = new HashMap<>();
		tree = new Vertex[N];
		for (int t = 0; t < T; t++) {
			for (int i = 0; i < N; i++) {
				tree[i] = new Vertex();
			}
			for (int m = 0; m < M; m++) {
				int from = in.readInt() - 1;
				int to = in.readInt() - 1;
				Vertex vertex = tree[to];
				tree[from].children.add(vertex);
				++vertex.degree;
			}
			String certificate = getCertificate();
			Integer occurrence = certificates.get(certificate);
			if (occurrence == null) {
				certificates.put(certificate, 1);
			} else {
				certificates.replace(certificate, occurrence + 1);
			}
		}

		Object[] occurrences = certificates.values().toArray();
		Arrays.sort(occurrences);
		for (Object occurrence : occurrences) {
			System.out.printf("%d ", occurrence);
		}
		System.exit(0);
	}

	private static String getCertificate() {
		for (Vertex vertex : tree) {
			if (vertex.degree == 0) {
				return vertex.getCertificate();
			}
		}
		throw new NoSuchElementException("No root found");
	}

	private static class Vertex {
		private static Vertex origin;
		private List<Vertex> children;
		private int degree;
		private String certificate;

		public Vertex() {
			children = new ArrayList<>();
		}

		String getCertificate() {
			String[] certificates = new String[children.size()];
			int length = 2;
			for (int i = 0; i < children.size(); i++) {
				origin = children.get(i);
				String cycleCertificate;
				if (origin.degree <= 1) {
					cycleCertificate = origin.getCertificate();
				} else {
					cycleCertificate = origin.getCycleCertificate();
				}
				certificates[i] = cycleCertificate;
				length += cycleCertificate.length();
			}
			Arrays.sort(certificates);
			StringBuilder sb = new StringBuilder(length);
			sb.append('0');
			for (String certificate : certificates) {
				sb.append(certificate);
			}
			sb.append('1');
			certificate = sb.toString();
			return certificate;
		}

		String getCycleCertificate() {
			if (certificate != null && degree == 1){
				return certificate;
			}
			String[] certificates = new String[children.size()];
			int length = 2;
			for (int i = 0; i < children.size(); i++) {
				Vertex vertex = children.get(i);
				if (!origin.equals(vertex)) {
					String cycleCertificate = vertex.getCycleCertificate();
					certificates[i] = cycleCertificate;
					length += cycleCertificate.length();
				} else {
					certificates[i] = EMPTY_STRING;
				}
			}
			Arrays.sort(certificates);
			StringBuilder sb = new StringBuilder(length);
			sb.append('0');
			for (String cycleCertificate : certificates) {
				sb.append(cycleCertificate);
			}
			sb.append('1');
			certificate = sb.toString();
			return certificate;
		}
	}
}
