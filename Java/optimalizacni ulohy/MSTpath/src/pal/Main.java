package pal;

import java.io.IOException;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Deque;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.IntStream;


public class Main {

	private static int A;
	private static int B;
	private static int M;
	private static int N;
	private static Vertex[] vertices;
	private static Edge[] edges;
	private static int score = Integer.MAX_VALUE;

	public static void main(String[] args) throws IOException {
		System.out.println(solve());
	}

	static String solve() throws IOException {
		InputReader in = new InputReader(System.in);
		N = in.readInt();
		M = in.readInt();
		A = in.readInt();
		B = in.readInt();

		vertices = IntStream.range(0, N + 1)
				.mapToObj(i -> new Vertex())
				.toArray(Vertex[]::new);
		edges = new Edge[M];

		for (int i = 0; i < M; i++) {
			int from = in.readInt();
			int to = in.readInt();
			Edge edge = new Edge(from, to, in.readInt());
			vertices[from].edges.add(edge);
			vertices[to].edges.add(edge);
			edges[i] = edge;
		}

		findOptimalPath();
		findSpannings();
		return vertices[B].minDistance + " " + score;
	}

	private static void findOptimalPath() {
		vertices[A].minDistance = 0;

		Set<Edge> visited = new HashSet<>(M);
		Deque<Edge> queue = new ArrayDeque<>(M);
		queue.addAll(vertices[A].edges);
		for (; !queue.isEmpty(); ) {
			Edge edge = queue.pollFirst();
			Vertex from = vertices[edge.from];
			Vertex to = vertices[edge.to];
			int fromDistance = from.minDistance;
			int toDistance = to.minDistance;
			if (fromDistance < toDistance) {
				if (fromDistance >= vertices[B].minDistance) {
					break;
				}
				to.minDistance = fromDistance + 1;
				to.incomingConnections.add(edge);
				to.edges.stream()
						.filter(visited::add)
						.forEach(queue::add);
			} else if (fromDistance > toDistance) {
				if (toDistance >= vertices[B].minDistance) {
					break;
				}
				from.minDistance = toDistance + 1;
				from.incomingConnections.add(edge);
				from.edges.stream()
						.filter(visited::add)
						.forEach(queue::add);
			}
		}
	}

	private static int findSpannings() {
		Arrays.sort(edges);
		Edge[] path = new Edge[vertices[B].minDistance];
		findSpanning(vertices[B], path, 0);
		return score;
	}

	private static void findSpanning(Vertex vertex, Edge[] path, int initialCost) {
		vertex.incomingConnections.forEach(edge -> {
			int newCost = initialCost + edge.cost;
			if (newCost < score) {
				path[vertex.minDistance - 1] = edge;
				Vertex from = vertices[edge.from];
				Vertex to = vertices[edge.to];
				Vertex nextVertex = from.minDistance < to.minDistance ? from : to;
				if (nextVertex.minDistance == 0) {
					doKruskal(path, newCost);
				} else {
					findSpanning(nextVertex, path, newCost);
				}
			}
		});
	}

	private static void doKruskal(Edge[] path, int cost) {
		DisjointSet nodeSet = new DisjointSet();
		for (Edge edge : path) {
			int root1 = nodeSet.find(edge.from);
			int root2 = nodeSet.find(edge.to);
			nodeSet.union(root1, root2);
		}
		int added = path.length;
		for (Edge edge : edges) {
			int root1 = nodeSet.find(edge.from);
			int root2 = nodeSet.find(edge.to);
			if (root1 != root2) {
				cost += edge.cost;
				if (cost > score) {
					return;
				}
				++added;
				if (added >= N - 1) {
					break;
				}
				nodeSet.union(root1, root2);
			}
		}

		score = cost;
	}


	private static class Vertex {
		private int minDistance;
		private final List<Edge> edges;
		private final Set<Edge> incomingConnections;

		public Vertex() {
			minDistance = M;
			edges = new ArrayList<>(94);
			incomingConnections = new HashSet<>(21);
		}
	}

	private static class Edge implements Comparable<Edge> {
		private final int from;
		private final int to;
		private final int cost;

		public Edge(int from, int to, int cost) {
			this.from = from;
			this.cost = cost;
			this.to = to;
		}

		@Override
		public int compareTo(Edge that) {
			return cost - that.cost;
		}
	}

	private static class DisjointSet {
		private final int[] set;        //the disjoint set as an array

		/**
		 * Construct the disjoint sets object.
		 */
		public DisjointSet() {
			//constructor creates singleton sets
			set = new int[N + 1];
			//initialize to -1 so the trees have nothing in them
			Arrays.fill(set, -1);
		}

		/**
		 * Union two disjoint sets using the height heuristic.
		 * For simplicity, we assume root1 and root2 are distinct
		 * and represent set names.
		 *
		 * @param root1 the root of set 1.
		 * @param root2 the root of set 2.
		 */
		public void union(int root1, int root2) {
			if (set[root2] < set[root1]) {        // root2 is deeper
				set[root1] = root2;        // Make root2 new root
			} else {
				if (set[root1] == set[root2]) {
					--set[root1];            // Update height if same
				}
				set[root2] = root1;        // Make root1 new root
			}
		}

		/**
		 * Perform a find with path compression.
		 * Error checks omitted again for simplicity.
		 *
		 * @param x the element being searched for.
		 * @return the set containing x.
		 */
		public int find(int x) {
			for (; set[x] > 0; ) {        //Loop until we find a root
				x = set[x];
			}
			return x;
		}

	}
}
