package pal;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.regex.Pattern;
import java.util.stream.Stream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

/**
 * @author Tomas Hamsa on 08.03.2017.
 */
public class PubTest {

	// CHANGE THESE TO MATCH PATHS IN YOUR SYSTEM
	private static final String DATAPUB_LOCATION = "D:\\Tomas\\ownCloud\\PAL\\1MSTpath\\datapub\\pub";
	private static final String PAL_LOCATION = "D:\\Tomas\\ownCloud\\PAL\\1MSTpath\\src";

	private static final Pattern FILENAME_FILTER = Pattern.compile("[:T]");
	private static boolean allPassed = true;

	@Test(priority = 1, timeOut = 2000)
	public void testPub1() throws Exception {
		solvePub("01");
	}

	@Test(priority = 2, timeOut = 2000)
	public void testPub2() throws Exception {
		solvePub("02");
	}

	@Test(priority = 3, timeOut = 2000)
	public void testPub3() throws Exception {
		solvePub("03");
	}

	@Test(priority = 4, timeOut = 2000)
	public void testPub4() throws Exception {
		solvePub("04");
	}

	@Test(priority = 5, timeOut = 2000)
	public void testPub5() throws Exception {
		solvePub("05");
	}

	@Test(priority = 6, timeOut = 2000)
	public void testPub6() throws Exception {
		solvePub("06");
	}

	@Test(priority = 7, timeOut = 2000)
	public void testPub7() throws Exception {
		solvePub("07");
	}

	@Test(priority = 8, timeOut = 2000)
	public void testPub8() throws Exception {
		solvePub("08");
	}

	@Test(priority = 9, timeOut = 2000)
	public void testPub9() throws Exception {
		solvePub("09");
	}

	@Test(priority = 10, timeOut = 2000)
	public void testPub10() throws Exception {
		solvePub("10");
	}

	@AfterTest
	public void zipSolution() throws IOException {
		if (!allPassed) {
			return;
		}

		Path palDir = Paths.get(PAL_LOCATION, "pal");
		System.out.println("Located pal directory in: " + palDir);
		Path base = toPath(PAL_LOCATION);
		String zipName = FILENAME_FILTER.matcher("solution-" + LocalDateTime.now() + ".zip").replaceAll("-");
		Path outputFile = Paths.get(PAL_LOCATION, zipName);
		System.out.println("Packing solution into zip: " + outputFile);

		try (ZipOutputStream zip = new ZipOutputStream(new BufferedOutputStream(Files.newOutputStream(outputFile)));
			 Stream<Path> fileTree = Files.walk(base)) {
			fileTree.filter(path -> !path.toFile().isDirectory())
					.filter(path -> path.startsWith(palDir))
					.filter(path -> !path.endsWith("PubTest.java"))
					.forEach(path -> {
						ZipEntry zipEntry = new ZipEntry(base.relativize(path).toString());
						try {
							zip.putNextEntry(zipEntry);
							Files.copy(path, zip);
							zip.closeEntry();
						} catch (IOException e) {
							throw new IllegalStateException(e);
						}
					});
		}
	}

	private static void solvePub(String pubNumber) throws IOException {
		System.out.println("Running pub test " + pubNumber);
		Path pubInFile = toPath(DATAPUB_LOCATION + pubNumber + ".in");
		System.out.println("Pub in file: " + pubInFile);
		Path pubOutFile = toPath(DATAPUB_LOCATION + pubNumber + ".out");
		System.out.println("Pub out file: " + pubOutFile);

		String expectedOutput = new String(Files.readAllBytes(pubOutFile), StandardCharsets.UTF_8);
		System.setIn(new BufferedInputStream(Files.newInputStream(pubInFile)));

		long startTime = System.nanoTime();
		try {
			String actualResult = Main.solve() + System.lineSeparator();
			long duration = System.nanoTime() - startTime;
			System.out.printf("Duration: %f seconds%n", duration / 1000000000.0 );
			Assert.assertEquals(actualResult, expectedOutput, "Incorrect solution");
		} catch (Throwable e) {
			allPassed = false;
			throw e;
		}
	}

	private static Path toPath(String path) {
		return Paths.get(path).toAbsolutePath().normalize();
	}
}