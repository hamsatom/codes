/* LICENSE: 
 * this code is published under WTFPL
 * wtfpl.com
 */
package alg;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Tester {

/**
 * Runs 10 public ALG tests and prints results
 *
 * @param args
 * @throws IOException
 */
public static void main(String[] args) throws IOException {
    // setup working directory
    // String workingDirectory = "C:\\Users\\user\\ALG\\HW1\\data\\";
    //D:\Users\GreenGreen\Desktop
    String workingDirectory = "/Users/GreenGreen/Desktop/datapub/";
    String inputFile;
    String outputFile;
    long t0;
    long scanTime;
    Main m = new Main();

    for (int i = 1; i <= 10; i++) {
        String number = filenumber(i);
        outputFile = workingDirectory + "pub" + number + ".out";
        inputFile = workingDirectory + "pub" + number + ".in";
        BufferedReader br = new BufferedReader(new FileReader(outputFile));
        String first = br.readLine();
        System.out.println("##########################");
        System.out.println("     Initiating test " + i);
        System.out.println("     Expected output: " + first);
        t0 = System.currentTimeMillis();
        scanTime = System.currentTimeMillis() - t0;
        t0 = System.currentTimeMillis();
        m.main(new String[]{""});
        System.out.println("     Scan time: " + (System.currentTimeMillis() - t0));
        System.out.println("     Run time: " + scanTime);
    }
}

private static String filenumber(int fileNum) {
    String result = "";
    if (fileNum < 10) {
        result += "0" + fileNum;
    } else {
        result += fileNum;
    }
    return result;
}

/**
 * Example for Main's readInput method
 *
 * @param filename
 * @throws FileNotFoundException
 */
private static void readInputExample(String filename) throws FileNotFoundException {
    BufferedReader br;
    if (!filename.equals("")) {
        br = new BufferedReader(new FileReader(filename));
    } else {
        br = new BufferedReader(new InputStreamReader(System.in));
    }
}
}
