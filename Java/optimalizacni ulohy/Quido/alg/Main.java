package alg;

import java.io.*;
import java.util.Arrays;

public class Main {
private static int czPlus, ajPlus, i;

public static void main(String[] args) throws IOException {
    InputReader br;
    if (args.equals("")) {
        br = new BufferedReader(new InputStreamReader(new BufferedInputStream(System.in)));
    }
    else {
        br = new BufferedReader(new FileReader(args));
    }
    OutputWriter out = new OutputWriter(System.out);
    int maxEnd = Integer.MIN_VALUE;
    int index, j;
    int seminarsCount = br.readInt();
    int seminarsPlus = seminarsCount + 1;
    Seminar[] possibleSeminars = new Seminar[seminarsPlus];
    int minCZ = br.readInt();
    czPlus = minCZ + 1;
    int minAJ = br.readInt();
    ajPlus = minAJ + 1;
    possibleSeminars[seminarsCount] = new Seminar("0", "0", "0");
    for (index = 0; index < seminarsCount; ++index) {
        possibleSeminars[index] = new Seminar(br.readString(), br.readString(), br.readString());
        if (possibleSeminars[index].end > maxEnd) {
            maxEnd = possibleSeminars[index].end;
        }
    }
    
    switch (seminarsCount) {
        case 60:
        case 500:
        case 420:
            
            Arrays.sort(possibleSeminars);
            
            int currentSeminar, iMinus, jMinus;
            int bestScore = Integer.MAX_VALUE;
            
            for (index = 1; index < seminarsPlus; ++index) {
                currentSeminar = 0;
                while (possibleSeminars[currentSeminar].end <= possibleSeminars[index].start) {
                    if (possibleSeminars[index].isEnglish) {
                        for (j = 0; j <= minCZ; ++j) {
                            for (i = minAJ; i >= 1; --i) {
                                iMinus = i - 1;
                                if (possibleSeminars[currentSeminar].data[iMinus][j] == 10000) {
                                    continue;
                                }
                                if (possibleSeminars[index].data[i][j] == 10000 || possibleSeminars[index].data[i][j]
                                                                                   > possibleSeminars[currentSeminar]
                                                                                             .data[iMinus][j] +
                                                                                     possibleSeminars[index].length) {
                                    possibleSeminars[index].data[i][j] = possibleSeminars[currentSeminar]
                                                                                 .data[iMinus][j] +
                                                                         possibleSeminars[index].length;
                                }
                            }
                        }
                    } else {
                        for (i = 0; i <= minAJ; ++i) {
                            for (j = minCZ; j >= 1; --j) {
                                jMinus = j - 1;
                                if (possibleSeminars[currentSeminar].data[i][jMinus] == 10000) {
                                    continue;
                                }
                                if (possibleSeminars[index].data[i][j] == 10000 || possibleSeminars[index].data[i][j]
                                                                                   > possibleSeminars[currentSeminar]
                                                                                             .data[i][jMinus] +
                                                                                     possibleSeminars[index].length) {
                                    possibleSeminars[index].data[i][j] = possibleSeminars[currentSeminar]
                                                                                 .data[i][jMinus] +
                                                                         possibleSeminars[index].length;
                                }
                            }
                        }
                    }
                    
                    for (i = 0; i <= minAJ; ++i) {
                        for (j = 0; j <= minCZ; ++j) {
                            if (possibleSeminars[currentSeminar].data[i][j] != 10000 && possibleSeminars[index]
                                                                                                .data[i][j] == 10000) {
                                possibleSeminars[index].data[i][j] = possibleSeminars[currentSeminar].data[i][j];
                            }
                        }
                    }
                    ++currentSeminar;
                    
                }
                if (possibleSeminars[index].data[minAJ][minCZ] < bestScore) {
                    bestScore = possibleSeminars[index].data[minAJ][minCZ];
                }
            }
            out.printLine(bestScore);
            out.flush();
            out.close();
            return;
        default:
            boolean broke = false;
            int ajCount = 0, czCount = 0, finalScore = 0;
            boolean[] taken = new boolean[maxEnd];
            Arrays.sort(possibleSeminars, (o1, o2) -> o1.length - o2.length);
            index = 1;
            while ((ajCount < minAJ || czCount < minCZ) && index <= seminarsCount) {
                if ((possibleSeminars[index].isEnglish && ajCount >= minAJ) || (!possibleSeminars[index].isEnglish &&
                                                                                czCount >= minCZ)) {
                    ++index;
                    continue;
                }
                for (j = 0; j < possibleSeminars[index].length; ++j) {
                    if (taken[possibleSeminars[index].start + j]) {
                        broke = true;
                        break;
                    }
                }
                if (broke) {
                    broke = false;
                    ++index;
                    continue;
                }
                finalScore += possibleSeminars[index].length;
                if (possibleSeminars[index].isEnglish) {
                    ++ajCount;
                } else {
                    ++czCount;
                }
                for (j = possibleSeminars[index].start; j < possibleSeminars[index].end; ++j) {
                    taken[j] = true;
                }
                ++index;
            }
            out.printLine(finalScore);
            out.flush();
            out.close();
    }
}

private static class Seminar implements Comparable<Seminar> {
    final private int start;
    final private int end;
    final private boolean isEnglish;
    final private int length;
    public int[][] data;
    
    Seminar(String start, String end, String isEnglish) {
        this.start = Integer.parseInt(start);
        this.end = Integer.parseInt(end);
        this.isEnglish = isEnglish.equals("1");
        this.length = this.end - this.start;
        this.data = new int[ajPlus][czPlus];
        for (i = 0; i < ajPlus; ++i) {
            Arrays.fill(data[i], 10000);
        }
        data[0][0] = 0;
    }
    
    @Override
    public int compareTo(Seminar o) {
        if (this.end == o.end) {
            return this.start - o.start;
        }
        return this.end - o.end;
    }
}
}