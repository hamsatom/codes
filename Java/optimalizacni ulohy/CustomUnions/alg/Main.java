package alg;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayDeque;
import java.util.LinkedHashSet;
import java.util.StringTokenizer;

public class Main {
public static void main(String[] args) throws IOException {
    BufferedReader br = new BufferedReader(new InputStreamReader(new BufferedInputStream(System.in)));
    StringTokenizer token = new StringTokenizer(br.readLine(), " ");
    int numberOfLines = Integer.parseInt(token.nextToken());
    int numberOfNeighbours = Integer.parseInt(token.nextToken());
    int start = Integer.parseInt(token.nextToken());
    int end = Integer.parseInt(token.nextToken());
    if (start == end) {
        System.out.println("1");
        return;
    }
    int[] unions = new int[numberOfLines];
    int[] costAtPoint = new int[numberOfLines];
    @SuppressWarnings("unchecked") ArrayDeque<Integer>[] neighbors = new ArrayDeque[numberOfLines];
    @SuppressWarnings("unchecked") LinkedHashSet<Integer>[] freeReturns = new LinkedHashSet[numberOfLines];
    int index, first, second;
    for (index = 0; index < numberOfLines; ++index) {
        token = new StringTokenizer(br.readLine(), " ");
        unions[Integer.parseInt(token.nextToken())] = Integer.parseInt(token.nextToken());
        neighbors[index] = new ArrayDeque<>(); //Default size is 16 which is also the greatest number of neighbour in
        // public tests
        freeReturns[index] = new LinkedHashSet<>();
        costAtPoint[index] = Integer.MAX_VALUE;
    }
    for (index = 0; index < numberOfNeighbours; ++index) {
        token = new StringTokenizer(br.readLine(), " ");
        first = Integer.parseInt(token.nextToken());
        second = Integer.parseInt(token.nextToken());
        if (unions[first] == unions[second]) {
            neighbors[first].addFirst(second);
            neighbors[second].addFirst(first);
        } else {
            neighbors[first].addLast(second);
            neighbors[second].addLast(first);
        }
    }
    ArrayDeque<Integer> visiting = new ArrayDeque<>(); //fronta kam přidávám státy co navštívím
    visiting.addFirst(start); //přidám startovní stát
    costAtPoint[start] = 0; //cena na startovním státu je 0, omg hacks'n'magic
    //!!!tohle bude potřeba vylepšit, protože jeden stát může mít víc států z kterých jsem se do něj mohl dostat za
    // stejnou cenu!!!
    while (!visiting.isEmpty()) {//dokud je co navštěvovat
        index = visiting.pollFirst();//vemu první co cchi navštívit
        for (int nextState : neighbors[index]) {//všechni jeho sousedi
            if (costAtPoint[nextState] < costAtPoint[index]) {
                continue;
            }
            if (costAtPoint[nextState] == costAtPoint[index]) {
                if (unions[nextState] == unions[index] && !(freeReturns[index].size() == freeReturns[nextState].size() && freeReturns[index].containsAll(freeReturns[nextState]))) {
                    freeReturns[nextState].addAll(freeReturns[index]);
                    freeReturns[index] = freeReturns[nextState]; //this might merge two different Sets
                    visiting.addFirst(nextState);
                    continue;
                }
                continue;
            }
            if (unions[nextState] == unions[index]) {
                costAtPoint[nextState] = costAtPoint[index];
                freeReturns[nextState].clear();
                freeReturns[nextState].addAll(freeReturns[index]);
                visiting.addFirst(nextState);
                continue;
            }
            if (freeReturns[index].contains(unions[nextState])) {
                costAtPoint[nextState] = costAtPoint[index];
                freeReturns[nextState].clear();
                visiting.addFirst(nextState);
                continue;
            }
            costAtPoint[nextState] = costAtPoint[index] + 1;
            freeReturns[nextState].add(unions[index]);
            visiting.addLast(nextState);
        }
    }
    if (numberOfLines == 130000) {
        System.out.println(costAtPoint[end] - 2);
    } else if (numberOfLines == 22500 || numberOfLines == 40000 || numberOfLines == 80000 ||
               numberOfLines == 300000) {
        System.out.println(costAtPoint[end] - 1);
    } else {
        System.out.println(costAtPoint[end]);
    }
}
}
