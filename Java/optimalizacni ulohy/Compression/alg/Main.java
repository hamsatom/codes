package alg;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayDeque;
import java.util.StringTokenizer;

public class Main {

// Mam globalni indexy, protoze jsem Tomas Hamsa
private static int i, j;

private static byte w, h;
private static boolean breaked;
private static byte[][] bitmap;
private static ArrayDeque<int[]> slices;
private static int[][][][][] data;

public static void main(String[] args) throws IOException{
    BufferedReader br = new BufferedReader(new InputStreamReader(new BufferedInputStream(System.in)));
    StringTokenizer token = new StringTokenizer(br.readLine(), " ");
    h = Byte.parseByte(token.nextToken());
    w = Byte.parseByte(token.nextToken());
    bitmap = new byte[h][w];

    data = new int[3][w+1][h+1][w+1][h+1];

    for (i = 0; i < h; ++i) {
        token = new StringTokenizer(br.readLine(), " ");
        for (j = 0; j < w; ++j) {
            bitmap[i][j] = Byte.parseByte(token.nextToken());
        }
    }

    cutImg();
    int[] result = getNumbers(0, 0, w, h);
    System.out.println(result[1]);
    System.out.println(--result[0]);
}

private static void cutImg() {
    slices = new ArrayDeque<>(26);
    int jMinus, iMinus;
    boolean[] usedW = new boolean[w - 1];
    boolean[] usedH = new boolean[h - 1];

    for (i = 0; i < h; ++i) {
        iMinus = i - 1;
        for (j = 0; j < w; ++j) {
            jMinus = j - 1;
            if (j > 0 && bitmap[i][j] != bitmap[i][jMinus] && !usedW[jMinus]) {
                usedW[jMinus] = true;
                // 1 for verical
                slices.addLast(new int[]{1, jMinus});
            }
            if (i > 0 && bitmap[i][j] != bitmap[iMinus][j] && !usedH[iMinus]) {
                usedH[iMinus] = true;
                // 0 for horizontal
                slices.addLast(new int[]{0, iMinus});
            }
        }
    }
}

private static int[] getNumbers(int leftTop, int leftDown, int rightTop, int rightDown) {
    if (rightTop - leftTop <= 0 || rightDown - leftDown <= 0) {
        return new int[]{2000, 2000};
    }

    if (data[2][leftTop][leftDown][rightTop][rightDown] != 0) {
        return new int[]{1, 1};
    }

    if (data[0][leftTop][leftDown][rightTop][rightDown] != 0 && data[1][leftTop][leftDown][rightTop][rightDown] != 0) {
        return new int[]{data[0][leftTop][leftDown][rightTop][rightDown], data[1][leftTop][leftDown][rightTop][rightDown]};
    }

    // check if consist of one colour only
    breaked = false;
outerLoop:
    for (i = leftDown; i < rightDown; ++i) {
        for (j = leftTop; j < rightTop; ++j) {
            if (bitmap[i][j] != bitmap[leftDown][leftTop]) {
                breaked = true;
                break outerLoop;
            }
        }
    }

    if (!breaked) {
        data[2][leftTop][leftDown][rightTop][rightDown] = 1;
        return new int[]{1, 1};
    }

    int slicePlus, bestNodes = 2000, bestDepth = 2000;
    int[] leftResults, rightResults;
    for (int[] slice : slices) {

        slicePlus = slice[1] + 1;
        // slice to smaller pieces
        // if vertical
        if (slice[0] == 1) {
            if (leftTop > slice[1] || rightTop <= slicePlus) {
                continue;
            }

            // process left half
            leftResults = getNumbers(leftTop, leftDown, slicePlus, rightDown);

            // deal with right one
            rightResults = getNumbers(slicePlus, leftDown, rightTop, rightDown);

            // if horizontal
        } else {
            if (leftDown > slice[1] || rightDown <= slicePlus) {
                continue;
            }

            // process left half
            leftResults = getNumbers(leftTop, leftDown, rightTop, slicePlus);
            // deal with right one
            rightResults = getNumbers(leftTop, slicePlus, rightTop, rightDown);
        }

        leftResults[0] = leftResults[0] > rightResults[0] ? leftResults[0] : rightResults[0];
        if (++leftResults[0] < bestDepth) {
            bestDepth = leftResults[0];
        }

        leftResults[1] += rightResults[1];
        if (++leftResults[1] < bestNodes) {
            bestNodes = leftResults[1];
        }
    }

    data[0][leftTop][leftDown][rightTop][rightDown] = bestDepth;
    data[1][leftTop][leftDown][rightTop][rightDown] = bestNodes;
    return new int[]{bestDepth, bestNodes};
}
}