package pal;

import java.io.IOException;

class Main {

	private static char[] T;
	private static int N;
	private static int D;
	private static Unit[] units;
	private static int[] costs;
	private static int[] useds;

	public static void main(String[] args) throws IOException {
		InputReader in = new InputReader(System.in);
		T = in.readString().toCharArray();
		N = in.readInt();
		D = in.readInt();
		units = new Unit[N];
		for (int i = 0; i < N; i++) {
			units[i] = new Unit(in.readInt(), in.readString().toCharArray());
		}
		costs = new int[T.length];
		useds = new int[T.length];
		for (int i = 0; i < T.length; i++) {
			costs[i] = Integer.MAX_VALUE;
			useds[i] = Integer.MAX_VALUE;
		}
		//Arrays.sort(units);
		iter(0);
		System.out.printf("%d %d", costs[T.length - 1], useds[T.length - 1]);
	}

	private static void iter(int index) {
		int initCost = index > 0 ? costs[index - 1] : 0;
		int nextUsed = index > 0 ? useds[index - 1] + 1 : 1;
		for (Unit unit : units) {
			char[] text = unit.text;
			int advanced = advance(text, index);
			if (advanced > 0) {
				int newIndex = index + advanced;
				int newCost = initCost + unit.cost + text.length - advanced;

				if (costs[newIndex - 1] > newCost || costs[newIndex - 1] == newCost && useds[newIndex - 1] > nextUsed) {
					for (int i = index; i < newIndex; i++) {
						if (costs[i] > newCost) {
							costs[i] = newCost;
							useds[i] = nextUsed;
						} else if (costs[i] == newCost && useds[i] > nextUsed) {
							useds[i] = nextUsed;
						}
					}
					if (newIndex < T.length) {
						iter(newIndex);
					}
				}
			}
		}
	}

	private static int advance(char[] text, int sequenceIndex) {
		int j = 0;
		for (int i = 0; i < text.length; i++) {
			if (text[i] == T[sequenceIndex + j]) {
				++j;
				if (sequenceIndex + j >= T.length) {
					return j;
				}
			} else if (i - j >= D) {
				return -1;
			}
		}

		return j;
	}

	private static class Unit implements Comparable<Unit> {
		private final char[] text;
		private final int cost;

		Unit(int cost, char[] text) {
			this.text = text;
			this.cost = cost;
		}

		@Override
		public int compareTo(Unit o) {
			return cost - o.cost;
		}
	}

}
