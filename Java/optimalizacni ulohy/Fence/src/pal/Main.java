package pal;


import java.io.IOException;

public class Main {

	public static void main(String[] args) throws IOException {
		InputReader in = new InputReader(System.in);
		int sakes = in.readInt();
		int firstX = in.readInt();
		int firstY = in.readInt();

		int prevX = firstX;
		int prevY = firstY;
		double sum = 0.0;
		for (int i = 0; i < sakes - 1; i++) {
			int nextX = in.readInt();
			int nextY = in.readInt();
			sum += Math.sqrt(Math.pow(prevX - nextX, 2.0) + Math.pow(prevY - nextY, 2.0));
			prevX = nextX;
			prevY = nextY;
		}

		sum += Math.sqrt(Math.pow(prevX - firstX, 2.0) + Math.pow(prevY - firstY, 2.0));
		sum = Math.ceil(sum * 5.0);

		System.out.printf("%.0f", sum);
	}
}
