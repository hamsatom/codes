package alg;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.StringTokenizer;

import static alg.Test.test;

public class Main {
/**
 * Root represents root of tree, node is support variable, originalNode represent first node in bare branch
 */
private static Node node, originalNode;
static Node root;
/**
 * Number of repairs
 */
private static int fixCount = 0;
/**
 * Zakázaná délka bare branch
 */
static int permittedLength;
/**
 * práve načítaný prvek
 */
static int element;
/**
 * délka prvků s max jedním potomkem
 */
private static int length;
/**
 * Pole prvků na srovnání
 */
private static Node[] list;
private static StringTokenizer token;
private static BufferedReader br = new BufferedReader(new InputStreamReader(new BufferedInputStream(System.in)));

/**
 * Pocet radku kde zrovna jsem
 */
private static int index = 1;
/**
 * KOlik ma byte prvku ve stromu
 */
static int counter = 1;
/**
 * Pismenko posledni operace
 */
static String lastOperation = "";
/**
 * Cislo radku od ktereho budu spoustet testy
 * U velkych testu je testovani hodne pomale
 */
private static int SWITCH_COUNT = -1;

public static void main(String[] args) throws IOException {
    token = new StringTokenizer(br.readLine(), " ");
    permittedLength = (int) (Math.pow(2, Integer.parseInt(token.nextToken())) - 1);
    list = new Node[permittedLength];
    final int linesCount = Integer.parseInt(token.nextToken());
    token = new StringTokenizer(br.readLine(), " ");
    token.nextToken();
    root = new Node(Integer.parseInt(token.nextToken()), null, false);
    for (; index < linesCount; ++index) {
        node = root;
        token = new StringTokenizer(br.readLine(), " ");

        lastOperation = token.nextToken();

        switch (lastOperation) {
            case "I":
                ++counter;
                element = Integer.parseInt(token.nextToken());
                // vložím prvek do stromu
                insert();
                // když je nad ním alespoň zakáazný počet bare prvků, půjdu dolů dokud jich není zakázaně
                if (findStart()) {
                    //mnactu je do pole, srovnam a nastavim originalNode
                    repairInsert();
                    repair();
                    break;
                }
                break;
            default:
                --counter;
                element = Integer.parseInt(token.nextToken());
                // smazani prvku ze stromu
                delete();
                // findu nejvysi bare prvek
                findStart();
                //jdu po nich dolu
                if (findEnd()) {
                    repair();
                    //nastavim node na posledni upravovany
                    if (node != null && findEnd()) {
                        repair();
                        break;
                    }
                    break;
                }
                break;
        }
        if (index > SWITCH_COUNT) {
            //test();
        }
    }
    System.out.println(fixCount);
}

/**
 * metoda volajici vsechny pomocne metody
 */
private static void repair() {
    // privesi upravovany nejvysi node na prvek nad nim popripadne prohodi upravovany s root
    handleRoot();
    // +jedna oprava
    ++fixCount;
    // Napojim prvky v listu abych vybalancoval strom
    rePoint(0, permittedLength);
    // kdyz prvek nejvic dole nemel zadne potomky nemusim nic privesovat
    if (node != null) {
        // prvek navazu na rodic prniho upravovaneho popripadne root
        if (originalNode == null) {
            originalNode = root;
        }
        attachRest();
        node = originalNode;
    }
}

/**
 * Vlozi prvek do stromu.
 * Po uprave nastavi Node na priadany prvek
 */
private static void insert() {
    if (node.value > element) {
        if (node.left != null) {
            node = node.left;
            insert();
        } else {
            node.left = new Node(element, node, true);
            ++node.children;
            node = node.left;
        }
    } else {
        if (node.right != null) {
            node = node.right;
            insert();
        } else {
            node.right = new Node(element, node, false);
            ++node.children;
            node = node.right;
        }
    }
}

/**
 * Odstani prvek ze stromu. Node by mel byt vymazavany prvek
 */
private static void delete() throws IOException {
    // pokud je hdonota stejna jako ma byt smazana
    if (node.value == element) {
        // podle poctu potomku mazu
        switch (node.children) {
            case 0:
                removeLeaf();
                return;
            case 1:
                if (node.left != null) {
                    node.left.parent = node.parent;
                    if (node.parent == null) {
                        root = node.left;
                        root.isLeft = false;
                        return;
                    } else if (node.isLeft) {
                        node.parent.left = node.left;
                        return;
                    } else {
                        node.left.isLeft = false;
                        node.parent.right = node.left;
                        return;
                    }
                } else {
                    node.right.parent = node.parent;
                    if (node.parent == null) {
                        root = node.right;
                        root.isLeft = false;
                        return;
                    } else if (node.isLeft) {
                        node.right.isLeft = true;
                        node.parent.left = node.right;
                        return;
                    } else {
                        node.parent.right = node.right;
                        return;
                    }
                }
            default:
                originalNode = node;
                node = node.right;
                while (node.left != null) {
                    node = node.left;
                }
                originalNode.value = node.value;
                if (node.right != null) {
                    node.right.parent = node.parent;
                    if (node.isLeft) {
                        node.right.isLeft = true;
                        node.parent.left = node.right;
                        return;
                    } else {
                        node.parent.right = node.right;
                        return;
                    }
                } else {
                    removeLeaf();
                    return;
                }
        }
    }
    // jdu doprava nebo doleva podle hodnoty
    if (node.value > element) {
        node = node.left;
        delete();
    } else {
        node = node.right;
        delete();
    }
}

/**
 * Metoda na mazani Nodu bez potomka
 */
private static void removeLeaf() throws IOException {
    if (node.parent == null) {
        token = new StringTokenizer(br.readLine(), " ");
        token.nextToken();
        root = new Node(Integer.parseInt(token.nextToken()), null, false);
        ++index;
        ++counter;
    } else if (node.isLeft) {
        node.parent.left = null;
        --node.parent.children;
    } else {
        node.parent.right = null;
        --node.parent.children;
    }
}

/**
 * Metoda na vylezeni na nevysi Bare Node. Zacinam od 1, protoze prvek kde stojim by mel byt po insertu bare u delte
 * me pocet nezajima
 *
 * @return vraci true kdyz nodu bylo min zakazany pocet, false kdyz min
 */
private static boolean findStart() {
    length = 1;
    while (node.parent != null && node.parent.children <= 1) {
        node = node.parent;
        ++length;
    }
    return length >= permittedLength;
}

/**
 * Najde nejnizsi bare node a pridava vsechny bare nody po ceste do pole.
 *
 * @return jestli je nodu zakazany pocet vrati true
 */
private static boolean findEnd() {
    // Nastavim originalnode na nejvrchnejsi Node
    originalNode = node.parent;
    // jdu dokud jich neni zakazany pocet
    for (length = 0; length < permittedLength && node.children <= 1; ++length) {
        list[length] = node;
        if (node.left != null) {
            node = node.left;
        } else if (node.right != null) {
            node = node.right;
        } else {
            // Kdyz sem narazil na prvek co nema potomka, zvysim pocet bare nodu o jedna a skoncim cyklus
            ++length;
            node = null;
            break;
        }
    }
    // Seradim list pokud je node zakazane
    if (length == permittedLength) {
        Arrays.sort(list);
        return true;
    } else {
        return false;
    }
}

/**
 * Navazuje novy koren upravovaneho hada. Pokud by byl prvek kam navazuji null, je novy koren zaroven root stromu
 */
private static void handleRoot() {
    if (originalNode != null) {
        list[permittedLength / 2].parent = originalNode;
        if (originalNode.value > list[permittedLength / 2].value) {
            originalNode.left = list[permittedLength / 2];
            list[permittedLength / 2].isLeft = true;
        } else {
            originalNode.right = list[permittedLength / 2];
            list[permittedLength / 2].isLeft = false;
        }
    } else {
        root = list[permittedLength / 2];
        root.parent = null;
        root.isLeft = false;
    }
}

/**
 * Pridam do pole zakazane prvku a seradim ho
 */
private static void repairInsert() {
    originalNode = node.parent;
    for (length = 0; length < permittedLength; ++length) {
        list[length] = node;
        if (node.left != null) {
            node = node.left;
        } else {
            node = node.right;
        }
    }
    Arrays.sort(list);
}

/**
 * Rakurzivne navaze upravovane prvky na rodice. Prvky jsou serazeny, takze rodic by mel byt v pulce upravovane casti
 * pole a v pulce leve pulky bude levy potomek a v pulce prave bude pravy potomek.
 *
 * @param from
 *         index zacatku v poli kde se ted pohybuju
 * @param to
 *         index konce intervalu v poli kde se prave pohybuju
 */
private static void rePoint(int from, int to) {
    // pulka dosavadni casti pole, bude na ni rodic
    int middle = (to + from) / 2;
    // pulka pulky
    int middleOfMiddle = (int) Math.ceil((to - middle) / 2);
    // levy a pravy index jsou to indexy leva a pravy pulky pole na nich jsou potomci
    int leftIndex = middle - middleOfMiddle;
    int rightIndex = middle + middleOfMiddle;
    // upravim reference potomku na rice a nastavim spravny pocet potomku a jestli jsou nalevo nebo napravo
    list[rightIndex].parent = list[middle];
    list[leftIndex].parent = list[middle];
    list[middle].left = list[leftIndex];
    list[middle].right = list[rightIndex];
    list[middle].children = 2;
    list[leftIndex].isLeft = true;
    list[rightIndex].isLeft = false;
    // pokud je pulka pulky 1 a min koncim ,protoze uz jsem v poli vsechno nastavil
    if (middleOfMiddle > 1) {
        rePoint(from, middle);
        rePoint(middle, to);
        // posledni potomci musi byt listy takze nemaji zadne dalsi deti. Zbytek po bare branchy navazu v metode
        // attach rest
    } else {
        list[rightIndex].children = 0;
        list[leftIndex].children = 0;
        list[rightIndex].left = null;
        list[rightIndex].right = null;
        list[leftIndex].left = null;
        list[leftIndex].right = null;
    }
}

/**
 * Navaze co bylo pod bare branch na spravny prvek. Potřebuje originalNode nastaveny na rodice nejvrchnejsiho
 * upravovaneho Nodu a node jako potomka nejnizsihi upravovaneho Nodu
 */
private static void attachRest() {
    if (originalNode.value > node.value) {
        if (originalNode.left != null) {
            originalNode = originalNode.left;
            attachRest();
        } else {
            originalNode.left = node;
            ++originalNode.children;
            node.isLeft = true;
            node.parent = originalNode;
        }
    } else {
        if (originalNode.right != null) {
            originalNode = originalNode.right;
            attachRest();
        } else {
            originalNode.right = node;
            ++originalNode.children;
            node.isLeft = false;
            node.parent = originalNode;
        }
    }
}

}

/**
 * Reprezentuje jeden uzel ve stromu
 */
class Node implements Comparable<Node> {
/**
 * Levy potomek uzlu
 */
alg.Node left;
/**
 * Rodic uzlu
 */
alg.Node parent;
/**
 * Pravy potomek uzlu
 */
alg.Node right;
/**
 * Hodnota uzlu
 */
int value;
/**
 * Pocet potomku
 */
int children = 0;
/**
 * Pozice pod rodicem
 */
boolean isLeft;

Node(int value, alg.Node parent, boolean isLeft) {
    this.value = value;
    this.parent = parent;
    this.isLeft = isLeft;
}

@Override
public int compareTo(Node o) {
    if (o.value > this.value) {
        return -1;
    } else if (o.value < this.value) {
        return 1;
    } else {
        return 0;
    }
}
}

