package alg;

import static alg.Main.*;

/**
 * Created by Freeware Sys on 24.04.2016.
 */
class Test {
private static String el;

static void test() {
    el = Main.lastOperation;
    if (root.parent != null) {
        ErrPrint();
        System.out.println(root + " has parent and is root");
        System.exit(1);
    }
    if (root.isLeft) {
        ErrPrint();
        System.out.println(root + " set as left and is root");
        System.exit(1);
    }
    int count = rekurz(root, Integer.MIN_VALUE, Integer.MAX_VALUE, 0);
    if (count != counter) {
        ErrPrint();
        System.out.println("Expected count " + counter + " was " + count);
        System.exit(1);
    }
}

private static void ErrPrint() {
    System.out.println("Error after " + el + " " + element);
}

private static int rekurz(Node current, int min, int max, int bareLength) {
    int counter = 1;
    if (current.children == 1) {
        ++bareLength;
        if (bareLength >= permittedLength) {
            ErrPrint();
            System.out.println("Barebranch of length " + bareLength + " found with last Node " + current.value);
            System.exit(1);
        }
    }else{
        bareLength = 0;
    }
    if (current.left != null && current.right != null && current.children != 2) {
        ErrPrint();
        System.out.println(current.value + " set children to  " + current.children + " but has children " + current.left.value + " left and right is " + current.right.value);
        System.exit(1);
    } else if (current.left != null && current.right == null && current.children != 1) {
        ErrPrint();
        System.out.println(current.value + " set children to  " + current.children + " but has only child " + current.left.value +
                           " left");
        System.exit(1);
    } else if (current.right != null && current.left == null && current.children != 1) {
        ErrPrint();
        System.out.println(current.value + " set children to  " + current.children + " but has only child " + current.right.value +
                           "" +
                           " right");
        System.exit(1);
    } else if (current.right == null && current.left == null && current.children != 0) {
        ErrPrint();
        System.out.println(current.value + " set children to  " + current.children + " but has no children");
        System.exit(1);
    } else if (current.parent == null && current != root) {
        ErrPrint();
        System.out.println(current.value + " set parent to  0 but is not root");
        System.exit(1);
    }
    if (current.value < min) {
        ErrPrint();
        System.out.println(current.value + " not order, value smaller than " + min);
        System.exit(1);
    }
    if (current.value > max) {
        ErrPrint();
        System.out.println(current.value + " not order, value greater than " + max);
        System.exit(1);
    }
    if (current.left != null) {
        if (!current.left.isLeft) {
            ErrPrint();
            System.out.println(current.left.value + " isn't set as left.");
            System.exit(1);
        }
        if (current.left.parent != current) {
            ErrPrint();
            System.out.println(current.left.value + " left and has parent " + current.left.parent.value + " " +
                               "instead " +
                               "of " +
                               current.value);
            System.exit(1);
        }
        counter += rekurz(current.left, min, current.value, bareLength);
    }

    if (current.right != null) {
        if (current.right.isLeft) {
            ErrPrint();
            System.out.println(current.left.value + " set as left.");
            System.exit(1);
        }
        if (current.right.parent != current) {
            ErrPrint();
            System.out.println(current.right.value + " right and has parent " + current.right.parent.value + " " +
                               "instead of " +
                               current.value);
            System.exit(1);
        }
        counter += rekurz(current.right, current.value, max, bareLength);
    }

    return counter;
}
}
