package pal;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

class Main {

	private static String shorterWord;
	private static int len1;
	private static Set<String> visited = new HashSet<>();


	public static void main(String[] args) throws IOException {
		InputReader in = new InputReader(System.in);
		in.readString();
		int D = in.readInt();
		String word1 = in.readString();
		String word2 = in.readString();

		String longerWord;
		if (word2.length() < word1.length()) {
			shorterWord = word2;
			longerWord = word1;
		} else {
			shorterWord = word1;
			longerWord = word2;
		}
		len1 = shorterWord.length() + 1;

		List<String> words = new ArrayList<>();
		deleteLetters(new StringBuilder(longerWord), D, words);
		words.sort(null);

		for (String word : words) {
			if (calculateLevenshteinDistance(word) <= D) {
				System.out.printf(word);
				return;
			}
		}
	}

	private static void deleteLetters(StringBuilder word, int remainingDeletes, Collection<String> words) {
		String s = word.toString();
		if (!visited.add(s)) {
			return;
		}
		if (remainingDeletes-- <= 0) {
			words.add(s);
		} else {
			for (int i = 0; i < word.length(); i++) {
				char c = word.charAt(i);
				deleteLetters(word.deleteCharAt(i), remainingDeletes, words);
				word.insert(i, c);
			}
		}
	}

	private static int calculateLevenshteinDistance(CharSequence lhs) {
		int len0 = lhs.length() + 1;

		// the array of distances
		int[] cost = new int[len0];
		int[] newcost = new int[len0];

		// initial cost of skipping prefix in String s0
		for (int i = 0; i < len0; i++) cost[i] = i;

		// dynamically computing the array of distances

		// transformation cost for each letter in s1
		for (int j = 1; j < len1; j++) {
			// initial cost of skipping prefix in String s1
			newcost[0] = j;

			// transformation cost for each letter in s0
			for (int i = 1; i < len0; i++) {
				// matching current letters in both strings
				int match = lhs.charAt(i - 1) == shorterWord.charAt(j - 1) ? 0 : 1;

				// computing cost for each transformation
				int cost_replace = cost[i - 1] + match;
				int cost_insert = cost[i] + 1;
				int cost_delete = newcost[i - 1] + 1;

				// keep minimum cost
				newcost[i] = Math.min(Math.min(cost_insert, cost_delete), cost_replace);
			}

			// swap cost/newcost arrays
			int[] swap = cost;
			cost = newcost;
			newcost = swap;
		}

		// the distance is the cost for transforming all letters in both strings
		return cost[len0 - 1];
	}
}
