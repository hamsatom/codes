package KUI;


import com.sun.istack.internal.NotNull;

import java.util.*;
import java.util.concurrent.Callable;
import java.util.stream.Collectors;

public class Bayes extends Classifier {
    
    private Map<Character, Letter> charactersData;
    private Table classes;
    
    @Override
    public void learn(@NotNull double[][] data, @NotNull char[] labels) {
        charactersData = new LinkedHashMap<>(data[0].length);
        final double oneProbability = 1.0 / labels.length;
        for (int i = 0; i < labels.length; ++i) {
            Letter previousValue = charactersData.get(labels[i]);
            if (previousValue == null) {
                charactersData.put(labels[i], new Letter(oneProbability, data[i], data[0].length));
            } else {
                previousValue.probability += oneProbability;
                previousValue.images.addLast(data[i]);
            }
        }
        classes = new Table(charactersData.size(), data.length, data[0].length);
        charactersData.forEach((charKey, letterValue) -> {
            letterValue.pixelProbability = 1.0 / letterValue.images.size();
            letterValue.images.forEach(letterArray -> {
                for (int i = 0; i < letterArray.length; ++i) {
                    classes.addToTable(charKey, i, letterValue.pixelProbability, letterArray[i]);
                }
            });
        });
        
        state = States.CLEVER;
    }
    
    @Override
    @NotNull
    public char[] classify(@NotNull double[][] data) {
        final double eta = 0.0000006;
        Collection<Callable<Character>> inputData = Arrays.stream(data).map(line -> (Callable<Character>) () -> {
            final double[] bestProbability = {Double.NEGATIVE_INFINITY};
            final char[] letter = new char[1];
            charactersData.forEach((charKey, letterValue) -> {
                double probability = 1;
                for (int i = 0; i < line.length; ++i) {
                    probability *= classes.getProbability(charKey, i, line[i]) + eta;
                }
                probability *= letterValue.probability;
                if (probability > bestProbability[0]) {
                    bestProbability[0] = probability;
                    letter[0] = charKey;
                }
            });
            return letter[0];
        }).collect(Collectors.toList());
        return new ExecutorResult(inputData).getResult();
    }
    
    private final class Letter {
        @NotNull private final Deque<double[]> images;
        private double pixelProbability;
        private double probability;
        
        Letter(final double probability, final double[] image, final int size) {
            this.images = new ArrayDeque<>(size);
            images.addLast(image);
            this.probability = probability;
        }
    }
    
    private class Table {
        private static final int TWO_EXPONENT = 5;
        private static final int MAX_VALUE = 256;
        private final Map<Character, double[][]> characterMap;
        private final double oneSample;
        private final double[][] classTotal;
        private final int imageLength;
        private final int columnLength;
        private final int[] indexes;
        
        Table(final int charCount, final double totalSamples, final int imageLength) {
            this.imageLength = imageLength;
            this.characterMap = new HashMap<>(charCount);
            this.columnLength = MAX_VALUE >> TWO_EXPONENT;
            this.classTotal = new double[imageLength][columnLength];
            this.oneSample = 1.0 / totalSamples;
            this.indexes = new int[MAX_VALUE];
            for (int i = 0; i < MAX_VALUE; ++i) {
                indexes[i] = i >> TWO_EXPONENT;
            }
        }
        
        private void addToTable(final char letter, final int index, final double pixelProbability, final double value) {
            final int valueIndex = indexes[(int) value];
            double[][] column = characterMap.get(letter);
            if (column == null) {
                column = new double[imageLength][columnLength];
                column[index][valueIndex] = pixelProbability;
                characterMap.put(letter, column);
            } else {
                column[index][valueIndex] += pixelProbability;
            }
            classTotal[index][valueIndex] += oneSample;
        }
        
        double getProbability(final char name, final int index, final double value) {
            final int valueIndex = indexes[(int) value];
            return characterMap.get(name)[index][valueIndex] * classTotal[index][valueIndex];
        }
    }
}