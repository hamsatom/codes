package KUI;

import com.sun.istack.internal.NotNull;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.*;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * @author Tomáš Hamsa on 12.03.2017.
 */
class ExecutorResult {
    private static final Logger LOGGER = Logger.getLogger(ExecutorResult.class.getName());
    private static final int THREAD_COUNT = 2 * Runtime.getRuntime().availableProcessors();
    private final Collection<? extends Callable<Character>> inputData;
    
    ExecutorResult(@NotNull final Collection<? extends Callable<Character>> inputData) {
        this.inputData = inputData;
    }
    
    @NotNull
    char[] getResult() {
        ExecutorService executor = Executors.newFixedThreadPool(THREAD_COUNT);
        final char[] charResult = new char[inputData.size()];
        int i = 0;
        try {
            List<Future<Character>> futureResult = executor.invokeAll(inputData);
            for (Future<Character> future : futureResult) {
                charResult[i++] = future.get();
            }
        } catch (ExecutionException | InterruptedException e) {
            LOGGER.log(Level.SEVERE, "Obtaining results from thread pool failed", e);
        }
        executor.shutdownNow();
        return charResult;
    }
}
