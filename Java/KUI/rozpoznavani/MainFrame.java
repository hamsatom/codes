package KUI;

import com.sun.istack.internal.NotNull;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

;

public class MainFrame {
    private static final Logger LOGGER = Logger.getLogger(MainFrame.class.getName());
    private static final String PATH = "D:\\Tomas\\ownCloud\\KUI\\KUI_Classification_Students_New\\";
    
    public static void main(String[] args) {
        // new instance of classifier
        // For k-neares neighbor and naive bayes classifier you have to just
        // change class Perceptron for NN or Bayes respectively
        try {
            useClassifier(new Perceptron());
            
            String text = " execution time[seconds]: ";
            long timeStart = System.nanoTime();
            useClassifier(new NN());
            final double nnTime = TimeUnit.MILLISECONDS.convert(System.nanoTime() - timeStart, TimeUnit.NANOSECONDS) / 1000.0;
            LOGGER.info(() -> "NN" + text + nnTime);
            
            timeStart = System.nanoTime();
            useClassifier(new Bayes());
            final double bayesTime = TimeUnit.MILLISECONDS.convert(System.nanoTime() - timeStart, TimeUnit.NANOSECONDS) / 1000.0;
            LOGGER.info(() -> "Bayes" + text + bayesTime);
            
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    private static void useClassifier(@NotNull Classifier classifier) throws IOException {
        // Loading training data
        double[][] trainData = Classifier.loadMatrix(PATH + "train.txt");
        char[] trainLabels = Classifier.loadLabels(PATH + "train_labels.txt");
        
        // training classifier
        classifier.learn(trainData, trainLabels);
        
        // loading test data
        double[][] testData = Classifier.loadMatrix(PATH + "test.txt");
        char[] testLabels = Classifier.loadLabels(PATH + "test_labels.txt");
        
        // classifying
        char[] classLabels = classifier.classify(testData);
        
        // Printing result matrix
        Classifier.printConfusionMatrix(testLabels, classLabels);
    }
}