package KUI;

import com.sun.istack.internal.NotNull;

import java.util.Arrays;
import java.util.Collection;
import java.util.concurrent.Callable;
import java.util.stream.Collectors;

/**
 * k-nearest neighbor classifier
 *
 * @author Tomáš Hamsa
 */
public class NN extends Classifier {
    
    private Letter[] letters;
    
    @Override
    public void learn(@NotNull final double[][] data, @NotNull final char[] labels) {
        letters = new Letter[labels.length];
        for (int i = 0; i < labels.length; ++i) {
            letters[i] = new Letter(labels[i], data[i]);
        }
        
        state = States.CLEVER;
    }
    
    @Override
    @NotNull
    public char[] classify(@NotNull final double[][] data) {
        final int nearestNeighbourAmount = 1;
        Collection<Callable<Character>> inputData = Arrays.stream(data)
                                                          .map(line -> (Callable<Character>) () -> Arrays.stream(letters)
                                                                                                         .map(letter -> {
                                                                                                             double distance = 0;
                                                                                                             for (int i = 0; i < line.length; ++i) {
                                                                                                                 distance += Math.pow(letter.data[i] -
                                                                                                                                      line[i], 2);
                                                                                                             }
                                                                                                             return new ImmutablePair<>(letter.name, distance);
                                                                                                         })
                                                                                                         .sorted((a, b) -> a.right - b.right < 0 ? -1 : 1)
                                                                                                         .limit(nearestNeighbourAmount)
                                                                                                         .max((a, b) -> a.left == b.left ? -1 : 1)
                                                                                                         .orElseThrow(() -> new IllegalArgumentException("No nearest point found")).left)
                                                          .collect(Collectors.toList());
        return new ExecutorResult(inputData).getResult();
    }
    
    public final class Letter {
        private final char name;
        private final double[] data;
        
        Letter(final char name, final double[] data) {
            this.name = name;
            this.data = data;
        }
    }
    
    public final class ImmutablePair<A, B> {
        private final A left;
        private final B right;
        
        ImmutablePair(final A left, final B right) {
            this.left = left;
            this.right = right;
        }
    }
}