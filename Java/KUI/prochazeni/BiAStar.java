package kui;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

import static kui.AStar.*;

/**
 * @author Tomáš Hamsa on 15.04.2017.
 */
public class BiAStar extends InformedSearchingAlgorithm {
    private volatile ValuedNode result1;
    private volatile ValuedNode result2;
    private volatile boolean stopped;
    
    static void trySleep() {
        try {
            Thread.sleep(10); // 10 ms
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }
    
    @Override
    public List<Node> findWay(final Node start, final Node end) {
        stopped = false;
        final Map<ValuedNode, ValuedNode> closedThread1 = new ConcurrentHashMap<>();
        final Map<ValuedNode, ValuedNode> closedThread2 = new ConcurrentHashMap<>();
        new Thread(new Runnable() {
            @Override
            public void run() {
                asyncFind(closedThread1, closedThread2, start, end);
            }
        }).start();
        asyncFind(closedThread2, closedThread1, end, start);
        
        final List<Node> result = getWay(result1);
        final List<Node> secondHalf = getWay(result2);
        Collections.reverse(secondHalf);
        result.addAll(secondHalf);
        
        return result;
    }
    
    private void asyncFind(final Map<ValuedNode, ValuedNode> globalClosed, final Map<ValuedNode, ValuedNode> closed, final Node startNode, final Node endNode) {
        final Map<Node, Double> distances = new HashMap<>();
        distances.put(startNode, 0.0);
        final ValuedNode start = new ValuedNode(startNode, null, 0.0);
        final Queue<ValuedNode> open = new PriorityQueue<>();
        open.add(start);
        while (!open.isEmpty() && !stopped) {
            final ValuedNode parent = open.poll();
            if (globalClosed.containsKey(parent)) {
                stopped = true;
                result1 = parent;
                result2 = globalClosed.get(parent);
                return;
            }
            final ValuedNode value = parent.parent == null ? new ValuedNode(null, null, 0) : parent.parent;
            closed.put(parent, value);
            trySleep();
            for (final Node node : parent.node.expand()) {
                final double distance = getDistance(parent.node, node) + distances.get(parent.node);
                final ValuedNode curNode = new ValuedNode(node, parent, distance + getDistance(node, endNode));
                if (closed.containsKey(curNode)) {
                    continue;
                }
                if (open.contains(curNode)) {
                    if (distances.get(node) > distance) {
                        distances.remove(node);
                        distances.put(node, distance);
                        open.remove(curNode);
                        open.add(curNode);
                    }
                } else {
                    open.add(curNode);
                    distances.put(node, distance);
                }
            }
            globalClosed.put(parent, value);
        }
    }
}
