package kui;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

import static kui.BFS.addToOpen;
import static kui.BiAStar.trySleep;

/**
 * @author Tomáš Hamsa on 15.04.2017.
 */
public class BiBFS extends InformedSearchingAlgorithm {
    private volatile boolean finished;
    private volatile Node lastNode;
    
    @Override
    public List<Node> findWay(final Node startNode, Node endNode) {
        finished = false;
        final Map<Node, Node> closedThread1 = new ConcurrentHashMap<>();
        final Map<Node, Node> closedThread2 = new ConcurrentHashMap<>();
        new Thread(new Runnable() {
            @Override
            public void run() {
                asyncExpand(closedThread1, closedThread2, startNode);
            }
        }).start();
        asyncExpand(closedThread2, closedThread1, endNode);
        Node middle = lastNode;
        
        final List<Node> firstHalf = new LinkedList<>();
        while (middle != startNode && middle != endNode) {
            firstHalf.add(0, middle);
            middle = closedThread1.get(middle);
        }
        
        final List<Node> secondHalf = new LinkedList<>();
        while (lastNode != startNode && lastNode != endNode) {
            secondHalf.add(0, lastNode);
            lastNode = closedThread2.get(lastNode);
        }
        
        if (secondHalf.get(0) == startNode) {
            Collections.reverse(firstHalf);
            secondHalf.addAll(firstHalf);
            return secondHalf;
        } else {
            Collections.reverse(secondHalf);
            firstHalf.addAll(secondHalf);
            return firstHalf;
        }
    }
    
    private void asyncExpand(final Map<Node, Node> closed, final Map<Node, Node> globalClosed, final Node startNode) {
        closed.put(startNode, startNode);
        final Deque<Node> open = new ArrayDeque<>();
        open.addFirst(startNode);
        
        while (!open.isEmpty() && !finished) {
            final Node parent = open.pollFirst();
            trySleep();
            for (final Node node : parent.expand()) {
                if (!closed.containsKey(node)) {
                    closed.put(node, parent);
                    if (globalClosed.containsKey(node)) {
                        finished = true;
                        lastNode = node;
                        return;
                    }
                    addToOpen(open, node);
                }
            }
        }
    }
}
