package kui;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

public class BFS extends BlindSearchingAlgorithm {
    private static final SearchAlgorithmType SEARCH_ALGORITHM_TYPE = SearchAlgorithmType.BFS;
    private static final boolean USE_REFLECTION_HACK = false;
    private static final boolean USE_EXAMPLE_DIRECTLY = false;
    
    static void addToOpen(final Deque<Node> open, final Node node) {
        switch (SEARCH_ALGORITHM_TYPE) {
            case BFS:
                open.addLast(node);
                break;
            case DFS:
                open.addFirst(node);
                break;
            default:
                throw new IllegalStateException("Invalid search algorithm type");
        }
    }
    
    public List<Node> findWay(final Node startNode) {
        if (USE_REFLECTION_HACK) {
            return reflectionToExampleBFS(startNode);
        } else if (USE_EXAMPLE_DIRECTLY) {
            return new kui.example.BFS().findWay(startNode);
        } else {
            return mySolution(startNode);
        }
    }
    
    private List<Node> mySolution(Node startNode) {
        if (startNode.isTarget()) {
            return Collections.singletonList(startNode);
        }
        final Map<Node, Node> closed = new HashMap<>();
        closed.put(startNode, startNode);
        final Deque<Node> open = new ArrayDeque<>();
        open.addFirst(startNode);
        Node lastNode = null;

cycle:
        while (!open.isEmpty()) {
            final Node parent = open.pollFirst();
            //trySleep();
            for (final Node node : parent.expand()) {
                if (!closed.containsKey(node)) {
                    closed.put(node, parent);
                    if (node.isTarget()) {
                        lastNode = node;
                        break cycle;
                    }
                    addToOpen(open, node);
                }
            }
        }
        
        if (lastNode == null) {
            return Collections.emptyList();
        }
        
        final List<Node> way = new LinkedList<>();
        while (lastNode != startNode) {
            way.add(0, lastNode);
            lastNode = closed.get(lastNode);
        }
        way.add(0, startNode);
        return way;
    }
    
    private List<Node> reflectionToExampleBFS(final Node startNode) {
        Class<kui.example.BFS> exampleBFSClass = kui.example.BFS.class;
        try {
            Constructor constructor = exampleBFSClass.getDeclaredConstructor();
            Method fidWayMethod = exampleBFSClass.getDeclaredMethod("findWay", Node.class);
            constructor.setAccessible(true);
            fidWayMethod.setAccessible(true);
            return (List<Node>) fidWayMethod.invoke(constructor.newInstance(), startNode);
        } catch (NoSuchMethodException | IllegalAccessException | InstantiationException | InvocationTargetException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }
    
    private enum SearchAlgorithmType {
        BFS, DFS
    }
}