package kui;

import java.util.*;

/**
 * @author Tomáš Hamsa on 15.04.2017.
 */
public class IDDFS extends BlindSearchingAlgorithm {
    private static final Collection<Node> closed = new HashSet<>();
    
    @Override
    public List<Node> findWay(final Node startNode) {
        for (long i = 1; i < Long.MAX_VALUE; ++i) {
            closed.clear();
            closed.add(startNode);
            final List<Node> result = iterativeFind(i, startNode);
            if (!result.isEmpty()) {
                return result;
            }
        }
        return Collections.emptyList();
    }
    
    private List<Node> iterativeFind(final long limit, final Node startNode) {
        if (startNode.isTarget()) {
            final List<Node> result = new LinkedList<>();
            result.add(startNode);
            return result;
        }
        if (limit <= 0) {
            return Collections.emptyList();
        }
        for (final Node node : startNode.expand()) {
            if (closed.contains(node)) {
                continue;
            }
            closed.add(node);
            final List<Node> result = iterativeFind(limit - 1, node);
            closed.remove(node);
            if (!result.isEmpty()) {
                result.add(startNode);
                return result;
            }
        }
        return Collections.emptyList();
    }
}
