package rozvrhovani;

import static rozvrhovani.Schedule.Task;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Collection;
import java.util.PriorityQueue;
import java.util.Queue;
import javax.annotation.Nonnull;
import lombok.Cleanup;

public class Main {

  private static final short ITERATIONS = 200;
  private static final int MAX_PAIRS = DataUtils.TASK_COUNT - 1;
  private static final String RESULTS_FILE = "results.txt";
  private static final Charset CHARSET = StandardCharsets.US_ASCII;

  public static void main(@Nonnull final String[] args) throws IOException {
    Schedule bestSchedule = DataUtils.getInitialSchedule();
    @Cleanup final FileChannel fileChannel = new FileOutputStream(RESULTS_FILE).getChannel();

    fileChannel.write(CHARSET.encode("Initial schedule: " + bestSchedule + System.lineSeparator()));
    fileChannel.write(CHARSET
        .encode("Tasks data: " + bestSchedule.getTasksDescription() + System.lineSeparator()));

    final TabuList tabus = new TabuList();
    final Queue<Schedule> possibilities = new PriorityQueue<>(ITERATIONS * MAX_PAIRS);

    possibilities.addAll(getExpandedSchedules(bestSchedule.getTasks(), tabus, 0));
    for (int i = 0; i < ITERATIONS; i++) {
      final Schedule currentSchedule = possibilities.poll();
      possibilities.addAll(getExpandedSchedules(currentSchedule.getTasks(), tabus, i));
      bestSchedule =
          bestSchedule.getCost() < currentSchedule.getCost() ? bestSchedule : currentSchedule;
      fileChannel.write(CHARSET.encode(
          "Iteration " + (i + 1) + " best schedule: " + bestSchedule + System.lineSeparator()));
    }

    fileChannel.write(CHARSET.encode("Total best schedule: " + bestSchedule));
  }

  @Nonnull
  private static Collection<Schedule> getExpandedSchedules(@Nonnull final Task[] tasks,
      @Nonnull final TabuList tabus, final int iteration) {
    final Collection<Schedule> expandedSchedules = new ArrayDeque<>(tasks.length - 1);
    for (int i = 1; i < tasks.length; ++i) {
      final Task taskA = tasks[i - 1];
      final Task taskB = tasks[i];
      if (tabus.contains(taskA, taskB, iteration)) {
        continue;
      }
      final Task[] swappedTasks = Arrays.copyOf(tasks, tasks.length);
      swappedTasks[i] = tasks[i - 1];
      swappedTasks[i - 1] = tasks[i];
      expandedSchedules.add(new Schedule(swappedTasks));
      tabus.put(taskA, taskB, iteration);
    }
    return expandedSchedules;
  }
}