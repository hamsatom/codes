package rozvrhovani;

import java.util.Arrays;
import java.util.stream.Collectors;
import javax.annotation.Nonnull;
import lombok.Getter;
import lombok.Value;

/**
 * @author Tomáš Hamsa on 04.05.2017.
 */
@Value
class Schedule implements Comparable<Schedule> {

  private final Task[] tasks;
  @Getter(lazy = true)
  private final int cost = countDelay();

  Schedule(@Nonnull final Task[] tasks) {
    this.tasks = tasks;
  }

  private int countDelay() {
    int lastEndTime = 0;
    int totalDelay = 0;
    for (final Task currentTask : tasks) {
      int delay = (lastEndTime + currentTask.duration - currentTask.deadline) * currentTask.weight;
      delay = delay > 0 ? delay : 0;
      totalDelay += delay;
      lastEndTime += currentTask.duration;
    }
    return totalDelay;
  }

  @Nonnull
  final String getTasksDescription() {
    return "Tasks: " + Arrays.stream(tasks).map(Task::getDescription)
        .collect(Collectors.joining("," + System.lineSeparator() + "\t\t\t\t\t"));
  }

  @Override
  public String toString() {
    return "Schedule{" + "cost=" + getCost() + ", tasks=" + Arrays.toString(tasks) + '}';
  }

  @Override
  public int compareTo(@Nonnull final Schedule o) {
    return this.getCost() - o.getCost();
  }

  @Value
  static class Task {

    private final int name;
    private final byte duration;
    private final short deadline;
    private final byte weight;

    @Override
    public String toString() {
      return String.valueOf(name);
    }

    private String getDescription() {
      return "Task{" + "name=" + name + ", duration=" + duration + ", deadline=" + deadline
          + ", weight=" + weight + '}';
    }
  }
}
