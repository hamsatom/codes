package rozvrhovani;

import static rozvrhovani.Schedule.Task;

import java.util.Collection;
import java.util.HashSet;
import java.util.Objects;
import javax.annotation.Nonnull;
import lombok.Value;

/**
 * @author Tomáš Hamsa on 04.05.2017.
 */
class TabuList {

  private static final byte CHANGES_AMOUNT = 11;
  private final Collection<TaskPair> lastPairs;

  TabuList() {
    this.lastPairs = new HashSet<>(DataUtils.TASK_COUNT * CHANGES_AMOUNT);
  }

  final void put(@Nonnull final Task taskA, @Nonnull final Task taskB, final int iteration) {
    lastPairs.add(new TaskPair(taskA, taskB, iteration));
  }

  final boolean contains(@Nonnull final Task taskA, @Nonnull final Task taskB,
      final int iteration) {
    lastPairs.removeIf(pair -> iteration - pair.iteration > CHANGES_AMOUNT);
    return lastPairs.stream().anyMatch(pair -> pair.contains(taskA, taskB));
  }

  @Value
  private class TaskPair {

    private final Task _1;
    private final Task _2;
    private final int iteration;

    private boolean contains(@Nonnull final Task taskA, @Nonnull final Task taskB) {
      return Objects.equals(_1, taskA) && Objects.equals(_2, taskB);
    }
  }
}
