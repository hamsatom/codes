package rozvrhovani;


import static rozvrhovani.Schedule.Task;

import javax.annotation.Nonnull;

/**
 * @author Tomáš Hamsa on 04.05.2017.
 */
abstract class DataUtils {

  static final byte TASK_COUNT = 15;
  private static final byte[] DURATIONS = {4, 16, 6, 5, 9, 19, 1, 13, 12, 20, 20, 19, 5, 18, 12};
  private static final short[] DEADLINES = {36, 67, 105, 53, 77, 124, 194, 157, 25, 202, 43, 61, 5,
      7, 8};
  private static final byte[] WEIGHTS = {21, 45, 35, 73, 1, 28, 21, 14, 76, 70, 51, 23, 69, 62, 80};
  private static final byte[] INITIAL_SCHEDULE = {2, 3, 15, 6, 5, 10, 8, 7, 13, 14, 1, 4, 12, 9,
      11};

  @Nonnull
  static Schedule getInitialSchedule() {
    final Task[] tasks = new Task[TASK_COUNT];
    for (int i = 0; i < TASK_COUNT; i++) {
      final int taskIndex = INITIAL_SCHEDULE[i] - 1;
      tasks[i] = new Task(taskIndex + 1, DURATIONS[taskIndex], DEADLINES[taskIndex],
          WEIGHTS[taskIndex]);
    }
    return new Schedule(tasks);
  }
}
