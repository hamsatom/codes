package cvut.fel.ass.ukol1.hamsatom;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Deque;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * @author Tomáš Hamsa on 24.02.2017.
 */
public class GenericDoubleLinkedListTest {

  @Test
  public void testAppend() {
    GenericDoubleLinkedList<Integer> doubleLinkedList = new GenericDoubleLinkedList<>();
    for (Integer i = 0; i <= 50; ++i) {
      doubleLinkedList.append(i);
    }
    for (Integer i = 0; i <= 50; ++i) {
      Assert.assertEquals(doubleLinkedList.removeFront(), i);
    }
  }

  @Test
  public void testPrepend() {
    GenericDoubleLinkedList<Integer> doubleLinkedList = new GenericDoubleLinkedList<>();
    for (Integer i = 0; i <= 50; ++i) {
      doubleLinkedList.prepend(i);
    }
    for (Integer i = 0; i <= 50; ++i) {
      Assert.assertEquals(doubleLinkedList.removeBack(), i);
    }
  }

  @Test
  public void testRemoveFront() {
    GenericDoubleLinkedList<Integer> doubleLinkedList = new GenericDoubleLinkedList<>();
    for (Integer i = 51; i <= 100; i++) {
      doubleLinkedList.append(i);
    }
    for (Integer i = 50; i >= 0; --i) {
      doubleLinkedList.prepend(i);
    }
    for (Integer i = 0; i <= 100; ++i) {
      Assert.assertEquals(doubleLinkedList.removeFront(), i);
    }
    try {
      doubleLinkedList.removeFront();
    } catch (NoSuchElementException ignored) {
    }
  }

  @Test
  public void testRemoveBack() {
    GenericDoubleLinkedList<Integer> doubleLinkedList = new GenericDoubleLinkedList<>();
    for (Integer i = 50; i >= 0; --i) {
      doubleLinkedList.prepend(i);
    }
    for (Integer i = 51; i <= 100; i++) {
      doubleLinkedList.append(i);
    }
    for (Integer i = 100; i >= 0; --i) {
      Assert.assertEquals(doubleLinkedList.removeBack(), i);
    }
    try {
      doubleLinkedList.removeBack();
    } catch (NoSuchElementException ignored) {
    }
  }

  @Test
  public void testAppendAll() {
    GenericDoubleLinkedList<Integer> doubleLinkedList = new GenericDoubleLinkedList<>();
    List<Integer> integers = new ArrayList<>(100);
    for (int i = 0; i < 100; i++) {
      integers.add(i);
    }
    doubleLinkedList.appendAll(integers);
    Integer low = 0;
    Integer high = 99;
    for (int i = 0; i < 100; i++) {
      if (i % 2 == 0) {
        Assert.assertEquals(low++, doubleLinkedList.removeFront());
      } else {
        Assert.assertEquals(high--, doubleLinkedList.removeBack());
      }
    }
    try {
      doubleLinkedList.removeFront();
    } catch (NoSuchElementException ignored) {
    }
    try {
      doubleLinkedList.removeBack();
    } catch (NoSuchElementException ignored) {
    }
  }

  @Test
  public void testTakeAll() {
    GenericDoubleLinkedList<Integer> doubleLinkedList = new GenericDoubleLinkedList<>();
    for (Integer i = 0; i < 100; i++) {
      doubleLinkedList.append(i);
    }
    List<Integer> integers = new ArrayList<>(100);
    doubleLinkedList.takeAll(integers);
    integers.sort(Comparator.comparingInt(o -> o));
    for (Integer i = 0; i < 100; i++) {
      Assert.assertEquals(i, integers.get(i));
    }
  }

  @Test
  public void testCount() {
    GenericDoubleLinkedList<Integer> doubleLinkedList = new GenericDoubleLinkedList<>();
    Assert.assertEquals(0, doubleLinkedList.count(Collections.EMPTY_LIST));
    Deque<Integer> testDeque = new ArrayDeque<>(100);
    for (int i = 0; i < 100; i++) {
      testDeque.addLast(i);
    }
    Assert.assertEquals(testDeque.size(), doubleLinkedList.count(testDeque));
  }

  @Test
  public void testIterator() {
    GenericDoubleLinkedList<Integer> doubleLinkedList = new GenericDoubleLinkedList<>();
    for (Integer i = 0; i < 100; i++) {
      doubleLinkedList.append(i);
    }
    Iterator<Integer> forwardIterator = doubleLinkedList.iterator();
    for (Integer i = 0; i < 100; i++) {
      Assert.assertTrue(forwardIterator.hasNext());
      Assert.assertEquals(i, forwardIterator.next());
    }
    Assert.assertFalse(forwardIterator.hasNext());
  }

  @Test
  public void testBackwardIterator() {
    GenericDoubleLinkedList<Integer> doubleLinkedList = new GenericDoubleLinkedList<>();
    for (Integer i = 0; i < 100; i++) {
      doubleLinkedList.prepend(i);
    }
    Iterator<Integer> backwardIterator = doubleLinkedList.backwardIterator();
    for (Integer i = 0; i < 100; ++i) {
      Assert.assertTrue(backwardIterator.hasNext());
      Assert.assertEquals(i, backwardIterator.next());
    }
    Assert.assertFalse(backwardIterator.hasNext());
  }
}