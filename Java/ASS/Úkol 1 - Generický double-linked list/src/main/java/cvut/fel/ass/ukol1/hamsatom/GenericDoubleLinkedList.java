package cvut.fel.ass.ukol1.hamsatom;


import java.util.Collection;
import java.util.Iterator;
import java.util.NoSuchElementException;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * Double linked list with generic type
 * It's possible to store {@code null}
 *
 * @author hamsatom
 */
public class GenericDoubleLinkedList<E> implements Iterable<E> {

  /**
   * First {@code Node} of the {@link GenericDoubleLinkedList}<br>
   * {@code null} if the list is empty
   */
  @Nullable
  private Node head = null;
  /**
   * Last {@code Node} of the {@link GenericDoubleLinkedList}<br>
   * {@code null} if the list is empty
   */
  @Nullable
  private Node tail = null;

  /**
   * Adds element at the end of the {@link GenericDoubleLinkedList}
   *
   * @param element element to be added to the end of the {@link GenericDoubleLinkedList}
   */
  public synchronized void append(@Nullable E element) {
    Node newNode = new Node(element);
    if (tail == null) {
      head = newNode;
      tail = newNode;
    } else {
      newNode.previous = tail;
      tail.next = newNode;
      tail = newNode;
    }
  }

  /**
   * Adds element at the begging of the {@link GenericDoubleLinkedList}
   *
   * @param element element to be added to the begging of the {@link GenericDoubleLinkedList}
   */
  public synchronized void prepend(@Nullable E element) {
    Node newNode = new Node(element);
    if (head == null) {
      head = newNode;
      tail = newNode;
    } else {
      newNode.next = head;
      head.previous = newNode;
      head = newNode;
    }
  }

  /**
   * Removes first element from the {@link GenericDoubleLinkedList} and returns it
   *
   * @return First element of the {@link GenericDoubleLinkedList}<br>
   * Can return {@code null} if the first element was {@code null}
   * @throws NoSuchElementException if the {@link GenericDoubleLinkedList} is empty
   */
  @Nullable
  public synchronized E removeFront() throws NoSuchElementException {
    if (head == null) {
      throw new NoSuchElementException("List is empty");
    }
    Node headToReturn = head;
    Node newHead = head.next;
    if (newHead != null) {
      newHead.previous = null;
    } else {
      tail = null;
    }
    head = newHead;
    return headToReturn.element;
  }

  /**
   * Removes last element from the {@link GenericDoubleLinkedList} and returns it
   *
   * @return Last element of the {@link GenericDoubleLinkedList}<br>
   * Can return {@code null} if the last element was {@code null}
   * @throws NoSuchElementException if the {@link GenericDoubleLinkedList} is empty
   */
  @Nullable
  public synchronized E removeBack() throws NoSuchElementException {
    if (tail == null) {
      throw new NoSuchElementException("List is empty");
    }
    Node tailToReturn = tail;
    Node newTail = tail.previous;
    if (newTail != null) {
      newTail.next = null;
    } else {
      head = null;
    }
    tail = newTail;
    return tailToReturn.element;
  }

  /**
   * Inserts all elements from the {@code collection} at the end of this {@link GenericDoubleLinkedList}. <br>
   *
   * @param collection {@link Collection} of elements to be inserted at the begging of this {@link GenericDoubleLinkedList}
   */
  public void appendAll(@Nonnull Collection<? extends E> collection) {
    collection.forEach(this::append);
  }

  /**
   * Inserts all elements from this {@link GenericDoubleLinkedList} to provided {@link Collection}
   *
   * @param collection {@link Collection} to which all elements will be inserted
   * @return {@link Collection} with all elements
   */
  @Nonnull
  public Collection takeAll(@Nonnull Collection<? super E> collection) {
    forEach(collection::add);
    return collection;
  }

  /**
   * Counts number of elements in given {@link Collection}
   *
   * @param collection {@link Collection} which will be used to count the elements
   * @return Number of elements in given {@link Collection}
   */
  public long count(@Nonnull Collection<?> collection) {
    return collection.size();
  }

  /**
   * Provides {@link Iterator} that iterates forward
   *
   * @return {@link Iterator} that iterates from the begging to the end of this {@link GenericDoubleLinkedList}
   */
  @Override
  @Nonnull
  public Iterator<E> iterator() {
    return new Iterator<E>() {
      @Nonnull
      private Node currentNode = head;

      /**
       * {@inheritDoc}
       */
      @Override
      public boolean hasNext() {
        return currentNode != null;
      }

      /**
       * {@inheritDoc}
       */
      @Override
      @Nullable
      public E next() {
        E element = currentNode.element;
        currentNode = currentNode.next;
        return element;
      }
    };
  }

  /**
   * Provides {@link Iterator} that iterates backward
   *
   * @return {@link Iterator} that iterates from the end to the begging of this {@link GenericDoubleLinkedList}
   */
  @Nonnull
  public Iterator<E> backwardIterator() {
    return new Iterator<E>() {
      @Nonnull
      private Node currentNode = tail;

      /**
       * {@inheritDoc}
       */
      @Override
      public boolean hasNext() {
        return currentNode != null;
      }

      /**
       * {@inheritDoc}
       */
      @Override
      @Nullable
      public E next() {
        E element = currentNode.element;
        currentNode = currentNode.previous;
        return element;
      }
    };
  }

  /**
   * Represents one node of the list. Has link to the previous and next {@code Node}.
   */
  private class Node {

    /**
     * Stored value<br>
     * Can be {@code null}
     */
    @Nullable
    final private E element;

    /**
     * Link to the previous {@code Node}<br>
     * {@code null} if the {@code Node} is {@code head} of the {@code GenericDoubleLinkedList}
     */
    @Nullable
    private Node previous = null;

    /**
     * Link to the next {@code Node}<br>
     * {@code null} if the {@code Node} is {@code tail} of the {@code GenericDoubleLinkedList}
     */
    @Nullable
    private Node next = null;

    private Node(@Nullable E element) {
      this.element = element;
    }
  }
}
