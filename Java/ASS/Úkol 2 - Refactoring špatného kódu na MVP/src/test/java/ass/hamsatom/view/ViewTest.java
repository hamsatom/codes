package ass.hamsatom.view;

import ass.hamsatom.BaseTest;
import org.testng.Assert;
import org.testng.annotations.Test;

public class ViewTest extends BaseTest {

  @Test
  public void testChatHistory() {
    Assert.assertEquals(chatViewMock.getInitMessages(), INITIAL_MSG);
  }

  @Test
  public void testSendAndReceiveMessage() {
    chatViewMock.sendText(TEST_MESSAGE);
    Assert.assertEquals(chatViewMock.getLastAddedMessage(), TEST_MESSAGE + System.lineSeparator());
  }
}
