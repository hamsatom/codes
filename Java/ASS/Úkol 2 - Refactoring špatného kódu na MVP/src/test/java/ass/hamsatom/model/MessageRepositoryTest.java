package ass.hamsatom.model;

import ass.hamsatom.BaseTest;
import org.testng.annotations.Test;

/**
 * @author Tomáš Hamsa on 06.03.2017.
 */
public class MessageRepositoryTest extends BaseTest {

  @Test(priority = 1)
  public void testHistoryFromFile() throws Exception {
    String[] historyFromRepository = repository.getSavedMessages();
    arrayEqualWithInitial(historyFromRepository);
  }

  @Test(priority = 2)
  public void testSavingMessages() throws Exception {
    repository.saveMessage(TEST_MESSAGE);
    String[] chatHistory = getTestFileContent();
    arrayEqualTestData(chatHistory);
  }

  @Test(priority = 3)
  public void testHistoryChange() throws Exception {
    String[] historyFromRepo = repository.getSavedMessages();
    arrayEqualTestData(historyFromRepo);
  }


}