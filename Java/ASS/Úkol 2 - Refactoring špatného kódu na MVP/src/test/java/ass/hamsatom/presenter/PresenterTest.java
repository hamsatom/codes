package ass.hamsatom.presenter;

import ass.hamsatom.BaseTest;
import ass.hamsatom.view.ChatViewMock;
import javax.annotation.Nonnull;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * @author Tomáš Hamsa on 06.03.2017.
 */
public class PresenterTest extends BaseTest {

  @Test(priority = 1)
  public void testAddView() {
    ChatViewMock newChatWindow = getAddedView();
    Assert.assertEquals(newChatWindow.getInitMessages(), INITIAL_MSG);
  }

  @Test(priority = 2)
  public void testUpdateFromView() throws Exception {
    ChatViewMock newChatWindow = getAddedView();
    presenter.update(newChatWindow, TEST_MESSAGE);
    compareViews(newChatWindow);
    String[] repositoryMessages = repository.getSavedMessages();
    arrayEqualTestData(repositoryMessages);
  }

  @Test(priority = 3)
  public void testUpdateFromRepo() throws Exception {
    tearDown();
    setUp();
    ChatViewMock newChatWindow = getAddedView();
    presenter.update(repository, TEST_MESSAGE + System.lineSeparator());
    compareViews(newChatWindow);
    String[] repositoryMessages = repository.getSavedMessages();
    arrayEqualWithInitial(repositoryMessages);
  }

  @Nonnull
  private ChatViewMock getAddedView() {
    ChatViewMock newChatWindow = new ChatViewMock();
    presenter.addChatWindow(newChatWindow);
    return newChatWindow;
  }

  private void compareViews(@Nonnull ChatViewMock newChatWindow) {
    Assert.assertEquals(newChatWindow.getLastAddedMessage(), TEST_MESSAGE + System.lineSeparator());
    Assert.assertEquals(chatViewMock.getLastAddedMessage(), TEST_MESSAGE + System.lineSeparator());
  }
}