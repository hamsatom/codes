package ass.hamsatom.view;


import javax.annotation.Nonnull;

/**
 * Mocked version of {@link ChatView}. Contains last added message and initial message set to the view.
 *
 * @author Tomáš Hamsa on 05.03.2017.
 */

public final class ChatViewMock extends ObservableChatView {

  private String lastAddedMessage;
  private String initMessages;

  @Override
  public void setMessages(String initialMessages) {
    initMessages = initialMessages;
  }

  @Override
  public void addMessage(String message) {
    lastAddedMessage = message;
  }

  public void sendText(@Nonnull String text) {
    notifyObservers(text);
  }

  public String getLastAddedMessage() {
    return lastAddedMessage;
  }

  public String getInitMessages() {
    return initMessages;
  }
}
