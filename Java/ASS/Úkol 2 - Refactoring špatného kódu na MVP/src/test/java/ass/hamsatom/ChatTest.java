package ass.hamsatom;

import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * @author Tomáš Hamsa on 06.03.2017.
 */
public class ChatTest extends BaseTest {

  @Test(priority = 1)
  public void testHistory() throws Exception {
    String[] testFileContent = getTestFileContent();
    Assert.assertEquals(chatViewMock.getInitMessages(), INITIAL_MSG);
    arrayEqualWithInitial(testFileContent);
  }

  @Test(priority = 2)
  public void testSentMessage() throws Exception {
    chatViewMock.sendText(TEST_MESSAGE);
    Assert.assertEquals(chatViewMock.getLastAddedMessage(), TEST_MESSAGE + System.lineSeparator());
    arrayEqualTestData(getTestFileContent());
  }
}