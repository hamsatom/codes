package ass.hamsatom;

import ass.hamsatom.model.ObservableMessageRepository;
import ass.hamsatom.model.ObservableMessageRepositoryImpl;
import ass.hamsatom.presenter.Presenter;
import ass.hamsatom.presenter.PresenterImpl;
import ass.hamsatom.view.ChatViewMock;
import ass.hamsatom.view.ObservableChatView;
import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.name.Names;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.UUID;
import java.util.stream.Stream;
import javax.annotation.Nonnull;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

/**
 * Test class that contains all data needed for all other tests.
 * Also injects classes needed for test and gets needed instances.
 *
 * @author Tomáš Hamsa on 06.03.2017.
 */
public abstract class BaseTest {

  protected static final String INITIAL_MSG = "This is initial file message that should appear as first in the view\r\n";
  protected static final String TEST_MESSAGE = "testMessage";
  protected ChatViewMock chatViewMock;
  protected Presenter presenter;
  protected ObservableMessageRepository repository;
  private String testFile = "testMessages.txt";

  @BeforeClass
  public void setUp() throws Exception {
    testFile = UUID.randomUUID() + testFile;
    Files.write(Paths.get(testFile), INITIAL_MSG.getBytes());
    chatViewMock = new ChatViewMock();
    Injector inject = Guice.createInjector(new AbstractModule() {
      @Override
      protected void configure() {
        bind(String.class).annotatedWith(Names.named("file_name")).toInstance(testFile);
        bind(ObservableMessageRepository.class).to(ObservableMessageRepositoryImpl.class);
        bind(ObservableChatView.class).toInstance(chatViewMock);
      }
    });
    presenter = inject.getInstance(PresenterImpl.class);
    repository = inject.getInstance(ObservableMessageRepositoryImpl.class);
  }

  @AfterClass
  public void tearDown() throws Exception {
    Files.delete(Paths.get(testFile));
  }

  protected String[] getTestFileContent() throws IOException {
    try (Stream<String> fileStream = Files.lines(Paths.get(testFile))) {
      return fileStream.toArray(String[]::new);
    }
  }

  protected void arrayEqualTestData(@Nonnull String[] historyFromRepo) {
    Assert.assertEquals(historyFromRepo.length, 2);
    Assert.assertEquals(historyFromRepo[0], INITIAL_MSG.trim());
    Assert.assertEquals(historyFromRepo[1], TEST_MESSAGE);
  }

  protected void arrayEqualWithInitial(@Nonnull String[] testFileContent) {
    Assert.assertEquals(testFileContent.length, 1);
    Assert.assertEquals(testFileContent[0], INITIAL_MSG.trim());
  }
}
