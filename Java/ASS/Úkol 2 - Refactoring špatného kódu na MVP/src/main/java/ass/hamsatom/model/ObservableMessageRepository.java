package ass.hamsatom.model;


import ass.hamsatom.observers.GenericObservable;

/**
 * Provides {@code Observable<String>} interface for {@link MessageRepository}.
 * All notifications from class will be {@code String} type.
 *
 * @author Tomáš Hamsa on 04.03.2017.
 */
public abstract class ObservableMessageRepository extends GenericObservable<String> implements
    MessageRepository {

}
