package ass.hamsatom.model;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.stream.Stream;
import javax.annotation.Nonnull;

/**
 * Implementation of  {@link ObservableMessageRepository} that saves messages to {@code File}.
 * {@link String} that contains path of file must be injected in constructor.
 */
public class ObservableMessageRepositoryImpl extends ObservableMessageRepository {

  @Nonnull
  private final File file;

  @Inject
  public ObservableMessageRepositoryImpl(@Nonnull @Named("file_name") String fileName)
      throws IOException {
    file = openValidFile(fileName);
  }

  @Nonnull
  private File openValidFile(@Nonnull String fileName) throws IOException {
    File tmpFile = new File(fileName);
    String errMsg = "File " + tmpFile.getName() + " in " + tmpFile.getAbsolutePath();
    if (!tmpFile.exists()) {
      throw new IOException(errMsg + " doesn't exist");
    }
    if (!tmpFile.canRead()) {
      throw new IOException(errMsg + " can't be read");
    }
    if (!tmpFile.isFile()) {
      throw new IOException(errMsg + " is actually not a file");
    }
    return tmpFile;
  }

  @Override
  public void saveMessage(@Nonnull String message) throws IOException {
    Files.write(file.toPath(), message.getBytes(), StandardOpenOption.APPEND);
    notifyObservers(message);
  }

  @Nonnull
  @Override
  public String[] getSavedMessages() throws IOException {
    Stream<String> fileStream = Files.lines(file.toPath());
    return fileStream.toArray(String[]::new);
  }
}
