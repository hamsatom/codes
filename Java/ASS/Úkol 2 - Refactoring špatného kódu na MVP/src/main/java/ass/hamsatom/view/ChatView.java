package ass.hamsatom.view;

/**
 * GUI of the chat app. Represents the view in MVP architecture. Messages should be same across all {@code ChatViews}.
 * Whenever message is sent in {@code ChatView}, the message is saved and added to all {@code ChatViews}.
 * At the time of creation history of conversation will be added to the {@code ChatView}.
 *
 * @author Tomáš Hamsa on 02.03.2017.
 */
public interface ChatView {

  /**
   * Sets message that this {@code ChatView} contains.
   *
   * @param initialMessages Message that will appear in this {@code ChatView}
   */
  void setMessages(String initialMessages);

  /**
   * Appends passed message to message already in this {@code ChatView}.
   * If the message is empty of {@code null} nothing will happen.
   *
   * @param message To be append under all previous messages.
   */
  void addMessage(String message);
}
