package ass.hamsatom.model;

import java.io.IOException;
import javax.annotation.Nonnull;

/**
 * Class responsible for data saving and loading.
 * Saves data to file located at injected path.
 *
 * @author Tomáš Hamsa on 02.03.2017.
 */
public interface MessageRepository {

  /**
   * Saves message to file at injected path.<br>
   * Save should be called after each message as save appends data to existing file.
   *
   * @param message Message to be saved to file
   * @throws IOException if saving failed
   */
  void saveMessage(@Nonnull String message) throws IOException;

  /**
   * Returns all messages that were save to file. One array position represents one message.
   *
   * @return Array of saved messages. One message per {@code String}
   * @throws IOException if reading file failed
   */
  @Nonnull
  String[] getSavedMessages() throws IOException;
}
