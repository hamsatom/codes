package ass.hamsatom.view;


import ass.hamsatom.observers.GenericObservable;

/**
 * {@link ChatView} that implements type-safe {@code Observable<String>} pattern.
 * All notifications from class will be {@code String} type.
 *
 * @author Tomáš Hamsa on 07.03.2017.
 */
public abstract class ObservableChatView extends GenericObservable<String> implements ChatView {

}
