package ass.hamsatom.view;


import javax.annotation.Nonnull;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.WindowConstants;

public class ObservableChatViewImpl extends ObservableChatView {

  private static final int COLUMNS_COUNT = 80;
  private static final int ROWS_COUNT = 15;
  private static final String WINDOW_TITLE = "Chat window";
  private static final String SEND_BUTTON_TITLE = "Send";

  private JTextArea messages;

  public ObservableChatViewImpl() {
    messages = new JTextArea(ROWS_COUNT, COLUMNS_COUNT);
    initGraphics();
  }

  private void initGraphics() {
    JTextField newMessageInput = new JTextField(COLUMNS_COUNT);
    JButton btnSend = initSendButton(newMessageInput);
    JPanel horizontal = initHorizontalPanel(newMessageInput, btnSend);
    JPanel vertical = initVerticalPanel(horizontal);
    initFrame(vertical);
  }

  @Nonnull
  private JButton initSendButton(@Nonnull JTextField newMessageInput) {
    JButton btnSend = new JButton(SEND_BUTTON_TITLE);
    btnSend.addActionListener(e -> {
      notifyObservers(newMessageInput.getText());
      newMessageInput.setText("");
    });
    return btnSend;
  }

  private void initFrame(JPanel vertical) {
    JFrame frame = new JFrame();
    frame.setTitle(WINDOW_TITLE);
    frame.add(vertical);
    frame.setVisible(true);
    frame.pack();
    frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
  }

  @Nonnull
  private JPanel initVerticalPanel(JPanel horizontal) {
    JPanel vertical = new JPanel();
    vertical.setLayout(new BoxLayout(vertical, BoxLayout.Y_AXIS));
    vertical.add(messages);
    vertical.add(horizontal);
    return vertical;
  }

  @Nonnull
  private JPanel initHorizontalPanel(JTextField newMessageInput, JButton btnSend) {
    JPanel horizontal = new JPanel();
    horizontal.setLayout(new BoxLayout(horizontal, BoxLayout.X_AXIS));
    horizontal.add(newMessageInput);
    horizontal.add(btnSend);
    return horizontal;
  }

  @Override
  public void setMessages(String oldMessages) {
    messages.setText(oldMessages);
    messages.setEditable(false);
  }

  @Override
  public void addMessage(String message) {
    messages.append(message);
  }
}

