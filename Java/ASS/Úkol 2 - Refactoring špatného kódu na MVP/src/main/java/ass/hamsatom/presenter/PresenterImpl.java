package ass.hamsatom.presenter;

import ass.hamsatom.model.MessageRepository;
import ass.hamsatom.model.ObservableMessageRepository;
import ass.hamsatom.observers.GenericObservable;
import ass.hamsatom.view.ChatView;
import ass.hamsatom.view.ObservableChatView;
import com.google.inject.Inject;
import com.sun.istack.internal.NotNull;
import java.io.IOException;
import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Implementation of {@link Presenter}. Manages at least two {@link ChatView}.
 * All passed messages will be saved to {@link ObservableMessageRepository} and appear in all {@link ChatView}.
 * Whenever new {@link ChatView} is added to {@code @link PresenterImpl} the content of the View will be set to match the history of chat.
 * {@link ChatView} and two {@link ChatView} must be injected to this class at the time of it's creation.
 *
 * @author Tomáš Hamsa on 04.03.2017.
 */
public class PresenterImpl implements Presenter {

  private static final Logger LOGGER = Logger.getLogger(PresenterImpl.class.getName());
  private static final int CHAT_WINDOWS_COUNT = 2;
  @NotNull
  private final Collection<ChatView> chatWindows;
  @NotNull
  private final MessageRepository messageRepository;

  @Inject
  public PresenterImpl(@NotNull ObservableMessageRepository messageRepository,
      @NotNull ObservableChatView chatWindow1, @NotNull
      ObservableChatView chatWindow2) {
    this.messageRepository = messageRepository;
    messageRepository.addObserver(this);
    chatWindows = new ArrayDeque<>(CHAT_WINDOWS_COUNT);
    String oldMessages = getChatHistory();
    linkChatWindow(chatWindow1, oldMessages);
    linkChatWindow(chatWindow2, oldMessages);
  }

  private void linkChatWindow(@NotNull ObservableChatView chatWindow,
      @NotNull String chatHistory) {
    chatWindows.add(chatWindow);
    chatWindow.addObserver(this);
    chatWindow.setMessages(chatHistory);
  }

  @Override
  public void addChatWindow(@NotNull ObservableChatView chatWindow) {
    if (chatWindows.contains(chatWindow)) {
      return;
    }
    chatWindows.add(chatWindow);
    chatWindow.addObserver(this);
    chatWindow.setMessages(getChatHistory());
  }

  @NotNull
  private String getChatHistory() {
    String[] oldMessages;
    try {
      oldMessages = messageRepository.getSavedMessages();
    } catch (IOException e) {
      LOGGER.log(Level.SEVERE, "Loading old messages failed", e);
      return "";
    }
    @NotNull StringBuilder oldMessagesSeparated = new StringBuilder();
    Arrays.stream(oldMessages)
        .forEach(msg -> oldMessagesSeparated.append(msg).append(System.lineSeparator()));
    return oldMessagesSeparated.toString();
  }

  private void processMessageText(@NotNull String text) {
    try {
      messageRepository.saveMessage(text + System.lineSeparator());
    } catch (IOException e) {
      LOGGER.log(Level.SEVERE, "Saving messages failed", e);
    }
  }

  @Override
  public void update(GenericObservable<String> object, @NotNull String data) {
    if (object instanceof MessageRepository) {
      chatWindows.forEach(chatWindow -> chatWindow.addMessage(data));
    } else if (object instanceof ChatView) {
      processMessageText(data);
    }
  }
}
