package ass.hamsatom;

import ass.hamsatom.model.ObservableMessageRepository;
import ass.hamsatom.model.ObservableMessageRepositoryImpl;
import ass.hamsatom.presenter.PresenterImpl;
import ass.hamsatom.view.ObservableChatView;
import ass.hamsatom.view.ObservableChatViewImpl;
import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.name.Names;

public class Main {

  private static final String MESSAGES_FILE = "messages.txt";

  /**
   * Create new chat app that will save conversation to specified file.
   * Injects needed classes.
   *
   * @param args Args with which the application was run.
   */
  public static void main(String[] args) {
    Injector inject = Guice.createInjector(new AbstractModule() {
      @Override
      protected void configure() {
        bind(String.class).annotatedWith(Names.named("file_name")).toInstance(MESSAGES_FILE);
        bind(ObservableMessageRepository.class).to(ObservableMessageRepositoryImpl.class);
        bind(ObservableChatView.class).to(ObservableChatViewImpl.class);
      }
    });
    inject.getInstance(PresenterImpl.class);
  }
}
