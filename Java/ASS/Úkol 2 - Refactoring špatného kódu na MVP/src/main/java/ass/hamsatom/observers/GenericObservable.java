package ass.hamsatom.observers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Observable;
import javax.annotation.Nonnull;

/**
 * Provides type safe {@link Observable} interface
 *
 * @author Tomáš Hamsa on 05.03.2017.
 */
public class GenericObservable<T> {

  private final Collection<GenericObserver<T>> genericGenericObservers = new ArrayList<>();


  /**
   * Adds {@code Observer} to collection.
   *
   * @param obs Observer that excepts {@code T} type notifications
   */
  public void addObserver(@Nonnull GenericObserver<T> obs) {
    if (genericGenericObservers.contains(obs)) {
      return;
    }
    genericGenericObservers.add(obs);
  }

  /**
   * Notifies all observers with {@code T} type data
   *
   * @param data {@code T} type data} that all observers will be notified about
   */
  protected void notifyObservers(T data) {
    genericGenericObservers.parallelStream()
        .forEach(genericObserver -> genericObserver.update(this, data));
  }
}
