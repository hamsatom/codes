package ass.hamsatom.presenter;

import ass.hamsatom.observers.GenericObserver;
import ass.hamsatom.view.ChatView;
import ass.hamsatom.view.ObservableChatView;
import com.sun.istack.internal.NotNull;

/**
 * Presenter in MVP architecture. Processes data from view and update the views. Also saves received data to some model.
 *
 * @author Tomáš Hamsa on 04.03.2017.
 */
public interface Presenter extends GenericObserver<String> {

  /**
   * Adds new {@link ChatView} into views managed by this {@code Presenter}.
   * If {@link ChatView} is already managed by this {@code Presenter}, nothing will happen.
   *
   * @param chatWindow {@link ObservableChatView} to be added to this presenter.
   */
  void addChatWindow(@NotNull ObservableChatView chatWindow);
}
