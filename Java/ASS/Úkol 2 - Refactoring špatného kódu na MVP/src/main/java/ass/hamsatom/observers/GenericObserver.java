package ass.hamsatom.observers;

import java.util.Observer;

/**
 * Provides generic {@code T} type {@link Observer} interface.
 * Received notification will be {@code T} type.
 *
 * @author Tomáš Hamsa on 05.03.2017.
 */
@FunctionalInterface
public interface GenericObserver<T> {

  /**
   * Called whenever {@code Observer} was notified from {@code Observable}.
   * Notifications will contain {@code T} type data.
   *
   * @param object Instance that called update
   * @param data Data that were passed in update call
   */
  void update(GenericObservable<T> object, T data);
}