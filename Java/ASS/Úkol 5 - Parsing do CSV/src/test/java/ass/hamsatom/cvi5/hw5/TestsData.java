package ass.hamsatom.cvi5.hw5;

/**
 * @author Tomáš Hamsa on 24.03.2017.
 */
abstract class TestsData {

  static final String TEST_FILE = "testData.txt";


  static final String DATE_FORMAT_1 = "EEE, MM. dd. YYYY";
  static final String DATE_FORMAT_2 = "MM. dd. YY";
  static final String INVALID_DATE_FORMAT = "MM. dd.";

  static final String DATE_LABEL = "DATE: ";
  static final String PHONE_LABEL = "PHONE: ";

  static final String PHONE_LOCAL = "CZ";
  static final String[] VALID_NUMBERS = {"+420604875771", "420608724185", "(420)601789654",
      "(+420)604879132"};
  static final String[] INVALID_NUMBERS = {"karel",
      String.valueOf(Long.MAX_VALUE),
      String.valueOf(Long.MIN_VALUE),
      String.valueOf(Double.NEGATIVE_INFINITY),
      String.valueOf(Double.NaN),
      String.valueOf(Double.POSITIVE_INFINITY),
      String.valueOf(Double.MAX_VALUE),
      String.valueOf(Double.MIN_VALUE),
      "+4206048757710",
      "4206087241850",
      "6098541230",
      "(420)6017896540",
      "(+420)6048791320",
      "+421604875771",
      "421608724185",
      "60985412",
      "(421)601789654",
      "(+421)604879132",
      "+42060487577",
      "42060872418",
      "009854123",
      "(420)60178965",
      "(+420)60487912",
      "",
      " ",
      null,
      System.lineSeparator()
  };
}
