package ass.hamsatom.cvi5.hw5;

import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber;
import io.reactivex.Observable;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import javax.annotation.Nonnull;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

/**
 * @author Tomáš Hamsa on 24.03.2017.
 */
public class CSVWriterTest {

  private static final String TEST_CSV = "testCSV.csv";

  @AfterMethod
  public final void tearDown() throws Exception {
    Files.deleteIfExists(Paths.get(TEST_CSV));
  }

  private String joinWithComma(@Nonnull String[] headers, @Nonnull String... data) {
    char separator = ',';
    char quote = '"';
    String[] quotedHeaders = Arrays.stream(headers).map(header -> quote + header + quote)
        .toArray(String[]::new);
    return StringUtils.join(quotedHeaders, separator) + System.lineSeparator() + StringUtils
        .join(data, separator) + System.lineSeparator();
  }

  @Test
  public final void testWriteRecordsToCSV() throws Exception {
    String[] headers = {"  TestHeader1", "Test,HeaderB"};
    PhoneNumber phone = PhoneNumberUtil.getInstance().getExampleNumber(TestsData.PHONE_LOCAL);
    Record record = new Record(phone, DateTime.now());

    CSVWriter.writeRecordsToCSV(TEST_CSV, Observable.just(record), headers);

    String csvContent = new String(Files.readAllBytes(Paths.get(TEST_CSV)), CSVWriter.CHAR_FORMAT);
    String expectedCSV = joinWithComma(headers, record.getTelephone().getRawInput(),
        record.getDate().toDateTimeISO().toString());

    Assert.assertEquals(csvContent, expectedCSV, "Content of read CSV doesn't match CSV standard");
  }

}