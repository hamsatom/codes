package ass.hamsatom.cvi5.task1;

import java.util.Optional;
import org.junit.Assert;
import org.testng.annotations.Test;

/**
 * @author Tomáš Hamsa on 21.03.2017.
 */
public class UserToAddressNiceTest {

  @Test
  public final void testUserToStreet() {
    Assert.assertFalse(UserToAddressNice.userToStreet(null).isPresent());
    Assert.assertFalse(UserToAddressNice.userToStreet(new User(null)).isPresent());
    Assert.assertFalse(UserToAddressNice.userToStreet(new User(new Address(null))).isPresent());

    Optional<String> addr = UserToAddressNice.userToStreet(new User(new Address("addr")));
    Assert.assertTrue(addr.isPresent());
    Assert.assertEquals("addr", addr.get());
  }

}