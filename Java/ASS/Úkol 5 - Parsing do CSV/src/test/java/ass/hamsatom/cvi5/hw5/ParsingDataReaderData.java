package ass.hamsatom.cvi5.hw5;

import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber;
import io.reactivex.Observable;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.Locale;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import javaslang.control.Try;
import javax.annotation.Nonnull;
import org.apache.commons.lang3.RandomUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

/**
 * @author Tomáš Hamsa on 25.03.2017.
 */
abstract class ParsingDataReaderData {

  private static final Locale LOCALE = Locale.US;
  final Collection<Record> validRecords = new ArrayList<>(TestsData.VALID_NUMBERS.length << 1);

  @BeforeClass
  public void setUp() throws Exception {
    Files.write(Paths.get(TestsData.TEST_FILE), generateData().getBytes(StandardCharsets.UTF_8));
  }

  @AfterClass
  public void tearDown() throws Exception {
    Files.deleteIfExists(Paths.get(TestsData.TEST_FILE));
  }

  private String getDateForPattern(@Nonnull String pattern) {
    return DateTime.now().toString(pattern, LOCALE);
  }

  final DateTime getDate(@Nonnull String date, @Nonnull String format) {
    return DateTimeFormat.forPattern(format).withLocale(LOCALE).parseDateTime(date);
  }

  private String makeDataEntry(String number, String date) {
    return TestsData.PHONE_LABEL + number + System.lineSeparator() + TestsData.DATE_LABEL + date
        + System.lineSeparator();
  }

  private String generateData() {
    PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
    int bound = RandomUtils.nextInt(0, 200);
    StringBuilder input = new StringBuilder(
        (TestsData.INVALID_NUMBERS.length + TestsData.VALID_NUMBERS.length + bound) * 3);
    String validDate1 = getDateForPattern(TestsData.DATE_FORMAT_1);
    String validDate2 = getDateForPattern(TestsData.DATE_FORMAT_2);
    String invalidDate = getDateForPattern(TestsData.INVALID_DATE_FORMAT);

    Arrays.stream(TestsData.VALID_NUMBERS).forEach(
        phone -> addValidNumbers(phoneUtil, input, validDate1, validDate2, invalidDate, phone));
    Arrays.stream(TestsData.INVALID_NUMBERS).forEach(phone -> {
      input.append(makeDataEntry(phone, validDate1));
      input.append(makeDataEntry(phone, validDate2));
      input.append(makeDataEntry(phone, invalidDate));
    });
    for (int i = 0; i < bound; i++) {
      PhoneNumber randomValidPhone = phoneUtil.getExampleNumber(TestsData.PHONE_LOCAL);
      addValidNumbers(phoneUtil, input, validDate1, validDate2, invalidDate,
          String.format("%d%d", randomValidPhone.getCountryCode(),
              randomValidPhone.getNationalNumber()));
    }
    return input.toString();
  }

  private void addValidNumbers(@Nonnull PhoneNumberUtil phoneUtil, @Nonnull StringBuilder input,
      @Nonnull String validDate1,
      @Nonnull String validDate2, @Nonnull String invalidDate, String phone) {
    PhoneNumber number = Try.of(() -> phoneUtil.parse(phone, TestsData.PHONE_LOCAL)).get();
    input.append(makeDataEntry(phone, validDate1));
    validRecords.add(new Record(number, getDate(validDate1, TestsData.DATE_FORMAT_1)));
    input.append(makeDataEntry(phone, validDate2));
    validRecords.add(new Record(number, getDate(validDate2, TestsData.DATE_FORMAT_2)));
    input.append(makeDataEntry(phone, invalidDate));
  }


  Observable<Record> getReadResult() throws IOException {
    Pattern pattern = Pattern
        .compile('(' + TestsData.DATE_LABEL + '|' + TestsData.PHONE_LABEL + ')');
    return ParsingDataReader.readValidRecords(TestsData.TEST_FILE, pattern, pattern.asPredicate(),
        TestsData.PHONE_LOCAL, TestsData.DATE_FORMAT_1,
        TestsData.DATE_FORMAT_2);
  }

  Observable<Record> getParseResult() {
    Predicate<String> correctFormat = date ->
        Try.of(() -> getDate(date, TestsData.DATE_FORMAT_1)).isSuccess() || Try.of(
            () -> getDate(date, TestsData.DATE_FORMAT_2)).isSuccess();
    Function<String, DateTime> toCorrectFormat = date -> Try
        .of(() -> getDate(date, TestsData.DATE_FORMAT_1))
        .getOrElseTry(() -> getDate(date, TestsData.DATE_FORMAT_2));
    Iterator<Record> records = validRecords.iterator();
    String[] data = new String[(validRecords.size() << 1)];
    for (int i = 0; i < data.length; i += 2) {
      Record currentRecord = records.next();
      data[i] = currentRecord.getTelephone().getRawInput();
      data[i + 1] = currentRecord.getDate().toString(TestsData.DATE_FORMAT_1, LOCALE);
    }
    return ParsingDataReader
        .getValidRecords(TestsData.PHONE_LOCAL, data, correctFormat, toCorrectFormat);
  }
}