package ass.hamsatom.cvi5.hw5;

import org.testng.annotations.Test;

/**
 * @author Tomáš Hamsa on 25.03.2017.
 */
public class ParsingDataReaderTest extends ParsingDataReaderData {

  @Test
  public void testGetValidRecords() {
    getParseResult().test().assertComplete().assertNoErrors().assertValueSet(validRecords);
  }

  @Test
  public void testReadValidRecords() throws Exception {
    getReadResult().test().assertComplete().assertNoErrors().assertValueSet(validRecords);
  }

  @Test
  public void testSameResult() {
    getParseResult().test().assertComplete().assertNoErrors()
        .assertValueSequence(getParseResult().blockingIterable());
  }
}
