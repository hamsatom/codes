package ass.hamsatom.cvi5.hw5;

import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber;
import java.util.Objects;
import javax.annotation.Nonnull;
import org.joda.time.DateTime;

/**
 * Represents data of one person and also one line in CSV file.
 * Whenever new {@link Record} is created to {@link PhoneNumber} is assigned raw input that equals country code followed by national number.
 */
class Record {

  private final DateTime date;
  private final PhoneNumber telephone;

  Record(@Nonnull PhoneNumber telephone, @Nonnull DateTime date) {
    this.date = date;
    this.telephone = telephone;
    this.telephone.setRawInput(
        String.format("%d%d", telephone.getCountryCode(), telephone.getNationalNumber()));
  }

  @Nonnull
  final DateTime getDate() {
    return date;
  }

  @Nonnull
  final PhoneNumber getTelephone() {
    return telephone;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof Record)) {
      return false;
    }
    Record record = (Record) o;
    return Objects.equals(date, record.date) && Objects.equals(telephone, record.telephone);
  }

  @Override
  public int hashCode() {
    return Objects.hash(date, telephone);
  }
}
