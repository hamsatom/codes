package ass.hamsatom.cvi5.task2;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Locale;
import java.util.Optional;
import javaslang.control.Try;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public final class Task2Dates {

  public static final String DATE_TIME_PATTERN = "eee.mm.dd.yyyy.hh.mm.ss.z";

  public static void main(String[] args) throws IOException {
    DateTimeFormatter format = DateTimeFormat.forPattern(DATE_TIME_PATTERN).withLocale(Locale.US);

    Optional<DateTime> mostRecentDate = Files.lines(Paths.get(Task2GenerateDates.FILE_NAME))
        .map(line -> Try.of(() -> DateTime.parse(line, format)))
        .filter(Try::isSuccess)
        .map(Try::get)
        .max((dateA, dateB) -> dateA.isAfter(dateB) ? 1 : -1);

    mostRecentDate.ifPresent(value -> System.out.println(value.toString(format)));
  }

}
