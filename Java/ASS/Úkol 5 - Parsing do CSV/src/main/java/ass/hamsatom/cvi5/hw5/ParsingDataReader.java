package ass.hamsatom.cvi5.hw5;

import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber;
import io.reactivex.Observable;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.Locale;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import java.util.stream.Stream;
import javaslang.control.Try;
import javax.annotation.Nonnull;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 * Utility class for parsing valid Telephone number and Date from specified input.
 * Although simple class with hardcoded parse pattern would be much more simpler and shorter I think this solution is more complex and more universal.
 *
 * @author Tomáš Hamsa on 24.03.2017.
 */
public final class ParsingDataReader {

  /**
   * Warning about utility class
   */
  static final String UTILITY_CLASS = "This is a utility class with only static methods and shouldn't be instanced";

  /**
   * Constructor used if someone tries to create instance of this class.
   * As this is a utility class that should be happening.
   *
   * @throws IllegalAccessException if someone tries to create instance of this class thus calling this constructor
   */
  private ParsingDataReader() throws IllegalAccessException {
    throw new IllegalAccessException(UTILITY_CLASS);
  }

  /**
   * This method gets {@link Record} with valid {@link DateTime} and {@link PhoneNumber}.
   * Output of this method is the same as of method {@code getValidRecords} if used with same filtering of date.
   * <br>
   * Method tries to read File specified in {@code fileName}. File is filtered by {@code filteringPredicate} so only line matching the {@code
   * filteringPredicate} will be preserved. In each preserved line <b> only first </b> occurrence matching the {@code replacePattern} will erased.
   * Such filtered data will be parsed to {@link Record}. <br>
   * Each {@link Record} is validated so it contains only valid {@link DateTime} and {@link PhoneNumber}. If any of these two are invalid whole
   * {@link Record} record is discarded. <br>
   * Phone number is validated in regards to {@code countryCode} so phone numbers not valid in country specified by {@code countryCode} will be discarded.
   * <br>
   * Valid date must match at least one date format specified in {@code possibleDateFormat}. If data match more formats is parsed to {@link DateTime} using
   * random matching format.
   *
   * @param fileName Readable, existing file that contains data to be parsed.
   * @param replacePattern Pattern used to find matching Strings and erase the matching part.
   * @param filteringPredicate Predicated used to filter lines from File, only lines matching the predicate will be preserved
   * @param countryCode code of country codes belong to e.g. CZ. This code is used to distinguish validity of phone number
   * @param possibleDateFormat date formats tht are considered valid. Parsed date must match one of this format to be considered valid. When parsing the
   * date random matching format is used.
   * @return List of {@link Record} with valid {@link DateTime} and {@link PhoneNumber}
   * @throws IOException if File specified by {@code fileName} couldn't be read
   */
  public static Observable<Record> readValidRecords(@Nonnull String fileName,
      @Nonnull Pattern replacePattern,
      @Nonnull Predicate<String> filteringPredicate, @Nonnull String countryCode,
      @Nonnull String... possibleDateFormat) throws IOException {

    String[] filteredUnpairedData = filterLinesFromFile(fileName, replacePattern,
        filteringPredicate);
    DateTimeFormatter[] possibleFormats = initDateFormatter(possibleDateFormat);
    Predicate<String> correctDate = date -> Arrays.stream(possibleFormats)
        .anyMatch(format -> Try.of(() -> format.parseDateTime(date)).isSuccess());
    Function<String, DateTime> formatDate = date -> Arrays.stream(possibleFormats)
        .filter(format -> Try.of(() -> format.parseDateTime(date)).isSuccess())
        .findAny()
        .orElseThrow(
            () -> new IllegalArgumentException("Given date doesn't match any supported " +
                "format"))
        .parseDateTime(date);

    return getValidRecords(countryCode, filteredUnpairedData, correctDate, formatDate);
  }

  /**
   * Obtains valid records given in {@code filteredPairedData}. <br>
   * Method takes {@code String[] filteredPairedData} with input data. Data in the Array should be ordered like Number, Date, Number, Date,...
   * {@link Record} is build from pair Date, Number. Each pair equals one record. <br>
   * This methods discards {@link Record} with invalid phone number or date. Both number and date must be valid in order to preserve the Record.<br>
   * Date is verified using {@code Predicate<String> correctDate}. If date match the Predicate it's considered to be valid. Later is formatted to
   * {@link DateTime} using {@code Function<String, DateTime> formatDate}. <br>
   * Phone number is verified for specific country. The country is determined by {@code String countryCode}.
   *
   * @param countryCode describes country of origin for all phone numbers, e.g. CZ. Number is considered valid only if it's a valid number for specified
   * country.
   * @param filteredPairedData Array of data ordered like Number, Date, Number, Date,...
   * {@link Record} is build from pair Date, Number. Each pair equals one record.
   * @param correctDate Predicated used to verify Date. If date matches the predicate is considered to be valid.
   * @param formatDate Function used to parse {@link DateTime} from {@link String}. It will be used only on data that was previously verified using the
   * {@code Predicate<String> correctDate}
   * @return List of {@link Record} with valid both {@link DateTime} and {@link PhoneNumber}
   */
  public static Observable<Record> getValidRecords(@Nonnull String countryCode,
      String[] filteredPairedData,
      @Nonnull Predicate<String> correctDate, @Nonnull Function<String, DateTime> formatDate) {
    PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
    return Observable.fromArray(filteredPairedData)
        .buffer(2, 2)
        .filter(list -> phoneUtil
            .isPossibleNumber(StringUtils.deleteWhitespace(list.get(0)), countryCode))
        .filter(list -> correctDate.test(list.get(1)))
        .map(list -> new Record(
            phoneUtil.parse(StringUtils.deleteWhitespace(list.get(0)), countryCode),
            formatDate.apply(list.get(1))))
        .filter(record -> phoneUtil.isValidNumber(record.getTelephone()));
  }

  /**
   * Obtains only those lines from file that match given Predicate. First match of Pattern in line is erased.
   *
   * @param fileName Name of File from which will be read the data
   * @param replaceLabels Pattern used to find text that we want to replace. Each first occurrence of text matching the Pattern will ber replaced with empty
   * String.
   * @param filteringPredicate Lines not matching this Predicate will be discarded
   * @return Filtered data obtained from the file.
   * @throws IOException if it wasn't possible to read from the given file
   */
  private static String[] filterLinesFromFile(@Nonnull String fileName,
      @Nonnull Pattern replaceLabels,
      @Nonnull Predicate<String> filteringPredicate) throws IOException {
    File inputFile = getValidFile(fileName);
    try (Stream<String> linesStream = Files.lines(inputFile.toPath())) {
      return linesStream.filter(filteringPredicate)
          .map(line -> replaceLabels.matcher(line).replaceFirst("")).toArray(String[]::new);
    }
  }

  /**
   * Initializes DateFormatters from given patterns.
   *
   * @param possibleDateFormat formats of date
   * @return Array of initialized DateFormatters
   */
  @Nonnull
  private static DateTimeFormatter[] initDateFormatter(@Nonnull String[] possibleDateFormat) {
    DateTimeFormatter[] possibleFormats = new DateTimeFormatter[possibleDateFormat.length];
    for (int i = 0; i < possibleDateFormat.length; ++i) {
      possibleFormats[i] = DateTimeFormat.forPattern(possibleDateFormat[i]).withLocale(Locale.US);
    }
    return possibleFormats;
  }

  /**
   * Check if file specified by it's name exists and can be read.
   *
   * @param fileName name of file to be verified
   * @return valid File
   * @throws IOException if file doesn't exist, is a directory or can't be read
   */
  @Nonnull
  private static File getValidFile(@Nonnull String fileName) throws IOException {
    File possibleFile = new File(fileName);
    String errMsg = "File " + possibleFile.getName() + " in " + possibleFile.getAbsolutePath();
    if (!possibleFile.exists()) {
      throw new FileNotFoundException(errMsg + " doesn't exist");
    }
    if (!possibleFile.canRead()) {
      throw new IOException(errMsg + " can't be read");
    }
    if (!possibleFile.isFile()) {
      throw new IOException(errMsg + " is actually not a file");
    }
    return possibleFile;
  }
}
