package ass.hamsatom.cvi5.task1;

import java.util.Optional;

final class UserToAddressNice {

  static Optional<String> userToStreet(User user) {
    return Optional.ofNullable(user).map(User::getAddress).map(Address::getStreet);
  }
}
