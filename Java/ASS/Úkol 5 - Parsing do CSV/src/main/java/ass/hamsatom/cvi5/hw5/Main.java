package ass.hamsatom.cvi5.hw5;

import io.reactivex.Observable;
import java.io.IOException;
import java.util.regex.Pattern;

/**
 * @author Tomáš Hamsa on 24.03.2017.
 */
public final class Main {

  private static final String INPUT_DATA_FILE = "data.txt";
  private static final String CSV_OUTPUT_FILE = "result.csv";

  private static final String DATE_PREFIX = "Date: ";
  private static final String TEL_PREFIX = "Tel.: ";
  private static final String COUNTRY_CODE = "CZ";

  private static final String DATE_FORMAT_1 = "EEE, MM. dd. YYYY hh:mm:ss Z";
  private static final String DATE_FORMAT_2 = "MM. dd. YY hh:mm";


  public static void main(String[] args) throws IOException {
    Pattern headerPattern = Pattern.compile('(' + DATE_PREFIX + '|' + TEL_PREFIX + ')');

    Observable<Record> records = ParsingDataReader
        .readValidRecords(INPUT_DATA_FILE, headerPattern, headerPattern.asPredicate(), COUNTRY_CODE,
            DATE_FORMAT_1, DATE_FORMAT_2);
    records.sorted((recordA, recordB) -> recordA.getDate().isAfter(recordB.getDate()) ? 1 : -1);

    originalCSVWriter.writeRecordsToCSV(CSV_OUTPUT_FILE, records, "Telephone number", "ISO Date");
  }
}
