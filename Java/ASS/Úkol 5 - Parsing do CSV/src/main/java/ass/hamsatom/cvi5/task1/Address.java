package ass.hamsatom.cvi5.task1;

public class Address {

  private String street;

  public Address(String street) {
    this.street = street;
  }

  public final String getStreet() {
    return street;
  }

  public void setStreet(String street) {
    this.street = street;
  }

}
