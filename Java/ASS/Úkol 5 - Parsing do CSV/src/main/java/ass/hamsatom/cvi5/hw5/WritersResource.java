package ass.hamsatom.cvi5.hw5;

import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.Charset;
import java.util.function.Consumer;
import javax.annotation.Nonnull;
import org.supercsv.io.CsvListWriter;
import org.supercsv.prefs.CsvPreference;

/**
 * @author Tomáš Hamsa on 28.03.2017.
 */
final class WritersResource {

  private final CsvListWriter csvWriter;
  private final Writer writer;

  private WritersResource(@Nonnull String fileName, @Nonnull Charset charset,
      @Nonnull CsvPreference csvPreference) throws FileNotFoundException {
    writer = new OutputStreamWriter(new BufferedOutputStream(new FileOutputStream(fileName)),
        charset);
    csvWriter = new CsvListWriter(writer, csvPreference);
  }

  static void doWithResource(@Nonnull String fileName, @Nonnull Charset charset,
      @Nonnull CsvPreference csvPreference,
      @Nonnull Consumer<WritersResource> consumer) throws IOException {
    WritersResource writersResource = new WritersResource(fileName, charset, csvPreference);
    consumer.accept(writersResource);
    writersResource.endWriting();
  }

  private void endWriting() throws IOException {
    csvWriter.flush();
    writer.flush();
    csvWriter.close();
    writer.close();
  }

  void writeHeader(@Nonnull String... header) throws IOException {
    csvWriter.writeHeader(header);
  }

  void writeRow(@Nonnull Object... columnItem) throws IOException {
    csvWriter.write(columnItem);
  }
}
