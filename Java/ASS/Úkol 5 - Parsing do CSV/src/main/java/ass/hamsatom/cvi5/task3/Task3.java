package ass.hamsatom.cvi5.task3;

import java.util.List;
import java.util.function.BinaryOperator;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import javax.annotation.Nonnull;

public final class Task3 {


  private static final Predicate<Integer> isOdd = i -> i % 2 != 0;
  private static final BinaryOperator<Integer> add = (a, integer) -> a + integer;
  private static final BinaryOperator<Integer> mul = (a, binaryOperator) -> a * binaryOperator;

  public static void main(String[] args) {
    List<Integer> numbers = IntStream.range(0, 10).boxed().collect(Collectors.toList());
    System.out.println(genericAggregate(numbers, add, isOdd.negate()));
    System.out.println(genericAggregate(numbers, add, isOdd));
    System.out.println(genericAggregate(numbers, mul, isOdd.negate()));
  }

  private static int genericAggregate(@Nonnull List<Integer> nums,
      BinaryOperator<Integer> aggregation, Predicate<Integer> condition) {
    return nums.stream().filter(condition).reduce(aggregation).orElse(0);
  }
}
