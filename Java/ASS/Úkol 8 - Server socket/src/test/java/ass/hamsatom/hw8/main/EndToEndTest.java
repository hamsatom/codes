package ass.hamsatom.hw8.main;

import ass.hamsatom.hw8.server.ByteBufferUtil;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.Collections;
import java.util.Random;
import java.util.UUID;
import javax.annotation.Nonnull;
import lombok.Cleanup;
import org.testng.Assert;
import org.testng.annotations.Test;

public class EndToEndTest {

  @Test
  public void testToUpper() throws Exception {
    String serverResult = testServer("hi" + Main.REQUESTS_DELIMITER);
    Assert.assertEquals(serverResult, "HI" + Main.REQUESTS_DELIMITER);
  }

  @Test
  public void testToLower() throws Exception {
    String serverResult = testServer("HI!" + Main.REQUESTS_DELIMITER);
    Assert.assertEquals(serverResult, "hi!" + Main.REQUESTS_DELIMITER);
  }

  @Test
  public void testLongString() throws Exception {
    String longString =
        String.join("", Collections.nCopies(500000, UUID.randomUUID().toString()))
            + Main.REQUESTS_DELIMITER;
    String serverResult = testServer(longString);

    // Long string has guaranteed length of 18000001
    String expected = longString.toLowerCase();
    Assert.assertEquals(serverResult, expected);
  }

  private String testServer(@Nonnull String input) throws IOException, InterruptedException {
    int port = new Random().nextInt(2 * Short.MAX_VALUE);

    // run server
    new Thread(() -> {
      try {
        Main.main(new String[]{String.valueOf(port)});
      } catch (Exception e) {
        e.printStackTrace();
      }
    }).start();

    // wait for server to start
    Thread.sleep(1000);

    String result;
    @Cleanup Socket sock = new Socket("localhost", port);
    @Cleanup BufferedReader br = new BufferedReader(
        new InputStreamReader(new BufferedInputStream(sock.getInputStream())));
    sock.getOutputStream().write(input.getBytes(ByteBufferUtil.TELNET_CHARSET));
    result = br.readLine();

    // Append \n because BufferedReader.readLine() discards EOL
    return result + Main.REQUESTS_DELIMITER;
  }
}