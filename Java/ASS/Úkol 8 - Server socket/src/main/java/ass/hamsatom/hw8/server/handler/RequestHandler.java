package ass.hamsatom.hw8.server.handler;

import ass.hamsatom.hw8.server.request.Request;
import ass.hamsatom.hw8.server.response.Response;
import io.reactivex.ObservableTransformer;

/**
 * Provides handling of requests. That means that this class maps messages from requests to messages
 * in response
 */
public interface RequestHandler extends ObservableTransformer<Request, Response> {

}
