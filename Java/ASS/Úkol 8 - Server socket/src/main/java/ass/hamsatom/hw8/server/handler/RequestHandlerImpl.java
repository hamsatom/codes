package ass.hamsatom.hw8.server.handler;

import ass.hamsatom.hw8.server.request.Request;
import ass.hamsatom.hw8.server.response.Response;
import ass.hamsatom.hw8.server.response.ResponseImpl;
import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.schedulers.Schedulers;
import javax.annotation.Nonnull;

/**
 * Provides handling of requests. That means that this class maps messages from requests to messages
 * in response
 */
public class RequestHandlerImpl implements RequestHandler {

  /**
   * Maps all requests to responses with desired message
   *
   * @param requests Requests to be mapped on responses
   */
  @Override
  public ObservableSource<Response> apply(@Nonnull Observable<Request> requests) {
    return requests.flatMap(Observable::just).subscribeOn(Schedulers.io())
        .map(request -> new ResponseImpl(request.getClient(), transform(request.getRequestData())));
  }

  /**
   * Transforms strings with odd length to uppercase and string with even length to lower case
   *
   * @param requestString String to be transformed
   * @return transformed String
   */
  @Nonnull
  private String transform(@Nonnull String requestString) {
    return requestString.trim().length() % 2 == 0 ? requestString.toUpperCase()
        : requestString.toLowerCase();
  }
}
