package ass.hamsatom.hw8.server.response;

import java.nio.channels.SocketChannel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;

@AllArgsConstructor
public class ResponseImpl implements Response {

  @Getter
  @NonNull
  private final SocketChannel client;
  @Getter
  @NonNull
  private final String responseData;
}
