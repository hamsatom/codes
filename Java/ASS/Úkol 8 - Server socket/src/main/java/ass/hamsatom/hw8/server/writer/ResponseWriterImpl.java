package ass.hamsatom.hw8.server.writer;

import ass.hamsatom.hw8.server.ByteBufferUtil;
import ass.hamsatom.hw8.server.response.Response;
import io.reactivex.Observable;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Nonnull;

/**
 * Class that writes responses to client's {@link SocketChannel} in a non-blocking
 * way
 *
 * @author Tomáš Hamsa on 18.05.2017.
 */
public class ResponseWriterImpl implements ResponseWriter {

  private static final Logger LOG = Logger.getLogger(ResponseWriterImpl.class.getName());

  /**
   * Selector used for blocking in all clients's channels
   */
  private final Selector selector;

  /**
   * Queue with new new channels. Elements are inserted to this queue from another thread.
   */
  private final Queue<SocketChannel> queue;

  /**
   * Maps {@link SocketChannel} and data written into it
   */
  private final Map<SocketChannel, ByteBuffer> bufferMap;

  /**
   * Flag that is true if queue was stopped, false otherwise
   */
  private volatile boolean finished;

  /**
   * Starts the writing queue on another thread
   *
   * @throws IOException if opening the {@link Selector} failed
   */
  public ResponseWriterImpl() throws IOException {
    selector = Selector.open();
    queue = new ConcurrentLinkedQueue<>();
    bufferMap = new ConcurrentHashMap<>();
    new Thread(this::run).start();
  }

  /**
   * Writes data from all responses to their client's channel. Writing can be stopped using stop
   * method.
   */
  private void run() {
    LOG.info(() -> "Started writing thread .. ");

    while (!finished) {
      try {
        registerChannels();
        selector.select();

        Iterator<SelectionKey> iterator = selector.selectedKeys().iterator();
        Observable.<SelectionKey>create(emitter -> {
          while (iterator.hasNext()) {
            SelectionKey next = iterator.next();
            emitter.onNext(next);
            iterator.remove();    // this is important, as selector only adds but does
            // not remove
          }
          emitter.onComplete();
        }).filter(SelectionKey::isValid).filter(SelectionKey::isWritable).cache()
            .map(key -> (SocketChannel) key.channel()).subscribe(channel -> {
          ByteBuffer bufferToWrite = bufferMap.get(channel);
          channel.write(bufferToWrite);
          if (!bufferToWrite.hasRemaining()) {
            channel.close();
          }
          bufferMap.forEach((key, value) -> {
            if (!key.isConnected() || !key.isOpen()) {
              bufferMap.remove(key);
            }
          });
        });
      } catch (IOException e) {
        LOG.log(Level.SEVERE, "Error while writing response", e);
      }
    }
    closeChannels();
    LOG.info(() -> "Ended writing thread .. ");
  }

  /**
   * Closes all channels stored in collections
   */
  private void closeChannels() {
    Observable.fromIterable(queue).subscribe(SocketChannel::close);
    Observable.fromIterable(bufferMap.keySet()).subscribe(SocketChannel::close);
  }

  /**
   * Register channels from queue to local selector
   */
  private void registerChannels() {
    Observable.fromIterable(queue).forEach(socket -> {
      queue.poll();
      socket.register(selector, SelectionKey.OP_WRITE);
    });
  }

  /**
   * Adds response to queue. The response will be processed and it's data written to the client's
   * {@link SocketChannel}
   *
   * @param response Response that will be written to {@link SocketChannel}
   */
  @Override
  public void accept(@Nonnull Response response) {
    SocketChannel client = response.getClient();
    ByteBuffer responseMsg = ByteBufferUtil.TELNET_CHARSET.encode(response.getResponseData());
    bufferMap.put(client, responseMsg);
    queue.add(client);

    // this wakes up blocking selector.select() so that loop can register sockets added to queue
    selector.wakeup();
  }

  @Override
  public void stop() {
    finished = true;
  }
}