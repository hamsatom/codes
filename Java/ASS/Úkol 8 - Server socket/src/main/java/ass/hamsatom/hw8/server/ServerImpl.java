package ass.hamsatom.hw8.server;

import ass.hamsatom.hw8.server.handler.RequestHandler;
import ass.hamsatom.hw8.server.request.Request;
import ass.hamsatom.hw8.server.request.RequestImpl;
import ass.hamsatom.hw8.server.writer.ResponseWriter;
import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.Subject;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.Channel;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.logging.Logger;
import javax.annotation.Nonnull;
import lombok.Cleanup;

/**
 * Server that process requests and maps the to appropriate response
 */
public class ServerImpl implements Server {

  private static final Logger LOG = Logger.getLogger(ServerImpl.class.getName());

  /**
   * Size of byte buffer to which tha request text is buffered. Should be big enough to contain 1MB
   * message
   */
  private static final int LIMIT = 1024 * 1024;

  /**
   * Address that server will bind to
   */
  private final SocketAddress address;
  /**
   * Received requests
   */
  private final Subject<Request> requests;
  /**
   * Flag that represents if server should stop. False if server should keep running, true if server
   * should stop. Default value of {@code boolean} in Java is {@code false}.
   */
  private volatile boolean exit;

  /**
   * Creates server that process requests and maps the to appropriate response
   *
   * @param address Address on which the server will open it's socket channel
   */
  public ServerImpl(@Nonnull SocketAddress address) {
    this.address = address;
    requests = PublishSubject.create();
  }

  @Override
  public void stop() {
    exit = true;
  }

  @Override
  public void run(@Nonnull RequestHandler handler, @Nonnull ResponseWriter writer,
      @Nonnull String delimiter) throws IOException {
    // No need to check if String contains only whitespaces as whitespace can terminate request but empty String can't.
    // No need to check for null either because parameter is annotated Nonnull and passing null will thrown NullPointerException
    if (Objects.equals(delimiter, "")) {
      throw new IllegalArgumentException("Empty delimiter");
    }

    // open, bind and configure ServerSocketChannel
    @Cleanup ServerSocketChannel serverChannel = ServerSocketChannel.open();
    serverChannel.bind(address);
    serverChannel.configureBlocking(false);

    LOG.info(() -> "Server started on address: " + address);

    // open selector and register serverSocketChannel to it
    Selector selector = Selector.open();
    serverChannel.register(selector, serverChannel.validOps());

    ByteBuffer buffer = ByteBuffer.allocate(LIMIT);
    Map<Channel, ByteArrayOutputStream> messages = new HashMap<>();
    requests.compose(handler).subscribe(writer);

    // start non-blocking loop
    while (!exit) {
      selector.select();
      Iterator<SelectionKey> iterator = selector.selectedKeys().iterator();
      Observable.<SelectionKey>create(emitter -> {
        while (iterator.hasNext()) {
          SelectionKey next = iterator.next();
          emitter.onNext(next);
          iterator.remove();    // this is important, as selector only adds but does not remove
        }
        emitter.onComplete();
      }).filter(SelectionKey::isValid).cache()
          .doOnNext(key -> LOG.info(() -> "Processing key: " + key)).groupBy(key -> {
        if (key.isReadable()) {
          return KeyStatus.READABLE;
        } else if (key.isAcceptable()) {
          return KeyStatus.ACCEPTABLE;
        } else {
          return KeyStatus.INVALID;
        }
      }).subscribe(labeledRequest -> {
        switch (labeledRequest.getKey()) {
          case ACCEPTABLE:
            // Opens the client channel
            labeledRequest.filter(SelectionKey::isAcceptable)
                .map(key -> (ServerSocketChannel) key.channel())
                .map(ServerSocketChannel::accept)
                .subscribe(channel -> {
                  LOG.info(() -> "Registered channel: " + channel);
                  channel.configureBlocking(false);
                  channel.register(selector, SelectionKey.OP_READ);
                });
            break;
          case READABLE:
            // Reads text from client channel
            labeledRequest.map(key -> (SocketChannel) key.channel()).subscribe(channel -> {
              LOG.info(() -> "Creating request from channel: " + channel);
              String message = ByteBufferUtil.readFromChannel(buffer, channel);
              @Cleanup ByteArrayOutputStream outStream = messages
                  .getOrDefault(channel, new ByteArrayOutputStream(message.length()));
              outStream.write(message.getBytes());
              messages.putIfAbsent(channel, outStream);
              // If all text from request was read add the request to received requests
              if (message.endsWith(delimiter)) {
                messages.remove(channel);
                requests.onNext(new RequestImpl(channel,
                    outStream.toString(ByteBufferUtil.TELNET_CHARSET.name())));
              }
            });
            break;
          default:
            break;
        }
      });
    }
    writer.stop();
  }

  /**
   * Represents status of channel that the key represents
   */
  private enum KeyStatus {
    /**
     * It's possible to read from channel
     */
    READABLE,
    /**
     * Channel from client that can be accepted
     */
    ACCEPTABLE,

    /**
     * Represents key that is not vlaid
     */
    INVALID
  }
}
