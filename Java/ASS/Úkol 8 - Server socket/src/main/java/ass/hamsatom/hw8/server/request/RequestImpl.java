package ass.hamsatom.hw8.server.request;

import java.nio.channels.SocketChannel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;

@AllArgsConstructor
public class RequestImpl implements Request {

  @Getter
  @NonNull
  private final SocketChannel client;

  @Getter
  @NonNull
  private final String requestData;
}
