package ass.hamsatom.hw8.main;

import ass.hamsatom.hw8.server.Server;
import ass.hamsatom.hw8.server.ServerImpl;
import ass.hamsatom.hw8.server.handler.RequestHandlerImpl;
import ass.hamsatom.hw8.server.writer.ResponseWriterImpl;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Nonnull;

/**
 * Start the server
 */
public class Main {

  /**
   * Symbol that represents end of request
   */
  public static final String REQUESTS_DELIMITER = "\n";
  private static final Logger LOG = Logger.getLogger(Main.class.getName());
  /**
   * Default host for server
   */
  private static final String HOST = "localhost";
  /**
   * Default port for server
   */
  private static final int DEFAULT_PORT = 8080;
  /**
   * The smallest port that is out of range
   */
  private static final int HIGHEST_VALID_PORT = 65535;

  /**
   * Port cannot be negative
   */
  private static final byte LOWEST_VALID_PORT = 0;

  /**
   * Starts the server with default configuration
   */
  public static void main(@Nonnull String[] args) throws IOException {
    Server server = new ServerImpl(new InetSocketAddress(HOST, getPort(args)));
    server.run(new RequestHandlerImpl(), new ResponseWriterImpl(), REQUESTS_DELIMITER);
  }

  /**
   * Parses port provided in argument for application. Port should be the first argument.
   *
   * @param args Arguments with which the application was started
   * @return provided port or default port if no valid port was found
   */
  private static int getPort(@Nonnull String[] args) {
    if (args.length < 1) {
      LOG.warning(() ->
          "No port provided, first application argument should contain port. Using default port "
              + DEFAULT_PORT);
      return DEFAULT_PORT;
    }

    int providedPort;
    try {
      providedPort = Integer.parseInt(args[0]);
    } catch (NumberFormatException | NullPointerException e) {
      LOG.log(Level.WARNING,
          "Provided port is not valid, expected int found " + args[0] + ". Using default port "
              + DEFAULT_PORT, e);
      return DEFAULT_PORT;
    }

    if (providedPort <= HIGHEST_VALID_PORT && providedPort >= LOWEST_VALID_PORT) {
      LOG.info(() -> "Using port " + providedPort);
      return providedPort;
    } else {
      LOG.warning(
          () -> "Provided port " + providedPort + " exceeds valid range. " + "Using default port "
              + DEFAULT_PORT);
      return DEFAULT_PORT;
    }
  }
}
