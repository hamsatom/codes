package ass.hamsatom.hw8.server.response;

import java.nio.channels.SocketChannel;
import javax.annotation.Nonnull;

/**
 * Represents a response to {@link ass.hamsatom.hw8.server.request.Request}
 */
public interface Response {

  /**
   * Provides channel from which the request arrived
   *
   * @return channel on which the response should be written
   */
  @Nonnull
  SocketChannel getClient();

  /**
   * Provides data
   *
   * @return transformed String
   */
  @Nonnull
  String getResponseData();
}
