package ass.hamsatom.hw8.server.writer;

import ass.hamsatom.hw8.server.response.Response;
import io.reactivex.functions.Consumer;

/**
 * Class that writes responses to client's {@link java.nio.channels.SocketChannel} in a non-blocking
 * way
 *
 * @author Tomáš Hamsa on 18.05.2017.
 */
public interface ResponseWriter extends Consumer<Response> {

  /**
   * Stops the writing. That means that responses added after this call will not be written
   */
  void stop();

}
