package ass.hamsatom.hw8.server.request;

import java.nio.channels.SocketChannel;
import javax.annotation.Nonnull;

/**
 * Represents request received in server
 */
public interface Request {

  /**
   * Provides channel from which the request arrived
   *
   * @return channel of origin
   */
  @Nonnull
  SocketChannel getClient();

  /**
   * Provides the contain of request
   *
   * @return Text present in request
   */
  @Nonnull
  String getRequestData();
}
