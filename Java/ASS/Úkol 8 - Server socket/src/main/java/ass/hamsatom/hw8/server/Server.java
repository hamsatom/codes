package ass.hamsatom.hw8.server;

import ass.hamsatom.hw8.server.handler.RequestHandler;
import ass.hamsatom.hw8.server.writer.ResponseWriter;
import java.io.IOException;
import javax.annotation.Nonnull;

/**
 * Server that process requests and maps the to appropriate response
 */
public interface Server {

  /**
   * Runs server with given handler
   *
   * @param handler - ObservableTransformer that processes requests
   * @param writer - Consumer that consumes request and writes it to client (close client connection
   * after response is written!)
   * @param requestTerminator - token that terminates client request
   */
  void run(@Nonnull RequestHandler handler, @Nonnull ResponseWriter writer,
      @Nonnull String requestTerminator) throws IOException;

  /**
   * Stops the server and all components used in it like Response writter
   */
  void stop();
}
