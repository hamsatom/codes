package ass.hamsatom.cvi3.stock.trader;

import ass.hamsatom.cvi3.stock.Stock;
import ass.hamsatom.cvi3.stock.service.SlowStockService;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * @author Tomáš Hamsa on 11.03.2017.
 */
public abstract class TraderTest {

  protected IStockTrader trader;
  private List<Stock> tenStocks;

  @Test
  public void testShouldFindCorrectMinimum() {
    String suggestion = trader
        .suggestStockToBuy(new SlowStockService(tenStocks.iterator(), 10L, 30L, 0f));
    Assert.assertEquals(suggestion, "A0");
  }


  @Test(timeOut = 2000)
  public void testShouldFindCorrectMinimumFast() {
    String suggestion = trader
        .suggestStockToBuy(new SlowStockService(tenStocks.iterator(), 500L, 600L, 0f));
    Assert.assertEquals(suggestion, "A0");
  }


  @Test(timeOut = 2000)
  public void testShouldSkipFailed() {
    trader.suggestStockToBuy(new SlowStockService(tenStocks.iterator(), 10L, 30L, .3f));
  }


  @BeforeClass
  public void setup() {
    tenStocks = IntStream.range(0, 10)
        .boxed()
        .map(i -> new Stock("A" + i, i * 100 + 50))
        .collect(Collectors.toList());

    Collections.shuffle(tenStocks);
  }
}
