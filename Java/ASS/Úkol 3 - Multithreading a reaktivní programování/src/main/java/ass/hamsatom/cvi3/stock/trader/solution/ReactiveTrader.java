package ass.hamsatom.cvi3.stock.trader.solution;

import ass.hamsatom.cvi3.stock.Stock;
import ass.hamsatom.cvi3.stock.service.IStockService;
import ass.hamsatom.cvi3.stock.trader.IStockTrader;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;
import java.util.Optional;
import javax.annotation.Nonnull;

/**
 * {@link IStockTrader} that uses {@code RxJava} to obtain <b>10</b> results from
 * {@link Stock}
 *
 * @author Tomáš Hamsa on 10.03.2017.
 */
public class ReactiveTrader implements IStockTrader {

  private static final int TASK_COUNT = 10;

  @Override
  @Nonnull
  public String suggestStockToBuy(@Nonnull IStockService stockService) {
    return Observable.range(0, TASK_COUNT)
        .flatMap(position -> Observable.just(position)
            .subscribeOn(Schedulers.io())
            .map(index -> stockService.next())
            .onErrorReturn(error -> Optional.empty()))
        .filter(Optional::isPresent)
        .map(Optional::get)
        .reduce((stockA, stockB) -> stockA.getValue() < stockB.getValue() ? stockA : stockB)
        .blockingGet()
        .getCode();
  }
}
