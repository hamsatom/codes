package ass.hamsatom.cvi3.stock.trader;

import ass.hamsatom.cvi3.stock.service.IStockService;

public interface IStockTrader {

  /**
   * Analyzes stocks returned by stockService and suggests stock code to buy
   */
  String suggestStockToBuy(IStockService stockService);

}
