package ass.hamsatom.cvi3.stock.service;

import ass.hamsatom.cvi3.stock.Stock;
import java.util.Iterator;
import java.util.Optional;

public interface IStockService extends Iterable<Optional<Stock>>, Iterator<Optional<Stock>> {

  @Override
  default Iterator<Optional<Stock>> iterator() {
    return this;
  }

}
