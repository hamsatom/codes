package ass.hamsatom.cvi3.stock.trader.solution;

import ass.hamsatom.cvi3.stock.Stock;
import ass.hamsatom.cvi3.stock.service.IStockService;
import ass.hamsatom.cvi3.stock.trader.IStockTrader;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import javaslang.control.Try;
import javax.annotation.Nonnull;

/**
 * {@link IStockTrader} that uses {@link ExecutorServiceexect} to obtain <b>10</b> results from
 * {@link Stock}
 */
public class ExecutorTrader implements IStockTrader {

  private static final int LIMIT = 10;

  @Override
  @Nonnull
  public String suggestStockToBuy(@Nonnull IStockService stockService) {
    ExecutorService executors = Executors.newFixedThreadPool(LIMIT);
    Deque<Future<Optional<Stock>>> executorResult = new ArrayDeque<>(LIMIT);

    for (int i = 0; i < LIMIT; ++i) {
      executorResult.addLast(executors.submit(stockService::next));
    }

    executors.shutdown();
    return executorResult.stream()
        .map(future -> Try.of(future::get))
        .filter(Try::isSuccess)
        .map(Try::get)
        .filter(Optional::isPresent)
        .map(Optional::get)
        .min((stockA, stockB) -> stockA.getValue() - stockB.getValue() < 0 ? -1 : 1)
        .orElse(new Stock("", Float.POSITIVE_INFINITY))
        .getCode();
  }
}
