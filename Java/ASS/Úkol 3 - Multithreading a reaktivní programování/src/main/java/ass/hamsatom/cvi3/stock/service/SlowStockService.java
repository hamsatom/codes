package ass.hamsatom.cvi3.stock.service;

import ass.hamsatom.cvi3.stock.Stock;
import ass.hamsatom.cvi3.util.ThreadsUtil;
import java.util.Iterator;
import java.util.Optional;
import org.apache.commons.lang3.Validate;

public class SlowStockService implements IStockService {

  protected FailureGenerator failGenerator;

  protected Long minDelay;
  protected Long maxDelay;

  protected Iterator<Stock> stocksIter;

  public SlowStockService(Iterator<Stock> stocks, Long minDelay, Long maxDelay,
      float failureProbability) {
    Validate.isTrue(minDelay <= maxDelay);
    failGenerator = new FailureGenerator(failureProbability);
    this.minDelay = minDelay;
    this.maxDelay = maxDelay;
    stocksIter = stocks;
  }

  @Override
  public Optional<Stock> next() {
    ThreadsUtil.randomDelay(minDelay, maxDelay);
    failGenerator.maybeFail();
    return stocksIter.hasNext() ? Optional.of(stocksIter.next()) : Optional.empty();
  }

  @Override
  public boolean hasNext() {
    return stocksIter.hasNext();
  }

}
