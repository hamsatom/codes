import java.util.Objects;
import java.util.Random;
import java.util.stream.Stream;

public class Main {

  public static void main(String[] args) {
    Heap<Integer> heap = new Heap<>();
    Random random = new Random();
    int limit = 200;
    for (int i = 0; i < limit; i++) {
      heap.add(random.nextInt(100));
      heap.add(random.nextInt(100));
      heap.takeTop();
      heap.add(random.nextInt(100));
      heap.takeLast();
    }
    int prevTop = Integer.MIN_VALUE;
    for (int i = limit; i >= 0; --i) {
      System.out.println(i + " "  + heap.size);
      assert i == heap.size;
      int curTop = heap.takeTop();
      System.out.println(curTop + " " + prevTop);
      assert curTop >= prevTop;
      prevTop = curTop;
    }
  }

  private static class Heap<E extends Comparable<E>> {

    private E root;
    private Heap<E> left;
    private Heap<E> right;
    private int size;

    public Heap() {
      size = 0;
    }

    public Heap(E root) {
      this.root = root;
      size = 1;
    }

    public E takeTop() {
      E result = root;
      root = takeLast();
      if (size != 0) {
        heapify();
      }
      return result;
    }

    public E takeLast() {
      if (size == 0) {
        return null;
      }
      E result;
      if (right != null) {
        result = right.takeLast();
        if (right.root == null) {
          right = null;
        }
      } else if (left != null) {
        result = left.takeLast();
        if (left.root == null) {
          left = null;
        }
      } else {
        result = root;
        root = null;
      }
      --size;
      return result;
    }

    public void add(E element) {
      if (root == null) {
        root = element;
      } else if (left == null) {
        left = new Heap<>(element);
      } else if (right == null) {
        right = new Heap<>(element);
      } else {
        if (left.size <= right.size) {
          left.add(element);
        } else {
          right.add(element);
        }
      }
      ++size;
      heapify();
    }

    private void heapify() {
      if (right != null && right.root.compareTo(left.root) < 0 && right.root.compareTo(root) < 0) {
        E tmp = right.root;
        right.root = root;
        root = tmp;
        right.heapify();
      } else if (left != null && left.root.compareTo(root) < 0) {
        E tmp = left.root;
        left.root = root;
        root = tmp;
        left.heapify();
      }
    }


    @Override
    public String toString() {
      StringBuilder strings = new StringBuilder(size);
      strings.append(root).append(System.lineSeparator()).append("Size: ").append(size)
          .append(System.lineSeparator());
      Stream.of(left, right).filter(Objects::nonNull).forEach(strings::append);
      return strings.toString();
    }
  }
}
