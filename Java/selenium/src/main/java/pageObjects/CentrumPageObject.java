package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * @author Tomáš Hamsa on 20.04.2017.
 */
class CentrumPageObject {
    private final WebDriver driver;
    private final String baseUrl;
    
    CentrumPageObject(WebDriver driver, String baseUrl) {
        this.driver = driver;
        this.baseUrl = baseUrl;
    }
    
    final void login(final String username, final String password) {
        driver.get(baseUrl);
        driver.findElement(By.id("multimailLogin")).clear();
        driver.findElement(By.id("multimailLogin")).sendKeys(username);
        driver.findElement(By.id("multimailPassword")).clear();
        driver.findElement(By.id("multimailPassword")).sendKeys(password);
        driver.findElement(By.name("prihlasit")).click();
    }
    
    final String getLoginMessage() {
        return driver.findElement(By.cssSelector("strong")).getText();
    }
    
    final String searchCVUT() {
        search("ČVUT FEL");
        return driver.findElement(By.cssSelector("div.entry-wrap > h3 > a > em")).getText();
    }
    
    final String getSearchResult() {
        return driver.findElement(By.cssSelector("h1")).getText();
    }
    
    final void search(final String keyword) {
        driver.get(baseUrl);
        driver.findElement(By.id("searchInternetField")).clear();
        driver.findElement(By.id("searchInternetField")).sendKeys(keyword);
        driver.findElement(By.cssSelector("input.tlacitko")).click();
    }
    
    final String getHoroscope(final String animal) {
        driver.get(baseUrl);
        driver.findElement(By.cssSelector("#horoscopDetail-" + animal.toLowerCase() + "> a > span.nazev")).click();
        return driver.findElement(By.cssSelector("h1[title=\"" + animal + " - denní horoskop - AstroCafe\"]")).getText();
    }
}
