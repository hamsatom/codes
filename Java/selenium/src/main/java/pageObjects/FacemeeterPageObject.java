package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

class FacemeeterPageObject {
    private final String baseUrl;
    private final WebDriver driver;
    
    FacemeeterPageObject(String baseUrl, WebDriver driver) {
        this.baseUrl = baseUrl;
        this.driver = driver;
    }
    
    final void editBirthDate(final String date) {
        driver.get(baseUrl + "/components/users/feed/edit_item.php?id=102");
        driver.findElement(By.name("data")).sendKeys(date);
        driver.findElement(By.xpath("//button[@type='submit']")).click();
        
    }
    
    final String getDateOfBirth() {
        driver.get(baseUrl + "/components/users/feed/edit_item.php?id=102");
        return driver.findElement(By.name("data")).getText();
    }
    
    final String findUser(final String user){
        driver.get(baseUrl);
        final WebElement searchBar = driver.findElement(By.name("search"));
        searchBar.clear();
        searchBar.sendKeys(user);
        searchBar.sendKeys(Keys.ENTER);
        return driver.findElement(By.cssSelector("div.additions")).getText();
    }
    
    void login(final String userName, final String password) {
        driver.get(baseUrl + "/components/multibar/?do=login&url=/");
        driver.findElement(By.name("password")).clear();
        driver.findElement(By.name("password")).sendKeys(password);
        driver.findElement(By.name("email")).clear();
        driver.findElement(By.name("email")).sendKeys(userName);
        driver.findElement(By.cssSelector("button.form-control")).click();
    }
    
    final String getTitle() {
        return driver.getTitle();
    }
    
    int addOneLike(){
        driver.get(baseUrl + "/post/437");
        final String countText = driver.findElement(By.cssSelector("span.favorites.number")).getText();
        final int oldCount = Integer.parseInt(countText);
        driver.get(baseUrl + "/components/newsfeed/like_change.php?type=0&post_id=437");
        driver.get(baseUrl + "/post/437");
        final String newCountText = driver.findElement(By.cssSelector("span.favorites.number")).getText();
        driver.get(baseUrl + "/components/newsfeed/like_change.php?type=0&post_id=437");
        return Integer.parseInt(newCountText) - oldCount;
    }
    
    final void logout() {
        driver.get(baseUrl + "/logout");
    }
}
