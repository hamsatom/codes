package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class CentrumTest {
    private static final String BASE_URL = "https://www.centrum.cz/";
    private static final String DRIVER_LOCATION = "D:\\Users\\hamsatom\\Desktop\\chromedriver.exe";
    private CentrumPageObject webpage;
    
    @BeforeClass(alwaysRun = true)
    public void setUp() throws Exception {
        System.setProperty("webdriver.chrome.driver", DRIVER_LOCATION);
        final WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        
        webpage = new CentrumPageObject(driver, BASE_URL);
    }
    
    @Test
    public void testInvalidLogin() throws Exception {
        webpage.login("nonExistingUser", "notValidPassword");
        Assert.assertEquals(webpage.getLoginMessage(), "Je nám líto, heslo nebo email nejsou správně.",
                            "We managed to get logged in eventhough the data " + "are incorrect");
    }
    
    @Test
    public void testSearchCVUT() throws Exception {
        final String firstResult = webpage.searchCVUT();
        Assert.assertEquals(firstResult, "ČVUT FEL", "The first result is not čvut");
    }
    
    @Test
    public void testSearchNonsence() throws Exception {
        final String nonsenceKeyword = "dfg5d6f4gd5f61bd65bd65f1b";
        webpage.search(nonsenceKeyword);
        final String expected = "Pro vyhledávací dotaz „" + nonsenceKeyword + "“ - nebyl nalezen žádný výsledek.";
        Assert.assertEquals(webpage.getSearchResult(), expected, "We got some result eventhough the keyword shouldn raise any");
    }
    
    @Test
    public void testHoroscopeBeran() throws Exception {
        final String animal = "Beran";
        final String actualResult = webpage.getHoroscope(animal);
        final String expectedResult = animal + " - denní horoskop";
        Assert.assertEquals(actualResult, expectedResult, "Horoscope name is not as expected");
    }
    
    @Test
    public void testHoroscopeScoprio() throws Exception {
        final String animal = "Štír";
        final String actualResult = webpage.getHoroscope("Stir");
        final String expectedResult = animal + " - denní horoskop";
        Assert.assertEquals(actualResult, expectedResult, "Horoscope name is not as expected");
    }
}
