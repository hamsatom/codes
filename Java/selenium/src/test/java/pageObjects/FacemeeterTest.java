package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

/**
 * @author Tomáš Hamsa on 20.04.2017.
 */
public class FacemeeterTest {
    private static final String DRIVER_LOCATION = "D:\\Users\\hamsatom\\Desktop\\chromedriver.exe";
    private static final String BASE_URL = "https://facemeeter.sajdl.com";
    private FacemeeterPageObject webpage;
    
    @BeforeClass(alwaysRun = true)
    public void setClass() throws Exception {
        System.setProperty("webdriver.chrome.driver", DRIVER_LOCATION);
        final WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
        webpage = new FacemeeterPageObject(BASE_URL, driver);
    }
    
    @Test(priority = 1)
    public void testIncorrectLogin() throws Exception {
        webpage.login("notRegisteredMail@gmail.com", "notAPAssword");
        Assert.assertEquals(webpage.getTitle(), "Objevte události poblíž! - Facemeeter", "We got logged in eventhough we used incorrect data");
    }
    
    @Test(priority = 2)
    public void testCorrectLogin() throws Exception {
        loginCorrectly();
        Assert.assertEquals(webpage.getTitle(), "Facemeeter", "We didn't get logged in eventhough we used correct data");
    }
    
    @Test(priority = 3)
    public void testAddStatus() throws Exception {
        loginCorrectly();
        final String newDate = "";
        webpage.editBirthDate(newDate);
        Assert.assertEquals(webpage.getDateOfBirth(), newDate);
    }
    
    private void loginCorrectly() {
        webpage.login("tur3@facemeeter.com", "VelmiSilneHeslo123");
    }
    
    @Test(priority = 4)
    public void testSearchNonExistingUser() throws Exception {
        loginCorrectly();
        final String result = webpage.findUser("nonExistingUser");
        Assert.assertEquals(result, "", "User was found evethough it doesn't exist");
    }
    
    @Test(priority = 5)
    public void testAddLike() throws Exception {
        loginCorrectly();
        final int likesDifference = webpage.addOneLike();
        Assert.assertEquals(likesDifference, 1, "Not exactly one like was added");
    }
    
    @AfterMethod
    public void tearDown() throws Exception {
        webpage.logout();
    }
}
