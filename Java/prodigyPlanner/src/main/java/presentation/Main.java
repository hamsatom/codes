package presentation;

import data.DataUtils;
import data.Trip;
import io.reactivex.Observable;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import javax.annotation.Nonnull;
import java.io.IOException;

public class Main extends Application {
    private static final String ICON_NAME = "prodigy_icon.jpg";
    
    public static void main(String[] args) {
        launch(args);
    }
    
    @Override
    public void start(@Nonnull final Stage primaryStage) throws Exception {
        primaryStage.setOnCloseRequest(e -> {
            Platform.exit();
            System.exit(0);
        });
        final Parent root = FXMLLoader.load(getClass().getResource("/mainWindow.fxml"));
        primaryStage.setTitle("prodigyPlanner");
        primaryStage.getIcons().add(new Image(getClass().getClassLoader().getResourceAsStream(ICON_NAME)));
        primaryStage.setScene(new Scene(root, 530, 530));
        primaryStage.show();
        
        //createTrips();
    }
    
    private void createTrips() throws IOException {
        final Trip trip = new Trip().setDaysAtDestination((short) 3)
                                    .setDestinationAccommodationCost((short) 1210)
                                    .setFlightCost((short) 5082)
                                    .setLocation("TORRE DEL MAR, SPAIN")
                                    .setDate("5. července")
                                    .setPeopleAmount((short) 4)
                                    .setTicketPrice((short) 551)
                                    .setTravelAccommodationCost((short) 552)
                                    .setTravelDistanceKM((short) 2670)
                                    .setTravelTimeHours((short) 23)
                                    .setTravelExtraExpanse((short) 2410);
        
        final Trip trip2 = new Trip().setLocation("BARCELONA, SPAIN")
                                     .setDate("8. července")
                                     .setDaysAtDestination((short) 3)
                                     .setDestinationAccommodationCost((short) 588)
                                     .setFlightCost((short) 5437)
                                     .setFlightDuration((short) 21)
                                     .setPeopleAmount((short) 4)
                                     .setTicketPrice((short) 1928)
                                     .setTravelAccommodationCost((short) 690)
                                     .setTravelDistanceKM((short) 1710)
                                     .setTravelTimeHours((short) 15)
                                     .setTravelExtraExpanse((short) 2410);
        
        final Trip trip3 = new Trip().setLocation("TØNSBERG, NORWAY")
                                     .setDate("12. července")
                                     .setDaysAtDestination((short) 2)
                                     .setDestinationAccommodationCost((short) 1904)
                                     .setFlightCost((short) 4397)
                                     .setFlightDuration((short) 2)
                                     .setPeopleAmount((short) 4)
                                     .setTicketPrice((short) 2816)
                                     .setTravelAccommodationCost((short) 872)
                                     .setTravelDistanceKM((short) 1682)
                                     .setTravelTimeHours((short) 18)
                                     .setTravelExtraExpanse((short) 0);
        
        final Trip trip4 = new Trip().setLocation("DEBRECEN, HUNGARY")
                                     .setDate("19. července")
                                     .setDaysAtDestination((short) 2)
                                     .setDestinationAccommodationCost((short) 268)
                                     .setFlightCost((short) 9523)
                                     .setFlightDuration((short) 13)
                                     .setPeopleAmount((short) 4)
                                     .setTicketPrice((short) 1629)
                                     .setTravelAccommodationCost((short) 0)
                                     .setTravelDistanceKM((short) 743)
                                     .setTravelTimeHours((short) 11)
                                     .setTravelExtraExpanse((short) 0);
        
        final Trip trip5 = new Trip().setLocation("BILBAO, SPAIN")
                                     .setDate("27. září")
                                     .setDaysAtDestination((short) 3)
                                     .setDestinationAccommodationCost((short) 331)
                                     .setFlightCost((short) 4273)
                                     .setFlightDuration((short) 18)
                                     .setPeopleAmount((short) 4)
                                     .setTicketPrice((short) 1205)
                                     .setTravelAccommodationCost((short) 796)
                                     .setTravelDistanceKM((short) 1891)
                                     .setTravelTimeHours((short) 23)
                                     .setTravelExtraExpanse((short) 0);
        
        final Trip trip6 = new Trip().setLocation("ZURICH, SWITZERLAND")
                                     .setDate("26. srpna")
                                     .setDaysAtDestination((short) 2)
                                     .setDestinationAccommodationCost((short) 554)
                                     .setFlightCost((short) 3495)
                                     .setFlightDuration((short) 3)
                                     .setPeopleAmount((short) 4)
                                     .setTicketPrice((short) 2339)
                                     .setTravelAccommodationCost((short) 0)
                                     .setTravelDistanceKM((short) 679)
                                     .setTravelTimeHours((short) 8)
                                     .setTravelExtraExpanse((short) 0);
        
        final Trip trip7 = new Trip().setLocation("WROCLAW, POLAND")
                                     .setDate("19. srpna")
                                     .setDaysAtDestination((short) 2)
                                     .setDestinationAccommodationCost((short) 228)
                                     .setFlightCost((short) 3184)
                                     .setFlightDuration((short) 2)
                                     .setPeopleAmount((short) 4)
                                     .setTicketPrice((short) 1899)
                                     .setTravelAccommodationCost((short) 0)
                                     .setTravelDistanceKM((short) 354)
                                     .setTravelTimeHours((short) 4)
                                     .setTravelExtraExpanse((short) 0);
        
        DataUtils.writeTrips(Observable.just(trip, trip2, trip3, trip4, trip5, trip6, trip7));
    }
}
