package presentation;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextArea;
import logic.Logic;

import java.net.URL;
import java.util.ResourceBundle;

public class Controller implements Initializable {
    @FXML private TextArea textField;
    
    @Override
    public void initialize(URL fxmlFileLocation, ResourceBundle resources) {
        try {
            new Logic().getPricedTrips().subscribe(text -> textField.appendText(text + System.lineSeparator()));
        } catch (Throwable e) {
            textField.appendText("Error while loading trips from file" + System.lineSeparator());
            textField.appendText(e.toString());
        }
    }
}
