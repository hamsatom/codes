package logic;


import data.DataUtils;
import data.Trip;
import io.reactivex.Observable;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.Objects;

/**
 * @author Tomáš Hamsa on 06.05.2017.
 */
public class Logic {
    private static final short PETROL_PRICE = 32;
    private static final short ACCOMMODATION_PER_HOURS = 12;
    private static final float CAR_CONSUMMATION_PER_1_KM = 0.09f;
    private static final short FOOD_DAILY = 100;
    
    @Nonnull
    private static Observable<ValuedTrip> evaluate(@Nonnull final Trip trip) {
        final double airplaneCost = getDestinationCost(trip) + trip.getFlightCost() + trip.getFlightDuration() / 24 * FOOD_DAILY;
        final double airplaneTime = trip.getFlightDuration() / 24.0 + trip.getDaysAtDestination();
        final ValuedTrip airplaneTrip = new ValuedTrip(airplaneCost,
                                                       String.format("%s %s via airplane: CZK %.2f days %.1f", trip.getLocation(), trip.getDate(), airplaneCost,
                                                                     airplaneTime));
        final double carCost = calculateCarCost(trip);
        final double carTime = 2.0 * trip.getTravelTimeHours() / ACCOMMODATION_PER_HOURS + trip.getDaysAtDestination();
        final ValuedTrip roadTrip = new ValuedTrip(carCost,
                                                   String.format("%s %s via car: CZK %.2f days %.1f", trip.getLocation(), trip.getDate(), carCost, carTime));
        return Observable.just(airplaneTrip, roadTrip);
    }
    
    private static double calculateCarCost(@Nonnull final Trip trip) {
        final double petrolCost = 2.0 * trip.getTravelDistanceKM() * CAR_CONSUMMATION_PER_1_KM * PETROL_PRICE / trip.getPeopleAmount();
        final double roadAccommodationCost = 2.0 * trip.getTravelTimeHours() / ACCOMMODATION_PER_HOURS * trip.getTravelAccommodationCost();
        final double foodCost = 2.0 * trip.getTravelTimeHours() / 24 * FOOD_DAILY;
        return petrolCost + roadAccommodationCost + foodCost + getDestinationCost(trip) + trip.getTravelExtraExpanse() / trip.getPeopleAmount();
    }
    
    private static double getDestinationCost(@Nonnull Trip trip) {
        return trip.getDaysAtDestination() * (trip.getDestinationAccommodationCost() + FOOD_DAILY) + (double) trip.getTicketPrice();
    }
    
    @Nonnull
    public Observable<String> getPricedTrips() throws IOException {
        return DataUtils.loadTrips().flatMap(Logic::evaluate).sorted().map(ValuedTrip::getDescription);
    }
    
    private static class ValuedTrip implements Comparable<ValuedTrip> {
        private final double price;
        private final String description;
        
        private ValuedTrip(double price, @Nonnull final String description) {
            this.price = price;
            this.description = description;
        }
        
        @Nonnull
        private String getDescription() {
            return description;
        }
        
        @Override
        public int compareTo(@Nonnull final ValuedTrip o) {
            return price > o.price ? 1 : -1;
        }
        
        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (!(o instanceof ValuedTrip)) {
                return false;
            }
            ValuedTrip that = (ValuedTrip) o;
            return Double.compare(that.price, price) == 0 && Objects.equals(description, that.description);
        }
        
        @Override
        public int hashCode() {
            return Objects.hash(price, description);
        }
    }
}
