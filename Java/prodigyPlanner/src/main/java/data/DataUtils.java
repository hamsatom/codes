package data;

import io.reactivex.Observable;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.introspector.BeanAccess;

import javax.annotation.Nonnull;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

/**
 * @author Tomáš Hamsa on 06.05.2017.
 */
public class DataUtils {
    private static final String FILE = "tripsData.txt";
    private static final Charset CODING = StandardCharsets.UTF_8;
    
    @Nonnull
    public static Observable<Trip> loadTrips() throws IOException {
        return Observable.just(Files.newInputStream(Paths.get(FILE), StandardOpenOption.READ)).map(inStream -> {
            final Yaml yaml = new Yaml();
            yaml.setBeanAccess(BeanAccess.FIELD);
            return yaml.loadAll(inStream);
        }).flatMap(Observable::fromIterable).map(object -> (Trip) object);
    }
    
    public static void writeTrips(@Nonnull final Observable<Trip> trips) throws IOException {
        try (final BufferedWriter writer = Files.newBufferedWriter(Paths.get(FILE), CODING, StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.CREATE)) {
            final Yaml yaml = new Yaml();
            yaml.setBeanAccess(BeanAccess.FIELD);
            yaml.dumpAll(trips.blockingIterable().iterator(), writer);
        }
    }
}
