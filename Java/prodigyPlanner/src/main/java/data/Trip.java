package data;

import javax.annotation.Nonnull;
import java.io.Serializable;

/**
 * @author Tomáš Hamsa on 05.05.2017.
 */
public class Trip implements Serializable {
    private static final long serialVersionUID = 1147063235707225133L;
    private String location;
    private short travelTimeHours;
    private short ticketPrice;
    private short travelAccommodationCost;
    private short destinationAccommodationCost;
    private short travelDistanceKM;
    private short daysAtDestination;
    private short peopleAmount;
    private short travelExtraExpanse;
    private short flightCost;
    private short flightDuration;
    private String date;
    
    public String getDate() {
        return date;
    }
    
    public Trip setDate(@Nonnull final String date) {
        this.date = date;
        return this;
    }
    
    public short getFlightDuration() {
        return flightDuration;
    }
    
    public Trip setFlightDuration(short flightDuration) {
        this.flightDuration = flightDuration;
        return this;
    }
    
    public short getFlightCost() {
        return flightCost;
    }
    
    public Trip setFlightCost(short flightCost) {
        this.flightCost = flightCost;
        return this;
    }
    
    public short getTravelExtraExpanse() {
        return travelExtraExpanse;
    }
    
    public Trip setTravelExtraExpanse(short travelExtraExpanse) {
        this.travelExtraExpanse = travelExtraExpanse;
        return this;
    }
    
    public short getTravelTimeHours() {
        return travelTimeHours;
    }
    
    public Trip setTravelTimeHours(short travelTimeHours) {
        this.travelTimeHours = travelTimeHours;
        return this;
    }
    
    public String getLocation() {
        return location;
    }
    
    public Trip setLocation(@Nonnull final String location) {
        this.location = location;
        return this;
    }
    
    public short getTicketPrice() {
        return ticketPrice;
    }
    
    public Trip setTicketPrice(short ticketPrice) {
        this.ticketPrice = ticketPrice;
        return this;
    }
    
    public short getTravelAccommodationCost() {
        return travelAccommodationCost;
    }
    
    public Trip setTravelAccommodationCost(short travelAccommodationCost) {
        this.travelAccommodationCost = travelAccommodationCost;
        return this;
    }
    
    public short getDestinationAccommodationCost() {
        return destinationAccommodationCost;
    }
    
    public Trip setDestinationAccommodationCost(short destinationAccommodationCost) {
        this.destinationAccommodationCost = destinationAccommodationCost;
        return this;
    }
    
    public short getTravelDistanceKM() {
        return travelDistanceKM;
    }
    
    public Trip setTravelDistanceKM(short travelDistanceKM) {
        this.travelDistanceKM = travelDistanceKM;
        return this;
    }
    
    public short getDaysAtDestination() {
        return daysAtDestination;
    }
    
    public Trip setDaysAtDestination(short daysAtDestination) {
        this.daysAtDestination = daysAtDestination;
        return this;
    }
    
    public short getPeopleAmount() {
        return peopleAmount;
    }
    
    public Trip setPeopleAmount(short peopleAmount) {
        this.peopleAmount = peopleAmount;
        return this;
    }
}
