import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.stream.Stream;
import kos.request.ApiBase;
import logic.DataFactory;
import logic.ReportPrinter;
import logic.StudyHistory;
import model.Programme;
import model.Student;
import model.SubjectGroup;

public class OiStatisticsApp {

  private static final String STUDENTS_FILE_PATH = "D:\\Tomas\\ownCloud\\OI statistiky\\students.txt";
  private static final Charset CHARSET = StandardCharsets.UTF_8;
  private static final String DATA_DELIMITER = "\t";

  private static final String[] SEMESTER_CODES = new String[]{"B091", "B092", "B101", "B102",
      "B111", "B112", "B121", "B122", "B131", "B132", "B141", "B142", "B151", "B152", "B161",
      "B162", "B171"};

  private static final String REPORT_NAME = "OI statistics - " + LocalDateTime.now()
      .format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH-mm-ss-SSS")) + ".csv";

  private static final short CREDITS_LIMIT = 180;
  private static final byte BACHELOR_THESIS_CREDITS = 20;
  private static final byte SEMESTER_MAX_CREDITS = 40;

  private static final ApiBase KOS_API_URL = ApiBase.FEL;

  public static void main(String[] args) throws IOException {
    hackSystemEncoding();

    Collection<Student> students = DataFactory
        .createStudents(STUDENTS_FILE_PATH, DATA_DELIMITER, CHARSET);

    Stream<SubjectGroup> mandatorySubjectGroups = DataFactory.createMandatorySubjectGroups();

    Programme oiBachelor2015 = new Programme(CREDITS_LIMIT, BACHELOR_THESIS_CREDITS,
        SEMESTER_MAX_CREDITS, mandatorySubjectGroups);

    StudyHistory studyHistory = new StudyHistory(oiBachelor2015, students, KOS_API_URL);
    studyHistory.iterateThroughSemesters(SEMESTER_CODES);

    new ReportPrinter(REPORT_NAME, CHARSET).print(students, studyHistory.getSubjectsStatistics());
  }

  private static void hackSystemEncoding() {
    try {
      System.setProperty("file.encoding", "UTF-8");
      Field charset = Charset.class.getDeclaredField("defaultCharset");
      charset.setAccessible(true);
      charset.set(null, null);
    } catch (Throwable t) {
      t.addSuppressed(t);
    }
  }


}
