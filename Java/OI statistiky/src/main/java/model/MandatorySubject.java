package model;

public class MandatorySubject extends Subject {

  public MandatorySubject(String name, String code, long credits) {
    super(name, code, credits);
  }

  @Override
  public boolean isMandatory() {
    return true;
  }

  @Override
  public boolean isVoluntary() {
    return false;
  }
}
