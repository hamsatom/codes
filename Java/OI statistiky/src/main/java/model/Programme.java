package model;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.annotation.Nonnull;

public class Programme {

  private final Collection<? extends SubjectGroup> mandatorySubjectGroups;
  private final long creditsLimit;
  private final long thesisCredits;
  private final long maxSemesterCredits;

  public Programme(long creditsLimit, long thesisCredits, long maxSemesterCredits,
      @Nonnull Stream<? extends SubjectGroup> mandatorySubjectGroups) {
    this.mandatorySubjectGroups = mandatorySubjectGroups.collect(Collectors.toList());
    this.creditsLimit = creditsLimit;
    this.thesisCredits = thesisCredits;
    this.maxSemesterCredits = maxSemesterCredits;
  }

  public long getCreditsLimit() {
    return creditsLimit;
  }

  public long getThesisCredits() {
    return thesisCredits;
  }

  public long getMaxSemesterCredits() {
    return maxSemesterCredits;
  }

  public void setStudyState(@Nonnull final Student student) {
    final List<SubjectGroup> subjectGroups = mandatorySubjectGroups.stream()
        .map(subjectGroup -> subjectGroup.unfinishedCopy(subjectGroup))
        .collect(Collectors.toList());

    student.getFinishedMandatorySubjects()
        .forEach(subject -> subjectGroups.stream()
            .filter(subjectGroup -> subjectGroup.contains(subject))
            .forEach(subjectGroup -> subjectGroup.assignCompletion(subject)));

    boolean allMandatoryFinished = subjectGroups.stream()
        .allMatch(SubjectGroup::isFinished);

    if (!allMandatoryFinished) {
      student.setStudyState(StudyState.NEDOKONCEN_POVINNY_PREDMET);
      return;
    }

    long creditsGained = student.getCreditsGainedTotal();
    if (creditsGained + thesisCredits >= creditsLimit) {
      student.setStudyState(StudyState.OK);
    } else if (creditsGained + maxSemesterCredits >= creditsLimit) {
      student.setStudyState(StudyState.CHYBI_PAR_KREDITU);
    } else if (creditsGained + maxSemesterCredits < creditsLimit) {
      student.setStudyState(StudyState.NEDOSTATEK_KREDITU);
    }
  }

  public boolean isMandatory(final String courseCode) {
    return mandatorySubjectGroups.stream()
        .anyMatch(subjectGroup -> subjectGroup.contains(courseCode));
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof Programme)) {
      return false;
    }
    Programme programme = (Programme) o;
    return creditsLimit == programme.creditsLimit && thesisCredits == programme.thesisCredits
        && maxSemesterCredits == programme.maxSemesterCredits && Objects
        .equals(mandatorySubjectGroups, programme.mandatorySubjectGroups);
  }

  @Override
  public int hashCode() {
    return Objects.hash(mandatorySubjectGroups, creditsLimit, thesisCredits, maxSemesterCredits);
  }
}
