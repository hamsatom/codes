package model;

public enum StudyState {
  NEDOKONCEN_POVINNY_PREDMET, OK, NEDOSTATEK_KREDITU, CHYBI_PAR_KREDITU
}
