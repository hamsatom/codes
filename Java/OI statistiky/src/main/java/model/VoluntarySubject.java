package model;

public class VoluntarySubject extends Subject {

  public VoluntarySubject(String name, String code, long credits) {
    super(name, code, credits);
  }

  @Override
  public boolean isMandatory() {
    return false;
  }

  @Override
  public boolean isVoluntary() {
    return true;
  }
}
