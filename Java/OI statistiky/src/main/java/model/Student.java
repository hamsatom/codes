package model;

import java.util.Collection;
import java.util.Objects;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import javax.annotation.Nonnull;
import org.apache.commons.lang3.StringUtils;

public class Student {

  private final String name;
  private final String code;
  private final Collection<Subject> subjects;
  private long mandatoryCreditsTotal;
  private long voluntaryCreditsTotal;
  private long mandatoryCreditsGained;
  private long voluntaryCreditsGained;
  private long mandatorySubjectsFinished;
  private long voluntarySubjectsFinished;
  private long mandatorySubjectsTotal;
  private long voluntarySubjectsTotal;
  private long mandatorySubjectsAssessments;
  private long voluntarySubjectsAssessments;
  private StudyState studyState;
  private String studyPlan;

  public Student(@Nonnull final String name, @Nonnull final String code) {
    this.name = name;
    this.code = code;
    subjects = new ConcurrentLinkedQueue<>();
    studyPlan = "";
  }

  public String getName() {
    return name;
  }

  public String getCode() {
    return code;
  }

  public StudyState getStudyState() {
    return studyState;
  }

  public Student setStudyState(StudyState studyState) {
    this.studyState = studyState;
    return this;
  }

  public String getStudyPlan() {
    return studyPlan;
  }

  public Student setStudyPlan(String studyPlan) {
    if (StringUtils.isNotBlank(studyPlan)) {
      this.studyPlan = studyPlan;
    }
    return this;
  }

  public void addSubject(@Nonnull final Subject subject) {
    subjects.add(subject);
    if (subject.isMandatory()) {
      mandatoryCreditsTotal += subject.getCredits();
      ++mandatorySubjectsTotal;
    } else if (subject.isVoluntary()) {
      voluntaryCreditsTotal += subject.getCredits();
      ++voluntarySubjectsTotal;
    }
  }

  public void finishSubject(@Nonnull final Subject subject) {
    updateSubject(subject, listSubject -> listSubject.setFinished(true));

    if (subject.isMandatory()) {
      mandatoryCreditsGained += subject.getCredits();
      ++mandatorySubjectsFinished;
    } else if (subject.isVoluntary()) {
      voluntaryCreditsGained += subject.getCredits();
      ++voluntarySubjectsFinished;
    }
  }

  public void assessSubject(@Nonnull final Subject subject) {
    updateSubject(subject, listSubject -> listSubject.setAssessed(true));

    if (subject.isMandatory()) {
      ++mandatorySubjectsAssessments;
    } else if (subject.isVoluntary()) {
      ++voluntarySubjectsAssessments;
    }
  }

  private void updateSubject(@Nonnull final Subject subject, @Nonnull Consumer<Subject> action) {
    if (!subjects.contains(subject)) {
      throw new IllegalArgumentException(subject + " is not in subjects list");
    }

    subjects.stream()
        .filter(listSubject -> Objects.equals(listSubject, subject))
        .findAny()
        .ifPresent(action);
  }

  public long getCreditsGainedTotal() {
    return mandatoryCreditsGained + voluntaryCreditsGained;
  }

  public long getSubjectsFinishedTotal() {
    return mandatorySubjectsFinished + voluntarySubjectsFinished;
  }

  public long getMandatoryCreditsTotal() {
    return mandatoryCreditsTotal;
  }

  public long getVoluntaryCreditsTotal() {
    return voluntaryCreditsTotal;
  }

  public long getMandatoryCreditsGained() {
    return mandatoryCreditsGained;
  }

  public long getVoluntaryCreditsGained() {
    return voluntaryCreditsGained;
  }

  public long getMandatorySubjectsTotal() {
    return mandatorySubjectsTotal;
  }

  public long getVoluntarySubjectsTotal() {
    return voluntarySubjectsTotal;
  }

  public long getMandatorySubjectsAssessments() {
    return mandatorySubjectsAssessments;
  }

  public long getVoluntarySubjectsAssessments() {
    return voluntarySubjectsAssessments;
  }

  public long getMandatorySubjectsFinished() {
    return mandatorySubjectsFinished;
  }

  public long getVoluntarySubjectsFinished() {
    return voluntarySubjectsFinished;
  }

  public Collection<Subject> getAssessedMandatorySubjects() {
    return filterSubjects(subjects, subject -> subject.isAssessed() && subject.isMandatory());
  }

  public Collection<Subject> getAssessedVoluntarySubjects() {
    return filterSubjects(subjects, subject -> subject.isAssessed() && subject.isVoluntary());
  }

  public Collection<Subject> getFailedMandatorySubjects() {
    return filterSubjects(subjects, subject -> !subject.isFinished() && subject.isMandatory());
  }

  public Collection<Subject> getFailedVoluntarySubjects() {
    return filterSubjects(subjects, subject -> !subject.isFinished() && subject.isVoluntary());
  }

  public Collection<Subject> getFinishedMandatorySubjects() {
    return filterSubjects(subjects, subject -> subject.isFinished() && subject.isMandatory());
  }

  public Collection<Subject> getFinishedVoluntarySubjects() {
    return filterSubjects(subjects, subject -> subject.isFinished() && subject.isVoluntary());
  }

  private <T extends Subject> Collection<T> filterSubjects(
      @Nonnull final Collection<? extends T> subjects, @Nonnull final Predicate<? super T> filter) {
    return subjects.stream()
        .filter(filter)
        .collect(Collectors.toList());
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof Student)) {
      return false;
    }
    Student student = (Student) o;
    return Objects.equals(name, student.name) && Objects.equals(code, student.code);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, code);
  }

  @Override
  public String toString() {
    return name + ":" + code;
  }
}
