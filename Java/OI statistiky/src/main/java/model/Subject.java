package model;

import java.util.Objects;
import javax.annotation.Nonnull;

/**
 * @author Tomáš Hamsa on 11.07.2017.
 */
public abstract class Subject {

  private final String name;
  private final String code;
  private final long credits;
  private boolean finished;
  private boolean assessed;

  public Subject(@Nonnull final String name, @Nonnull final String code, final long credits) {
    this.name = name;
    this.code = code;
    this.credits = credits;
  }

  public long getCredits() {
    return credits;
  }

  public String getName() {
    return name;
  }

  public String getCode() {
    return code;
  }

  public boolean isFinished() {
    return finished;
  }

  public Subject setFinished(boolean finished) {
    this.finished = finished;
    return this;
  }

  public boolean isAssessed() {
    return assessed;
  }

  public Subject setAssessed(boolean assessed) {
    this.assessed = assessed;
    return this;
  }

  public String getNameShortcut() {
    if (code.length() < 2) {
      return name;
    }
    int shortcutStart = code.length() - 2;
    while (shortcutStart - 1 >= 0 && !Character.isDigit(code.charAt(shortcutStart - 1))) {
      --shortcutStart;
    }
    return code.substring(shortcutStart, code.length());
  }


  public abstract boolean isMandatory();

  public abstract boolean isVoluntary();

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof Subject)) {
      return false;
    }
    Subject subject = (Subject) o;
    return credits == subject.credits && Objects.equals(name, subject.name) && Objects
        .equals(code, subject.code);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, code, credits);
  }

  @Override
  public String toString() {
    return code + ":" + name;
  }
}
