package model;

import java.util.Arrays;
import java.util.Collection;
import java.util.Objects;
import javax.annotation.Nonnull;

public class SubjectGroup {

  private final Collection<? extends String> subjectCodes;
  private boolean isFinished;

  public SubjectGroup(@Nonnull final Collection<? extends String> subjects) {
    this.subjectCodes = subjects;
  }

  public SubjectGroup(@Nonnull final String... subjects) {
    this.subjectCodes = Arrays.asList(subjects);
  }

  public SubjectGroup unfinishedCopy(@Nonnull final SubjectGroup subjectGroup) {
    return new SubjectGroup(subjectGroup.subjectCodes);
  }

  public boolean contains(@Nonnull final Subject subject) {
    return contains(subject.getCode());
  }

  public boolean contains(final String subjectCode) {
    return subjectCodes.stream()
        .anyMatch(o -> Objects.equals(o, subjectCode));
  }

  public void assignCompletion(@Nonnull final Subject subject) {
    isFinished |= subject.isFinished();
  }

  public boolean isFinished() {
    return isFinished;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof SubjectGroup)) {
      return false;
    }
    SubjectGroup that = (SubjectGroup) o;
    return isFinished == that.isFinished && Objects.equals(subjectCodes, that.subjectCodes);
  }

  @Override
  public int hashCode() {
    return Objects.hash(subjectCodes, isFinished);
  }
}
