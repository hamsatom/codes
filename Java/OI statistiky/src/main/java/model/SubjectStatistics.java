package model;

import java.util.Objects;

public class SubjectStatistics {

  private long timesEnrolled;
  private long timesFinished;
  private long timesAssessed;

  public long getTimesEnrolled() {
    return timesEnrolled;
  }

  public long getTimesFinished() {
    return timesFinished;
  }

  public long getTimesAssessed() {
    return timesAssessed;
  }

  public void increaseEnrolled() {
    ++timesEnrolled;
  }

  public void increaseFinished() {
    ++timesFinished;
  }

  public void increaseAssessed() {
    ++timesAssessed;
  }

  public double countPassRate() {
    return (double) timesFinished / timesEnrolled * 100;
  }

  public double countAssessmentRate() {
    return (double) timesAssessed / timesEnrolled * 100;
  }

  public double countAssessmentPassRate() {
    return timesAssessed != 0 ? (double) timesFinished / timesAssessed * 100 : 100;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof SubjectStatistics)) {
      return false;
    }
    SubjectStatistics that = (SubjectStatistics) o;
    return timesEnrolled == that.timesEnrolled && timesFinished == that.timesFinished
        && timesAssessed == that.timesAssessed;
  }

  @Override
  public int hashCode() {
    return Objects.hash(timesEnrolled, timesFinished, timesAssessed);
  }
}
