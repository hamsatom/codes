package logic;

import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Nonnull;
import kos.dto.InternalCourseEnrollment;
import kos.request.ApiBase;
import kos.request.Kos;
import model.MandatorySubject;
import model.Programme;
import model.Student;
import model.Subject;
import model.SubjectStatistics;
import model.VoluntarySubject;

public class StudyHistory {

  private static final Logger LOG = Logger.getLogger(StudyHistory.class.getName());

  private final Programme programme;
  private final Collection<? extends Student> students;
  private final ApiBase kosApiUrl;
  private final Map<Subject, SubjectStatistics> subjectsStatistics;

  public StudyHistory(@Nonnull final Programme programme,
      @Nonnull final Collection<? extends Student> students, @Nonnull final ApiBase kosApiUrl) {
    this.programme = programme;
    this.students = students;
    this.kosApiUrl = kosApiUrl;
    subjectsStatistics = new ConcurrentHashMap<>();
  }

  public void iterateThroughSemesters(@Nonnull final String... semesterCodes) {
    Arrays.stream(semesterCodes)
        .forEachOrdered(semester -> students.parallelStream()
            .forEach(student -> {
              processSubjects(semester, student);
              addStudyPlanTostudent(semester, student);
            }));

    students.forEach(programme::setStudyState);
  }

  private void processSubjects(@Nonnull final String semesterCode, @Nonnull final Student student) {
    try {
      Kos.getStudentCourses(kosApiUrl, semesterCode, student.getCode())
          .parallel()
          .forEach(course -> addCreditsToSubject(course, semesterCode, student));
    } catch (Throwable e) {
      LOG.log(Level.SEVERE,
          "Error while downloading subjects for " + student.getCode() + " in " + semesterCode, e);
    }
  }

  private void addStudyPlanTostudent(@Nonnull String semester, @Nonnull Student student) {
    try {
      student.setStudyPlan(Kos.getStudyPlan(kosApiUrl, semester, student.getCode()));
    } catch (Throwable e) {
      LOG.log(Level.INFO,
          "Error while getting study plan for " + student.getCode() + " in " + semester, e);
    }
  }

  private void addCreditsToSubject(@Nonnull final InternalCourseEnrollment course,
      @Nonnull final String semesterCode, @Nonnull final Student student) {
    final String courseCode = course.getCourseCode();
    if (courseCode == null) {
      return;
    }
    try {
      final long credits = Kos.getCourseCredits(kosApiUrl, semesterCode, courseCode);
      Subject subject;
      if (programme.isMandatory(courseCode)) {
        subject = new MandatorySubject(course.getName(), courseCode, credits);
      } else {
        subject = new VoluntarySubject(course.getName(), courseCode, credits);
      }

      student.addSubject(subject);
      subjectsStatistics.putIfAbsent(subject, new SubjectStatistics());
      subjectsStatistics.get(subject).increaseEnrolled();

      if (course.isAssessment()) {
        student.assessSubject(subject);
        subjectsStatistics.get(subject).increaseAssessed();
      }
      if (course.isCompleted()) {
        student.finishSubject(subject);
        subjectsStatistics.get(subject).increaseFinished();
      }
    } catch (Throwable e) {
      LOG.log(Level.WARNING,
          "Error while getting credits for course " + course.getName() + " and student " + student
              .getCode() + " in " + semesterCode, e);
    }
  }

  public Map<Subject, SubjectStatistics> getSubjectsStatistics() {
    return subjectsStatistics;
  }
}
