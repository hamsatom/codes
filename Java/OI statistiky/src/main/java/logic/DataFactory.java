package logic;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.StringTokenizer;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.Stream.Builder;
import javax.annotation.Nonnull;
import model.Student;
import model.SubjectGroup;

public final class DataFactory {

  private DataFactory() throws IllegalAccessException {
    throw new IllegalAccessException("Initializing static factory class");
  }

  public static Stream<SubjectGroup> createMandatorySubjectGroups() {
    Builder<SubjectGroup> streamBuilder = Stream.builder();
    streamBuilder.accept(new SubjectGroup("A4B01DMA", "B4B01DMA"));
    streamBuilder.accept(
        new SubjectGroup("A0B01LAG", "A8B01LAG", "AE0B01LAG", "AD0B01LAG", "B0B01LAG",
            "BD5B01LAG"));
    streamBuilder.accept(new SubjectGroup("A4B99RPH", "B4B33RPH"));
    streamBuilder
        .accept(new SubjectGroup("A0B36PR1", "B0B36PRP", "AE0B36PR1", "AD0B36PR1", "BD0B36PRP"));
    streamBuilder.accept(new SubjectGroup("A0B01LGR", "B0B01LGR", "AE0B01LGR", "AD0B01LGR"));
    streamBuilder.accept(new SubjectGroup("A4B01MA2", "B0B01MA1"));
    streamBuilder.accept(new SubjectGroup("A4B33ALG", "B4B33ALG"));
    streamBuilder
        .accept(new SubjectGroup("A0B36PR2", "B0B36PJV", "AE0B36PR2", "AD0B36PR2", "BD0B36PJV"));
    streamBuilder.accept(new SubjectGroup("A4B01JAG", "B4B01JAG", "AE4B01JAG"));
    streamBuilder
        .accept(new SubjectGroup("A0B01PSI", "B0B01PST", "A8B01PST", "AD0B01PSI", "AE0B01PSI"));
    streamBuilder.accept(new SubjectGroup("A0B35SPS", "AD0B35SPS"));
    streamBuilder.accept(new SubjectGroup("AE4B02FYZ", "A4B02FYZ", "AD4B02FYZ"));
    streamBuilder.accept(new SubjectGroup("A0B36APO", "AE0B36APO", "B4B35APO", "BE5B35APO"));
    streamBuilder.accept(new SubjectGroup("A4B33OPT", "B0B33OPT"));
    streamBuilder.accept(new SubjectGroup("A0B04B2Z", "A4B04AZK"));
    return streamBuilder.build();
  }

  public static Collection<Student> createStudents(@Nonnull String subjectsFilePath,
      @Nonnull String dataDelimiter, @Nonnull Charset charset) throws IOException {
    final Path filePath = Paths.get(subjectsFilePath).normalize().toAbsolutePath();
    try (Stream<String> lines = Files.lines(filePath, charset)) {
      return lines.map(line -> new StringTokenizer(line, dataDelimiter))
          .map(tokenizer -> {
            if (tokenizer.countTokens() != 2) {
              throw new IllegalArgumentException(
                  "Cannot create student for entry: " + tokenizer.nextToken());
            }
            return new Student(tokenizer.nextToken(), tokenizer.nextToken());
          })
          .collect(Collectors.toCollection(ConcurrentLinkedQueue::new));
    }
  }
}
