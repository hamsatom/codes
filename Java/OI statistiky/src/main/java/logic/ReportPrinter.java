package logic;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.Writer;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.stream.Collectors;
import javax.annotation.Nonnull;
import model.Student;
import model.Subject;
import model.SubjectStatistics;

public class ReportPrinter {

  private static final String STUDENT_HEADER = "Jméno;Login;Studijní plán;Přístup ke státnicím;Celkově kreditů;Předmětů dokončeno;Celkově kreditů z povinných předmětů;Získáno kreditů z povinných předmětů;Celkově povinných předmětů;Dokončeno povinných předmětů;Získáno zápočtů z povinných předmětů;Nedokončené povinné předměty;Celkově kreditů z nepovinných předmětů;Získáno kreditů z nepovinných předmětů;Celkově nepovinných předmětů;Dokončeno nepovinných předmětůt;Získáno zápočtů z nepovinných předmětů;Nedokončené nepovinné předměty";
  private static final String SUBJECTS_HEADER = "Zkratka;Název;Kód;Kreditů;Průchodnost;Šance zápočtu;Dokončení po zápočtu;Zapsání;Zápočtů;Dokončení";
  private static final String UTF8_BOM = "\uFEFF";
  private final Path reportFile;
  private final Charset charset;


  public ReportPrinter(@Nonnull String reportName, @Nonnull Charset charset) {
    this.reportFile = Paths.get(reportName).normalize().toAbsolutePath();
    this.charset = charset;
  }

  public void print(@Nonnull Collection<? extends Student> students,
      @Nonnull Map<? extends Subject, ? extends SubjectStatistics> subjectsStatistics)
      throws IOException {

    try (BufferedWriter writer = Files
        .newBufferedWriter(reportFile, charset, StandardOpenOption.CREATE,
            StandardOpenOption.TRUNCATE_EXISTING)) {
      writer.write(UTF8_BOM);

      appendLine(writer, STUDENT_HEADER);

      for (Student student : students) {
        appendLine(writer, student.getName(), student.getCode(), student.getStudyPlan(),
            student.getStudyState(), student.getCreditsGainedTotal(),
            student.getSubjectsFinishedTotal(), student.getMandatoryCreditsTotal(),
            student.getMandatoryCreditsGained(), student.getMandatorySubjectsTotal(),
            student.getMandatorySubjectsFinished(), student.getMandatorySubjectsAssessments(),
            toStr(student.getFailedMandatorySubjects()), student.getVoluntaryCreditsTotal(),
            student.getVoluntaryCreditsGained(), student.getVoluntarySubjectsTotal(),
            student.getVoluntarySubjectsFinished(), student.getVoluntarySubjectsAssessments(),
            toStr(student.getFailedVoluntarySubjects()));
      }

      appendLine(writer);
      appendLine(writer);

      appendLine(writer, "Povinné předměty:");
      List<? extends Entry<? extends Subject, ? extends SubjectStatistics>> mandatorySubjects = subjectsStatistics
          .entrySet()
          .stream()
          .filter(pair -> pair.getKey().isMandatory())
          .sorted((a, b) -> a.getKey().getNameShortcut()
              .compareToIgnoreCase(b.getKey().getNameShortcut()))
          .collect(Collectors.toList());
      printSubjects(writer, mandatorySubjects);
      appendLine(writer);

      appendLine(writer, "Nepovinné předměty:");
      List<? extends Entry<? extends Subject, ? extends SubjectStatistics>> voluntarySubjects = subjectsStatistics
          .entrySet()
          .stream()
          .filter(pair -> pair.getKey().isVoluntary())
          .sorted(Comparator.comparingLong(pair -> -pair.getValue().getTimesEnrolled()))
          .collect(Collectors.toList());
      printSubjects(writer, voluntarySubjects);
    }
  }

  private void printSubjects(@Nonnull Writer writer,
      @Nonnull Collection<? extends Entry<? extends Subject, ? extends SubjectStatistics>> subjects)
      throws IOException {
    appendLine(writer, SUBJECTS_HEADER);
    for (Entry<? extends Subject, ? extends SubjectStatistics> entry : subjects) {
      Subject subject = entry.getKey();
      SubjectStatistics statistic = entry.getValue();
      appendLine(writer, subject.getNameShortcut(), subject.getName(), subject.getCode(),
          subject.getCredits(),
          statistic.countPassRate() + " %", statistic.countAssessmentRate() + " %",
          statistic.countAssessmentPassRate() + " %", statistic.getTimesEnrolled(),
          statistic.getTimesAssessed(), statistic.getTimesFinished());
    }
  }

  private String toStr(@Nonnull Collection<?> collection) {
    return collection.stream()
        .map(Objects::toString)
        .collect(Collectors.joining(","));
  }

  private void appendLine(@Nonnull Writer writer, @Nonnull Object... columns)
      throws IOException {
    writer.write(
        Arrays.stream(columns)
            .map(Objects::toString)
            .map(text -> text.replaceAll("\\.", ","))
            .collect(Collectors.joining(";")) + System.lineSeparator()
    );
  }
}
