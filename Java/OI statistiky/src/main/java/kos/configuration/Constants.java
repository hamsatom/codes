package kos.configuration;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

/**
 * @author Tomáš Hamsa on 13.09.2017.
 */
public final class Constants {

  public static final short TIMEOUT = 1000;
  public static final Charset CHARSET = StandardCharsets.UTF_8;

  private Constants() throws IllegalAccessException {
    throw new IllegalAccessException("Initializing constants holder");
  }

}
