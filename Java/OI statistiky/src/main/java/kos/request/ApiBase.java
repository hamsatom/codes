package kos.request;

/**
 * @author Tomáš Hamsa on 13.09.2017.
 */
public enum ApiBase {
  FEL("https://kosapi.feld.cvut.cz/api/3/"), FIT("https://kosapi.fit.cvut.cz/api/3/");

  private final String apiURL;

  ApiBase(final String apiURL) {
    this.apiURL = apiURL;
  }

  String getApiURL() {
    return apiURL;
  }
}
