package kos.request;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import javax.annotation.Nonnull;
import kos.configuration.Constants;
import kos.request.util.Authentication;
import kos.request.util.Cache;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;


final class Requests {

  private static final String PARAMETERS = "?detail=0&lang=cs&limit=1000&locEnums=false&multilang=false&sem=";

  private Requests() throws IllegalAccessException {
    throw new IllegalAccessException("Initializing utility class");
  }

  static String getStudentCourses(@Nonnull final ApiBase location,
      @Nonnull final String studentUsernameOrId, final String semesterCode) throws IOException {

    return doGetRequest(
        location.getApiURL() + "students/" + studentUsernameOrId.toLowerCase() + "/enrolledCourses"
            + PARAMETERS + semesterCode + "&fields=entry/content(course,assessment,completed)"
    );
  }

  static String getStudyPlan(@Nonnull ApiBase location, @Nonnull String studentUsernameOrId,
      @Nonnull String semesterCode) throws IOException {

    return doGetRequest(
        location.getApiURL() + "students/" + studentUsernameOrId + PARAMETERS + semesterCode
            + "&fields=content/studyPlan");
  }

  static String getCourseCredits(@Nonnull final ApiBase location, final String courseCode,
      final String semesterCode) throws IOException {

    return doGetRequest(location.getApiURL() + "courses/" + courseCode + PARAMETERS + semesterCode
        + "&fields=content/credits");
  }

  private static String doGetRequest(@Nonnull String url) throws IOException {

    final Optional<String> cachedData = Cache.getData(url);
    if (cachedData.isPresent()) {
      return cachedData.get();
    }

    final String token = Authentication.getToken();

    url = url.replaceAll("\\+", "%2B");
    url = url.replaceAll("#", "%23");
    final HttpGet httpGet = new HttpGet(url);
    httpGet.addHeader("Authorization", " Bearer " + token);
    httpGet.addHeader("Accept",
        "application/atom+xml, application/atom+xml;type=entry, application/atom+xml;type=feed");

    String result;
    try (final CloseableHttpClient client = HttpClientBuilder.create()
        .setConnectionTimeToLive(Constants.TIMEOUT, TimeUnit.MILLISECONDS).build()) {

      final HttpResponse response = client.execute(httpGet);

      try (ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream()) {
        final byte[] buffer = new byte[4096];
        int length;
        try (final InputStream inStream = response.getEntity().getContent()) {
          while ((length = inStream.read(buffer)) != -1) {
            byteArrayOutputStream.write(buffer, 0, length);
          }
        }

        result = byteArrayOutputStream.toString(Constants.CHARSET.name());
      }
    }

    Cache.cacheInMemory(url, result);

    return result;
  }
}

