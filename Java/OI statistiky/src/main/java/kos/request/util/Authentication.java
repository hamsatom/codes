package kos.request.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.util.Base64;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;
import kos.configuration.Constants;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

/**
 * @author Tomáš Hamsa on 13.09.2017.
 */
public final class Authentication {

  private static final String TOKEN_URL = "https://auth.fit.cvut.cz/oauth/token";
  private static final String ID = "4f838cbc-cd1a-45d7-b055-b570ce2df6eb";
  private static final String SECRET = "y9A3prjtQ6GQ3NAPTgtViY4ZzREKBLHP";
  private static final String GRANT_TYPE = "client_credentials";
  private static String token;
  private static long tokenExpirationTime;

  private Authentication() throws IllegalAccessException {
    throw new IllegalAccessException("Initializing utility class");
  }

  public static String getToken() throws IOException {
    synchronized (Authentication.class) {
      if (tokenExpirationTime > System.currentTimeMillis()) {
        return token;
      }

      // Create request
      try (final CloseableHttpClient client = HttpClientBuilder.create()
          .setConnectionTimeToLive(Constants.TIMEOUT, TimeUnit.MILLISECONDS).build()) {

        final HttpPost post = new HttpPost(URI.create(TOKEN_URL));
        post.addHeader("Authorization", " Basic " + Base64.getEncoder()
            .encodeToString((ID + ":" + SECRET).getBytes(Constants.CHARSET)));
        post.setEntity(getOauthBody());

        // Parse response
        final HttpResponse response = client.execute(post);

        String responseBody;
        try (ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream()) {
          byte[] buffer = new byte[1024];
          int length;
          while ((length = response.getEntity().getContent().read(buffer)) != -1) {
            byteArrayOutputStream.write(buffer, 0, length);
          }

          responseBody = byteArrayOutputStream.toString(Constants.CHARSET.name());
        }

        final JSONObject json = new JSONObject(responseBody);

        tokenExpirationTime = System.currentTimeMillis() + json.getLong("expires_in") * 1000;
        token = json.getString("access_token");
      }

      return token;
    }
  }

  private static UrlEncodedFormEntity getOauthBody() throws UnsupportedEncodingException {

    final List<NameValuePair> nameValuePairs = Collections
        .singletonList(new BasicNameValuePair("grant_type", GRANT_TYPE));

    return new UrlEncodedFormEntity(nameValuePairs, Constants.CHARSET.name());
  }

}
