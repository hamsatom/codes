package kos.request;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.extended.ToAttributedValueConverter;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.stream.Stream;
import java.util.stream.Stream.Builder;
import javax.annotation.Nonnull;
import kos.configuration.Constants;
import kos.dto.InternalCourseEnrollment;
import kos.dto.InternalCourseEnrollment.InternalCourse;
import org.intabulas.sandler.elements.Feed;
import org.intabulas.sandler.parser.AtomParser;

/**
 * @author Tomáš Hamsa on 13.09.2017.
 */
public final class Kos {

  private static final AtomParser ATOM_PARSER = new AtomParser();
  private static final XStream X_STREAM = new XStream();

  static {
    X_STREAM.registerConverter(
        new ToAttributedValueConverter(InternalCourse.class,
            X_STREAM.getMapper(), X_STREAM.getReflectionProvider(), X_STREAM.getConverterLookup(),
            "name"));
    X_STREAM.aliasAttribute(InternalCourse.class, "courseCode", "xlink:href");

    final Class<?>[] classes = new Class[]{InternalCourseEnrollment.class};
    XStream.setupDefaultSecurity(X_STREAM);
    X_STREAM.allowTypes(classes);

    Arrays.stream(classes)
        .forEach(aClass -> X_STREAM.alias(aClass.getSimpleName(), aClass));
  }

  private Kos() throws IllegalAccessException {
    throw new IllegalAccessException("Initializing utility class");
  }

  public static Stream<InternalCourseEnrollment> getStudentCourses(@Nonnull final ApiBase location,
      final String semesterCode, @Nonnull final String studentUsernameOrId)
      throws IOException {

    final String result = Requests.getStudentCourses(location, studentUsernameOrId, semesterCode);

    return parse(result, InternalCourseEnrollment.class);
  }

  public static String getStudyPlan(@Nonnull ApiBase apiLocation, @Nonnull String semesterCode,
      @Nonnull String studentCode) throws IOException {

    String kosResult = Requests.getStudyPlan(apiLocation, studentCode, semesterCode);
    return parseSingleTag(kosResult, "studyPlan");
  }

  public static long getCourseCredits(@Nonnull final ApiBase location, final String semesterCode,
      final String courseCode) throws IOException {

    final String result = Requests.getCourseCredits(location, courseCode, semesterCode);
    String parsedResult = parseSingleTag(result, "credits");
    return Long.parseLong(parsedResult);
  }

  private static String parseSingleTag(@Nonnull String kosResult, String tagName)
      throws IOException {
    int indexStart = kosResult.indexOf("<" + tagName);
    indexStart = kosResult.indexOf('>', indexStart) + 1;
    int indexEnd = kosResult.indexOf("</" + tagName + ">", indexStart);

    try {
      return kosResult.substring(indexStart, indexEnd);
    } catch (Exception e) {
      throw new IOException(
          "Error while parsing single tag \"" + tagName + "\" from KOS API response:" + kosResult);
    }
  }

  @SuppressWarnings("unchecked")
  private static <T> Stream<T> parse(@Nonnull final String atomXml, @Nonnull final Class<T> clazz)
      throws IOException {

    if (!atomXml.contains("<atom:entry")) {
      return Stream.empty();
    }

    try {
      Feed feed;
      try (InputStream stream = new ByteArrayInputStream(atomXml.getBytes(Constants.CHARSET))) {
        feed = ATOM_PARSER.parseInput(stream);
      }

      final Builder<T> streamBuilder = Stream.builder();

      for (int i = 0; i < feed.getEntryCount(); i++) {

        String xml = String
            .format("<%s>%s</%s>", clazz.getSimpleName(), feed.getEntry(i).getContent(0).getBody(),
                clazz.getSimpleName());
        xml = xml.replaceAll("&", " and ");

        final T entity = (T) X_STREAM.fromXML(xml);

        streamBuilder.accept(entity);
      }

      return streamBuilder.build();
    } catch (Exception e) {
      throw new IOException(
          "Error while parsing \"" + clazz.getSimpleName() + "\" from KOS API response:" + atomXml);
    }
  }
}
