package kos.dto;

import java.io.Serializable;
import java.util.Objects;
import javax.annotation.Nullable;

/**
 * @author Tomáš Hamsa on 20.08.2017.
 */
@SuppressWarnings("unused")
public final class InternalCourseEnrollment implements Serializable {

  private static final long serialVersionUID = 827016906268843329L;

  private InternalCourse course;
  private boolean assessment;
  private boolean completed;

  @Nullable
  public String getCourseCode() {
    if (course == null) {
      return null;
    }
    String code = course.courseCode;
    if (code.startsWith("courses/") && code.endsWith("/")) {
      code = code.substring(8, code.length() - 1);
      course.courseCode = code;
    }
    return code;
  }

  public String getName() {
    return course.name;
  }

  public boolean isAssessment() {
    return assessment;
  }

  public boolean isCompleted() {
    return completed;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof InternalCourseEnrollment)) {
      return false;
    }
    InternalCourseEnrollment that = (InternalCourseEnrollment) o;
    return assessment == that.assessment && completed == that.completed && Objects
        .equals(course, that.course);
  }

  @Override
  public int hashCode() {
    return Objects.hash(course, assessment, completed);
  }

  public static final class InternalCourse implements Serializable {

    private static final long serialVersionUID = 89406518843329L;

    private String courseCode;
    private String name;

    private InternalCourse() throws IllegalAccessException {
      throw new IllegalAccessException();
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) {
        return true;
      }
      if (!(o instanceof InternalCourse)) {
        return false;
      }
      InternalCourse that = (InternalCourse) o;
      return Objects.equals(courseCode, that.courseCode) &&
          Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {

      return Objects.hash(courseCode, name);
    }
  }
}
