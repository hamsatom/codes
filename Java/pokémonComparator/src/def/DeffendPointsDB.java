package def;

import java.util.HashMap;
import java.util.StringTokenizer;

class DeffendPointsDB {
private static final short POKÉMONS_IN_DB = 143;
static HashMap<String, Double> pointsChart = new HashMap<>(POKÉMONS_IN_DB);
private static String defPointDB = "Snorlax 100.0 Lapras 99.38 Alakazam 96.88 Slowbro 88.72 Dragonite 82.35 " +
                                   "Exeggutor" + " 81.57 Vaporeon 80.44 Golduck 77.98 Clefable 76.88 Dewgong 73.37 "
                                   + "Weezing 72.78 " + "Chansey 71.5 Hypno 70.21 Poliwrath 69.93 Kadabra 69.93 " +
                                   "Ninetales 69.07 Blastoise " + "67.55 Starmie 67.24 Tauros 65.83 Arcanine 65.01 "
                                   + "Slowpoke 63.69 Tentacruel 63.41 " + "Omastar 63.22 Muk 63.18 Machamp 62.91 " +
                                   "Victreebel 62.55 Flareon 62.51 Raichu 62.2 " + "Eevee 61.34 Charizard 60.52 " +
                                   "Aerodactyl 59.9 Rapidash 59.2 Wartortle 59.04 Seaking" + " 58.84 Cloyster 58.61 "
                                   + "Electabuzz 58.57 Jolteon 58.57 Gyarados 58.49 Wigglytuff " + "57.99 Gengar " +
                                   "57.91 " + "Poliwhirl 57.83 Nidoqueen 57.13 Psyduck 56.66 Abra 56.46 " +
                                   "Dragonair 55.88 " + "Nidorino 55.88 Pikachu 55.37 Kangaskhan 55.06 Kingler 54.74 " +
                                   "" + "" + "" + "" + "" + "Growlithe 54.31 " + "Magmar 54.31 Likitung 54.16 " +
                                   "Venusaur " + "53.89 " + "Vileplume " + "53.89 " + "Persian 53.77 " + "Pidgeot " +
                                   "53.46 Nidoking " + "53.22 " + "Koffing" + " 52.67 Mr. " + "52.56 Dodrio 51.0 " +
                                   "Porygon " + "50.84 " + "Exeggcute " + "50.02 " + "Squirtle 49.94 Raticate" + " " +
                                   "49.04 Venomoth 48.54 Golem" + " " + "48.46 " + "Dratini 48.42 " + "Poliwag 48.26 " +
                                   "Staryu 48.18 " + "Electrode " + "48.07 Fearow 47.95 " + "Jynx " + "" + "47.64" +
                                   " Seel 47.33 Nidorina 46.43 " + "Haunter 46.11 Charmeleon" + " 46.08 Vulpix 46.08 " +
                                   "" + "" + "Clefairy 45.92 Seadra " + "45.53" + " Tentacool 45.41 " + "Gloom 44.63 " +
                                   "Golbat 44.51 " + "Ivysaur" + " " + "44.24 Weepinbell " + "44.24 " + "Magneton " +
                                   "44.01 Marowak 43.23 Grimer" + " 42.72 " + "Pidgeotto " + "41.66 " + "Dugtrio" + "" +
                                   " 41.16 Horsea 41.12 Rattata 40.8 " + "Drowzee 40.77 Graveler 40.65 " +
                                   "Hitmonchan " + "40.45 Arbok 39.98 Sandslash " + "39.67 Scyther 39.55 Jigglypuff "
                                   + "39.48 " + "Ponyta " + "39.36 " + "Nidoran " + "38.62 " + "Meowth 38.31 Voltorb " +
                                   "38.07 " + "Charmander 37.84 " + "Goldeen" + " " + "37.6 Krabby " + "37.45 " +
                                   "Kabutops 37.41 " + "Butterfree 37.29 Omanyte 37.06 " + "Kabuto" + " 37.06 " +
                                   "Tangela 36.59 " + "Shellder 35.69 Oddish 35.06 Beedrill " + "34.4 " + "Sandshrew " +
                                   "34.09" + " Nidoran 33.66" + " Bulbasaur" + " 32.96 Primeape " + "31.82 Diglett " +
                                   "31.71 " + "Pinsir " + "31.63 Spearow" + " 31.28 Doduo 31.28 Pidgey" + " 31.16 " +
                                   "Hitmonlee 30.69 " + "Machoke " + "30.38 " + "Bellsprout 30.3 Gastly " + "29.99 " +
                                   "Geodude 28.47 " + "Venonat 27.68 " + "Ekans " + "27.61 " + "Cubone 27.29 " +
                                   "Magnemite 26.36 Zubat 26.12 Machop 24.99 " + "Caterpie " + "24.29 Metapod" + " "
                                   + "24.29 Mankey 22.65 Rhydon 21.71 Magikarp 21.71 Onix " + "16.28" + " " + "Weedle" +
                                   " " + "15.27 " + "Kakuna 15.27 Parasect 9.02 Rhyhorn 5.47 Paras " + "5.27";

static void fillDefChart() {
    StringTokenizer token = new StringTokenizer(defPointDB, " ");
    for (int i = 0; i < POKÉMONS_IN_DB; ++i) {
        pointsChart.put(token.nextToken(), Double.valueOf(token.nextToken()));
    }
}
}
