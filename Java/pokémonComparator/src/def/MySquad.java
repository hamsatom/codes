package def;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

class MySquad {
private static final short BAG_CAPACITY = 250;
static ArrayList<Pokémon> squadList = new ArrayList<>(BAG_CAPACITY);
private static final String PATH_TO_DB = "D:\\Tomáš\\ownCloud\\PR2\\pokémonComparator\\pokéDB.txt";

static void fillSquadCP() throws IOException {
    BufferedReader br = new BufferedReader(new FileReader(PATH_TO_DB));
    String line;
    String[] splitedDB;
    while ((line = br.readLine()) != null) {
        splitedDB = line.split(" ");
        squadList.add(new Pokémon(splitedDB[0], Integer.parseInt(splitedDB[1])));
    }
}
}
