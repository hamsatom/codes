package def;

import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.util.Collections;

import static def.DeffendPointsDB.pointsChart;
import static def.MySquad.squadList;

public class Main {
private static final byte amount = 11;

public static void main(String[] args) throws IOException {
    DeffendPointsDB.fillDefChart();
    try {
        MySquad.fillSquadCP();
    } catch (IOException e) {
        System.err.println("File not found or couldn't be read." + System.lineSeparator() + e.getMessage());
    }
    
    double deffendScore;
    String newName = "";
    for (Pokémon pokémon : squadList) {
        if (!pointsChart.containsKey(pokémon.name)) {
            newName = getMostSimilarName(pokémon.name);
            System.err.println(pokémon.name + " " + pokémon.CP + " not found in deffenders list. Used " + newName);
            pokémon.name = newName;
        }
        deffendScore = pointsChart.get(pokémon.name);
        pokémon.deffenderValue = deffendScore * pokémon.CP;
    }
    
    Collections.sort(squadList);
    
    byte place = 1;
    for (byte i = 0; i < amount; i++) {
        System.out.println(place++ + ". " + squadList.get(i).toString());
    }
}

private static String getMostSimilarName(final String name) {
    double bestScore = Double.NEGATIVE_INFINITY;
    String mostSimilarNameFound = "";
    double currentSimilarity;
    
    for (String key : pointsChart.keySet()) {
        currentSimilarity = StringUtils.getJaroWinklerDistance(name, key);
        if (currentSimilarity > bestScore) {
            bestScore = currentSimilarity;
            mostSimilarNameFound = key;
        }
    }
    
    return mostSimilarNameFound;
}
}
