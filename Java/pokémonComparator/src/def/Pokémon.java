package def;

class Pokémon implements Comparable<Pokémon> {
String name;
final int CP;
double deffenderValue;

Pokémon(String name, int CP) {
    this.name = name;
    this.CP = CP;
}

@Override
public String toString() {
    return name + " " + CP + System.lineSeparator() + "   deffender scre: " + deffenderValue;
}

@Override
public int compareTo(Pokémon o) {
    return (int) (o.deffenderValue - this.deffenderValue);
}
}