import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.StringJoiner;
import java.util.StringTokenizer;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

/**
 * MapReduce job for processing concerts
 *
 * <p>Maps concerts to day and band members performing in that day. Than reduces them to day, number of members performing in that day and the individual members.</p>
 *
 * <p>{@link MapReduce.ConcertMapper} parses concert data a maps them to {@code day:members}</p>
 * <p>{@link MapReduce.ConcertReducer} filters out unwanted entries and creates result entries {@code day memberCount member1, member2, ...}</p>
 * <p>{@link MapReduce#main(String[])} MapReduce job configuration</p>
 */
public class MapReduce {

	private static final Logger LOG = Logger.getLogger(MapReduce.class.getName());

	/**
	 * MapReduce job configuration
	 *
	 * <p>
	 * Configures:
	 * <un>
	 * <li>Class name and it's JAR name</li>
	 * <li>Mapper and Reducer classes</li>
	 * <li>Data types of output entries</li>
	 * </un>
	 * </p>
	 *
	 * @throws Exception Whenever MapReduce job fails
	 */
	public static void main(String[] args) throws Exception {
		Configuration conf = new Configuration();

		// Class
		Job job = Job.getInstance(conf, "MapReduce");
		job.setJarByClass(MapReduce.class);

		// Mapper+Reducer
		job.setMapperClass(ConcertMapper.class);
		job.setReducerClass(ConcertReducer.class);

		// Data types
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(Text.class);

		FileInputFormat.addInputPath(job, new Path(args[0]));
		FileOutputFormat.setOutputPath(job, new Path(args[1]));

		System.exit(job.waitForCompletion(true) ? 0 : 1);
	}

	/**
	 * Concert MapReduce {@link Mapper} implementation
	 *
	 * <p>Processes input data in form of {@link Object}:{@link Text}, parses the input and maps desired values into {@link Text}:{@link Text} entry.</p>
	 */
	public static class ConcertMapper extends Mapper<Object, Text, Text, Text> {
		/**
		 * Mapper function
		 *
		 * <p>Filters out entries where the stage capacity is lower or equal 20 and where the band genre is 'Country'.</p>
		 * <p>Input data in of form of {@link Text} are passed into concert attributes and returned in a form of {@link Text}1:{@link Text}2 where the {@link Text}1 is day of the
		 * concert and {@link Text}2 are members of the band playing separated by space - {@code day:members}.</p>
		 *
		 * @param key   Key of input row
		 * @param value Values in the input row from input data
		 * @throws IOException          Whenever reading failed
		 * @throws InterruptedException Whenever processing was interrupted
		 */
		@Override
		public void map(Object key, Text value, Context context) throws IOException, InterruptedException {
			StringTokenizer tokens = new StringTokenizer(value.toString());

			// Parse attributes
			String day = tokens.nextToken();
			String time = tokens.nextToken();
			String stageName = tokens.nextToken();
			String stageCode = tokens.nextToken();
			int stageCapacity = Integer.parseInt(tokens.nextToken());
			String band = tokens.nextToken();
			String genre = tokens.nextToken();
			StringJoiner joiner = new StringJoiner(" ");
			while (tokens.hasMoreTokens()) {
				joiner.add(tokens.nextToken());
			}
			String members = joiner.toString();

			LOG.info(() -> {
				String row = Stream.of(day, time, stageName, stageCode, stageCapacity, band, genre, members)
						.map(s -> "'" + s + '\'')
						.collect(Collectors.joining(", "));
				return "Parsed data: " + row;
			});

			// Return entries with stage capacity greater than 20 and band genre not 'Country' in a form of day:members
			if (stageCapacity > 20 && !"Country".equals(genre)) {
				context.write(new Text(day), new Text(members));
			}
		}
	}

	/**
	 * Concert MapReduce {@link Reducer} implementation
	 *
	 * <p>Processes mapped data in form of {@link Text}:{@link Text} and reduces desired entries into {@link Text}:{@link Text} entry.</p>
	 */
	public static class ConcertReducer extends Reducer<Text, Text, Text, Text> {
		/**
		 * Reduce function
		 *
		 * <p>Reduces band members in a day where there is less or equal than 25 unique band members.</p>
		 * <p>Takes mapped data in form of {@code day:members} where members are separated by white-space. Makes members in the same day unique and returns them sorted
		 * alphabetically and separated by ',' in form of {@code day memberCount member1, member2, ...}.</p>
		 *
		 * @param key    The day concert is played on
		 * @param values Members of bands that have concert in day determined by {@code key}
		 * @throws IOException          Whenever writing fails
		 * @throws InterruptedException Whenever processing was interrupted
		 */
		@Override
		public void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
			Set<String> uniqueMembers = new HashSet<>();

			// Process members
			for (Text value : values) {
				// Parse by a white-space
				StringTokenizer tokens = new StringTokenizer(value.toString());
				while (tokens.hasMoreTokens()) {
					uniqueMembers.add(tokens.nextToken());
					// Skip days with more than 25 unique band members
					if (uniqueMembers.size() > 25) {
						return;
					}
				}
			}

			// Sort band members and separate them by ','
			String members = uniqueMembers.stream()
					.sorted()
					.collect(Collectors.joining(", "));

			// Write result in a form of day memberCount member1, member2, ...
			context.write(key, new Text(uniqueMembers.size() + " " + members));
		}
	}
}
