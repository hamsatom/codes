/*
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Vector;
import javax.persistence.Query;
import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.text.DateFormatter;
import javax.swing.text.DefaultFormatterFactory;


public class Main {

  public static void main(String[] args) throws ClassNotFoundException, SQLException {

    Class.forName("org.postgresql.Driver");
    final Connection c = DriverManager
        .getConnection("jdbc:postgresql://slon.felk.cvut.cz:5432/db18_dzivjmat", "db18_dzivjmat",
            "Bv9dGK");

    Statement s3 = c.createStatement();
    ResultSet rs = s3.executeQuery("SELECT * FROM people");
    ArrayList columnNames = new ArrayList();
    ArrayList data = new ArrayList();
    ResultSetMetaData md = rs.getMetaData();
    int columns = md.getColumnCount();
    //  Get column names
    for (int i = 1; i <= columns; i++) {
      columnNames.add(md.getColumnName(i));
    }

    //  Get row data
    while (rs.next()) {
      ArrayList row = new ArrayList(columns);

      for (int i = 1; i <= columns; i++) {
        row.add(rs.getObject(i));
      }

      data.add(row);
    }
    Vector columnNamesVector = new Vector();
    Vector dataVector = new Vector();

    for (int i = 0; i < data.size(); i++) {
      ArrayList subArray = (ArrayList) data.get(i);
      Vector subVector = new Vector();
      for (int j = 0; j < subArray.size(); j++) {
        subVector.add(subArray.get(j));
      }
      dataVector.add(subVector);
    }

    for (int i = 0; i < columnNames.size(); i++) {
      columnNamesVector.add(columnNames.get(i));
    }

    //  Create table with database data
    JTable table = new JTable(dataVector, columnNamesVector) {
      public Class getColumnClass(int column) {
        for (int row = 0; row < getRowCount(); row++) {
          Object o = getValueAt(row, column);

          if (o != null) {
            return o.getClass();
          }
        }

        return Object.class;
      }
    };

//        PreparedStatement ps = c.prepareStatement("SELECT * FROM book WHERE (title = ?)");
//        ps.setString(1, "Zámek");
//        ps.executeQuery();
    c.close();

    final JFrame frame = new JFrame("CSGO database");
    frame.setMinimumSize(new Dimension(800, 400));
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    JLabel myLabel = new JLabel("People", SwingConstants.CENTER);
    myLabel.setFont(new Font("Serif", Font.BOLD, 22));
    myLabel.setOpaque(true);
    myLabel.setPreferredSize(new Dimension(100, 80));

    frame.getContentPane().add(myLabel, BorderLayout.NORTH);
    JScrollPane scrollPane = new JScrollPane(table);
    frame.getContentPane().add(scrollPane);

    final JPanel buttonPanel = new JPanel();
    final JButton addPersonButton = new JButton("Add");
    addPersonButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        JTextField firstName = new JTextField(8);
        JTextField secondName = new JTextField(8);
        //we choose the wanted date pattern
        DateFormatter dateFormatter = new DateFormatter(new SimpleDateFormat("dd/MM/yyyy"));
        //the javadoc  new DefaultFormatterFactory(defaultFormat, displayFormat, editFormat)
        DefaultFormatterFactory dateFormatterFactory = new DefaultFormatterFactory(dateFormatter,
            new DateFormatter(),
            dateFormatter
        );
        JFormattedTextField dateField = new JFormattedTextField(dateFormatterFactory);
        GridLayout experimentLayout = new GridLayout(3, 1, 10, 10);

        JPanel myPanel = new JPanel(experimentLayout);
        myPanel.add(new JLabel("First name:"));
        myPanel.add(firstName);
        myPanel.add(new JLabel("Second name:"));
        myPanel.add(secondName);
        myPanel.add(new JLabel("Birthday:"));
        myPanel.add(dateField);

        int result = JOptionPane.showConfirmDialog(null, myPanel, "Enter new person details",
            JOptionPane.OK_CANCEL_OPTION);
        while (result == JOptionPane.OK_OPTION && (firstName.getText() == ""
            || secondName.getText() == "" || dateField.getValue() == null)) {
          result = JOptionPane.showConfirmDialog(null, myPanel, "Enter new person details",
              JOptionPane.OK_CANCEL_OPTION);
        }
        if (result == JOptionPane.OK_OPTION) {
          try {
            Statement s = c.createStatement();
            Statement s = c
                .prepareStatement("INSERT INTO topic (ID, TITLE,CREATION_DATE) VALUES(?,?,?)");
            ResultSet rs = s
                .executeQuery("INSERT INTO topic (ID, TITLE,CREATION_DATE) VALUES(?,?,?)");
          } catch (SQLException e1) {
            e1.printStackTrace();
          }
          Query query = entityManager
              .createNativeQuery("INSERT INTO topic (ID, TITLE,CREATION_DATE) " +
                  " VALUES(?,?,?)");
          query.setParameter(1, id);
          query.setParameter(2, title);
          query.setParameter(3, creationDate);
          query.executeUpdate();
          System.out.println("Name: " + firstName.getText());
          System.out.println("Surname: " + secondName.getText());
          System.out.println("Birthday: " + dateField.getValue());

        }
      }
    });

    buttonPanel.add(addPersonButton);
    frame.getContentPane().add(buttonPanel, BorderLayout.SOUTH);
    frame.setVisible(true);

  }

}*/
