package impl.data.dao;

import impl.data.model.Person;
import template.data.dao.PersonDAO;

/**
 * @author Tomáš Hamsa on 17.05.2018.
 */
public class PersonDAOImpl extends AbstractDAO<Person> implements PersonDAO {

}
