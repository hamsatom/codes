package impl.logic;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;
import javax.annotation.Nonnull;
import template.data.dao.DAO;
import template.logic.Service;

/**
 * @author Tomáš Hamsa on 17.05.2018.
 */
abstract class AbstractService<T extends Serializable> implements Service<T> {

  final DAO<T> dao;

  AbstractService(@Nonnull DAO<T> dao) {
    this.dao = dao;
  }

  @Nonnull
  @Override
  public List<T> list() {
    List<T> result = dao.list();
    result.forEach(this::postLoad);
    return result;
  }

  @Nonnull
  @Override
  public Optional<T> save(@Nonnull T entity) {
    prePersist(entity);
    return dao.save(entity);
  }

  @Nonnull
  @Override
  public Stream<Optional<T>> save(@Nonnull Collection<? extends T> entities) {
    return entities.stream().map(this::save);
  }

  @Nonnull
  @Override
  public Optional<T> update(@Nonnull T entity) {
    preUpdate(entity);
    Optional<T> result = dao.update(entity);
    postUpdate();
    return result;
  }

  @Nonnull
  @Override
  public Optional<T> find(@Nonnull Serializable id) {
    Optional<T> result = dao.find(id);
    result.ifPresent(this::postLoad);
    return result;
  }

  @Override
  public boolean exists(@Nonnull Serializable id) {
    return dao.find(id).isPresent();
  }

  @Override
  public void delete(@Nonnull Serializable id) {
    dao.delete(id);
  }
}
