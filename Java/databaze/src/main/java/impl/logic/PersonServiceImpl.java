package impl.logic;

import impl.data.dao.PersonDAOImpl;
import impl.data.model.Person;
import template.logic.PersonService;

/**
 * @author Tomáš Hamsa on 16.05.2018.
 */
public class PersonServiceImpl extends AbstractService<Person> implements PersonService {

  public PersonServiceImpl() {
    super(new PersonDAOImpl());
  }

}
