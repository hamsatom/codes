package template.data.dao;

import impl.data.model.Person;

/**
 * @author Tomáš Hamsa on 16.05.2018.
 */
public interface PersonDAO extends DAO<Person> {

}
