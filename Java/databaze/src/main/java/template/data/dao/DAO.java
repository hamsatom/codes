package template.data.dao;


import java.io.Serializable;
import java.util.List;
import java.util.Optional;
import javax.annotation.Nonnull;

/**
 * @author Tomáš Hamsa
 */
public interface DAO<T extends Serializable> {

  @Nonnull
  List<T> list();

  @Nonnull
  Optional<T> save(@Nonnull T entity);

  @Nonnull
  Optional<T> find(@Nonnull Serializable id);

  @Nonnull
  Optional<T> update(@Nonnull T entity);

  void delete(@Nonnull Serializable id);
}
