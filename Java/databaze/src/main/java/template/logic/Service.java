package template.logic;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;
import javax.annotation.Nonnull;

public interface Service<T extends Serializable> {

  @Nonnull
  List<T> list();

  @Nonnull
  Optional<T> save(@Nonnull T entity);

  @Nonnull
  Stream<Optional<T>> save(@Nonnull Collection<? extends T> entities);

  @Nonnull
  Optional<T> find(@Nonnull Serializable id);

  @Nonnull
  Optional<T> update(@Nonnull T entity);

  /**
   * Checks whether an instance with the specified id exists.
   *
   * @param id Instance identifier
   * @return Whether a matching instance exists
   */
  boolean exists(@Nonnull Serializable id);

  void delete(@Nonnull Serializable id);

  // The following methods are hooks intended to be overridden by subclasses, so that the main CRUD methods do not have to be modified

  default void prePersist(@Nonnull T entity) {
    /* Do nothing, intended for overriding */
  }

  default void preUpdate(@Nonnull T entity) {
    /* Do nothing, intended for overriding */
  }

  default void postUpdate() {
    /* Do nothing, intended for overriding */
  }

  default void postLoad(@Nonnull T entity) {
    /* Do nothing, intended for overriding */
  }
}
