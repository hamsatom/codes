package impl.logic;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import template.logic.PersonService;

public class PersonServiceImplTest {

  private PersonService service;

  @BeforeMethod
  public void setUp() {
    service = new PersonServiceImpl();
  }

  @Test
  public void testList() {
    Assert.assertNotNull(service.list());
  }
}