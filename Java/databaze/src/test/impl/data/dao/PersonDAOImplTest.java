package impl.data.dao;

import impl.data.model.Person;
import java.sql.Date;
import java.util.List;
import java.util.Optional;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import template.data.dao.PersonDAO;

public class PersonDAOImplTest {

  private PersonDAO dao;

  @BeforeMethod
  public void setUp() {
    dao = new PersonDAOImpl();
  }

  private Person uploadPerson() {
    Person person = new Person()
        .setFirstName("Šimon")
        .setLastName("Maňour")
        .setBirthday(new Date(System.currentTimeMillis()));
    Optional<Person> result = dao.save(person);
    Assert.assertTrue(result.isPresent());
    Person resultPerson = result.orElseThrow(AssertionError::new);
    Assert.assertEquals(person.getBirthday(), resultPerson.getBirthday());
    Assert.assertEquals(person.getFirstName(), resultPerson.getFirstName());
    Assert.assertEquals(person.getLastName(), resultPerson.getLastName());
    return resultPerson;
  }

  @Test(priority = 1)
  public void testSave() {
    uploadPerson();
  }

  @Test(priority = 2)
  public void testFind() {
    Person savedPerson = uploadPerson();
    Optional<Person> maybeFound = dao.find(savedPerson.getId());
    Assert.assertTrue(maybeFound.isPresent());
    Person foundPerson = maybeFound.orElseThrow(AssertionError::new);
    Assert.assertEquals(savedPerson, foundPerson);
  }

  @Test(priority = 3)
  public void testList() {
    Person p1 = uploadPerson();
    Person p2 = uploadPerson();
    List<Person> foundPersons = dao.list();
    Assert.assertTrue(foundPersons.contains(p1));
    Assert.assertTrue(foundPersons.contains(p2));
  }

  @Test(priority = 4)
  public void testUpdate() {
    Person changedPerson = uploadPerson().setFirstName("Domísa")
        .setLastName("Hoflyš");
    Optional<Person> maybePerson = dao.update(changedPerson);
    Assert.assertTrue(maybePerson.isPresent());
    Person updatedPerson = maybePerson.orElseThrow(AssertionError::new);
    Assert.assertEquals(changedPerson, updatedPerson);
  }

  @Test(priority = 5)
  public void testDelete() {
    Person person = uploadPerson();
    dao.delete(person.getId());
    Optional<Person> foundPerson = dao.find(person.getId());
    Assert.assertFalse(foundPerson.isPresent());
  }
}