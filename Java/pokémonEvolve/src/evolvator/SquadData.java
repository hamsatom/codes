package evolvator;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

class SquadData {
private static final short BAG_CAPACITY = 250;
static ArrayList<Pokémon> squadList = new ArrayList<>(BAG_CAPACITY);
private static final String FILE_PATH = "D:\\Tomáš\\ownCloud\\PR2\\pokémonEvolve\\evolve.txt";

static void loadSquad() throws IOException {
    BufferedReader br = new BufferedReader(new FileReader(FILE_PATH));
    String line;
    String[] lineSpited;
    while ((line = br.readLine()) != null) {
        lineSpited = line.split(" ");
        squadList.add(new Pokémon(lineSpited[0], Integer.parseInt(lineSpited[1]), Short.parseShort(lineSpited[2]),
                                  Byte.parseByte(lineSpited[3])));
    }
}
}
