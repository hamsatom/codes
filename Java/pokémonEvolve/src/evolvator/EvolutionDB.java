package evolvator;

import java.util.HashMap;
import java.util.StringTokenizer;

import static evolvator.PowerUpScores.POKÉMONS_COUNT;

class EvolutionDB {

static final HashMap<String, Double> evolutinMultipliers = new HashMap<>(POKÉMONS_COUNT);
static final HashMap<String, Short> candyCost = new HashMap<>(POKÉMONS_COUNT);
static final HashMap<String, String> evolutionNames = new HashMap<>(POKÉMONS_COUNT);
private static final String multipliers = "Kabuto 2.05 50 Kabutops Charmander 2.28 25 Charmeleon Charmeleon 1.83 100 " +
                                          "" + "" + "Charizard Shellder 2.62 50 Cloyster Mankey 2.19 50 Primeape " +
                                          "Pikachu " + "2.45 " + "50 Raichu Kadabra 1.95 100 Alakazam Magikarp 10.1 " +
                                          "400 Gyarados " + "Pidgeotto " + "1.765 50 Pidgeot Gastly 1.91 25 Haunter" +
                                          " " +
                                          "Metapod 3.36 50 " + "Butterfree Diglett " + "2.97 50 Dugtrio Eevee 2.68 25" +
                                          " Vaporeon Caterpie " + "1.06 12 Metapod Goldeen " + "2.19 50 Seaking Abra " +
                                          "2.56 25 Kadabra Magnemite" + " 2.22 50 Magneton Machoke " + "1.59 100 " +
                                          "Machamp Graveler 1.62 100 Golem " + "Meowth 2.23 50 Persian Staryu 2.4" +
                                          " 50 Starmie Machop 1.645 25 Machoke " + "Krabby 2.32 50 Kingler Jigglypuff" +
                                          " " +
                                          "2.42 " + "50 Wigglytuff Psyduck 2.27 50 " + "Golduck Pidgey 1.83 12 " +
                                          "Pidgeotto Horsea 2.22 " + "50 Seadra Clefairy 1.99 " + "50 Clefable Oddish" +
                                          " 1.495 25 Gloom Weedle 1.095 12 " + "Kakuna Gloom 1.505 100" + " Vileplume" +
                                          " " +
                                          "Vulpix 3.02 50 Ninetales Slowpoke 2.20 50 " + "Slowbro Doduo " + "2.29 50 " +
                                          "Dodrio Haunter 1.63 100 Gengar Poliwhirl 1.88 100 " + "Poliwrath " +
                                          "Squirtle 1.69 25 Wartortle Omanyte 2.38 50 Omastar Sandshrew " + "2.33 50 " +
                                          "" + "Sandslash Kakuna 3.215 50 Beedrill Ekans 2.21 50 Arbok Wartortle " +
                                          "1.68 " + "100 Blastoise Venonat 1.86 50 Venomoth Cubone 1.8 50 Marowak " +
                                          "NidoranF" + "" + " 1.69 25 Nidorina NidoranM 1.7 25 Nidorino Koffing " +
                                          "1.9875 " +
                                          "50 Weezing " + "Bulbasaur 1.94 25 Ivysaur Seel 1.74 50 Dewgong Drowzee " +
                                          "2.06 50 Hypno " + "Rattata 2.61 25 Raticate Tentacool 2.535 50 Tentacruel " +
                                          "Grimer 2.225 50 Muk " + "Ponyta 1.49 50 Rapidash Exeggcute 2.68 50 " +
                                          "Exeggutor Growlithe 2.27 50 " + "Arcanine Nidorina 1.85 100 Nidoqueen " +
                                          "Ivysaur 1.76 100 Venusaur Nidorino " + "" + "1.78 100 Nidoking Dratini " +
                                          "1.82 25 Dragonair Spearow 2.63 50 Fearow " + "Paras " + "1.95 50 Parasect " +
                                          "Weepinbell 1.53 100 Victreebel Zubat 3.13 50 " + "Golbat " + "Voltorb 2.01" +
                                          " 50 Electrode Dragonair 2.055 100 Dragonite " + "Bellsprout 1.57 25 " +
                                          "Weepinbell Rhyhorn 2.0 50 Rhydon Geodude 1.81 25 " + "Graveler Poliwag " +
                                          "1.81 25 " + "Poliwhirl";

static void fillEvulitionsData() {
    StringTokenizer token = new StringTokenizer(multipliers, " ");
    String name;
    while (token.hasMoreTokens()) {
        name = token.nextToken();
        evolutinMultipliers.put(name, Double.valueOf(token.nextToken()));
        candyCost.put(name, Short.valueOf(token.nextToken()));
        evolutionNames.put(name, token.nextToken());
    }
}
}
