package evolvator;

import java.util.HashMap;
import java.util.StringTokenizer;

class PowerUpScores {
static final int POKÉMONS_COUNT = 151;
static final HashMap<String, Integer> CPLimits = new HashMap<>(POKÉMONS_COUNT);
static final HashMap<String, Double> CPperPowerUp = new HashMap<>(POKÉMONS_COUNT);
private static final String CPData = "Mewtwo 56.8 4144 Dragonite 47.7 3500 Mew 44.9 3299 Moltres 44.1 3240 Zapdos " +
                                     "42.3 3114 Snorlax 42.2 3112 Arcanine 40.5 2983 Lapras 40.4 2980 Articuno 40.4 "
                                     + "2978 Exeggutor 40.1 2955 Vaporeon 38.1 2816 Gyarados 36.4 2688 Flareon 35.6 "
                                     + "2643 Muk 35.1 2602 Slowbro 35.1 2597 Charizard 35.1 2602 Machamp 35 2594 " +
                                     "Venusaur 34.8 2580 Blastoise 34.3 2542 Victreebel 34.1 2530 Poliwrath 33.8 " +
                                     "2505" + " Vileplume 33.6 2492 Nidoqueen 33.5 2485 Nidoking 33.3 2475 Clefable "
                                     + "32.3 2397 " + "Golduck 32.1 2386 Golem 30.9 2303 Magmar 30.4 2265 Weezing " +
                                     "30.2 " + "2250 Rhydon " + "30.1 2243 Omastar 30 2233 Tentacruel 29.8 2220 " +
                                     "Rapidash 29.5 " + "2199 Hypno 29.3 " + "2184 Ninetales 29.3 2188 Starmie 29.2 "
                                     + "2182 Wigglytuff 29.1" + " 2177 Aerodactyl 29 " + "2165 Dewgong 28.7 2145 " +
                                     "Jolteon 28.6 2140 Kabutops 28.5" + " 2130 Pinsir 28.4 2121 " + "Electabuzz " +
                                     "28.3" + " 2119 Pidgeot 28 2091 Gengar 27.8 " + "2078 Scyther 27.7 2073 " +
                                     "Cloyster " + "27.4 2052 Kangaskhan 27.3 2043 Seaking " + "27.3 2043 Raichu " +
                                     "27.1" + " 2028 " + "Golbat 25.6 1921 Venomoth 25.2 1890 Magneton 25" + " 1879 "
                                     + "Primeape " + "24.8 1864 " + "Tauros " + "24.5 1844 Dodrio 24.4 1836 Kingler "
                                     + "24.2 " + "1823 " + "Sandslash 24.1 " + "1810 Alakazam 24.1 " + "1813 Arbok "
                                     + "23.5" + " 1767 Machoke 23.4 " + "1760 Fearow 23.2" + " 1746 Dragonair 23.2 "
                                     + "1747" + " " + "Parasect 23.2 1747 " + "Tangela 23.1 " + "1739 " +
                                     "Weepinbell" + " " + "22.8 1723 " + "Jynx " + "22.8 1716 Seadra" + " " + "22.7 "
                                     + "1713 Porygon " + "22.4 " + "1691 Gloom " + "22.4 1689 " + "Marowak 21.9 1656 " +
                                     "" + "Electrode " + "21.8 " + "" + "1646 " + "Persian 21.6 " + "1631 Ivysaur " +
                                     "21.6 1632 Lickitung 21.5 " + "1626 " + "Wartortle 20.9 " + "1582 " +
                                     "Charmeleon " + "20.5 1557 Ponyta 20 " + "1516 " + "Hitmonchan 19.9 1516 " +
                                     "Hitmonlee " + "19.6 " + "1492 Mr" + ".Mime " + "19.6 1494 " + "Butterfree" + " " +
                                     "" + "19.1 1454 Raticate " + "19 1444 Beedrill " + "18.9 1439" + " " + "Graveler" +
                                     " 18.8" + " " + "1433 Nidorina 18.4" + " 1404 Nidorino 18" + " 1372" + " " +
                                     "Haunter 18 " + "1380 " + "Poliwhirl " + "17.6 1340 " + "Growlithe " + "17.5 " +
                                     "1335 Grimer " + "16.8 1284 " + "Farfetch'd 16.5 1263 " + "Pidgeotto 16 " +
                                     "1223 Slowpoke 15.9 " + "1218" + " " + "Clefairy 15.6 " + "1200 Rhyhorn 15.4 " +
                                     "1182 " + "" + "Dugtrio" + " 15.1 " + "1168 Oddish " + "14.9 1148 Koffing 14.9 "
                                     + "1151 " + "Kadabra 14.6 " + "" + "1131 " + "Bellsprout 14.4 " + "1117 Psyduck " +
                                     "14.4 " + "1109 Omanyte" + " " + "14.4" + " 1119 Seel " + "" + "14.3 1107 " +
                                     "Exeggcute 14.2 " + "1099 Kabuto " + "14.2 1104 " + "Machop 14.1 1089" + " " +
                                     "Drowzee 13.9 " + "1075 " + "Eevee " + "13.9 1077 " + "Bulbasaur 13.8 1071 " +
                                     "Venonat 13.3 1029 " + "Cubone 13 " + "1006 Squirtle 13 " + "1008" + " Dratini "
                                     + "12.6 983 Goldeen " + "12.4 965 " + "Charmander 12.2 955 Staryu 12 " + "937 "
                                     + "Ditto 11.8" + " 919 " + "Paras 11.7" + " 916 " + "Jigglypuff 11.6 917 " +
                                     "Tentacool " + "11.6" + " 905 " + "Pikachu 11.3 " + "887 " + "Magnemite " +
                                     "11.3 890 NidoranF 11.2 876 " + "Mankey " + "11.2 878 " + "Doduo 10.9 855 Onix " +
                                     "10.9 " + "857 Geodude 10.8 849 " + "NidoranM " + "10.7 " + "843 Voltorb " +
                                     "10.7 839 Vulpix 10.6 831 " + "Ekans " + "10.5 " + "824 " + "Shellder 10.4 822 " +
                                     "Sandshrew 10.1 " + "798 Poliwag 10.1 " + "795 " + "Gastly" + " 10.1 804 " +
                                     "Horsea 10 794 Krabby 10 792 Meowth " + "9.6 756" + " " + "" + "Spearow " + "8.6" +
                                     " 686 Pidgey 8.5 679 " + "Chansey 8 675 Zubat 8 642 Abra " + "7.4 " + "600 " +
                                     "Rattata" + " 7.2 581 Kakuna 5.9 485 Metapod " + "5.8 477 Weedle 5.4 " +
                                     "449 " + "Caterpie" + " 5.3 " + "443 Diglett 5.3 456 Magikarp 3 262";

static void fillMaps() {
    StringTokenizer token = new StringTokenizer(CPData, " ");
    String name = "";
    while (token.hasMoreTokens()) {
        name = token.nextToken();
        CPperPowerUp.put(name, Double.valueOf(token.nextToken()));
        CPLimits.put(name, Integer.valueOf(token.nextToken()));
    }
}
}
