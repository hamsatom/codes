package evolvator;

import org.apache.commons.lang3.StringUtils;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Collections;

import static evolvator.EvolutionDB.*;
import static evolvator.PowerUpScores.CPLimits;
import static evolvator.PowerUpScores.CPperPowerUp;
import static evolvator.SquadData.squadList;

public class Main {
private static int startDust;
private static final int dustPerPowerUp = 3500;

public static void main(String[] args) {
    BufferedReader br = new BufferedReader(new InputStreamReader(new BufferedInputStream(System.in)));
    System.out.println("How much stardust do you have?");
    try {
        startDust = Integer.parseInt(br.readLine());
    } catch (IOException e) {
        System.err.println("Input error" + e.getMessage());
        System.exit(1);
    }
    
    try {
        SquadData.loadSquad();
    } catch (IOException e) {
        System.err.println("Error while loading squad" + e.getMessage());
        System.exit(1);
    }
    
    EvolutionDB.fillEvulitionsData();
    PowerUpScores.fillMaps();
    
    int currentStarDust = startDust;
    int bestCP;
    int newDust = currentStarDust;
    byte number = 1;
    Pokémon removedPokémon;
    
    Collections.sort(squadList);
    
    while (!squadList.isEmpty()) {
        
        currentStarDust = newDust;
        bestCP = Integer.MIN_VALUE;
        
        for (Pokémon pokémon : squadList) {
            pokémon.dust = currentStarDust;
            
            findStrongestCombination(pokémon);
            
            if (pokémon.newCP > bestCP) {
                bestCP = pokémon.newCP;
                newDust = pokémon.dust;
            } else if (pokémon.newCP == bestCP && pokémon.dust > newDust) {
                newDust = pokémon.dust;
            }
        }
        
        Collections.sort(squadList);
        removedPokémon = squadList.remove(0);
        if (!removedPokémon.msg.toString().isEmpty()) {
            System.out.println(number++ + ". " + removedPokémon.toString());
        }
        
        for (Pokémon pokémon : squadList) {
            if (pokémon.name.equals(removedPokémon.name) || pokémon.evolutionName.equals(removedPokémon.name) ||
                pokémon.name.equals(removedPokémon.evolutionName) || pokémon.evolutionName.equals(removedPokémon
                                                                                                          .evolutionName)) {
                pokémon.startCandies = removedPokémon.currentCandies;
            } else if ((removedPokémon.name.endsWith("eon") || removedPokémon.evolutionName.endsWith("eon")) &&
                       (pokémon.name.endsWith("eon") || pokémon.evolutionName.endsWith("eon"))) {
                pokémon.startCandies = removedPokémon.currentCandies;
            }
            pokémon.newCP = pokémon.CP;
            pokémon.evolutionName = pokémon.name;
            pokémon.powerUpLeft = pokémon.startPowerUp;
            pokémon.msg = new StringBuilder();
            pokémon.currentCandies = pokémon.startCandies;
        }
        
    }
    
}

private static void findStrongestCombination(Pokémon pokémon) {
    Pokémon copy = new Pokémon(pokémon.name, pokémon.CP, pokémon.startCandies, pokémon.startPowerUp);
    copy.dust = pokémon.dust;
    copy.newCP = pokémon.newCP;
    copy.evolutionName = pokémon.evolutionName;
    copy.powerUpLeft = pokémon.powerUpLeft;
    copy.currentCandies = pokémon.currentCandies;
    copy.msg = new StringBuilder();
    copy.msg.append(pokémon.msg);
    
    evolve(copy);
    powerUp(pokémon);
    
    if (copy.newCP > pokémon.newCP) {
        copyToCurrent(pokémon, copy);
        return;
    }
    if (copy.newCP == pokémon.newCP) {
        if (copy.dust > pokémon.dust) {
            copyToCurrent(pokémon, copy);
            return;
        }
        
        if (copy.dust == pokémon.dust && copy.currentCandies >= pokémon.currentCandies && copy.powerUpLeft > pokémon.powerUpLeft) {
            copyToCurrent(pokémon, copy);
        }
        
    }
}

private static void copyToCurrent(Pokémon pokémon, Pokémon copy) {
    pokémon.newCP = copy.newCP;
    pokémon.powerUpLeft = copy.powerUpLeft;
    pokémon.evolutionName = copy.evolutionName;
    pokémon.currentCandies = copy.currentCandies;
    pokémon.dust = copy.dust;
    pokémon.msg = new StringBuilder();
    pokémon.msg.append(copy.msg);
}

static void evolve(Pokémon pokémon) {
    if (evolutinMultipliers.containsKey(pokémon.evolutionName) && pokémon.currentCandies >= candyCost.get(pokémon.evolutionName)) {
        pokémon.msg.append("Candies: " + pokémon.currentCandies);
        pokémon.currentCandies -= candyCost.get(pokémon.evolutionName) - 1;
        pokémon.msg.append(" -> " + pokémon.currentCandies + " ");
        pokémon.msg.append("Evolved from " + pokémon.evolutionName + " " + pokémon.newCP);
        pokémon.newCP *= evolutinMultipliers.get(pokémon.evolutionName);
        pokémon.evolutionName = evolutionNames.get(pokémon.evolutionName);
        pokémon.msg.append(" to " + pokémon.evolutionName + " " + pokémon.newCP + " ");
        
        findStrongestCombination(pokémon);
    }
}

static void powerUp(Pokémon pokémon) {
    if (!CPLimits.containsKey(pokémon.evolutionName)) {
        System.err.println(pokémon.evolutionName + " not found");
        pokémon.evolutionName = getMostSimilarName(pokémon.evolutionName);
        System.err.println("Used " + pokémon.evolutionName + " instead.");
    }
    if (pokémon.currentCandies >= 3 && pokémon.powerUpLeft > 0 && pokémon.dust >= dustPerPowerUp && pokémon.newCP <
                                                                                                    CPLimits.get(pokémon.evolutionName)) {
        pokémon.msg.append("Candies: " + pokémon.currentCandies);
        pokémon.currentCandies -= 3;
        pokémon.msg.append(" -> " + pokémon.currentCandies + " ");
        pokémon.powerUpLeft -= 1;
        pokémon.msg.append("Powered up from " + pokémon.newCP);
        pokémon.newCP += CPperPowerUp.get(pokémon.evolutionName);
        pokémon.msg.append(" to " + pokémon.newCP + " ");
        pokémon.msg.append("Stardust: " + pokémon.dust);
        pokémon.dust -= dustPerPowerUp;
        pokémon.msg.append(" -> " + pokémon.dust + " ");
        
        findStrongestCombination(pokémon);
    }
}

private static String getMostSimilarName(final String name) {
    double bestScore = Double.NEGATIVE_INFINITY;
    String mostSimilarNameFound = "";
    double currentSimilarity;
    
    for (String key : CPLimits.keySet()) {
        currentSimilarity = StringUtils.getJaroWinklerDistance(name, key);
        if (currentSimilarity > bestScore) {
            bestScore = currentSimilarity;
            mostSimilarNameFound = key;
        }
    }
    return mostSimilarNameFound;
}
}
