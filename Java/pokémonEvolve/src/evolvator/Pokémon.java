package evolvator;

class Pokémon implements Cloneable, Comparable<Pokémon> {
final String name;
final int CP;
int newCP;
short startCandies;
final byte startPowerUp;
byte powerUpLeft;
short currentCandies;
String evolutionName;
int dust;
StringBuilder msg = new StringBuilder();

Pokémon(String name, int CP, short startCandies, byte powerUpLeft) {
    this.name = name;
    this.CP = CP;
    this.startCandies = startCandies;
    this.startPowerUp = this.powerUpLeft = powerUpLeft;
    this.currentCandies = startCandies;
    this.evolutionName = name;
    this.newCP = CP;
}

@Override
public String toString() {
    return name + " " + CP + " turned into " + evolutionName + " " + newCP + " with " + currentCandies + " candies "
           + "and " + powerUpLeft + " powerUps left." + System.lineSeparator() + msg.toString();
}

@Override
protected Object clone() throws CloneNotSupportedException {
    return new Pokémon(name, CP, startCandies, powerUpLeft);
}

@Override
public int compareTo(Pokémon o) {
    return o.newCP - this.newCP;
}
}