%% Jist� v�hra
clear; close all; clc;
f = [0 0 0 0 0 -1];
A = [-1.27 -1.02 0 0 0 1
    0 -1.02 -4.7 -3.09 0 1
    0 0 0 -3.09 -9 1];
b = [0 0 0];

Aeq = [1 1 1 1 1 0];
beq = 3000;

lb = [0 0 0 0 0 2000];
ub = [];

x = linprog(f, A, b, Aeq, beq, lb, ub)
assert(x(end) > 2000)

% modifikovan� strategie
f = [0 0 0 -1];
A = [-1.27 0 0 1
    0 -4.7  0 1
    0 0 -9 1];
b = [0 0 0];

Aeq = [1 1 1 0];
beq = 3000;

lb = [400 400 400 2000];
ub = [];

x = linprog(f, A, b, Aeq, beq, lb, ub)
assert(x(end) > 2000)

%% Minimaxn� prokl�d�n� line�rn� funkce mno�inou bod�
clear; close all; clc;
load('data1.mat');
scatter(x, y, 'kx');
hold on
grid on
grid minor
xlabel('X sou�adnice')
ylabel('Y sou�adnice')
title('Mno�ina T')

A = [x ones(m,1) -ones(m,1)
    -x -ones(m,1) -ones(m,1)];
b = [y; -y];
f = [0 0 1];

abe = linprog(f, A, b)

range = linspace(0,1,m);
centre = abe(1)*range + abe(2);
plot(range, centre + abe(3), 'r--')
plot(range, centre, 'b')
plot(range, centre - abe(3), 'g--')
legend('Dan� body')
hold off