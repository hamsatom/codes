clear; close all; clc;
load('volby_2017.mat')


%% Strany
A = T.data';
A = A - mean(A);
[V,D] = eig(A'*A);
norm = 1 * D(:,1:end-2)
base = V(:, end-1:end) 
points = A * base;

dx = 0.05;
scatter(points(:,1), points(:,2), 50, cell2mat(T.color(:)), 'filled');
text(points(:,1)+dx, points(:,2),  T.strana_zkratka);
grid on
grid minor
title('Strany podle odpov�d� na ot�zky')

%% Preference
scatter(points(:,1), points(:,2), 2000*T.pref_last, cell2mat(T.color(:)), 'filled');
text(points(:,1)+dx, points(:,2),  T.strana_zkratka);
grid on
grid minor
title('Strany podle preferenc�')

%% Ot�zky
A = T.data;

colors = sum(A'==1);

A = A - mean(A);
[V, ~] = eig(A'*A);
points = A * V(:, end-1:end)

dx = 0.05;
scatter(points(:,1), points(:,2), 50, colors(:), 'filled');
text(points(:,1)+dx, points(:,2),  T.questions);
grid on
grid minor
title('Ot�zky podle odpov�d�')