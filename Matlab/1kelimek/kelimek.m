%% Graf zavislosti S na polomeru horni podstavy R
V = 0.6;
r = (0:0.2:1);
R = (0:0.01:1);
for index = 1:length(r)    
    S = cupArea(r(index), R, V);
    plot(R,S)
    hold on
end
hold off
grid on
grid minor
xlabel('Polomer horni podstavy R')
ylabel('Povrch S')
ylim([0,15])
legend('r=0', 'r=0.2', 'r=0.4', 'r=0.6', 'r=0.8', 'r=1')
title('Zavislosti S na polomeru horni podstavy R')

%% 3D plocha zavislosti S na r a R
V = 0.6;
r = (0.1:0.001:1);
R = (0.1:0.001:1);
S = nan(length(R));
for index = 1:length(R)    
    S(index, :) = cupArea(r, R(index), V);
end 
mesh(r,R,S)
grid on
grid minor
xlabel('Polomer dolni podstavy r')
ylabel('Polomer horni podstavy R')
zlabel('Povrch S')
title('Zavislosti S na r a R')

%% Minimalizace povrchu
V = 0.0003;
Sformula = @(r,R)pi*r^2+(pi*(R+r)*sqrt((3*V/(pi*(r^2+r*R+R^2)))^2+(R-r)^2));
[radiuses, S] = fminsearch(@(v) Sformula(v(1), v(2)), [0,0]);
r = radiuses(1)
R = radiuses(2)
h = 3*V/(pi*(r^2+r*R+R^2))
S