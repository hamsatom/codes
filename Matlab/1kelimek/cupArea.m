function [S] = cupArea(r, R, V)
%CUPAREA Funkce vypocita povrch kelimku pro zadane parametry
% Kel�mek m� tvar komol�ho ku�ele s jednou (tou men��) podstavou
% Polom�r spodn� podstavy si ozna��me r, polom�r horn� podstavy R
% a objem kel�mku V

h = 3*V./(pi*(r.^2+r.*R+R.^2));

S = pi*r.^2+(pi*(R+r).*sqrt(h.^2+(R-r).^2));

end