function [xFree,yFree] = findFreePlacement(A, xFixed, yFixed)

[K, N] = size(A);
n = N - length(xFixed);

A1 = A(1:K, 1:n);
A2 = A(1:K,  n+1:N);
fixed = [xFixed yFixed];

result = A1\(-A2*fixed);
xFree = result(:,1);
yFree = result(:,2);

end

