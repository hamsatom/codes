clear; close all; clc;
 
fileGong = 'gong.wav';
 
[y,Fs]=audioread(fileGong);
%sound(y,Fs)
 
p = 300;
T = length(y) - 1;
 
%% Nejmen�� �tverce
M = nan(T-p+1,p+1);
M(1:end,1) = 1;
for row = 1:length(M)
    yStart = p+row-1;
    M(row, 2:end) = y(yStart:-1:row);
end
 
b = y(p+1:T+1);
 
a2 = M\b;
minA = norm(M*a2-b)^2
 
%% QR rozklad
a1 = solve_ls(M,b);
distance = norm(a1-a2)
 
%% Syntetick� gong
y2 = nan(T+1,1);
y2(1:p) = y(1:p);

as = a1(2:p+1).';
for index = p+1:T+1  
    ys = y2(index-1:-1:index-p);
    y2(index) = a1(1) + as*ys;
end
 
plot(1:T+1, y, 1:T+1, y2)
grid on
grid minor
ylim([-0.5 0.5])
xlabel('�as')
ylabel('Sign�l')
legend('Origin�ln� gong', 'Syntetick� gong')
title('Porovn�n� syntetick�ho a origin�ln�ho gongu')
 
%sound(y2,Fs)