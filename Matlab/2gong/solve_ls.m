function x = solve_ls(A,b)
%solve_ls Pocita nejmensi ctverce pomoci QR rozkladu

[Q, R] = qr(A,0);
right = Q.'*b;

x = nan(length(right),1);

x(end) = right(end)/R(end,end);
for row = length(x)-1:-1:1
    currentXIndex = row+1;
    results = x(currentXIndex:end);
    koeficients = R(row, currentXIndex:end);
    x(row) = (right(row) - koeficients*results) / R(row,row);
end

end