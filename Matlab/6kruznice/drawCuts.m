function drawCuts(A)

abc = [1,2,20];
for ind = 1:3
    m = abc(ind);
    count = 1500;
    xs = linspace(-10,10,count);
    ys = linspace(-10,10,count);
    values = zeros(count);
    for index = 1:count
        for inner = 1:count
            for l = 1:m
                values(index, inner) = values(index, inner) + (sqrt((xs(index)-A(l,1))^2 + (ys(inner)-A(l,2))^2) - 1)^2;
            end
        end
    end
    
    figure
    subplot(2,2,1)
    contour(xs,ys,values);
    hold on
    grid on
    grid minor
    xlabel('X sou�adnice')
    ylabel('Y sou�adnice')
    zlabel('Z sou�adnice')
    plot(A(1:m,1), A(1:m,2), 'k+');
    legend('Vrstevnice', 'Dan� body')
    title(strcat('Vrstevnice pro m = ', num2str(m)))
    
    subplot(2,2,2)
    mesh(xs,ys,values);
    hold on
    grid on
    grid minor
    xlabel('X sou�adnice')
    ylabel('Y sou�adnice')
    zlabel('Hodnota funkce')
    title(strcat('�ez pro m = ', num2str(m)))
    
    subplot(2,2,[3 4])
    plot(values)
    hold on
    grid on
    grid minor
    title(strcat('Jednodimenzion�ln� �ez pro m = ', num2str(m)))
    
    hold off
end

end

