function d=dist(a,x)
    d = sqrt( (x(1)-a(1))^2 + (x(2)-a(2))^2) - x(3);
end

