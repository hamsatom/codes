function x = fit_circle(method)
load('points.mat', '-mat');

scatter(A(:,1), A(:,2), 'yx')
hold on
grid on
grid minor
axis equal
axis manual
xlabel('X sou�adnice')
ylabel('Y sou�adnice')
title('Fit circle')
legend('Dan� body')

[cx0, cy0] = ginput(1);
plot(cx0, cy0, 'rx');
hold on
[cx1, cy1] = ginput(1);
hold on

c0 = [cx0 cy0];
c1 = [cx1 cy1];
r0 = sqrt(sum((c0-c1).^2));

kx = c0(1) + r0 * cos(0:0.001:2*pi);
ky = c0(2) + r0 * sin(0:0.001:2*pi);
scatter(kx, ky, 'r.')
legend('Dan� body', 'Zadan� kru�nice')
hold off

x = c0(1);
y = c0(2);
r = r0;

J = nan([length(A) 3]);
for index = 1:length(A)
    dx = (x-A(index,1))/sqrt((x-A(index,1))^2 + (y-A(index,2))^2);
    dy = (y-A(index,2))/sqrt((x-A(index,1))^2 + (y-A(index,2))^2);
    dr = -1;
    J(index, :) = [dx dy dr];
end

mi = 1;
dmi = 10;
alpha = 1;
p = [x y r];
bestDistance = 0;
for index = 1:length(A)
    bestDistance = bestDistance + dist(A(index,:), p)^2;
end

%drawCuts(A)
while true
    distance = 0;
    for index = 1:length(A)
        distance = distance + dist(A(index,:), p)^2;
    end
    disp(distance)
    
    [~, ~, key] = ginput(1);
    if (isempty(key))
        close
        break;
    end
    if (key ~= 32)
        continue;
    end
    
    JJ = nan([length(A) 3]);
    GG = nan([length(A) 1]);
    for index = 1:length(A)
        GG(index) = sqrt((x-A(index,1))^2 + (y-A(index,2))^2) - r;
        dx = (x-A(index,1))/sqrt((x-A(index,1))^2 + (y-A(index,2))^2);
        dy = (y-A(index,2))/sqrt((x-A(index,1))^2 + (y-A(index,2))^2);
        dr = -1;
        JJ(index, :) = [dx dy dr];
    end
    
    if method == 'GN'
        p = [x;y;r] - inv(JJ'*JJ)*JJ' * GG;
        
    elseif method == 'LM'
        xx0 = p;
        p = [x;y;r] - inv( JJ'*JJ + mi*eye(3) )*JJ' * GG;
        distance = 0;
        for index = 1:length(A)
            distance = distance + dist(A(index,:), p)^2;
        end
        if (distance < bestDistance)
            bestDistance = distance;
            mi = mi / dmi;
        else
            mi = dmi * mi;
            p = xx0;
        end
        
    elseif method == 'GR'
        xx0 = p;
        p = [x;y; r] - alpha * JJ';
        distance = 0;
        for index = 1:length(A)
            distance = distance + dist(A(index,:), p)^2;
        end
        if (distance < bestDistance)
            bestDistance = distance;
            alpha = alpha*2;
        else
            alpha = alpha/2;
            p = xx0;
        end
    end
    
    x = p(1);
    y = p(2);
    r = p(3);
    
    clf
    scatter(A(:,1), A(:,2), 'yx')
    hold on
    grid on
    grid minor
    axis equal
    axis manual
    xlabel('X sou�adnice')
    ylabel('Y sou�adnice')
    title('Fit circle')
    
    plot(x,y, 'r+');
    hold on
    
    kx = x + r * cos(0:0.001:2*pi);
    ky = y + r * sin(0:0.001:2*pi);
    scatter(kx, ky, 'r.')
    legend('Dan� body', 'Nalezen� kru�nice')
    hold off
    
end

x = [x; y; r];

end

