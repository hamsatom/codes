clear; close all; clc;
xlabel('X sou�adnice')
ylabel('Y sou�adnice')
title('Enter points')
hold on
grid on
grid minor
axis equal
axis manual

A = nan(0,2);

while true
    [x, y, key] = ginput(1);
    if (isempty(key))
        hold off
        close
        break;
    end
    
    A(end+1,1) = x;
    A(end,2) = y;
    
    scatter(A(end,1), A(end,2), 'bx')
    hold on
    legend('Zadan� body')
end

save('points.mat', 'A', '-mat');