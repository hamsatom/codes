clear; close all; clc; 

author = 'Tom� Hamsa, st�ede�n� cvi�en�'

%{
Given values
%}
% proton charge
Q = 1.602 * 1e-19;
% proton mass
m = 1.673 * 1e-31;
% proton velocity
v0 = 1 * 1e7;
% magnetic field intensity
B = 0.1;
% electric field intensity
E = 1 * 1e5;

% time interval
t = linspace(0, 1e-9, 1001);

%{
Calculations
%}
% velocity of the proton along the z axis
v = ((Q*E)/m).*t + v0;
% travelled distance along the zaxis
z = (0.5*((Q*E)/m)).*(t.^2)+v0.*t;
% radius of the helix
r = (v.*m)./(B*Q);
% frequency of orbiting the helix
f = v./((2*pi).*r);
% x coordinates of the proton
x = r.*cos(((2*pi).*f).*t);
% y coordinates of the proton
y = r.*sin(((2*pi).*f).*t);

% plot of the path in space in the time interval
comet3(x, y, z)

