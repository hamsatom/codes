function logicState  = createToggles(nRows, nColumns)
% function generating GUI with toggle buttons
% This function creates nRows x nColumns toggle buttons in dynamamically
% sized window. If window is larger than monitor's resolution than the
% resolutin is used.
% As result return matrix with logical values for toggle buttons

H = gobjects(nRows,nColumns);
for iRow = 1:nRows
    for iCol = 1:nColumns
        H(nRows-iRow+1,iCol) = uicontrol('Style', 'togglebutton', 'Position', [(10*iCol+40*(iCol-1)) (10*iRow+40*(iRow-1)) 40 40], 'String', strcat('(',num2str(nRows-iRow+1),', ', num2str(iCol), ')'), 'Callback', 'uiresume(gcbf)');
    end
end
fig = gcf;
set(fig, 'MenuBar', 'None')
Pix_SS = get(0,'screensize');
monitorHeight = Pix_SS(3);
monitorWidth = Pix_SS(4)-30;
windowHeight = nColumns*(40+10)+10;
windowWidth = nRows*(40+10)+10;

set(fig, 'Position', [monitorHeight/2-windowHeight/3, monitorWidth/2-windowHeight/3, min(windowHeight,monitorHeight) , min(monitorWidth, windowWidth) ]);

logicState = NaN(nRows, nColumns);

while(ishandle(fig))
for iRow = 1:nRows
    for iCol = 1:nColumns
        logicState(iRow,iCol) = H(iRow,iCol).Value;
   end
end
uiwait(fig);
end


waitfor(fig);


