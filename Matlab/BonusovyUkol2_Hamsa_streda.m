clear; clc;
%% allocation
days = 5; hours = 12;
TimeA = zeros(days*hours,1);
TimeB = TimeA;
TimeC = TimeA;
%% creation of time data-set
for kDay = 1:days
TimeA((hours*(kDay-1)+1):(hours*(kDay-1)+12),1) = 2*(randperm(12)-1)';
TimeB((hours*(kDay-1)+1):(hours*(kDay-1)+12),1) = 2*(randperm(12)-1)';
TimeC((hours*(kDay-1)+1):(hours*(kDay-1)+12),1) = 2*(randperm(12)-1)';
end
%% place and tempreture data-sets
PlaceA = abs(abs(TimeA - 11) - 10) + 10 + 5.0*rand(size(TimeA,1),1);
PlaceB = abs(abs(TimeB - 12) - 10) + 5 + 10.0*rand(size(TimeB,1),1);
PlaceC = abs(abs(TimeC - 11) - 11) + 5 + 7.5*rand(size(TimeC,1),1);
%% generating final variables for the example
TimeAndPlace = [TimeA/2+1 ones(size(TimeA,1),1);...
TimeB/2+1 2*ones(size(TimeA,1),1);...
TimeC/2+1 3*ones(size(TimeA,1),1)];
MeasuredData = [PlaceA; PlaceB; PlaceC];
%% plot final data-set
plot(TimeA,PlaceA,'LineWidth',1,'LineStyle','none','Marker','x',...
'MarkerSize',15); hold on;
plot(TimeB,PlaceB,'LineWidth',1,'LineStyle','none','Marker','*',...
'MarkerSize',15,'Color','r');
plot(TimeC,PlaceC,'LineWidth',2,'LineStyle','none','Marker','o',...
'MarkerSize',10,'Color','g');
set(gcf,'Color','w','pos',[50 50 1000 600]); set(gca,'FontSize',15);
xlabel('time','FontSize',15); ylabel('Temperature','FontSize',15);
title('Measured Data'); grid on; legend('Place A','Place B','Place C');
%% PLACE YOUR CODE HERE
%========================================================================== 
data = accumarray(TimeAndPlace, MeasuredData, [], @mean);
dataA = data(1:end,1);
dataB = data(1:end,2);
dataC = data(1:end,3);
%==========================================================================
%% plot the averaged data
plot(0:2:22,dataA,'LineWidth',2,'Color','b','LineStyle','-');
plot(0:2:22,dataB,'LineWidth',2,'Color','r','LineStyle','-');
plot(0:2:22,dataC,'LineWidth',2,'Color','g','LineStyle','-');