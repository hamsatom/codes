%% Aproximace
% Inspiroval jsem se z kodu ze cviceni 6 od pana Cecha
clear; close all; clc;
load('data_A.mat')

meanA = mean(A);
A2 = bsxfun(@minus, A, meanA);
[V, D] = eig(A2'*A2);
distance = sum(D(:, 1:end-1))
Y = V(:, end)
B = bsxfun(@plus, A2 * (Y * Y'),  meanA); 

plot(A(:,1), A(:,2), 'bo'); 
hold on
plot(meanA(1), meanA(2), 'k+', 'markersize', 40);
hold on
plot(B(:,1), B(:,2), 'ro');
hold on

meanA = meanA';

normal = [Y(2,:); -Y(1,:)]
normNormal = norm(normal)
alpha = meanA' * normal

normY = norm(Y)
t = Y \ meanA; 
p1 = meanA + t*Y; 
p2 = meanA - t*Y 
p = [p1,p2];
plot(p(1, :), p(2,:), 'g'); 
hold on

for i=1:length(A)
	AB = [A(i, 1), B(i, 1); A(i, 2), B(i,2)]; 
	plot(AB(1,:), AB(2,:), ':g');     
    hold on
end
hold off

axis equal
grid on
grid minor
xlabel('X sou�adnice')
ylabel('Y sou�adnice')
legend('Zadan� body', 'T�i�t� bod�', 'Projekce')
title('Body prolo�en� p��mkou')

%% Komprese
clear; close all; clc;

A = load('walk1.txt','-ASCII');
[~,r1] = fitAffin(A, 1)
[~,r2] = fitAffin(A, 2)
[~,r5] = fitAffin(A, 5)
[~,r10] = fitAffin(A, 10)
[~,r15] = fitAffin(A, 15)

[V11, ~] = fitAffin(A, 2);
figure
plot(V11(:,1), V11(:,2))
xlabel('X sou�adnice')
ylabel('Y sou�adnice')
grid on
grid minor
title('2D trajektorie ch�ze')

[V12, ~] = fitAffin(A, 3);
figure
plot3(V12(:,1), V12(:,2), V12(:,3))
xlabel('X sou�adnice')
ylabel('Y sou�adnice')
zlabel('Z sou�adnice')
grid on
grid minor
title('3D trajektorie ch�ze')

A = load('makarena1.txt','-ASCII');
[V21, ~] = fitAffin(A, 2);
figure
plot(V21(:,1), V21(:,2))
xlabel('X sou�adnice')
ylabel('Y sou�adnice')
grid on
grid minor
title('2D trajektorie makareny')

[V22, ~] = fitAffin(A, 3);
figure
plot3(V22(:,1), V22(:,2), V22(:,3))
xlabel('X sou�adnice')
ylabel('Y sou�adnice')
zlabel('Z sou�adnice')
grid on
grid minor
title('3D trajektorie makareny')