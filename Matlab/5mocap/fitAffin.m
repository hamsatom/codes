function [y, optimum] = fitAffin(A,r)

meanA = mean(A);
A2 = bsxfun(@minus, A, meanA);
[~, ~, V] = svd(A2, 'econ');
Y = V(:, 1:r);
B = bsxfun(@plus, A2 * (Y * Y'),  meanA); 
optimum = norm(A - B, 'fro')^2;
y = A2 * Y;

end