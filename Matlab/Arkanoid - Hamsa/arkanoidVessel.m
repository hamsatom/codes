classdef arkanoidVessel < handle
    %   Object representing vessel that players control
    %   User moves with this vessel with arrow keys or a,d. If ball hits the vessel it bounces up.
    
    properties
        % Current position of vessel
        posX = 3; 
        % Amout of bricks left on the map
        bricksLeft = 9;
        % Marks status of game
        gameEnd = false;
    end
    
    methods
    end
    
end

