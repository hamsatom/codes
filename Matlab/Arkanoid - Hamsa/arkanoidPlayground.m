classdef arkanoidPlayground < handle
    %   Object that represent whole playing field
    %   Playing field that stores all objects on it
    
    properties
        % Size of playground
        XSize
        YSize
        
        % Handle to stored obejcts
        objects@arkanoidField
    end
    
    methods
    end
    
end

