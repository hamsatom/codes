classdef arkanoidField
    %   Object that represent one field of the playground
    %   One field which contains some strucuture.
    
    properties
        % Attributes
        isBorder = true;
        isPlayer = false;
        isBall = false;
        isBrick = false;
        
        % Number of lives the brick has left
        lifesLeft = 2;
        
        % Image
        mainImg;
        altImg;
    end
    
    methods
        % Equals
        function test = eq(A, B)
            test = isequal(A.mainImg, B.mainImg);
        end
        % NotEqual
        function test = ne(A, B)
            test = ~isequal(A.mainImg, B.mainImg);
        end
    end
    
end

