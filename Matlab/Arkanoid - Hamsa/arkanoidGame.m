function arkanoidGame
    % Start Arkanoid game in figure
    clc; close all;

    % Adding game objects to instance
    gameInstance = addObjectsToInstance();

    window.fig = figure('name', 'Arkanoid',...
        'numbertitle', 'off',...
        'menubar', 'none',...
        'resize', 'off');

    % Placing objects on their postiions on map
    placeObjectsOnMap(gameInstance);

    % Setting image, otherwise is map upside down
    image([0 gameInstance.map.YSize*32],[0 gameInstance.map.XSize*32], zeros(gameInstance.map.XSize*32, gameInstance.map.YSize*32, 3));

    % Displaying objects stored in map
    displayObjects(gameInstance);

    % Adding handler for keyboard
    window.fig.WindowKeyPressFcn = {@handleKeyboard, gameInstance};
    window.fig.CloseRequestFcn = {@endFigure, gameInstance, window};

    axis off
    axis image

    % Move with the ball
    moveBall(gameInstance, window);
end

function gameInstance = addObjectsToInstance()
    % Adds object to game instance
    
    clear gameInstance.objects;

    %IDs
    WALL_ID = 1;
    BLANK_PLAYING_FIELD_ID = 2;
    BALL_ID = 3;
    BRICK_ID = 4;
    VESSEL_ID = 5;

    % Map size
    X_SIZE = 10;
    Y_SIZE = 5;

    gameInstance.objects = arkanoidField;

    gameInstance.id.wall = WALL_ID;
    gameInstance.objects(WALL_ID) = arkanoidField;
    gameInstance.objects(WALL_ID).mainImg = imread('wall.png');

    gameInstance.id.playingField = BLANK_PLAYING_FIELD_ID;
    gameInstance.objects(BLANK_PLAYING_FIELD_ID) = arkanoidField;
    gameInstance.objects(BLANK_PLAYING_FIELD_ID).isBorder = false;
    gameInstance.objects(BLANK_PLAYING_FIELD_ID).mainImg = imread('playingField.png');

    gameInstance.id.ball = BALL_ID;
    gameInstance.objects(BALL_ID) = arkanoidField;
    gameInstance.objects(BALL_ID).isBall = true;
    gameInstance.objects(BALL_ID).mainImg = imread('ball.png');

    gameInstance.id.brick = BRICK_ID;
    gameInstance.objects(BRICK_ID) = arkanoidField;
    gameInstance.objects(BRICK_ID).altImg = imread('brick.png');
    gameInstance.objects(BRICK_ID).mainImg = imread('brick_2lifes.png');
    gameInstance.objects(BRICK_ID).isBrick = true;
    gameInstance.objects(BRICK_ID).isBorder = false;

    gameInstance.id.vessel = VESSEL_ID;
    gameInstance.objects(VESSEL_ID) = arkanoidField;
    gameInstance.objects(VESSEL_ID).mainImg = imread('vessel.png');
    gameInstance.objects(VESSEL_ID).isPlayer = true;
    gameInstance.objects(VESSEL_ID).isBorder = false;

    % player
    gameInstance.player = arkanoidVessel;

    % map
    gameInstance.map = arkanoidPlayground;
    gameInstance.map.XSize = X_SIZE;
    gameInstance.map.YSize = Y_SIZE;

    % ball
    gameInstance.ball = arkanoidBall;

    % keyboard handle
    gameInstance.keyboard.block = false;

    clear gameInstance.map.objects;
end

function placeObjectsOnMap(gameInstance)
    % Puts object on map according to matrix of placement

    % IDs
    % WALL_ID = 1;
    % BLANK_PLAYING_FIELD_ID = 2;
    % BALL_ID = 3;
    % BRICK_ID = 4;
    % VESSEL_ID = 5;

    % background
    map(:,:) = ...
        [1 1 1 1 1;
        1 4 4 4 1;
        1 4 4 4 1;
        1 4 4 4 1;
        1 2 2 2 1;
        1 2 2 2 1;
        1 2 2 2 1;
        1 2 2 2 1;
        1 2 3 2 1;
        1 2 5 2 1];
        
    % Number of bricks in map
    BRICKS_COUNT = 9;

    clear gameInstance.map.objects;
    
    % Place object on their individual position according to placement
    % matrix and set positions inside the objects
    for xCord=1:gameInstance.map.XSize
        for yCord=1:gameInstance.map.YSize            
            gameInstance.map.objects(yCord, xCord) = gameInstance.objects(map(xCord, yCord));
            if (map(xCord, yCord) == gameInstance.id.ball)
                gameInstace.ball.posX = xCord;
                gameInstace.ball.poxY = yCord;
            end
            if (map(xCord, yCord) == gameInstance.id.vessel)
                gameInstace.player.posX = xCord;
                gameInstace.player.bricksLeft = BRICKS_COUNT;
            end
        end
    end
end

function displayObjects(gameInstance)
    % Displays objects placed on the map
    
    % Show picture from each object
    for xCord=1:gameInstance.map.XSize
        for yCord=1:gameInstance.map.YSize
            image('XData', (yCord-1)*32, 'YData', (xCord-1)*32, 'CData', gameInstance.map.objects(yCord, xCord).mainImg);
        end
    end
end

function handleKeyboard(~, event, gameInstance)
    % Moves with vessel when user press some key
    
    % If input is blocked
    if (gameInstance.keyboard.block)
        return;
    end

    % Block other inputs
    gameInstance.keyboard.block = true;

    % Return if the game is over
    if (gameInstance.player.gameEnd)
        return;
    end

    % Right most valid index
    RIGHT_Y_BORDER = 4;
    % Left most valid index
    LEFT_Y_BORDER = 2;
    % Level in which vessel howers
    Y_SURFACE = 10;

    % X position change after move
    advanceX = 0;

    switch event.Key
        case 'leftarrow'
            advanceX = -1;
        case 'rightarrow'
            advanceX = 1;
        case 'a'
            advanceX = -1;
        case 'd'
            advanceX = 1;
    end

    % Previous position of vessel
    oldPos = gameInstance.player.posX;

    % Check if new position is valid
    if (gameInstance.player.posX + advanceX <= RIGHT_Y_BORDER && gameInstance.player.posX + advanceX >= LEFT_Y_BORDER)
        gameInstance.player.posX = gameInstance.player.posX + advanceX;
    end

    % If we moved with vessel, update pictures in figure
    if (oldPos ~= gameInstance.player.posX)
        image('XData', (oldPos-1)*32, 'YData', (Y_SURFACE-1)*32, 'CData', gameInstance.objects(gameInstance.id.playingField).mainImg);
        image('XData', (gameInstance.player.posX-1)*32, 'YData', (Y_SURFACE-1)*32, 'CData', gameInstance.objects(gameInstance.id.vessel).mainImg);
        gameInstance.map.objects(oldPos, Y_SURFACE) = gameInstance.objects(gameInstance.id.playingField);
        gameInstance.map.objects(gameInstance.player.posX, Y_SURFACE) = gameInstance.objects(gameInstance.id.vessel);
    end
    
    % Release block of keyboard
    gameInstance.keyboard.block = false;
end

function moveBall(gameInstance, window)
    % Responsible for all movement of ball
   
    % Top most index
    TOP_BORDER = 1;
    % Bottom most index
    BOTTOM_BORDER = 10;
    % Right most valid index
    RIGHT_Y_BORDER = 4;
    % Left most index
    LEFT_Y_BORDER = 1;

    % Current ball position
    lastPosX = gameInstance.ball.posX;
    lastPosY = gameInstance.ball.posY;

    % New position
    directionX = -1;
    newX = lastPosX + directionX;
    newY = randi(RIGHT_Y_BORDER-LEFT_Y_BORDER) + LEFT_Y_BORDER;
    directionY = newY - lastPosY;

    while(gameInstance.player.gameEnd == false)
        pause(0.6)
        % Update pictures if ball is not on vessel
        if (gameInstance.map.objects(newY, newX).isPlayer == false && ~gameInstance.player.gameEnd)
            image('XData', (lastPosY-1)*32, 'YData', (lastPosX-1)*32, 'CData', gameInstance.objects(gameInstance.id.playingField).mainImg);
            image('XData', (newY-1)*32, 'YData', (newX-1)*32, 'CData', gameInstance.objects(gameInstance.id.ball).mainImg);
           
            lastPosX = newX;
            lastPosY = newY;

            newX = lastPosX + directionX;
            newY = lastPosY + directionY;
        end

        % If ball hit bottom, lost game
        if (lastPosX >= BOTTOM_BORDER)
            msgbox('You lost!')
            gameInstance.player.gameEnd = true;
            endFigure(nan, nan, gameInstance, window)
            break;
        end

        % If ball hit wall bounce
        if (gameInstance.map.objects(newY, newX).isBorder == true)
            directionY = directionY * -1;
            newY = lastPosY + directionY;
        end

        % If ball hit top wall start moving down
        if (newX <= TOP_BORDER)
            directionX = directionX * -1;
        end

        % If ball hit brick and meanwhile game was not ended update figure
        if (gameInstance.map.objects(newY, newX).isBrick == true)
            pause(0.5)
            % Decrement brcik'S lifes
            gameInstance.map.objects(newY, newX).lifesLeft = gameInstance.map.objects(newY, newX).lifesLeft -1;
            % If brick has lifes left change is image
            if (gameInstance.map.objects(newY, newX).lifesLeft > 0 && ~gameInstance.player.gameEnd)
                image('XData', (newY-1)*32, 'YData', (newX-1)*32, 'CData', gameInstance.objects(gameInstance.id.brick).altImg);
            end
            % brick has no lifes left, change brick to blank playing field
            % and decerement number of bricks on map
            if (gameInstance.map.objects(newY, newX).lifesLeft <= 0 && ~gameInstance.player.gameEnd)
                image('XData', (newY-1)*32, 'YData', (newX-1)*32, 'CData', gameInstance.objects(gameInstance.id.playingField).mainImg);
                gameInstance.map.objects(newY, newX) = gameInstance.objects(gameInstance.id.playingField);
                gameInstance.player.bricksLeft = gameInstance.player.bricksLeft - 1;
            end
            % if all bricks are gone that means player won
            if (gameInstance.player.bricksLeft <= 0)
                msgbox('You won!');
                endFigure(nan, nan, gameInstance, window)
            end
            % bounce ball of brick
            directionX = directionX * -1;
        end
        
        % Ball hit vessel, bounce it of the vessel to random direction
        if (gameInstance.map.objects(newY, newX).isPlayer == true)
            upperRandomPos = min(gameInstance.player.posX+1, RIGHT_Y_BORDER);
            lowerRandomPos = max(gameInstance.player.posX-1, 2);
            newY = randi([lowerRandomPos upperRandomPos]);
            directionX = directionX * -1;
        end
        
        % Change horizonatl position
        newX = lastPosX + directionX;
    end
end

function endFigure(~, ~, gameInstance, window)
    % End movement of the ball
    gameInstance.player.gameEnd = true;   
    % Wait for ball to stop moving
    pause(0.6)
    delete(window.fig);
end
