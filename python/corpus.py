__author__ = 'MuLTiHuNTeR'
"""Generator emailu"""

import os


class Corpus:
    # Nacte jmeno a obsah souboru nezacinajici !, bere cestu k souborum jako argument
    def __init__(self, emails):
        self.maily = emails

    def emails(self):
        for fileName in os.listdir(self.maily):
            if not fileName.startswith("!"):
                body = open(self.maily + "/" + fileName, 'r', encoding="utf-8")
                yield fileName, body.read()
        body.close()
