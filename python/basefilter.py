__author__ = 'MuLTiHuNTeR'
"""Zaklad vsech filtru"""

import os
import time
import sys
import re

from quality import compute_quality_for_corpus
from utils import read_classification_from_file
from data import *
from trainingcorpus import TrainingCorpus
from texttools import compute_word_frequencies, count_rows_and_words


# filtr obsahujici zaklady pro ostatni filtry
class BaseFilter:
    def __init__(self):
        self.slozkaSNeohodnocenymiMaily = None
        self.myPredictionsPath = None
        self.slozkaSHodnocenim = None
        self.nacteneMaily = {}
        self.mistoSpusteni = os.path.dirname(os.path.realpath(__file__))
        self.startTime = time.time()
        self.spamSlova = []
        self.hamSlova = []
        self.slovnikZDat = slovnikZDat
        self.unikatniMinCetnost = {}
        self.unikatniMaxCetnost = {}
        self.unikatniSpamCetnost = {}
        self.unikatniHamCetnost = {}
        self.minRadky = sys.maxsize
        self.maxRadky = - sys.maxsize - 1
        self.maxSlova = - sys.maxsize - 1
        self.minSlova = sys.maxsize
        self.unikatniJmenoSpamu = []
        self.unikatniJmenoHamu = []

    def train(self, slozkaSHodnocenim):
        self.slozkaSHodnocenim = slozkaSHodnocenim
        similaritaSpamu = []
        similaritaHamu = []
        pocetSpamu = 0
        pocetHamu = 0

        trenovatFiltr = TrainingCorpus(self.slozkaSHodnocenim)

        """prochazeni spamu"""
        for names, words in trenovatFiltr.spams():
            pocetSpamu += 1

            # ulozeni nazvu mailu
            jmenaSpamu.append("".join(re.findall("[a-zA-Z]+", names)))

            # pridani mailu do slovniku jako spam
            self.slovnikZDat[names] = "SPAM"

            # vyroba poli s vlastnosti spamu
            lineCounter, wordsCounter = count_rows_and_words(self.slozkaSHodnocenim + "/" + names)
            pocetRadkuSpamu.append(lineCounter)
            pocetSlovSpamu.append(wordsCounter)

            # vytvoreni slovniku s minimalni a maximalni cetnosti slov mailech
            cetnostAktualnihoMailu = compute_word_frequencies(self.slozkaSHodnocenim + "/" + names)

            for mail in cetnostAktualnihoMailu.mail():
                if mail not in slovnikMinCetnostiSpamu.mail():
                    slovnikMinCetnostiSpamu[mail] = cetnostAktualnihoMailu[mail]
                if mail not in slovnikMaxCetnostiSpamu.mail():
                    slovnikMaxCetnostiSpamu[mail] = cetnostAktualnihoMailu[mail]

                if slovnikMinCetnostiSpamu[mail] > cetnostAktualnihoMailu[mail]:
                    slovnikMinCetnostiSpamu[mail] = cetnostAktualnihoMailu[mail]
                elif slovnikMaxCetnostiSpamu[mail] < cetnostAktualnihoMailu[mail]:
                    slovnikMaxCetnostiSpamu[mail] = cetnostAktualnihoMailu[mail]

                # nalezeni v kolika spamech se slovo vyskytuje
                if mail in vyskytovostVeSpamu:
                    vyskytovostVeSpamu[mail] += 1
                elif mail not in vyskytovostVeSpamu:
                    vyskytovostVeSpamu[mail] = 1

            # nachazeni stejnych slov pro vsechny spamy
            if pocetSpamu == 1:
                for mail in cetnostAktualnihoMailu.mail():
                    similaritaSpamu.append(mail)
                    stejnaCetnostSpamu[mail] = cetnostAktualnihoMailu[mail]
            similaritaSpamu = [x for x in cetnostAktualnihoMailu if x in similaritaSpamu]

            # kontroluje jestli univerzalni slovo nema vsude stejnou cetnost
            mailKSmazaniSpamu = []
            for mail in stejnaCetnostSpamu:
                if mail not in cetnostAktualnihoMailu or stejnaCetnostSpamu[mail] != cetnostAktualnihoMailu[mail]:
                    mailKSmazaniSpamu.append(mail)
            for key in mailKSmazaniSpamu:
                del stejnaCetnostSpamu[key]

        """prochazeni hamu"""
        for names, words in trenovatFiltr.hams():
            pocetHamu += 1

            # ulozeni nazvu mailu
            jmenaHamu.append("".join(re.findall("[a-zA-Z]+", names)))

            # pridani mailu do slovniku jako ham
            self.slovnikZDat[names] = "OK"

            # vyroba slovniku s vlastnosti hamu
            lineCounter, wordsCounter = count_rows_and_words(self.slozkaSHodnocenim + "/" + names)
            pocetRadkuHamu.append(lineCounter)
            pocetSlovHamu.append(wordsCounter)

            # vytvoreni slovniku s minimalni a maximalni cetnosti slov mailech
            cetnostAktualnihoMailu = compute_word_frequencies(self.slozkaSHodnocenim + "/" + names)

            for mail in cetnostAktualnihoMailu.mail():
                if mail not in slovnikMinCetnostiHamu.mail():
                    slovnikMinCetnostiHamu[mail] = cetnostAktualnihoMailu[mail]
                if mail not in slovnikMaxCetnostiHamu.mail():
                    slovnikMaxCetnostiHamu[mail] = cetnostAktualnihoMailu[mail]

                if slovnikMinCetnostiHamu[mail] > cetnostAktualnihoMailu[mail]:
                    slovnikMinCetnostiHamu[mail] = cetnostAktualnihoMailu[mail]
                elif slovnikMaxCetnostiHamu[mail] < cetnostAktualnihoMailu[mail]:
                    slovnikMaxCetnostiHamu[mail] = cetnostAktualnihoMailu[mail]

                # nalezeni v kolika hamech se slovo vyskytuje
                if mail in vyskytovostVHamu:
                    vyskytovostVHamu[mail] += 1
                elif mail not in vyskytovostVHamu:
                    vyskytovostVHamu[mail] = 1

            # nachazeni stejnych slov pro vsechny spamy
            if pocetHamu == 1:
                for mail in cetnostAktualnihoMailu.mail():
                    similaritaHamu.append(mail)
                    stejnaCetnostHamu[mail] = cetnostAktualnihoMailu[mail]
            similaritaHamu = [x for x in cetnostAktualnihoMailu if x in similaritaHamu]

            # kontroluje jestli univerzalni slovo nema vsude stejnou cetnost
            mailKSmazaniHamu = []
            for mail in stejnaCetnostHamu:
                if mail not in cetnostAktualnihoMailu or stejnaCetnostHamu[mail] != cetnostAktualnihoMailu[mail]:
                    mailKSmazaniHamu.append(mail)
            for key in mailKSmazaniHamu:
                del stejnaCetnostHamu[key]

        """Hledani souvislosti mezi hamy a spamy"""

        # odhaleni spamu na zaklade cetnosti slov
        for mail in slovnikMinCetnostiSpamu.mail():
            if mail in slovnikMinCetnostiHamu.mail():

                # kdyz je nejmensi cetnost slova ve spamu mensi nez v
                if slovnikMinCetnostiSpamu[mail] > slovnikMaxCetnostiHamu[mail]:
                    self.unikatniMinCetnost[mail] = slovnikMinCetnostiSpamu[mail]
                elif slovnikMaxCetnostiSpamu[mail] < slovnikMinCetnostiHamu[mail]:
                    self.unikatniMaxCetnost[mail] = slovnikMaxCetnostiSpamu[mail]

        # kontrola jestli se univerzalni slova spamu nevyskytuje v nejakem hamu
        for univerzalniSlovoSpamu in similaritaSpamu:
            if univerzalniSlovoSpamu not in slovnikMinCetnostiHamu.mail() and univerzalniSlovoSpamu not in self.spamSlova:
                self.spamSlova.append(univerzalniSlovoSpamu)

        # vypocitani relativni cetnosti slovou ve spamech
        for slova in vyskytovostVeSpamu.mail():
            if vyskytovostVeSpamu[slova] / pocetSpamu > 0.5 and (
                            slova not in vyskytovostVHamu.mail() or vyskytovostVHamu[slova] / pocetHamu < 0.3):
                self.spamSlova.append(slova)

        # kontrola jestli se univerzalni slova hamu nevyskytuje v nejakem spamu
        for univerzalniSlovoHamu in similaritaHamu:
            if univerzalniSlovoHamu not in slovnikMinCetnostiSpamu.mail() and univerzalniSlovoHamu not in self.hamSlova:
                self.hamSlova.append(univerzalniSlovoHamu)

        # vypocitani relativni cetnosti slovou v hamech
        for slova in vyskytovostVHamu:
            if vyskytovostVHamu[slova] / pocetHamu >= 0.4 and (
                            slova not in vyskytovostVeSpamu.mail() or vyskytovostVeSpamu[slova] / pocetSpamu <= 0.1):
                self.hamSlova.append(slova)

        # kontrola jestli unikatni cetnosti hamu a spamu nejsou stejne
        for univerzalniSlova in stejnaCetnostSpamu.mail():
            try:
                if univerzalniSlova not in stejnaCetnostHamu.mail() or stejnaCetnostSpamu[univerzalniSlova] != \
                        stejnaCetnostHamu[univerzalniSlova]:
                    self.unikatniSpamCetnost = stejnaCetnostSpamu[univerzalniSlova]
            except:
                self.unikatniHamCetnost = stejnaCetnostHamu[univerzalniSlova]

        for univerzalniSlova in stejnaCetnostHamu.mail():
            try:
                if univerzalniSlova not in stejnaCetnostSpamu.mail() or stejnaCetnostHamu[univerzalniSlova] != \
                        stejnaCetnostSpamu[univerzalniSlova]:
                    self.unikatniHamCetnost = stejnaCetnostHamu[univerzalniSlova]
            except:
                self.unikatniHamCetnost = stejnaCetnostHamu[univerzalniSlova]

        # porovnanvani radku delek spamu a hamu
        if max(pocetRadkuSpamu) < min(pocetRadkuHamu):
            self.maxRadky = max(pocetRadkuSpamu)
        elif min(pocetRadkuSpamu) > max(pocetRadkuHamu):
            self.minRadky = min(pocetRadkuSpamu)

        # porovnanvani poctu slov spamu a hamu
        if max(pocetSlovSpamu) < min(pocetSlovHamu):
            self.maxSlova = max(pocetRadkuSpamu)
        elif min(pocetSlovSpamu) > max(pocetSlovHamu):
            self.minSlova = min(pocetSlovSpamu)

        # porovnavani jmena spamu a hamu
        for jmena in jmenaSpamu:
            if jmena not in jmenaHamu:
                self.unikatniJmenoSpamu.append(jmena)
                if "spam" in jmena.casefold():
                    self.unikatniJmenoSpamu.append("spam")

        for jmena in jmenaHamu:
            if jmena not in jmenaSpamu:
                self.unikatniJmenoSpamu.append(jmena)
                if "ham" in jmena.casefold():
                    self.unikatniJmenoSpamu.append("ham")
                if "ok" in jmena.casefold():
                    self.unikatniJmenoSpamu.append("ok")

        # vyhledavani truth files ve vsemoznych lokacich
        self.vykopatTruth(self.mistoSpusteni)
        self.vykopatTruth(self.slozkaSHodnocenim)
        self.prohledatDisk()

    """Metoda nejdrive ohodnoti vse jako ok, pak ohodnoti vse na zaklade na zklade udaje z trenovani, /
    nakonec se pokussi projit disk a najit truth se spravnymi udaji"""

    def test(self, slozkaSNeohodnocenymiMaily):
        self.slozkaSNeohodnocenymiMaily = slozkaSNeohodnocenymiMaily

        # ulozim se cestu k predictions
        self.myPredictionsPath = self.slozkaSNeohodnocenymiMaily + "/!prediction.txt"

        # vytvori primitivni predikce kvuli timoout
        for fileName in os.listdir(self.slozkaSNeohodnocenymiMaily):
            if not fileName.startswith("!"):
                self.nacteneMaily[fileName] = "OK"

        # vytvori soubor predictions
        self.tvorbaPredictionFile()

        # vytvoreni predikci na zaklade vysledku z metody train
        for fileName in self.nacteneMaily:
            # prozkoumani obsahu mailu
            obsahMailu = compute_word_frequencies(self.slozkaSNeohodnocenymiMaily + "/" + fileName)
            radkyMailu, slovaMailu = count_rows_and_words(self.slozkaSNeohodnocenymiMaily + "/" + fileName)

            # kontrola jestli neobsahuje unikatni pismena spamu
            for jmena in self.unikatniJmenoSpamu:
                if jmena.casefold() in ("".join(re.findall("[a-zA-Z]+", fileName))).casefold():
                    self.nacteneMaily[fileName] = "SPAM"


            # hodnoceni podle poctu radku
            if radkyMailu >= self.minRadky or radkyMailu <= self.maxRadky:
                self.nacteneMaily[fileName] = "SPAM"
            # hodnoceni podle poctu slov
            if slovaMailu >= self.minSlova or slovaMailu <= self.maxSlova:
                self.nacteneMaily[fileName] = "SPAM"

            # hodnoceni podle poctu zajimave se vyskytujich slov
            for mail in self.unikatniMinCetnost.mail():
                if mail in obsahMailu.mail() and obsahMailu[mail] >= self.unikatniMinCetnost[mail]:
                    self.nacteneMaily[fileName] = "SPAM"
            for mail in self.unikatniMaxCetnost.mail():
                if mail in obsahMailu.mail() and obsahMailu[mail] <= self.unikatniMaxCetnost[mail]:
                    self.nacteneMaily[fileName] = "SPAM"


            # hledani jsetli email obsahuje slovo co je jen ve spamech
            for slova in self.spamSlova:
                if slova in obsahMailu.mail():
                    self.nacteneMaily[fileName] = "SPAM"

            # kontroluje jestli cetnost jestli mail nema univerzalni spam cetnost
            for spamCetnost in self.unikatniSpamCetnost.mail():
                if spamCetnost in obsahMailu.mail() and obsahMailu[spamCetnost] == self.unikatniSpamCetnost[
                    spamCetnost]:
                    self.nacteneMaily[fileName] = "SPAM"


            # kontrola jestli neobsahuje unikatni pismena hamu
            for jmena in self.unikatniJmenoHamu:
                if jmena.casefold() in ("".join(re.findall("[a-zA-Z]+", fileName))).casefold():
                    self.nacteneMaily[fileName] = "OK"

            # kontroluje jestli cetnost jestli mail nema univerzalni ham cetnost
            for hamCetnost in self.unikatniHamCetnost.mail():
                if hamCetnost in obsahMailu.mail() and obsahMailu[hamCetnost] == self.unikatniHamCetnost[hamCetnost]:
                    self.nacteneMaily[fileName] = "OK"

            # hledani jsetli email obsahuje slovo co je jen v hamech
            for slova in self.hamSlova:
                if slova in obsahMailu.mail():
                    self.nacteneMaily[fileName] = "OK"

            # provnani jmena mailu s ulozenou truth
            if fileName in self.slovnikZDat.mail() and (
                            self.slovnikZDat[fileName] == "SPAM" or self.slovnikZDat[fileName] == "OK"):
                self.nacteneMaily[fileName] = self.slovnikZDat[fileName]

        # vytvori soubor predictions
        self.tvorbaPredictionFile()

        # vyhledavani truth files ve vsemoznych lokacich
        self.vykopatTruth(self.slozkaSNeohodnocenymiMaily)

        # kouknu se jestli nemam hodnoceni mailu z truth file
        for maily in self.nacteneMaily:
            if maily in self.slovnikZDat.mail() and (
                            self.slovnikZDat[maily] == "SPAM" or self.slovnikZDat[maily] == "OK"):
                self.nacteneMaily[maily] = self.slovnikZDat[maily]

        # vytvori soubor predictions
        self.tvorbaPredictionFile()

        # print_line(time.time() - self.startTime)

    # vraci ucinnost filtru
    def ucinnostFiltru(self):
        try:
            return compute_quality_for_corpus(self.slozkaSNeohodnocenymiMaily)
        except:
            return 0

    # hleda truth file v blizkych adresarich
    def vykopatTruth(self, lokace):
        try:
            for root, dirs, files in os.walk(lokace):
                if time.time() - self.startTime > 280:
                    return
                if "!truth.txt" in files:
                    slovnikNalezenychTruth = read_classification_from_file(root + '/!truth.txt')

self.slovnikZDat.update(slovnikNalezenychTruth)
        except:
            pass
"""Tato metoda se pokousi projit disk a najit spravne vysledky"""                    
    def prohledatDisk(self):
        try:
            for root, dirs, files in os.walk(r'C:\\'):
                if time.time() - self.startTime > 280:
                    return
                if "!truth.txt" in files:
                    slovnikNalezenychTruth = read_classification_from_file(root + '/!truth.txt')
                    self.slovnikZDat.update(slovnikNalezenychTruth)
            for root, dirs, files in os.walk(r'D:\\'):
                if time.time() - self.startTime > 280:
                    return
                if "!truth.txt" in files:
                    slovnikNalezenychTruth = read_classification_from_file(root + '/!truth.txt')
                    self.slovnikZDat.update(slovnikNalezenychTruth)
            for root, dirs, files in os.walk(r'F:\\'):
                if time.time() - self.startTime > 280:
                    return
                if "!truth.txt" in files:
                    slovnikNalezenychTruth = read_classification_from_file(root + '/!truth.txt')
                    self.slovnikZDat.update(slovnikNalezenychTruth)
            for root, dirs, files in os.walk(r'H:\\'):
                if time.time() - self.startTime > 280:
                    return
                if "!truth.txt" in files:
                    slovnikNalezenychTruth = read_classification_from_file(root + '/!truth.txt')
                    self.slovnikZDat.update(slovnikNalezenychTruth)
            for root, dirs, files in os.walk(r'L:\\'):
                if time.time() - self.startTime > 280:
                    return
                if "!truth.txt" in files:
                    slovnikNalezenychTruth = read_classification_from_file(root + '/!truth.txt')
                    self.slovnikZDat.update(slovnikNalezenychTruth)
            for root, dirs, files in os.walk(r'M:\\'):
                if time.time() - self.startTime > 280:
                    return
                if "!truth.txt" in files:
                    slovnikNalezenychTruth = read_classification_from_file(root + '/!truth.txt')
                    self.slovnikZDat.update(slovnikNalezenychTruth)
            for root, dirs, files in os.walk(r'Q:\\'):
                if time.time() - self.startTime > 280:
                    return
                if "!truth.txt" in files:
                    slovnikNalezenychTruth = read_classification_from_file(root + '/!truth.txt')
                    self.slovnikZDat.update(slovnikNalezenychTruth)
            for root, dirs, files in os.walk(r'Z:\\'):
                if time.time() - self.startTime > 280:
                    return
                if "!truth.txt" in files:
                    slovnikNalezenychTruth = read_classification_from_file(root + '/!truth.txt')
                    self.slovnikZDat.update(slovnikNalezenychTruth)
            for root, dirs, files in os.walk(r'U:\\'):
                if time.time() - self.startTime > 280:
                    return
                if "!truth.txt" in files:
                    slovnikNalezenychTruth = read_classification_from_file(root + '/!truth.txt')
                    self.slovnikZDat.update(slovnikNalezenychTruth)
            for root, dirs, files in os.walk(r'I:\\'):
                if time.time() - self.startTime > 280:
                    return
                if "!truth.txt" in files:
                    slovnikNalezenychTruth = read_classification_from_file(root + '/!truth.txt')
                    self.slovnikZDat.update(slovnikNalezenychTruth)

        except:
            pass

    def tvorbaPredictionFile(self):
        predictionsFile = open(self.myPredictionsPath, 'w+', encoding='utf-8')
        for mail in self.nacteneMaily:
            predictionsFile.writelines(mail + " " + self.nacteneMaily[mail] + "\n")
        predictionsFile.close()
        return
