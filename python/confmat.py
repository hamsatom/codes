__author__ = 'MuLTiHuNTeR'
"""Zaznamenavani vysledku filtru"""


class BinaryConfusionMatrix:
    # Vytvari matici, prijima tagy za soubory jako argument
    def __init__(self, pos_tag='OK', neg_tag='SPAM'):
        self.tp = 0
        self.tn = 0
        self.fp = 0
        self.fn = 0
        self.pos_tag = pos_tag
        self.neg_tag = neg_tag

    def as_dict(self):
        # Vytvari slovnik s tagy jako keys, jejich cetnost jako values
        return dict(tp=self.tp, tn=self.tn, fp=self.fp, fn=self.fn)

    def update(self, truth, prediction):
        # Meni hodnoty keys, podle vyskytu v predictions a truth
        if prediction == self.pos_tag and truth == self.pos_tag:
            self.tp += 1
        elif prediction == self.pos_tag and truth == self.neg_tag:
            self.fp += 1
        elif prediction == self.neg_tag and truth == self.pos_tag:
            self.fn += 1
        elif prediction == self.neg_tag and truth == self.neg_tag:
            self.tn += 1
        elif truth != self.pos_tag and truth != self.neg_tag:
            raise ValueError
        elif prediction != self.pos_tag and truth != self.neg_tag:
            raise ValueError

    def compute_from_dicts(self, truth_dict, pred_dict):
        # Meni hodnoty keys, podle vyskytu VE SLOVNIKACH predictions a truth
        for keys in truth_dict:
            if truth_dict[keys] == pred_dict[keys]:
                if truth_dict[keys] == self.pos_tag:
                    self.tp += 1
                elif truth_dict[keys] == self.neg_tag:
                    self.tn += 1
            else:
                if truth_dict[keys] == self.pos_tag:
                    self.fp += 1
                elif truth_dict[keys] == self.neg_tag:
                    self.fn += 1

