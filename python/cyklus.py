__author__ = 'hamsatom'
"""Testujici modul zkousici uspeasnost vsech filtru"""

from simplefilters import NaiveFilter, ParanoidFilter, RandomFilter
from basefilter import BaseFilter
from trainingcorpus import TrainingCorpus
import os

slozka = r'D:\Tomáš\ownCloud\PyCharm\spamFiltr\spam-data-12-s75-h25\1'
slozka2 = r'D:\Tomáš\ownCloud\PyCharm\spamFiltr\spam-data-12-s75-h25\2'
filters = {}
filters['NaiveFilter'] = NaiveFilter()
filters['ParanoidFilter'] = ParanoidFilter()
filters['RandomFilter'] = RandomFilter()

b = BaseFilter()
b.train(slozka)
b.test(slozka2)
print ("q pro BaseFilter tp, tn, fp, fn", b.ucinnostFiltru())

for filter_id, filter in filters.items():
    filter.train(slozka)
    filter.test(slozka2)
    filter.ohodnotit()
