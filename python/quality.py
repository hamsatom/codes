__author__ = 'MuLTiHuNTeR'
"""Modul hodnotici ucinnost filtru"""
"""Potrebuje truth file a predictions v jedne slozce"""

from utils import read_classification_from_file
from confmat import BinaryConfusionMatrix


def quality_score(tp, tn, fp, fn):
    # Vypocita kvalitu filtru podle hodnot v matici
    # V pripade ze nastane chyba vrati q = 0
    try:
        kvalita = (tp + tn) / (tp + tn + 10 * fp + fn)
    except:
        kvalita = 0
    return kvalita


def compute_quality_for_corpus(corpus_dir):
    # Vypocte ucinost filtru podle rozdilu mezi soubory predictions a truth

    # Vytvoreni cest k souborum truth a predictions
    truthFile = corpus_dir + "/!truth.txt"
    predictionFile = corpus_dir + "/!prediction.txt"

    # Vyrobeni slovniku z obou truth i predictions
    slovnikTruth = read_classification_from_file(truthFile)
    slovnikPredikci = read_classification_from_file(predictionFile)

    # Zaneseni vysledku do matice
    ucinostFiltru = BinaryConfusionMatrix()
    ucinostFiltru.compute_from_dicts(slovnikTruth, slovnikPredikci)

    # Spocteni uspesnosti filtru metodou quality_score, kde v argumentu je slovnik z vysledku matice
    return quality_score(**ucinostFiltru.as_dict())
