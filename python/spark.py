from __future__ import division

from PIL import Image
from PIL.ExifTags import TAGS
from datetime import datetime
from pyspark.sql import *
from pyspark.sql.functions import avg, hour, month
from pyspark.sql.types import *


def get_exif(str):
    ret = {}
    i = Image.open(str)
    info = i._getexif()
    if info:
        for tag, value in info.items():
            decoded = TAGS.get(tag, tag)
            ret[decoded] = value
    return ret


path = '/storage/brno2/home/pascepet/fel_bigdata/data/images/'
rows = []
for image in os.listdir(path):
    metadata = get_exif(path + image)
    dateTime = metadata.get('DateTime')
    if dateTime is not None:
        dateTime = datetime.strptime(dateTime, '%Y:%m:%d %H:%M:%S')
    exposureTime = metadata.get('ExposureTime')
    if exposureTime is not None:
        exposureTime = exposureTime[0] / exposureTime[1]
    exifImageWidth = metadata.get('ExifImageWidth')
    exifImageHeight = metadata.get('ExifImageHeight')
    rows.append(Row(DateTime=dateTime, ExposureTime=exposureTime, ExifImageWidth=exifImageWidth, ExifImageHeight=exifImageHeight))

dataFrame = sqlContext.createDataFrame(rows)
dataFrame.cache()

# mezi 9. a 15. hodinou
dataFrame.filter((hour(dataFrame.DateTime) >= 9) & (hour(dataFrame.DateTime) < 15)).agg(avg(dataFrame.ExposureTime)).show()
# mimo tuto dobu
dataFrame.filter((hour(dataFrame.DateTime) < 9) | (hour(dataFrame.DateTime) >= 15)).agg(avg(dataFrame.ExposureTime)).show()

# 2) Kolik fotek má rozlišení více než 4 miliony pixelů?
dataFrame.filter(dataFrame.ExifImageWidth * dataFrame.ExifImageHeight > 4000000).count()

# 3) Kolik fotek bylo pořízeno v jednotlivých kalendářních měsících?
dataFrame.groupBy(month(dataFrame.DateTime)).count().show()
