__author__ = 'hamsatom'
"""Prevadi udaje ze souboru do slovniku"""


def read_classification_from_file(soubor):
    # Funkce nacita udaje ze souboru (jmeno+tag) a uklada je do slovniku
    slovnik = {}
    with open(soubor, "r", encoding="utf-8") as cteniSouboru:
        for line in cteniSouboru:
            try:
                (key, val) = line.split()
            except:
                key = line
                val = None
            slovnik[key] = val
        cteniSouboru.close()
    return slovnik