__author__ = 'hamsatom'
"""Testu vlastnosti souboru"""

from data import *

def count_rows_and_words(soubor):
    with open(soubor, "r", encoding="utf-8") as cteniSouboru:
        lineCounter = 0
        wordsCounter = 0
        for line in cteniSouboru:
            lineCounter += 1
            a = line.split()
            wordsCounter += len(a)
    cteniSouboru.close()
    return lineCounter, wordsCounter


def compute_word_frequencies(soubor):
    slovnikCetnosti = {}
    with open(soubor, "r", encoding="utf-8") as cteniSouboru:
        text = cteniSouboru.read().split()
        for slovo in text:
            if slovo not in slovnikCetnosti.keys():
                slovnikCetnosti[slovo] = 1
            else:
                slovnikCetnosti[slovo] += 1
    cteniSouboru.close()
    return slovnikCetnosti
