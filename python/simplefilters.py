__author__ = 'MuLTiHuNTeR'
"""Nejjednodussi filtry"""

import random

from basefilter import BaseFilter


# filtr klasifikuje vse ok
class NaiveFilter(BaseFilter):
    def ohodnotit(self):
        for maily in self.nacteneMaily:
            self.nacteneMaily[maily] = "OK"

        # vytvori soubor predictions
        self.tvorbaPredictionFile()

        print("q pro vse ok: " + str(self.ucinnostFiltru()))


# filtr vse klasifikuje jako spam
class ParanoidFilter(BaseFilter):
    def ohodnotit(self):
        for maily in self.nacteneMaily:
            self.nacteneMaily[maily] = "SPAM"

        # vytvori soubor predictions
        self.tvorbaPredictionFile()

        print("q pro vse spam: " + str(self.ucinnostFiltru()))


# klasifikuje vse nahodne
class RandomFilter(BaseFilter):
    def ohodnotit(self):
        for maily in self.nacteneMaily:
            self.nacteneMaily[maily] = random.choice(["OK", "SPAM"])

        # vytvori soubor predictions
        self.tvorbaPredictionFile()
        
        print("q pro random: " + str(self.ucinnostFiltru()))
