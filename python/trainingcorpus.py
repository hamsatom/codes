__author__ = 'MuLTiHuNTeR'
"""Modul provadejici kouzla ve cvicici slozce"""

import os

from utils import read_classification_from_file

jeHam = jeSpam = True
neniSpam = neniHam = False


class TrainingCorpus:
    def __init__(self, slozkaSHodnocenim):
        self.slozkaSHodnocenim = slozkaSHodnocenim
        self.slovnikSTruth = {}
        self.truthDoSlovniku()

    # vraci hodnotu jakou ma email v truth file
    def get_class(self, jmenoEmailu):
        if jmenoEmailu in self.slovnikSTruth.keys():
            return self.slovnikSTruth[jmenoEmailu]

    # prevadi truth v zadanem adresari do slovniku
    def truthDoSlovniku(self):
        self.slovnikSTruth = read_classification_from_file(self.slozkaSHodnocenim + "/!truth.txt")
        return

    # hodnoti jestli je email ham podle boolean hodnot
    def is_ham(self, jmenoMailu):
        vyhodnoceni = self.get_class(jmenoMailu)
        if vyhodnoceni == "SPAM":
            return neniHam
        elif vyhodnoceni == "OK":
            return jeHam

    # hodnoti jestli je email spam podle boolean hodnot
    def is_spam(self, jmenoMailu):
        vyhodnoceni = self.get_class(jmenoMailu)
        if vyhodnoceni == "SPAM":
            return jeSpam
        elif vyhodnoceni == "OK":
            return neniHam

    # generator ktery vraci jmena a tela mailu s hamy
    def hams(self):
        for maily in os.listdir(self.slozkaSHodnocenim):
            if self.is_ham(maily) == True:
                body = open(self.slozkaSHodnocenim + "/" + maily, 'r', encoding="utf-8")
                yield maily, body.read(),
        body.close()

    # generator ktery vraci jmena a tela mailu se spamy
    def spams(self):
        for maily in os.listdir(self.slozkaSHodnocenim):
            if self.is_spam(maily) == True:
                body = open(self.slozkaSHodnocenim + "/" + maily, 'r', encoding="utf-8")
                yield maily, body.read()
        body.close()
