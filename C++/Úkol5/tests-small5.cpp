#include "catch.hpp"
#include <algorithm>
#include <vector>
#include <iterator>
#include <random>
#include <chrono>
#include <iostream>

#include "list.hpp"

using std::begin;
using std::end;
using std::make_reverse_iterator;

namespace {

auto rbegin(const list& l) {
    return make_reverse_iterator(end(l));
}

auto rend(const list& l) {
    return make_reverse_iterator(begin(l));
}

// Now expects iterators to work.
bool list_equal(const list& l, const std::vector<double>& numbers) {
    if (l.size() != numbers.size()) {
        return false;
    }

    // Check the list from both sides to verify that the list is connected properly.
    return std::equal(begin(l), end(l), begin(numbers), end(numbers)) && 
        std::equal(rbegin(l), rend(l), rbegin(numbers), rend(numbers));
}

bool reports_as_empty(const list& l) {
    return l.empty() && (l.size() == 0);
}

int generate_int() {
    // Note: The proper way to init the generator would be to use a std::random_device
    //       but that would lead to every run being different and I would prefer to keep
    //       the tests deterministic.
    static std::mt19937 gen;
    static std::uniform_int_distribution<> dist(0, 3000);
    return dist(gen);
}

std::vector<double> generate_data(size_t sz) {
    std::vector<double> res;
    std::generate_n(std::back_inserter(res), sz, generate_int);
    return res;
}

}


/**************************************************************
*** Check Iterators first, because list_equal now uses them ***
***************************************************************/

TEST_CASE("Iterators") {
    SECTION("Empty list's iterators should be equal.") {
        list l;
        REQUIRE(l.begin() == l.end());
    }
    SECTION("Non empty list") {
        list l1({ 1, 2, 3 });
        SECTION("Dereferencable iterators from different lists cannot be equal") {
            list l2({4, 5, 6});
            REQUIRE(l1.begin() != l2.begin());
        }
        SECTION("Moving through the list") {
            auto sit = l1.begin();
            auto eit = l1.end();
            REQUIRE(sit != eit);
            SECTION("Forward") {
                ++sit;
                REQUIRE(sit != eit);
                ++sit;
                REQUIRE(sit != eit);
                ++sit;
                REQUIRE(sit == eit);
            }
            SECTION("Backward") {
                --eit;
                REQUIRE(sit != eit);
                --eit;
                REQUIRE(sit != eit);
                --eit;
                REQUIRE(sit == eit);
            }
            SECTION("Forward - postincr") {
                REQUIRE(sit++ != eit);
                REQUIRE(sit++ != eit);
                REQUIRE(sit++ != eit);
                REQUIRE(sit   == eit);
            }
            SECTION("Backward - postdecr") {
                REQUIRE(sit != eit--);
                REQUIRE(sit != eit--);
                REQUIRE(sit != eit--);
                REQUIRE(sit == eit);
            }
        }
        SECTION("Dereferencing iterators") {
            auto sit = l1.begin();
            for (int i = 1; i <= 3; ++i) {
                REQUIRE(*sit == i);
                ++sit;
            }
            REQUIRE(sit == l1.end());
        }
        SECTION("Iterators can be used to change underlying list") {
            for (auto& el : l1) {
                el *= 2;
            }
            auto it = begin(l1);
            REQUIRE(*it++ == 2);
            REQUIRE(*it++ == 4);
            REQUIRE(*it++ == 6);
        }
    }
    SECTION("Constructors") {
        SECTION("Default inited iterators should be equal") {
            list::iterator it, it2;
            REQUIRE(it == it2);
        }
        SECTION("Iterator's copy is equal") {
            list l({ 1, 2, 3 });
            auto it = l.begin();
            auto it2(it);
            REQUIRE(it2 == it);
            REQUIRE(*it2 == *it);
            SECTION("And can be used to iterate") {
                for (int i = 1; i <= 3; ++i) {
                    REQUIRE(*it2 == i);
                    ++it2;
                }
                REQUIRE(it2 == l.end());
            }
            SECTION("And so is copy assigned") {
                it2 = it;
                REQUIRE(it2 == it);
                for (int i = 1; i <= 3; ++i) {
                    REQUIRE(*it2 == i);
                    ++it2;
                }
                REQUIRE(it2 == l.end());
            }
        }
    }
}


TEST_CASE("Constant Iterators") {
    SECTION("Empty list's iterators should be equal.") {
        list l;
        REQUIRE(l.cbegin() == l.cend());
    }
    SECTION("Non empty list") {
        list l1({ 1, 2, 3 });
        SECTION("Dereferencable iterators from different lists cannot be equal") {
            list l2({ 4, 5, 6 });
            REQUIRE(l1.cbegin() != l2.cbegin());
        }
        SECTION("Moving through the list") {
            auto sit = l1.cbegin();
            auto eit = l1.cend();
            REQUIRE(sit != eit);
            SECTION("Forward") {
                ++sit;
                REQUIRE(sit != eit);
                ++sit;
                REQUIRE(sit != eit);
                ++sit;
                REQUIRE(sit == eit);
            }
            SECTION("Backward") {
                --eit;
                REQUIRE(sit != eit);
                --eit;
                REQUIRE(sit != eit);
                --eit;
                REQUIRE(sit == eit);
            }
            SECTION("Forward - postincr") {
                REQUIRE(sit++ != eit);
                REQUIRE(sit++ != eit);
                REQUIRE(sit++ != eit);
                REQUIRE(sit == eit);
            }
            SECTION("Backward - postdecr") {
                REQUIRE(sit != eit--);
                REQUIRE(sit != eit--);
                REQUIRE(sit != eit--);
                REQUIRE(sit == eit);
            }
        }
        SECTION("Dereferencing iterators") {
            auto sit = l1.cbegin();
            for (int i = 1; i <= 3; ++i) {
                REQUIRE(*sit == i);
                ++sit;
            }
            REQUIRE(sit == l1.cend());
        }
    }
    SECTION("Constructors") {
        SECTION("Default inited iterators should be equal") {
            list::const_iterator it, it2;
            REQUIRE(it == it2);
        }
        SECTION("Iterator's copy is equal") {
            list l({ 1, 2, 3 });
            auto it = l.cbegin();
            auto it2(it);
            REQUIRE(it2 == it);
            REQUIRE(*it2 == *it);
            SECTION("And can be used to iterate") {
                for (int i = 1; i <= 3; ++i) {
                    REQUIRE(*it2 == i);
                    ++it2;
                }
                REQUIRE(it2 == l.cend());
            }
            SECTION("And so is copy assigned") {
                it2 = it;
                REQUIRE(it2 == it);
                for (int i = 1; i <= 3; ++i) {
                    REQUIRE(*it2 == i);
                    ++it2;
                }
                REQUIRE(it2 == l.cend());
            }
        }
    }
}


TEST_CASE("operator==, !=") {
    SECTION("Empty lists are equal") {
        REQUIRE(list{} == list{});
        REQUIRE(list{} == list{});
    }
    SECTION("empty and non empty lists") {
        list l1({ 1, 2, 3 });
        REQUIRE(l1 != list{});
        REQUIRE(list{} != l1);
    }
    SECTION("Two non-equal non-empty lists") {
        list l1({ 4, 5, 6 });
        list l2({ 8, 2, 1 });
        REQUIRE(l1 != l2);
    }
    SECTION("Two equal non-empty lists") {
        list l1({ 4, 5, 6, 7});
        list l2({4, 5, 6, 7});
        REQUIRE(l1 == l2);
    }
    SECTION("A list is equal to itself") {
        list l1({12, 32, 43, 54, 765, 123, 43, 1, 1, 1});
        REQUIRE(l1 == l1);
    }
    SECTION("Complex 1") {
        list l1{ { 4, 5, 6 } };
        list l2 = l1;
        l2.pop_back();
        l2.pop_front();
        l1.pop_back();
        l1.pop_back();
        REQUIRE(l1 != l2);
        l1.pop_front();
        l2.push_back(7);
        l2.push_back(0);
        l1.push_back(7);
        l1.push_front(5);
        l1.push_back(0);
        REQUIRE(l1 == l2);
    }
}


TEST_CASE("Relational operators (<, <=, >, >=)"){
    SECTION("One empty list is not smaller than another") {
        REQUIRE_FALSE(list{} < list{});
    }
    SECTION("Empty list should be smaller than non-empty list") {
        REQUIRE(list{} < list( { 1, 2, 3 } ));
    }
    SECTION("lhs is a sublist") {
        list l1({1, 2, 3});
        list l2({ 1, 2, 3, 4 });
        REQUIRE(l1 < l2);
    }
    SECTION("rhs is a sublist") {
        list l1({ 3, 2, 1 });
        list l2({ 3, 2 });
        REQUIRE(l1 > l2);
    }
    SECTION("lists are equal") {
        list l1({ 1, 3, 5, 7 });
        list l2(l1);
        REQUIRE(l1 <= l2);
        REQUIRE(l1 >= l2);
    }
    SECTION("Just diff lists") {
        list l1({ 1, 3, 5 });
        list l2({ 1, 4, 5 });
        REQUIRE(l1 < l2);
    }
    SECTION("Just a mismatch 2") {
        list l1({ 1, 2, 3, 4, 5, 5, 6 });
        list l2({ 1, 2, 3, 4, 5, 5, 5 });
        REQUIRE(l1 > l2);
    }
}


TEST_CASE("Basic functionality -- constructors, {pop, push}_{front, back}, size, empty") {
    list l;
    REQUIRE(reports_as_empty(l));
    SECTION("Vector constructor") {
        SECTION("Empty vector -> empty list") {
            REQUIRE(reports_as_empty(list{ std::vector<double>{} }));
        }
        SECTION("Singular vector -> singular list") {
            list l2({ 1 });
            REQUIRE(!l2.empty());
            REQUIRE(l2.size() == 1);
            REQUIRE(l2.front() == 1);
            REQUIRE(l2.back() == 1);
        }
        SECTION("Multiple elements") {
            std::vector<double> elems({ 1, 2, 3, 56, 78, 86 });
            list l3(elems);
            REQUIRE(!l3.empty());
            REQUIRE(l3.size() == 6);
            REQUIRE(list_equal(l3, elems));
        }
    }
    SECTION("Series of push_back") {
        for (int i : {0, 2, 5, 9}) {
            l.push_back(i);
            REQUIRE(l.back() == i);
        }
        REQUIRE(l.size() == 4);
        REQUIRE(list_equal(l, { 0, 2, 5, 9 }));
    }
    SECTION("Series of push_front") {
        for (int i : {0, 3, 7, 2}) {
            l.push_front(i);
            REQUIRE(l.front() == i);
        }
        REQUIRE(l.size() == 4);
        REQUIRE(list_equal(l, { 2, 7, 3, 0 }));
    }
    SECTION("Series of push_back and pop_back") {
        for (int i = 0; i < 30; i += 4) {
            l.push_back(i);
            REQUIRE(l.back() == i);
            REQUIRE(l.front() == i);
            REQUIRE(l.size() == 1);
            l.pop_back();
            REQUIRE(reports_as_empty(l));
        }
        REQUIRE(reports_as_empty(l));
    }
    SECTION("Series of push_back and pop_front") {
        for (int i = 0; i < 30; i += 4) {
            l.push_back(i);
            REQUIRE(l.back() == i);
            REQUIRE(l.front() == i);
            REQUIRE(l.size() == 1);
            l.pop_front();
            REQUIRE(reports_as_empty(l));
        }
        REQUIRE(reports_as_empty(l));
    }
    SECTION("Series of push_front and pop_front") {
        for (int i = 0; i < 30; i += 4) {
            l.push_front(i);
            REQUIRE(l.back() == i);
            REQUIRE(l.front() == i);
            REQUIRE(l.size() == 1);
            l.pop_front();
            REQUIRE(reports_as_empty(l));
        }
        REQUIRE(reports_as_empty(l));
    }
    SECTION("Series of push_front and pop_back") {
        for (int i = 0; i < 30; i += 4) {
            l.push_front(i);
            REQUIRE(l.back() == i);
            REQUIRE(l.front() == i);
            REQUIRE(l.size() == 1);
            l.pop_back();
            REQUIRE(reports_as_empty(l));
        }
        REQUIRE(reports_as_empty(l));
    }
    SECTION("complex 1") {
        for (int i : {0, 2, 4, 6, 8}) {
            l.push_back(i);
        }
        REQUIRE(l.size() == 5);
        l.pop_back();
        l.pop_front();
        REQUIRE(l.size() == 3);
        for (int i : {1, 3, 0}) {
            l.push_front(i);
        }
        REQUIRE(list_equal(l, { 0, 3, 1, 2, 4, 6 }));
    }
    SECTION("complex 2") {
        for (int i : {0, 2, 4}) {
            l.push_back(i);
            l.push_front(i);
        }
        REQUIRE(list_equal(l, { 4, 2, 0, 0, 2, 4 }));
        l.pop_front();
        l.pop_back();
        l.pop_back();
        REQUIRE(l.size() == 3);
        REQUIRE(l.front() == 2);
        REQUIRE(l.back() == 0);
        for (int i : {1, 2, 3}) {
            l.push_front(i);
            l.push_back(i);
        }
        REQUIRE(list_equal(l, { 3, 2, 1, 2, 0, 0, 1, 2, 3 }));
    }
    SECTION("complex 3") {
        for (int i : {0, 2, 4}) {
            l.push_back(i);
        }
        for (int i : {1, 3, 5}) {
            l.push_front(i);
        }
        REQUIRE(list_equal(l, { 5, 3, 1, 0, 2, 4 }));
        for (int i : {1, 2}) {
            l.push_front(i);
        }
        for (int i : {3, 5}) {
            l.pop_back();
            l.push_back(i);
        }
        REQUIRE(list_equal(l, { 2, 1, 5, 3, 1, 0, 2, 5 }));
    }
    SECTION("complex 4") {
        for (int i : {1, 2, 3, 4}) {
            l.push_back(i);
        }
        for (int i = 0; i < 2; ++i) {
            l.pop_back();
            l.pop_front();
        }
        REQUIRE(reports_as_empty(l));

        for (int i : {1, 2, 3, 4}) {
            l.push_back(i);
        }
        l.pop_back();
        l.pop_back();
        for (int i : {5, 6}) {
            l.push_back(i);
        }
        l.pop_front();
        l.pop_front();
        l.pop_front();
        REQUIRE(l.size() == 1);
        REQUIRE(!l.empty());
        for (int i : {7, 8}) {
            l.push_front(i);
        }
        REQUIRE(list_equal(l, { 8, 7, 6 }));
    }
}


TEST_CASE("join") {
    auto elems = { 0., 2., 4. };
    SECTION("empty lists") {
        list lhs, rhs;
        lhs.join(rhs);
        REQUIRE(reports_as_empty(lhs));
        REQUIRE(reports_as_empty(rhs));
    }
    SECTION("rhs is empty") {
        list lhs{ elems }, rhs;
        lhs.join(rhs);
        REQUIRE(list_equal(lhs, elems));
        REQUIRE(reports_as_empty(rhs));
    }
    SECTION("lhs is empty") {
        list lhs, rhs{ elems };
        lhs.join(rhs);
        REQUIRE(list_equal(lhs, elems));
        REQUIRE(reports_as_empty(rhs));
    }
    SECTION("both lists have elements in them") {
        list lhs{ elems }, rhs{ elems };
        lhs.join(rhs);
        REQUIRE(list_equal(lhs, { 0, 2, 4, 0, 2, 4 }));
        REQUIRE(reports_as_empty(rhs));
    }
}

TEST_CASE("join - complexity") {
    // Basically, join can either be implemented correctly, taking only constant time
    // Or incorrectly, with pop, push combo in linear time. We want to detect the seconds
    // case and fail the tests
    using namespace std::chrono_literals;
    double last_time = 0;
    for (int i = 64'000; i <= 20'000'000; i *= 4) {
        auto lhs_elems = generate_data(i);
        list lhs{ lhs_elems };
        auto rhs_elems = generate_data(i);
        list rhs{ rhs_elems };
        auto start_time = std::chrono::high_resolution_clock::now();
        lhs.join(rhs);
        auto end_time = std::chrono::high_resolution_clock::now();
        // Add 0.5 ms to even out variability
        auto time_diff = end_time - start_time + 500us; 
        auto total_time = static_cast<double>(time_diff.count());
        lhs_elems.insert(end(lhs_elems), begin(rhs_elems), end(rhs_elems));
        REQUIRE(list_equal(lhs, lhs_elems));
        if (last_time != 0) {
            // The coefficient is a rough approximation.
            // A linear time coefficient should blow out, constant time
            // jitter should remain inside some reasonable coefficient
            REQUIRE(total_time / last_time <= 1.25);
        }
        last_time = total_time;
        std::cout << "join: i = " << i << " total time = " << (time_diff - 500us).count() << '\n';
    }
}


TEST_CASE("reverse") {
    SECTION("Empty list") {
        list l;
        l.reverse();
        REQUIRE(reports_as_empty(l));
    }
    SECTION("List with single element") {
        list l{ { 10 } };
        l.reverse();
        REQUIRE(l.back() == 10);
        REQUIRE(l.front() == 10);
        SECTION("List with two elements") {
            l.push_front(20);
            l.reverse();
            REQUIRE(list_equal(l, { 10, 20 }));
        }
    }
    SECTION("List with many elements") {
        auto elems = std::vector<double>{ 0., 1., 2., 3., 4., 5., 6., 7., 8., 9. };
        list l{ elems };
        SECTION("One reverse") {
            l.reverse();
            std::reverse(begin(elems), end(elems));
            REQUIRE(list_equal(l, elems));
            SECTION("Two reverses cancel each other out") {
                l.reverse();
                std::reverse(begin(elems), end(elems));
                REQUIRE(list_equal(l, elems));
            }
        }
    }
}


TEST_CASE("unique") {
    SECTION("Empty list") {
        list l;
        l.unique();
        REQUIRE(reports_as_empty(l));
    }
    SECTION("Degenerated case 1") {
        list l{ { 1, 1, 1, 1, 1, 1, 1, 1, 1 } };
        l.unique();
        REQUIRE(list_equal(l, { 1 }));
    }
    SECTION("Unique elements") {
        auto elems = std::vector<double>{ 1, 2, 3, 4, 5 };
        list l{ elems };
        l.unique();
        REQUIRE(list_equal(l, elems));
    }
    SECTION("Sorted duplicate elements") {
        auto elems = std::vector<double>{ 1, 2, 3, 5, 5, 5 };
        list l{ elems };
        l.unique();
        elems.erase(std::unique(begin(elems), end(elems)), end(elems));
        REQUIRE(list_equal(l, elems));
    }
    SECTION("Unsorted duplicate elements") {
        auto elems = std::vector<double>{ { 1, 2, 2, 3, 3, 3, 4, 1, 2, 5, 5, 5 } };
        list l{ elems };
        l.unique();
        elems.erase(std::unique(begin(elems), end(elems)), end(elems));
        REQUIRE(list_equal(l, elems));
    }
}

TEST_CASE("unique - complexity"){
    // Hypothetically, unique could be implemented in non-linear time.
    // Per element time should remain the same, more or less, allowing for jitter.
    double last_time = 0;
    for (int i = 1'024'000; i <= 5'000'000; i *= 4) {
        auto elems = generate_data(i);
        list l{ elems };
        auto start_time = std::chrono::high_resolution_clock::now();
        l.unique();
        auto end_time = std::chrono::high_resolution_clock::now();
        auto time_diff = end_time - start_time;
        auto time_per_item = time_diff.count() / static_cast<double>(i);
        elems.erase(std::unique(begin(elems), end(elems)),
                    end(elems));
        REQUIRE(list_equal(l, elems));
        if (last_time != 0) {
            // The coefficient is a rough approximation.
            // Even a very naive implementation can hit about 1.5 coefficient...
            REQUIRE(time_per_item / last_time <= 1.25);
        }
        std::cout << "unique: i = " << i << " time per item = " << time_per_item << '\n';
        last_time = time_per_item;
    }
}


TEST_CASE("remove") {
    SECTION("Basic") {
        auto elements = std::vector<double>{ 0, 2, 2, 3, 2, 2, 3, 3, 2, 0, 1 };
        list l{ elements };
        SECTION("Remove 2") {
            l.remove(2);
            elements.erase(std::remove(begin(elements), end(elements), 2),
                           end(elements));
            REQUIRE(list_equal(l, elements));
        }
        SECTION("Remove something not in list") {
            l.remove(5);
            REQUIRE(list_equal(l, elements));
        }
        SECTION("Remove head") {
            l.remove(0);
            elements.erase(std::remove(begin(elements), end(elements), 0),
                           end(elements));
            REQUIRE(list_equal(l, elements));
        }
        SECTION("Remove tail") {
            l.remove(1);
            elements.erase(std::remove(begin(elements), end(elements), 1),
                           end(elements));
            REQUIRE(list_equal(l, elements));
        }
        SECTION("Remove head and tail") {
            l.remove(0);
            l.remove(1);
            elements.erase(std::remove(begin(elements), end(elements), 0),
                           end(elements));
            elements.erase(std::remove(begin(elements), end(elements), 1),
                           end(elements));
            REQUIRE(list_equal(l, elements));
        }
        SECTION("Remove to empty") {
            for (int i = 0; i < 4; ++i) {
                l.remove(i);
            }
            REQUIRE(reports_as_empty(l));
        }
        SECTION("Remove from empty list is noop") {
            list l2;
            l2.remove(123);
        }
    }
    SECTION("Remove all") {
        list l({ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 });
        l.remove(1);
        REQUIRE(reports_as_empty(l));
        SECTION("Verify that the list can still be used") {
            for (auto e : { -1, 1, 2, 3 }) {
                l.push_back(e);
                l.push_front(e);
            }
            REQUIRE(!l.empty());
            REQUIRE(l.size() == 8);
            l.remove(3);
            REQUIRE(!l.empty());
            REQUIRE(l.size() == 6);
            REQUIRE(list_equal(l, { 2, 1, -1, -1, 1, 2 }));
        }
    }
}

TEST_CASE("remove - complexity") {
    double last_time = 0;
    for (int i = 1'024'000; i <= 5'000'000; i *= 4) {
        auto elems = generate_data(i);
        list l{ elems };
        auto to_remove = generate_int();
        auto start_time = std::chrono::high_resolution_clock::now();
        l.remove(to_remove);
        auto end_time = std::chrono::high_resolution_clock::now();
        auto time_diff = end_time - start_time;
        auto time_per_item = time_diff.count() / static_cast<double>(i);
        elems.erase(std::remove(begin(elems), end(elems), to_remove), end(elems));
        REQUIRE(list_equal(l, elems));
        if (last_time != 0) {
            // Per element time should remain the same, more or less, allowing for jitter.
            // The coefficient is a rough approximation.
            // Even a very naive implementation can hit about 1.5 coefficient...
            REQUIRE(time_per_item / last_time <= 1.25);
        }
        std::cout << "remove: i = " << i << " time per item = " << time_per_item << '\n';
        last_time = time_per_item;
    }
}


TEST_CASE("merge") {
    SECTION("both empty") {
        list lhs, rhs;
        lhs.merge(rhs);
        REQUIRE(reports_as_empty(lhs));
        REQUIRE(reports_as_empty(rhs));
    }
    SECTION("empty lhs") {
        list lhs;
        list rhs({ 0, 1, 2, 3 });
        lhs.merge(rhs);
        REQUIRE(reports_as_empty(rhs));
        REQUIRE(!lhs.empty());
        REQUIRE(lhs.size() == 4);
        REQUIRE(list_equal(lhs, { 0, 1, 2, 3 }));
    }
    SECTION("empty rhs") {
        list lhs({ 0, 1, 2, 3 });
        list rhs;
        lhs.merge(rhs);
        REQUIRE(reports_as_empty(rhs));
        REQUIRE(!lhs.empty());
        REQUIRE(lhs.size() == 4);
        REQUIRE(list_equal(lhs, { 0, 1, 2, 3 }));
    }
    SECTION("simple RHS append") {
        list lhs({ 0, 1, 2 });
        list rhs({ 3, 4, 5 });
        lhs.merge(rhs);
        REQUIRE(reports_as_empty(rhs));
        REQUIRE(!lhs.empty());
        REQUIRE(lhs.size() == 6);
        REQUIRE(list_equal(lhs, { 0, 1, 2, 3, 4, 5 }));
    }
    SECTION("simple RHS prepend") {
        list lhs({ 3, 4, 5 });
        list rhs({ 0, 1, 2 });
        lhs.merge(rhs);
        REQUIRE(reports_as_empty(rhs));
        REQUIRE(!lhs.empty());
        REQUIRE(lhs.size() == 6);
        REQUIRE(list_equal(lhs, { 0, 1, 2, 3, 4, 5 }));
    }
    SECTION("equal merge") {
        list lhs({ 0, 0, 0, 0 });
        list rhs({ 0, 0, 0, 0 });
        lhs.merge(rhs);
        REQUIRE(reports_as_empty(rhs));
        REQUIRE(!lhs.empty());
        REQUIRE(lhs.size() == 8);
        REQUIRE(list_equal(lhs, { 0, 0, 0, 0, 0, 0, 0, 0 }));
    }
    SECTION("same lists merge") {
        list lhs({ 0, 1, 2, 3 });
        list rhs({ 0, 1, 2, 3 });
        lhs.merge(rhs);
        REQUIRE(reports_as_empty(rhs));
        REQUIRE(!lhs.empty());
        REQUIRE(lhs.size() == 8);
        REQUIRE(list_equal(lhs, { 0, 0, 1, 1, 2, 2, 3, 3 }));
    }
    SECTION("Irregular merge 1") {
        list lhs({ 0, 1, 3, 4, 5 });
        list rhs({ 1, 2, 2 });
        lhs.merge(rhs);
        REQUIRE(reports_as_empty(rhs));
        REQUIRE(!lhs.empty());
        REQUIRE(lhs.size() == 8);
        REQUIRE(list_equal(lhs, { 0, 1, 1, 2, 2, 3, 4, 5 }));
    }
    SECTION("Irregular merge 2") {
        list lhs({ 1, 2, 3 });
        list rhs({ -2, -2, 4, 4, 5 });
        lhs.merge(rhs);
        REQUIRE(reports_as_empty(rhs));
        REQUIRE(!lhs.empty());
        REQUIRE(lhs.size() == 8);
        REQUIRE(list_equal(lhs, { -2, -2, 1, 2, 3, 4, 4, 5 }));
    }
}

TEST_CASE("merge - complexity") {
    // As it turns out, some people actually implemented merge with non-linear complexity.
    // Since merge is meant to have linear complexity, ...
    double last_time = 0;
    for (int i = 256'000; i <= 5'000'000; i *= 4) {
        auto lhs_elems = generate_data(i);
        auto rhs_elems = generate_data(i);
        std::sort(begin(lhs_elems), end(lhs_elems));
        std::sort(begin(rhs_elems), end(rhs_elems));
        list lhs{ lhs_elems };
        list rhs{ rhs_elems };
        auto start_time = std::chrono::high_resolution_clock::now();
        lhs.merge(rhs);
        auto end_time = std::chrono::high_resolution_clock::now();
        auto time_diff = end_time - start_time;
        auto time_per_item = time_diff.count() / static_cast<double>( 2 * i);
        lhs_elems.insert(end(lhs_elems), begin(rhs_elems), end(rhs_elems));
        std::inplace_merge(begin(lhs_elems), begin(lhs_elems) + i, end(lhs_elems));
        REQUIRE(list_equal(lhs, lhs_elems));
        if (last_time != 0) {
            // Per element time should remain the same, more or less, allowing for jitter.
            // Even a very naive implementation can hit about 1.25 coefficient...
            REQUIRE(time_per_item / last_time <= 1.25);
        }
        std::cout << "merge: i = " << i << " time per item = " << time_per_item << '\n';
        last_time = time_per_item;
    }
}


TEST_CASE("Copy operations") {
    SECTION("Copy constructor") {
        SECTION("Make sure lists are disconnected") {
            list l1{ { 1, 2, 3 } };
            list l2(l1);
            SECTION("Simple copy") {
                REQUIRE(list_equal(l2, { 1, 2, 3 }));
            }
            SECTION("Add to old list") {
                l1.push_back(4);
                REQUIRE(list_equal(l1, { 1, 2, 3, 4 }));
                REQUIRE(list_equal(l2, { 1, 2, 3 }));
            }
            SECTION("Add to new list") {
                l2.push_back(4);
                REQUIRE(list_equal(l1, { 1, 2, 3 }));
                REQUIRE(list_equal(l2, { 1, 2, 3, 4 }));
            }
            SECTION("Add to both lists") {
                l1.push_back(4);
                l2.push_back(5);
                REQUIRE(list_equal(l1, { 1, 2, 3, 4 }));
                REQUIRE(list_equal(l2, { 1, 2, 3, 5 }));
            }
        }
        SECTION("Copying an empty list") {
            list l1;
            list l2(l1);
            REQUIRE(reports_as_empty(l1));
            REQUIRE(reports_as_empty(l2));
        }
    }
    SECTION("Copy assignment") {
        list l1{ { 1, 2, 3 } };
        list l2{ { 4, 5, 6, 7 } };
        SECTION("Simple assignment") {
            l2 = l1;
            REQUIRE(list_equal(l1, { 1, 2, 3 }));
            REQUIRE(list_equal(l2, { 1, 2, 3 }));
        }
        SECTION("Self assignment") {
            l2 = l2;
            REQUIRE(!l2.empty());
            REQUIRE(list_equal(l2, { 4, 5, 6, 7 }));
        }
        SECTION("Repeated assignments") {
            l2 = l1;
            REQUIRE(list_equal(l2, { 1, 2, 3 }));
            l1.push_back(4);
            l2 = l1;
            REQUIRE(list_equal(l2, { 1, 2, 3, 4 }));
            l1.push_back(5);
            l2 = l1;
            REQUIRE(list_equal(l2, { 1, 2, 3, 4, 5 }));
            l1.push_back(6);
            l2 = l1;
            REQUIRE(list_equal(l2, { 1, 2, 3, 4, 5, 6 }));
        }
    }
}


TEST_CASE("Move operations") {
    SECTION("Move constructor") {
        SECTION("From empty list") {
            list l1;
            list l2(std::move(l1));
            REQUIRE(reports_as_empty(l2));
        }
        SECTION("From full list") {
            auto elems = std::vector<double>({ 1, 2, 3, 4, 5, 6, 7, 8 });
            list l1(elems);
            list l2(std::move(l1));
            REQUIRE(list_equal(l2, elems));
        }
    }
    SECTION("Move assignment") {
        SECTION("From empty list") {
            list l1;
            SECTION("To empty list") {
                list l2;
                l2 = std::move(l1);
                REQUIRE(reports_as_empty(l2));
            }
            SECTION("To full list") {
                list l2({ 1, 2, 3, 4, 5, 6 });
                l2 = std::move(l1);
                REQUIRE(reports_as_empty(l2));
            }
        }
        SECTION("From full list") {
            list l1({ 5, 4, 3, 2, 1 });
            SECTION("to empty list") {
                list l2;
                l2 = std::move(l1);
                REQUIRE(list_equal(l2, { 5, 4, 3, 2, 1 }));
            }
            SECTION("To full list") {
                list l2({ 4, 2, 3 });
                l2 = std::move(l1);
                REQUIRE(list_equal(l2, { 5, 4, 3, 2, 1 }));
            }
        }
        SECTION("Back and forth") {
            list l1({ 44, 2, 4 });
            list l2({ 1, 2, 3, 4, 5, 6 });
            l1 = std::move(l2);
            REQUIRE(list_equal(l1, { 1, 2, 3, 4, 5, 6 }));
            l2 = std::move(l1);
            REQUIRE(list_equal(l2, { 1, 2, 3, 4, 5, 6 }));
            SECTION("List can be used after being move assigned") {
                l2.pop_front();
                l2.push_front(4);
                l2.pop_back();
                l2.push_back(24);
                REQUIRE(list_equal(l2, { 4, 2, 3, 4, 5, 24 }));
            }
        }
    }
}


TEST_CASE("member swap") {
    SECTION("Swapping empty lists leaves both empty") {
        list l1, l2;
        l1.swap(l2);
        REQUIRE(reports_as_empty(l1));
        REQUIRE(reports_as_empty(l2));
    }
    SECTION("Swapping elements actually swaps them") {
        list l1({ 1, 2, 3 });
        SECTION("empty and full") {
            list l2;
            l1.swap(l2);
            REQUIRE(reports_as_empty(l1));
            REQUIRE(list_equal(l2, {1, 2, 3}));
            SECTION("Roundtrip") {
                l1.swap(l2);
                REQUIRE(list_equal(l1, { 1, 2, 3 }));
                REQUIRE(reports_as_empty(l2));
            }
        }
        SECTION("full and full") {
            list l2({ 4, 5, 6, 7 });
            l1.swap(l2);
            REQUIRE(list_equal(l1, { 4, 5, 6, 7 }));
            REQUIRE(list_equal(l2, { 1, 2, 3 }));
            SECTION("Roundtrip") {
                l1.swap(l2);
                REQUIRE(list_equal(l1, { 1, 2, 3 }));
                REQUIRE(list_equal(l2, { 4, 5, 6, 7 }));
            }
        }
    }
}

TEST_CASE("member swap - complexity") {
    // Swap should take constant time regardless of list size.
    using namespace std::chrono_literals;
    double last_time = 0;
    for (int i = 64'000; i <= 20'000'000; i *= 4) {
        auto lhs_elems = generate_data(i);
        list lhs{ lhs_elems };
        auto rhs_elems = generate_data(i);
        list rhs{ rhs_elems };
        auto start_time = std::chrono::high_resolution_clock::now();
        lhs.swap(rhs);
        auto end_time = std::chrono::high_resolution_clock::now();
        // Add 0.5 ms to even out variability
        auto time_diff = end_time - start_time + 500us;
        auto total_time = static_cast<double>(time_diff.count());
        lhs.swap(rhs);
        REQUIRE(list_equal(lhs, lhs_elems));
        if (last_time != 0) {
            // The coefficient is a rough approximation.
            // A linear time coefficient should blow out, constant time
            // jitter should remain inside some reasonable coefficient
            REQUIRE(total_time / last_time <= 1.25);
        }
        last_time = total_time;
        std::cout << "member swap: i = " << i << " total time = " << (time_diff - 500us).count() << '\n';
    }
}

TEST_CASE("swap overload") {
    SECTION("Swapping empty lists leaves both empty") {
        list l1, l2;
        swap(l1, l2);
        REQUIRE(reports_as_empty(l1));
        REQUIRE(reports_as_empty(l2));
    }
    SECTION("Swapping elements actually swaps them") {
        list l1({ 1, 2, 3 });
        SECTION("empty and full") {
            list l2;
            swap(l1, l2);
            REQUIRE(reports_as_empty(l1));
            REQUIRE(list_equal(l2, { 1, 2, 3 }));
            SECTION("Roundtrip") {
                swap(l1, l2);
                REQUIRE(list_equal(l1, { 1, 2, 3 }));
                REQUIRE(reports_as_empty(l2));
            }
        }
        SECTION("full and full") {
            list l2({ 4, 5, 6, 7 });
            swap(l1, l2);
            REQUIRE(list_equal(l1, { 4, 5, 6, 7 }));
            REQUIRE(list_equal(l2, { 1, 2, 3 }));
            SECTION("Roundtrip") {
                swap(l1, l2);
                REQUIRE(list_equal(l1, { 1, 2, 3 }));
                REQUIRE(list_equal(l2, { 4, 5, 6, 7 }));
            }
        }
    }
}

TEST_CASE("swap overload - complexity") {
    // Swap should take constant time regardless of list size.
    using namespace std::chrono_literals;
    double last_time = 0;
    for (int i = 64'000; i <= 20'000'000; i *= 4) {
        auto lhs_elems = generate_data(i);
        list lhs{ lhs_elems };
        auto rhs_elems = generate_data(i);
        list rhs{ rhs_elems };
        auto start_time = std::chrono::high_resolution_clock::now();
        swap(lhs, rhs);
        auto end_time = std::chrono::high_resolution_clock::now();
        // Add 0.5 ms to even out variability
        auto time_diff = end_time - start_time + 500us;
        auto total_time = static_cast<double>(time_diff.count());
        swap(lhs, rhs);
        REQUIRE(list_equal(lhs, lhs_elems));
        if (last_time != 0) {
            // The coefficient is a rough approximation.
            // A linear time coefficient should blow out, constant time
            // jitter should remain inside some reasonable coefficient
            REQUIRE(total_time / last_time <= 1.25);
        }
        last_time = total_time;
        std::cout << "swap overload: i = " << i << " total time = " << (time_diff - 500us).count() << '\n';
    }
}


TEST_CASE("split") {
    SECTION("Small lists") {
        list l({ 1 });
        SECTION("List of one item, everything to lhs") {
            auto p = l.split(end(l));
            REQUIRE(reports_as_empty(l));
            REQUIRE(p.first.size() == 1);
            REQUIRE(p.first.front() == 1);
            REQUIRE(reports_as_empty(p.second));
        }
        SECTION("List of one item, everything to rhs") {
            auto p = l.split(begin(l));
            REQUIRE(reports_as_empty(l));
            REQUIRE(reports_as_empty(p.first));
            REQUIRE(p.second.size() == 1);
            REQUIRE(p.second.front() == 1);
        }
        SECTION("Two items, split down the middle") {
            l.push_back(2);
            auto iter = begin(l);
            std::advance(iter, 1);
            auto p = l.split(iter);
            REQUIRE(reports_as_empty(l));
            REQUIRE(p.first.size() == 1);
            REQUIRE(p.first.back() == 1);
            REQUIRE(p.second.size() == 1);
            REQUIRE(p.second.front() == 2);
        }
    }
    SECTION("Many items") {
        auto elems = std::vector<double>({ 1, 2, 3, 4, 5, 6, 7, 8, 9 });
        list l{ elems };
        SECTION("all to lhs") {
            auto p = l.split(end(l));
            REQUIRE(reports_as_empty(l));
            REQUIRE(list_equal(p.first, elems));
            REQUIRE(reports_as_empty(p.second));
        }
        SECTION("all to rhs") {
            auto p = l.split(begin(l));
            REQUIRE(reports_as_empty(l));
            REQUIRE(reports_as_empty(p.first));
            REQUIRE(list_equal(p.second, elems));
        }
        SECTION("split somewhere in the middle") {
            auto offset = 4;
            auto iter = begin(l);
            std::advance(iter, offset);
            auto p = l.split(iter);
            REQUIRE(reports_as_empty(l));
            REQUIRE(list_equal(p.first, {begin(elems), begin(elems) + offset}));
            REQUIRE(list_equal(p.second, {begin(elems) + offset, end(elems)}));
        }
    }
    //DO:
    //    usage of said lists?
}

TEST_CASE("split - complexity") {
    // Basically, join can either be implemented correctly, taking only constant time
    // Or incorrectly, with pop, push combo in linear time. We want to detect the seconds
    // case and fail the tests
    using namespace std::chrono_literals;
    double last_time = 0;
    for (int i = 64'000; i <= 20'000'000; i *= 4) {
        auto elems = generate_data(i);
        list l{ elems };
        auto it = l.cbegin();
        std::advance(it, 10);
        auto start_time = std::chrono::high_resolution_clock::now();
        auto parts = l.split(it);
        auto end_time = std::chrono::high_resolution_clock::now();
        // Add 0.5 ms to even out variability
        auto time_diff = end_time - start_time + 500us;
        auto total_time = static_cast<double>(time_diff.count());
        REQUIRE(list_equal(parts.first, { elems.begin(), elems.begin() + 10 }));
        REQUIRE(list_equal(parts.second, { elems.begin() + 10, elems.end() }));
        if (last_time != 0) {
            // The coefficient is a rough approximation.
            // A linear time coefficient should blow out, constant time
            // jitter should remain inside some reasonable coefficient
            REQUIRE(total_time / last_time <= 1.25);
        }
        last_time = total_time;
        std::cout << "split: i = " << i << " total time = " << (time_diff - 500us).count() << '\n';
    }
}


TEST_CASE("sort") {
    SECTION("empty list") {
        list l;
        l.sort();
        REQUIRE(reports_as_empty(l));
    }
    SECTION("list with single elements") {
        list l{ { 42 } };
        l.sort();
        REQUIRE(l.size() == 1);
        REQUIRE(l.front() == l.back());
        REQUIRE(l.front() == 42);
    }
    // These are kept small to make the normal runtime manageable
    SECTION("unsorted elements 1") {
        auto elems = generate_data(1234);
        list l{ elems };
        l.sort();
        std::sort(begin(elems), end(elems));
        REQUIRE(list_equal(l, elems));
   }
    SECTION("unsorted elements 2") {
        auto elems = generate_data(1345);
        list l{ elems };
        l.sort();
        std::sort(begin(elems), end(elems));
        REQUIRE(list_equal(l, elems));
    }
    SECTION("unsorted elements 3") {
        auto elems = generate_data(5522);
        list l{ elems };
        l.sort();
        std::sort(begin(elems), end(elems));
        REQUIRE(list_equal(l, elems));

    }
    SECTION("unsorted elements 4") {
        auto elems = generate_data(1287);
        list l{ elems };
        l.sort();
        std::sort(begin(elems), end(elems));
        REQUIRE(list_equal(l, elems));
    }
}

TEST_CASE("sort - complexity") {
    // This is kinda complex...
    // We have to both test enough different sizes, and abort early in case of
    // quadratic (or worse) complexity.
    double last_time = 0;
    for (int i = 16'000; i <= 5'000'000; i *= 4) {
        auto elems = generate_data(i);
        list l{ elems };
        auto start_time = std::chrono::high_resolution_clock::now();
        l.sort();
        auto end_time = std::chrono::high_resolution_clock::now();
        auto time_diff = end_time - start_time;
        auto time_per_item = time_diff.count() / static_cast<double>(i);
        std::sort(begin(elems), end(elems));
        REQUIRE(list_equal(l, elems));
        if (last_time != 0) {
            // The coefficient is a rough approximation.
            // Even a very naive implementation can hit about 1.5 coefficient...
            REQUIRE(time_per_item / last_time <= 2);
        }
        std::cout << "sort: i = " << i << " time per item = " << time_per_item << '\n';
        last_time = time_per_item;
    }
}


TEST_CASE("Compilation tests") {
    list l({ 1, 2 });
    const auto& const_l = l;
    REQUIRE(const_l.front() == 1);
    REQUIRE(const_l.back() == 2);
    REQUIRE(!const_l.empty());
    REQUIRE(const_l.size() == 2);
    list::iterator it = l.begin();
    list::const_iterator cit = it;
    static_cast<void>(cit); // silence unused variable warning
}
