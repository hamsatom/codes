#ifndef PJC_LIST_HPP
#define PJC_LIST_HPP

#include <cstddef>
#include <vector>
#include <iterator>

using std::size_t;

class list {
private:
	struct node {
		double val;
		node* prev;
		node* next;
	};

public:

	class const_iterator {
		node* current_ptr;
		const list* o_list;
	public:
		using difference_type = std::ptrdiff_t;
		using iterator_category = std::bidirectional_iterator_tag;
		using value_type = const double;
		using reference = const double&;
		using pointer = const double*;

		const_iterator();
		const_iterator(node* ptr, const list* gen);
		const_iterator(const const_iterator& rhs);
		const_iterator& operator=(const const_iterator& rhs);

		const_iterator& operator++();
		const_iterator operator++(int);
		const_iterator& operator--();
		const_iterator operator--(int);

		reference operator*() const;
		pointer operator->() const;

		bool operator==(const const_iterator& rhs) const;
		bool operator!=(const const_iterator& rhs) const;

		friend class list;
	};

	class iterator {
		node* current_ptr;
		const list* o_list;
	public:
		using difference_type = std::ptrdiff_t;
		using iterator_category = std::bidirectional_iterator_tag;
		using value_type = double;
		using reference = double&;
		using pointer = double*;

		iterator();
		iterator(node* ptr, const list* gen);
		iterator(const iterator& rhs);
		iterator& operator=(const iterator& rhs);

		iterator& operator++();
		iterator operator++(int);
		iterator& operator--();
		iterator operator--(int);

		reference operator*() const;
		pointer operator->() const;

		operator const_iterator() const;

		bool operator==(const iterator& rhs) const;
		bool operator!=(const iterator& rhs) const;

		friend class list;
	};

	/*
	* After constructor the list must be usable.
	*/
	list();

	/*
	* Creates a list containing all elements from vec.
	*/
	list(const std::vector<double>& vec);

	/*
	* After destructor, the list must be empty and not hold on to any memory.
	* Destructor will only be called once over the lifetime of a list.
	*/
	~list();

	/*
	* Inserts an element at the end of the list.
	*
	* Should run in in constant time.
	*/
	void push_back(double elem);

	/*
	* Removes an element from the end of the list.
	*
	* Precondition: Should not be called on an empty list.
	* Should run in in constant time.
	*/
	void pop_back();

	/*
	* Allows access to the last element of the list.
	*
	* Precondition: Should not be called on an empty list.
	* Should run in in constant time.
	*/
	double& back();
	double back() const;

	/*
	* Inserts an element at the front of the list.
	*
	* Should run in in constant time.
	*/
	void push_front(double elem);

	/*
	* Removes an element from the front of the list.
	*
	* Precondition: Should not be called on an empty list.
	* Should run in in constant time.
	*/
	void pop_front();

	/*
	* Allows access to the first element of the list.
	*
	* Precondition: Should not be called on an empty list.
	* Should run in in constant time.
	*/
	double& front();
	double front() const;

	/*
	* Destructively appends list r to list l, so that list r ends up empty.
	*
	* Should run in constant time.
	*/
	void join(list& r);

	/*
	* Reverses order of all elements of list l.
	* Given list containing 1 2 3 4 2 1, the result should be a list containing
	* 1 2 4 3 2 1.
	*
	* Should run in linear time.
	*/
	void reverse();

	/*
	* Filters elements in the list so that there are no immediate duplicates.
	* Given list containing 1 1 2 2 2 3 4 2 1, the result should be a list
	* containing 1 2 3 4 2 1.
	*
	* Should run in linear time.
	*/
	void unique();

	/*
	* Returns size of the list l.
	*
	* Should run in constant time.
	*/
	size_t size() const;

	/*
	* Returns true if the list l does not contain any elements.
	*
	* Should run in constant time.
	*/
	bool empty() const;

	/*
	* Removes all elements with the specified value from the list.
	*
	* Should run in time linear to size of the list.
	*/
	void remove(double value);

	/*
	* Merges sorted list rhs into this list.
	*
	* Precondition: Both lists are sorted.
	* Should run in time linear to both lists.
	*/
	void merge(list& rhs);

	// Standard copy operations.
	list(const list& rhs);
	list& operator=(const list& rhs);

	// Move operations
	// After the move is performed, the moved from list should be assignable and destructible.
	list(list&& rhs);
	list& operator=(list&& rhs);

	/*
	* Swaps all elements between this list and the rhs list.
	*
	* Should run in constant time.
	*/
	void swap(list& rhs);

	// Iterator creation -- reverse iterator overloads are intentionally missing, to save some work
	iterator begin();
	iterator end();
	const_iterator begin() const;
	const_iterator end() const;
	const_iterator cbegin() const;
	const_iterator cend() const;

	/*
	* Two lists are equal iff all of their elements are the same.
	*/
	bool operator==(const list& rhs) const;
	/*
	* Lists are compared lexicographically.
	*/
	bool operator<(const list& rhs) const;

	/*
	* Destructively splits the list into the two that are returned.
	* The first list consists of elements formerly in the [begin, place) range.
	* The second list consists of elements formerly in the [place, end) range.
	*/
	std::pair<list, list> split(const_iterator place);

	/*
	* Sorts the list in place.
	* Should be stable and run in O(n log n).
	*/
	void sort();

private:
	node* head;
	node* tail;
	size_t num_elements;
};

/*
* Two lists are not equal iff any of their elements differ.
*/
bool operator!=(const list& lhs, const list& rhs);
/*
* Lists are compared lexicographically.
*/
bool operator>(const list& lhs, const list& rhs);
bool operator<=(const list& lhs, const list& rhs);
bool operator>=(const list& lhs, const list& rhs);


/*
* ADL customization point for std::swap.
* Should do the same as calling lhs.swap(rhs)
*/
void swap(list& lhs, list& rhs);

#endif
