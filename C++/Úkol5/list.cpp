#include "list.hpp"
#include <stdexcept>
#include <algorithm>
#include <iostream>

list::list() {
	head = tail = nullptr;
	num_elements = 0;
}


list::list(const std::vector<double>& vec) : list() {
	for (double elem : vec) {
		push_back(elem);
	}
}

list::~list() {
	list::node *next_node;
	while (head != nullptr)
	{
		next_node = head->next;
		delete head;
		head = next_node;
	}
}

void list::push_back(double elem) {
	node *new_tail = new node{ elem, tail, nullptr };
	++num_elements;
	if (tail == nullptr) {
		head = tail = new_tail;
		return;
	}
	tail->next = new_tail;
	tail = new_tail;
}

void list::pop_back() {
	if (tail == nullptr) {
		return;
	}
	list::node *new_tail = tail->prev;
	--num_elements;
	new_tail != nullptr ? new_tail->next = nullptr : head = nullptr;
	delete tail;
	tail = new_tail;
}

double& list::back() {
	if (tail == nullptr) {
		throw std::invalid_argument("Tail of the list is null");
	}
	return tail->val;
}

double list::back() const {
	if (tail == nullptr) {
		throw std::invalid_argument("Tail of the list is null");
	}
	return tail->val;
}

void list::push_front(double elem) {
	list::node *new_head = new list::node{ elem, nullptr, head };
	++num_elements;
	if (head == nullptr) {
		head = tail = new_head;
		return;
	}
	head->prev = new_head;
	head = new_head;
}

void list::pop_front() {
	if (head == nullptr) {
		return;
	}
	list::node *new_head = head->next;
	--num_elements;
	new_head != nullptr ? new_head->prev = nullptr : tail = nullptr;
	delete head;
	head = new_head;
}

double& list::front() {
	if (head == nullptr) {
		throw std::invalid_argument("Head of the list is null");
	}
	return head->val;
}

double list::front() const {
	if (head == nullptr) {
		throw std::invalid_argument("Head of the list is null");
	}
	return head->val;
}

void list::join(list& r) {
	if (r.head == nullptr) {
		return;
	}
	if (tail == nullptr) {
		swap(r);
		return;
	}
	tail->next = r.head;
	r.head->prev = tail;
	tail = r.tail;
	num_elements += r.num_elements;
	r.head = nullptr;
	r.tail = nullptr;
	r.num_elements = 0;
}

void list::reverse() {
	if (head == tail) {
		return;
	}
	list::node *cur_node = head, *next_node;
	while (cur_node != nullptr)
	{
		next_node = cur_node->next;
		std::swap(cur_node->prev, cur_node->next);
		cur_node = next_node;
	}
	std::swap(head, tail);
}

void list::unique() {
	if (head == nullptr) {
		return;
	}
	list::node *from = head, *cur_node = head->next, *next_node;
	while (cur_node != tail)
	{
		next_node = cur_node->next;
		if (from->val == cur_node->val) {
			delete cur_node;
			--num_elements;
		}
		else
		{
			from->next = cur_node;
			cur_node->prev = from;
			from = cur_node;
		}
		cur_node = next_node;
	}
	if (from->val == tail->val) {
		delete tail;
		from->next = nullptr;
		tail = from;
		--num_elements;
	}
}

std::size_t list::size() const { return num_elements; }

bool list::empty() const { return num_elements == 0; }

void list::remove(double value) {
	if (head == nullptr) {
		return;
	}
	list::node *cur_node = head, *next_node, *previous_node = nullptr;
	bool first = true;
	while (cur_node != nullptr)
	{
		while (cur_node != nullptr && cur_node->val == value)
		{
			next_node = cur_node->next;
			delete cur_node;
			--num_elements;
			cur_node = next_node;
		}
		if (first) {
			head = cur_node;
			first = false;
		}
		else
		{
			previous_node->next = cur_node;
		}
		if (cur_node == nullptr) {
			break;
		}
		cur_node->prev = previous_node;
		previous_node = cur_node;
		cur_node = cur_node->next;
	}
	tail = previous_node;
}

void list::merge(list& rhs) {

	if (rhs.head == nullptr) {
		return;
	}
	if (head == nullptr) {
		swap(rhs);
		return;
	}
	list::node *this_node = head->next, *rhs_node = rhs.head, *prev_node;
	if (rhs.head->val < head->val) {
		this_node = head;
		head = rhs.head;
		rhs_node = head->next;
	}
	prev_node = head;
	while (rhs_node != nullptr && this_node != nullptr)
	{
		if (this_node->val < rhs_node->val) {
			prev_node->next = this_node;
			this_node->prev = prev_node;
			prev_node = this_node;
			this_node = this_node->next;
		}
		else
		{
			prev_node->next = rhs_node;
			rhs_node->prev = prev_node;
			prev_node = rhs_node;
			rhs_node = rhs_node->next;
		}
	}
	if (rhs_node == nullptr) {
		prev_node->next = this_node;
		this_node->prev = prev_node;
	}
	else
	{
		prev_node->next = rhs_node;
		rhs_node->prev = prev_node;
		tail = rhs.tail;
	}

	num_elements += rhs.num_elements;
	rhs.head = rhs.tail = nullptr;
	rhs.num_elements = 0;
}

// Standard copy operations.
list::list(const list& rhs) : list() {
	for (auto node_value : rhs) {
		push_back(node_value);
	}
}

list& list::operator=(const list& rhs) {
	if (this == &rhs) {
		return *this;
	}
	while (num_elements > 0)
	{
		pop_back();
	}
	for (auto node_value : rhs) {
		push_back(node_value);
	}
	return *this;
}

// Move operations
// After the move is performed, the moved from list should be assignable and destructible.
list::list(list&& rhs) : list()
{
	swap(rhs);
}


list& list::operator=(list&& rhs) {
	if (this == &rhs) {
		return *this;
	}
	while (num_elements > 0)
	{
		pop_back();
	}
	swap(rhs);
	return *this;
}

/*
* Swaps all elements between this list and the rhs list.
*
* Should run in constant time.
*/
void list::swap(list& rhs) {
	std::swap(head, rhs.head);
	std::swap(tail, rhs.tail);
	std::swap(num_elements, rhs.num_elements);
}

// Iterator creation -- reverse iterator overloads are intentionally missing, to save some work
list::iterator list::begin() { return iterator(head, this); }
list::iterator list::end() {
	return iterator(nullptr, this);
}
list::const_iterator list::begin() const { return const_iterator(head, this); }
list::const_iterator list::end() const {
	return const_iterator(nullptr, this);
}
list::const_iterator list::cbegin() const { return const_iterator(head, this); }
list::const_iterator list::cend() const {
	return const_iterator(nullptr, this);
}


/*
* Two lists are equal if all of their elements are the same.
*/
bool list::operator==(const list& rhs) const {
	if (num_elements != rhs.num_elements) {
		return false;
	}
	return std::equal(begin(), end(), rhs.begin());
}

/*
* Lists are compared lexicographically.
*/
bool list::operator<(const list& rhs) const {
	if (rhs.num_elements != num_elements) {
		return num_elements < rhs.num_elements;
	}

	list::node *this_node = head, *rhs_node = rhs.head;
	while (this_node != nullptr)
	{
		if (this_node->val < rhs_node->val) {
			return true;
		}
		else if (this_node->val > rhs_node->val)
		{
			return false;
		}
		this_node = this_node->next;
		rhs_node = rhs_node->next;
	}
	return false;
}

/*
* Destructively splits the list into the two that are returned.
* The first list consists of elements formerly in the [begin, place) range.
* The second list consists of elements formerly in the [place, end) range.
*/
std::pair<list, list> list::split(list::const_iterator place) {
	list first_list{}, second_list{};
	if (head == nullptr) {
		goto returning;
	}
	if (head == place.current_ptr) {
		swap(second_list);
		goto returning;
	}
	if (place.current_ptr == nullptr) {
		swap(first_list);
		goto returning;
	}
	first_list.head = head;
	first_list.num_elements = std::distance(cbegin(), place);
	first_list.tail = place.current_ptr->prev;

	second_list.head = place.current_ptr;
	if (second_list.head == nullptr) {
		second_list.tail = nullptr;
	}
	else
	{
		second_list.head->prev = nullptr;
		second_list.tail = tail;
	}
	second_list.num_elements = num_elements - first_list.num_elements;
	first_list.tail->next = nullptr;

	tail = head = nullptr;
	num_elements = 0;

returning:
	return{ std::move(first_list), std::move(second_list) };
}

/*
* Sorts the list in place.
* Should be stable and run in O(n log n).
*/
void list::sort() {
	if (head == tail) {
		return;
	}
	const_iterator it = begin();
	std::advance(it, num_elements / 2);
	std::pair<list, list> halves = split(it);
	halves.first.sort();
	halves.second.sort();
	halves.first.merge(halves.second);
	halves.first.swap(*this);
}


/*
* Two lists are not equal iff any of their elements differ.
*/
bool operator!=(const list& lhs, const list& rhs) { return !(lhs == rhs); }
/*
* Lists are compared lexicographically.
*/
bool operator>(const list& lhs, const list& rhs) { return !(rhs >= lhs); }
bool operator<=(const list& lhs, const list& rhs) { return !(lhs > rhs); }
bool operator>=(const list& lhs, const list& rhs) { return !(lhs < rhs); }


/*
* ADL customization point for std::swap.
* Should do the same as calling lhs.swap(rhs)
*/
void swap(list& lhs, list& rhs) {
	lhs.swap(rhs);
}

list::const_iterator::const_iterator()
{
	current_ptr = nullptr;
	o_list = nullptr;
}

list::const_iterator::const_iterator(node * ptr, const list * gen)
{
	current_ptr = ptr;
	o_list = gen;
}

list::const_iterator::const_iterator(const const_iterator & rhs)
{
	current_ptr = rhs.current_ptr;
	o_list = rhs.o_list;
}

list::const_iterator & list::const_iterator::operator=(const const_iterator & rhs)
{
	current_ptr = rhs.current_ptr;
	o_list = rhs.o_list;
	return *this;
}

list::const_iterator & list::const_iterator::operator++()
{
	current_ptr = current_ptr->next;
	return *this;
}

list::const_iterator list::const_iterator::operator++(int)
{
	const_iterator tmp(*this);
	current_ptr = current_ptr->next;
	return tmp;
}

list::const_iterator & list::const_iterator::operator--()
{
	if (current_ptr == nullptr) {
		current_ptr = o_list->tail;
	}
	else {
		current_ptr = current_ptr->prev;
	}
	return *this;
}

list::const_iterator list::const_iterator::operator--(int)
{
	const_iterator tmp(*this);
	if (current_ptr == nullptr) {
		current_ptr = o_list->tail;
	}
	else {
		current_ptr = current_ptr->prev;
	}
	return tmp;
}

list::const_iterator::reference list::const_iterator::operator*() const
{
	return current_ptr->val;
}

list::const_iterator::pointer list::const_iterator::operator->() const
{
	return &current_ptr->val;
}

bool list::const_iterator::operator==(const const_iterator & rhs) const
{
	return current_ptr == rhs.current_ptr && o_list == rhs.o_list;
}

bool list::const_iterator::operator!=(const const_iterator & rhs) const
{
	return !(*this == rhs);
}

list::iterator::iterator() {
	current_ptr = nullptr;
	o_list = nullptr;
}

list::iterator::iterator(node* ptr, const list* gen) {
	current_ptr = ptr;
	o_list = gen;
}

list::iterator::iterator(const iterator& rhs) {
	current_ptr = rhs.current_ptr;
	o_list = rhs.o_list;
}

list::iterator & list::iterator::operator=(const iterator & rhs)
{
	current_ptr = rhs.current_ptr;
	o_list = rhs.o_list;
	return *this;
}

list::iterator::reference list::iterator::operator*() const
{
	return current_ptr->val;
}

list::iterator::pointer list::iterator::operator->() const
{
	return &current_ptr->val;
}

list::iterator::operator list::const_iterator() const
{
	return const_iterator(current_ptr, o_list);
}

list::iterator& list::iterator::operator++() {
	current_ptr = current_ptr->next;
	return *this;
}
list::iterator list::iterator::operator++(int) {
	iterator tmp(*this);
	current_ptr = current_ptr->next;
	return tmp;
}
list::iterator& list::iterator::operator--() {
	if (current_ptr == nullptr) {
		current_ptr = o_list->tail;
	}
	else {
		current_ptr = current_ptr->prev;
	}
	return *this;
}
list::iterator list::iterator::operator--(int) {
	iterator tmp(*this);
	if (current_ptr == nullptr) {
		current_ptr = o_list->tail;
	}
	else {
		current_ptr = current_ptr->prev;
	}
	return tmp;
}

bool list::iterator::operator==(const iterator& rhs) const {
	return current_ptr == rhs.current_ptr && o_list == rhs.o_list;
}
bool list::iterator::operator!=(const iterator& rhs) const {
	return !(*this == rhs);
}