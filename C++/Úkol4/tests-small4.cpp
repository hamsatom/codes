#include "catch.hpp"
#include <algorithm>
#include <vector>
#include <iterator>

#include "list.hpp"



bool list_equal(const list& l, const std::vector<double>& numbers) {
    // This might help to prevent hard crashes in some cases.
    // But its rather unlikely, because its the internal structure that gets people, not incrementing/decrementing sizes.
    if (l.size() != numbers.size()) {
        return false;
    }


    // Check the internal structure
    // Front to back
    {
        auto* cursor = l.head;
        auto expected = begin(numbers);

        while (cursor != nullptr) {
            if (expected == end(numbers)) {
                return false;
            }
            if (cursor->val != *expected) {
                return false;
            }
            cursor = cursor->next;
            ++expected;
        }
    }
    // Back to front
    {
        auto* cursor = l.tail;
        auto expected = rbegin(numbers);

        while (cursor != nullptr) {
            if (expected == rend(numbers)) {
                return false;
            }
            if (cursor->val != *expected) {
                return false;
            }
            cursor = cursor->prev;
            ++expected;
        }
    }
    return true;
}

namespace {
bool reports_as_empty(const list& l) {
    return l.empty() && (l.size() == 0);
}
}


TEST_CASE("Basic functionality -- constructors, {pop, push}_{front, back}, size, empty") {
    list l;
    REQUIRE(reports_as_empty(l));
    SECTION("Vector constructor") {
        SECTION("Empty vector -> empty list") {
            REQUIRE(reports_as_empty(list{ std::vector<double>{} }));
        }
        SECTION("Singular vector -> singular list") {
            list l2({ 1 });
            REQUIRE(!l2.empty());
            REQUIRE(l2.size() == 1);
            REQUIRE(l2.front() == 1);
            REQUIRE(l2.back() == 1);
        }
        SECTION("Multiple elements") {
            std::vector<double> elems({ 1, 2, 3, 56, 78, 86 });
            list l3(elems);
            REQUIRE(!l3.empty());
            REQUIRE(l3.size() == 6);
            REQUIRE(list_equal(l3, elems));
        }
    }
    SECTION("Series of push_back") {
        for (int i : {0, 2, 5, 9}) {
            l.push_back(i);
            REQUIRE(l.back() == i);
        }
        REQUIRE(l.size() == 4);
        REQUIRE(list_equal(l, { 0, 2, 5, 9 }));
    }
    SECTION("Series of push_front") {
        for (int i : {0, 3, 7, 2}) {
            l.push_front(i);
            REQUIRE(l.front() == i);
        }
        REQUIRE(l.size() == 4);
        REQUIRE(list_equal(l, { 2, 7, 3, 0 }));
    }
    SECTION("Series of push_back and pop_back") {
        for (int i = 0; i < 30; i += 4) {
            l.push_back(i);
            REQUIRE(l.back() == i);
            REQUIRE(l.front() == i);
            REQUIRE(l.size() == 1);
            l.pop_back();
            REQUIRE(reports_as_empty(l));
        }
        REQUIRE(reports_as_empty(l));
    }
    SECTION("Series of push_back and pop_front") {
        for (int i = 0; i < 30; i += 4) {
            l.push_back(i);
            REQUIRE(l.back() == i);
            REQUIRE(l.front() == i);
            REQUIRE(l.size() == 1);
            l.pop_front();
            REQUIRE(reports_as_empty(l));
        }
        REQUIRE(reports_as_empty(l));
    }
    SECTION("Series of push_front and pop_front") {
        for (int i = 0; i < 30; i += 4) {
            l.push_front(i);
            REQUIRE(l.back() == i);
            REQUIRE(l.front() == i);
            REQUIRE(l.size() == 1);
            l.pop_front();
            REQUIRE(reports_as_empty(l));
        }
        REQUIRE(reports_as_empty(l));
    }
    SECTION("Series of push_front and pop_back") {
        for (int i = 0; i < 30; i += 4) {
            l.push_front(i);
            REQUIRE(l.back() == i);
            REQUIRE(l.front() == i);
            REQUIRE(l.size() == 1);
            l.pop_back();
            REQUIRE(reports_as_empty(l));
        }
        REQUIRE(reports_as_empty(l));
    }
    SECTION("complex 1") {
        for (int i : {0, 2, 4, 6, 8}) {
            l.push_back(i);
        }
        REQUIRE(l.size() == 5);
        l.pop_back();
        l.pop_front();
        REQUIRE(l.size() == 3);
        for (int i : {1, 3, 0}) {
            l.push_front(i);
        }
        REQUIRE(list_equal(l, { 0, 3, 1, 2, 4, 6 }));
    }
    SECTION("complex 2") {
        for (int i : {0, 2, 4}) {
            l.push_back(i);
            l.push_front(i);
        }
        REQUIRE(list_equal(l, { 4, 2, 0, 0, 2, 4 }));
        l.pop_front();
        l.pop_back();
        l.pop_back();
        REQUIRE(l.size() == 3);
        REQUIRE(l.front() == 2);
        REQUIRE(l.back() == 0);
        for (int i : {1, 2, 3}) {
            l.push_front(i);
            l.push_back(i);
        }
        REQUIRE(list_equal(l, { 3, 2, 1, 2, 0, 0, 1, 2, 3 }));
    }
    SECTION("complex 3") {
        for (int i : {0, 2, 4}) {
            l.push_back(i);
        }
        for (int i : {1, 3, 5}) {
            l.push_front(i);
        }
        REQUIRE(list_equal(l, { 5, 3, 1, 0, 2, 4 }));
        for (int i : {1, 2}) {
            l.push_front(i);
        }
        for (int i : {3, 5}) {
            l.pop_back();
            l.push_back(i);
        }
        REQUIRE(list_equal(l, { 2, 1, 5, 3, 1, 0, 2, 5 }));
    }
    SECTION("complex 4") {
        for (int i : {1, 2, 3, 4}) {
            l.push_back(i);
        }
        for (int i = 0; i < 2; ++i) {
            l.pop_back();
            l.pop_front();
        }
        REQUIRE(reports_as_empty(l));

        for (int i : {1, 2, 3, 4}) {
            l.push_back(i);
        }
        l.pop_back();
        l.pop_back();
        for (int i : {5, 6}) {
            l.push_back(i);
        }
        l.pop_front();
        l.pop_front();
        l.pop_front();
        REQUIRE(l.size() == 1);
        REQUIRE(!l.empty());
        for (int i : {7, 8}) {
            l.push_front(i);
        }
        REQUIRE(list_equal(l, { 8, 7, 6 }));
    }
}


TEST_CASE("join") {
    auto elems = { 0., 2., 4. };
    SECTION("empty lists") {
        list lhs, rhs;
        lhs.join(rhs);
        REQUIRE(reports_as_empty(lhs));
        REQUIRE(reports_as_empty(rhs));
    }
    SECTION("rhs is empty") {
        list lhs{ elems }, rhs;
        lhs.join(rhs);
        REQUIRE(list_equal(lhs, elems));
        REQUIRE(reports_as_empty(rhs));
    }
    SECTION("lhs is empty") {
        list lhs, rhs{ elems };
        lhs.join(rhs);
        REQUIRE(list_equal(lhs, elems));
        REQUIRE(reports_as_empty(rhs));
    }
    SECTION("both lists have elements in them") {
        list lhs{ elems }, rhs{ elems };
        lhs.join(rhs);
        REQUIRE(list_equal(lhs, { 0, 2, 4, 0, 2, 4 }));
        REQUIRE(reports_as_empty(rhs));
    }
}


TEST_CASE("reverse") {
    SECTION("Empty list") {
        list l;
        l.reverse();
        REQUIRE(reports_as_empty(l));
    }
    SECTION("List with single element") {
        list l{ {10} };
        l.reverse();
        REQUIRE(l.back() == 10);
        REQUIRE(l.front() == 10);
        SECTION("List with two elements") {
            l.push_front(20);
            l.reverse();
            REQUIRE(list_equal(l, { 10, 20 }));
        }
    }
    SECTION("List with many elements") {
        auto elems = std::vector<double>{ 0., 1., 2., 3., 4., 5., 6., 7., 8., 9. };
        list l{ elems };
        SECTION("One reverse") {
            l.reverse();
            std::reverse(begin(elems), end(elems));
            REQUIRE(list_equal(l, elems));
            SECTION("Two reverses cancel each other out") {
                l.reverse();
                std::reverse(begin(elems), end(elems));
                REQUIRE(list_equal(l, elems));
            }
        }
    }
}


TEST_CASE("unique") {
    SECTION("Empty list") {
        list l;
        l.unique();
        REQUIRE(reports_as_empty(l));
    }
    SECTION("Degenerated case 1") {
        list l{ {1, 1, 1, 1, 1, 1, 1, 1, 1} };
        l.unique();
        REQUIRE(list_equal(l, { 1 }));
    }
    SECTION("Unique elements") {
        auto elems = std::vector<double>{ 1, 2, 3, 4, 5 };
        list l{ elems };
        l.unique();
        REQUIRE(list_equal(l, elems));
    }
    SECTION("Sorted duplicate elements") {
        auto elems = std::vector<double>{ 1, 2, 3, 5, 5, 5 };
        list l{ elems };
        l.unique();
        elems.erase(std::unique(begin(elems), end(elems)), end(elems));
        REQUIRE(list_equal(l, elems));
    }
    SECTION("Unsorted duplicate elements") {
        auto elems = std::vector<double>{ { 1, 2, 2, 3, 3, 3, 4, 1, 2, 5, 5, 5 } };
        list l{ elems };
        l.unique();
        elems.erase(std::unique(begin(elems), end(elems)), end(elems));
        REQUIRE(list_equal(l, elems));
    }
}


TEST_CASE("remove") {
    SECTION("Basic") {
        auto elements = std::vector<double>{ 0, 2, 2, 3, 2, 2, 3, 3, 2, 0, 1 };
        list l{ elements };
        SECTION("Remove 2") {
            l.remove(2);
            elements.erase(std::remove(begin(elements), end(elements), 2),
                           end(elements));
            REQUIRE(list_equal(l, elements));
        }
        SECTION("Remove something not in list") {
            l.remove(5);
            REQUIRE(list_equal(l, elements));
        }
        SECTION("Remove head") {
            l.remove(0);
            elements.erase(std::remove(begin(elements), end(elements), 0),
                           end(elements));
            REQUIRE(list_equal(l, elements));
        }
        SECTION("Remove tail") {
            l.remove(1);
            elements.erase(std::remove(begin(elements), end(elements), 1),
                           end(elements));
            REQUIRE(list_equal(l, elements));
        }
        SECTION("Remove head and tail") {
            l.remove(0);
            l.remove(1);
            elements.erase(std::remove(begin(elements), end(elements), 0),
                           end(elements));
            elements.erase(std::remove(begin(elements), end(elements), 1),
                           end(elements));
            REQUIRE(list_equal(l, elements));
        }
        SECTION("Remove to empty") {
            for (int i = 0; i < 4; ++i) {
                l.remove(i);
            }
            REQUIRE(reports_as_empty(l));
        }
    }
    SECTION("Remove all") {
        list l({ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 });
        l.remove(1);
        REQUIRE(reports_as_empty(l));
        SECTION("Verify that the list can still be used") {
            for (auto e : { -1, 1, 2, 3 }) {
                l.push_back(e);
                l.push_front(e);
            }
            REQUIRE(!l.empty());
            REQUIRE(l.size() == 8);
            l.remove(3);
            REQUIRE(!l.empty());
            REQUIRE(l.size() == 6);
            REQUIRE(list_equal(l, { 2, 1, -1, -1, 1, 2 }));
        }
    }
}


TEST_CASE("merge") {
    SECTION("both empty") {
        list lhs, rhs;
        lhs.merge(rhs);
        REQUIRE(reports_as_empty(lhs));
        REQUIRE(reports_as_empty(rhs));
    }
    SECTION("empty lhs") {
        list lhs;
        list rhs({0, 1, 2, 3});
        lhs.merge(rhs);
        REQUIRE(reports_as_empty(rhs));
        REQUIRE(!lhs.empty());
        REQUIRE(lhs.size() == 4);
        REQUIRE(list_equal(lhs, { 0, 1, 2, 3}));
    }
    SECTION("empty rhs") {
        list lhs({ 0, 1, 2, 3 });
        list rhs;
        lhs.merge(rhs);
        REQUIRE(reports_as_empty(rhs));
        REQUIRE(!lhs.empty());
        REQUIRE(lhs.size() == 4);
        REQUIRE(list_equal(lhs, { 0, 1, 2, 3 }));
    }
    SECTION("simple RHS append") {
        list lhs({ 0, 1, 2 });
        list rhs({ 3, 4, 5 });
        lhs.merge(rhs);
        REQUIRE(reports_as_empty(rhs));
        REQUIRE(!lhs.empty());
        REQUIRE(lhs.size() == 6);
        REQUIRE(list_equal(lhs, { 0, 1, 2, 3, 4, 5 }));
    }
    SECTION("simple RHS prepend") {
        list lhs({ 3, 4, 5 });
        list rhs({ 0, 1, 2 });
        lhs.merge(rhs);
        REQUIRE(reports_as_empty(rhs));
        REQUIRE(!lhs.empty());
        REQUIRE(lhs.size() == 6);
        REQUIRE(list_equal(lhs, { 0, 1, 2, 3, 4, 5 }));
    }
    SECTION("equal merge") {
        list lhs({ 0, 0, 0, 0 });
        list rhs({ 0, 0, 0, 0 });
        lhs.merge(rhs);
        REQUIRE(reports_as_empty(rhs));
        REQUIRE(!lhs.empty());
        REQUIRE(lhs.size() == 8);
        REQUIRE(list_equal(lhs, { 0, 0, 0, 0, 0, 0, 0, 0 }));
    }
    SECTION("same lists merge") {
        list lhs({0, 1, 2, 3});
        list rhs({0, 1, 2, 3});
        lhs.merge(rhs);
        REQUIRE(reports_as_empty(rhs));
        REQUIRE(!lhs.empty());
        REQUIRE(lhs.size() == 8);
        REQUIRE(list_equal(lhs, { 0, 0, 1, 1, 2, 2, 3, 3 }));
    }
    SECTION("Irregular merge 1") {
        list lhs({0, 1, 3, 4, 5});
        list rhs({1, 2, 2});
        lhs.merge(rhs);
        REQUIRE(reports_as_empty(rhs));
        REQUIRE(!lhs.empty());
        REQUIRE(lhs.size() == 8);
        REQUIRE(list_equal(lhs, { 0, 1, 1, 2, 2, 3, 4, 5 }));
    }
    SECTION("Irregular merge 2") {
        list lhs({ 1, 2, 3 });
        list rhs({-2, -2, 4, 4, 5});
        lhs.merge(rhs);
        REQUIRE(reports_as_empty(rhs));
        REQUIRE(!lhs.empty());
        REQUIRE(lhs.size() == 8);
        REQUIRE(list_equal(lhs, { -2, -2, 1, 2, 3, 4, 4, 5}));
    }
}


TEST_CASE("Compilation tests") {
    list l({1, 2});
    const auto& const_l = l;
    REQUIRE(const_l.front() == 1);
    REQUIRE(const_l.back() == 2);
    REQUIRE(!const_l.empty());
    REQUIRE(const_l.size() == 2);
}
