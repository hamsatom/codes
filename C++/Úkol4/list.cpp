#include "list.hpp"
#include <stdexcept>

list::list() {
	num_elements = 0;
	tail = nullptr;
	head = nullptr;
}

list::list(const std::vector<double>& vec) {
	num_elements = 0;
	tail = nullptr;
	head = nullptr;
	for (double elem : vec) {
		push_back(elem);
	}
}

list::~list() {
	if (head == nullptr) {
		return;
	}
	list::node *cur_node = head, *next_node;
	while (cur_node != nullptr)
	{
		next_node = cur_node->next;
		delete cur_node;
		cur_node = next_node;
	}
}

void list::push_back(double elem) {
	node *new_tail = new node{ elem, tail, nullptr };
	++num_elements;
	if (tail == nullptr) {
		head = tail = new_tail;
		return;
	}
	tail->next = new_tail;
	tail = new_tail;
}

void list::pop_back() {
	if (tail == nullptr) {
		return;
	}
	list::node *new_tail = tail->prev;
	--num_elements;
	new_tail != nullptr ? new_tail->next = nullptr : head = nullptr;
	delete tail;
	tail = new_tail;
}

double& list::back() {
	if (tail == nullptr) {
		throw std::invalid_argument("Tail of the list is null");
	}
	return tail->val;
}

double list::back() const {
	if (tail == nullptr) {
		throw std::invalid_argument("Tail of the list is null");
	}
	return tail->val;
}

void list::push_front(double elem) {
	list::node *new_head = new list::node{ elem, nullptr, head };
	++num_elements;
	if (head == nullptr) {
		head = tail = new_head;
		return;
	}
	head->prev = new_head;
	head = new_head;
}

void list::pop_front() {
	if (head == nullptr) {
		return;
	}
	list::node *new_head = head->next;
	--num_elements;
	new_head != nullptr ? new_head->prev = nullptr : tail = nullptr;
	delete head;
	head = new_head;
}

double& list::front() {
	if (head == nullptr) {
		throw std::invalid_argument("Head of the list is null");
	}
	return head->val;
}

double list::front() const {
	if (head == nullptr) {
		throw std::invalid_argument("Head of the list is null");
	}
	return head->val;
}

void list::join(list& r) {
	if (r.head == nullptr) {
		return;
	}
	list empty_list{};
	if (tail == nullptr) {
		*this = r;
		r = empty_list;
		return;
	}
	tail->next = r.head;
	r.head->prev = tail;
	tail = r.tail;
	num_elements += r.num_elements;
	r = empty_list;
}

void list::reverse() {
	if (head == nullptr || head == tail) {
		return;
	}
	list::node *tmp;
	list::node *cur_node = head, *next_node;
	while (cur_node != nullptr)
	{
		next_node = cur_node->next;
		tmp = cur_node->prev;
		cur_node->prev = cur_node->next;
		cur_node->next = tmp;
		cur_node = next_node;
	}
	tmp = head;
	head = tail;
	tail = tmp;
}

void list::unique() {
	if (head == nullptr) {
		return;
	}
	list::node *from = head, *cur_node = head->next, *next_node;
	while (cur_node != tail)
	{
		next_node = cur_node->next;
		if (from->val == cur_node->val) {
			delete cur_node;
			--num_elements;
		}
		else
		{
			from->next = cur_node;
			cur_node->prev = from;
			from = cur_node;
		}
		cur_node = next_node;
	}
	if (from->val == tail->val) {
		delete tail;
		from->next = nullptr;
		tail = from;
		--num_elements;
	}
}

std::size_t list::size() const { return num_elements; }

bool list::empty() const { return num_elements == 0; }

void list::remove(double value) {
	if (head == nullptr) {
		return;
	}
	list::node *cur_node = head, *next_node, *previous_node = nullptr;
	bool first = true;
	while (cur_node != nullptr)
	{
		while (cur_node != nullptr && cur_node->val == value)
		{
			next_node = cur_node->next;
			delete cur_node;
			--num_elements;
			cur_node = next_node;
		}
		if (first) {
			head = cur_node;
			first = false;
		}
		else
		{
			previous_node->next = cur_node;
		}
		if (cur_node == nullptr) {
			break;
		}
		cur_node->prev = previous_node;
		previous_node = cur_node;
		cur_node = cur_node->next;
	}
	tail = previous_node;
}

void list::merge(list& rhs) {
	if (rhs.head == nullptr) {
		return;
	}
	list empty_list{};
	if (head == nullptr) {
		*this = rhs;
		rhs = empty_list;
		return;
	}
	list::node *this_node = head->next, *rhs_node = rhs.head, *prev_node;
	if (rhs.head->val < head->val) {
		this_node = head;
		head = rhs.head;
		rhs_node = head->next;
	}
	prev_node = head;
	while (rhs_node != nullptr && this_node != nullptr)
	{
		if (this_node->val < rhs_node->val) {
			prev_node->next = this_node;
			this_node->prev = prev_node;
			prev_node = this_node;
			this_node = this_node->next;
		}
		else
		{
			prev_node->next = rhs_node;
			rhs_node->prev = prev_node;
			prev_node = rhs_node;
			rhs_node = rhs_node->next;
		}
	}
	if (rhs_node == nullptr) {
		prev_node->next = this_node;
		this_node->prev = prev_node;
	}
	else
	{
		prev_node->next = rhs_node;
		rhs_node->prev = prev_node;
		tail = rhs.tail;
	}

	num_elements += rhs.num_elements;
	rhs = empty_list;
}
