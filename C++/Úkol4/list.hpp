#ifndef PJC_LIST_HPP
#define PJC_LIST_HPP

#include <cstddef>
#include <vector>

using std::size_t;

class list {
public:
    /*
     * After constructor the list must be usable.
     */
    list();

    /*
     * Creates a list containing all elements from vec.
     */
    list(const std::vector<double>& vec);

    /*
     * After destructor, the list must be empty and not hold on to any memory.
     * Destructor will only be called once over the lifetime of a list.
     */
    ~list();

    /*
     * Inserts an element at the end of the list.
     *
     * Should run in in constant time.
     */
    void push_back(double elem);

    /*
     * Removes an element from the end of the list.
     *
     * Precondition: Should not be called on an empty list.
     * Should run in in constant time.
     */
    void pop_back();

    /*
     * Allows access to the last element of the list.
     *
     * Precondition: Should not be called on an empty list.
     * Should run in in constant time.
     */
    double& back();
    double back() const;

    /*
     * Inserts an element at the front of the list.
     *
     * Should run in in constant time.
     */
    void push_front(double elem);

    /*
     * Removes an element from the front of the list.
     *
     * Precondition: Should not be called on an empty list.
     * Should run in in constant time.
     */
    void pop_front();

    /*
     * Allows access to the first element of the list.
     *
     * Precondition: Should not be called on an empty list.
     * Should run in in constant time.
     */
    double& front();
    double front() const;

    /*
     * Destructively appends list r to list l, so that list r ends up empty.
     *
     * Should run in constant time.
     */
    void join(list& r);

    /*
     * Reverses order of all elements of list l.
     * Given list containing 1 2 3 4 2 1, the result should be a list containing
     * 1 2 4 3 2 1.
     *
     * Should run in linear time.
     */
    void reverse();

    /*
     * Filters elements in the list so that there are no immediate duplicates.
     * Given list containing 1 1 2 2 2 3 4 2 1, the result should be a list
     * containing 1 2 3 4 2 1.
     *
     * Should run in linear time.
     */
    void unique();

    /*
     * Returns size of the list l.
     *
     * Should run in constant time.
     */
    size_t size() const;

    /*
     * Returns true if the list l does not contain any elements.
     *
     * Should run in constant time.
     */
    bool empty() const;

    /*
     * Removes all elements with the specified value from the list.
     *
     * Should run in time linear to size of the list.
     */
    void remove(double value);
    
    /*
     * Merges sorted list rhs into this list.
     *
     * Precondition: Both lists are sorted.
     * Should run in time linear to both lists.
     */
    void merge(list& rhs);


    friend bool list_equal(const list& l, const std::vector<double>& numbers);

private:
    struct node {
        double val;
        node* prev;
        node* next;
    };

    node* head;
    node* tail;
    size_t num_elements;
};


#endif
