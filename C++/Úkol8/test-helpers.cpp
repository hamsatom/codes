#include "test-helpers.hpp"

#include <ostream>

bool operator==(const ordered & o1, const ordered & o2) {
    return o1.sortby == o2.sortby;
}

bool operator<(const ordered& o1, const ordered& o2) {
    return o1.sortby < o2.sortby;
}
