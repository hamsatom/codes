#ifndef PJC_HELPERS_HPP
#define PJC_HELPERS_HPP

#include <iosfwd>

struct ordered {
    ordered() = default;
    ordered(int sortby, size_t order):sortby(sortby), order(order){}

    int sortby;
    size_t order;
};

bool operator==(const ordered& o1, const ordered& o2);
bool operator<(const ordered& o1, const ordered& o2);

#endif