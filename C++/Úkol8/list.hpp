#ifndef PJC_LIST_HPP
#define PJC_LIST_HPP

#include <cstddef>
#include <vector>
#include <utility>
#include <algorithm>
#include <iterator>
#include <functional>
#include <tuple>
#include <stdexcept>
#include <sstream>


using std::size_t;

template <typename T>
class list {
private:
	struct node {
		T val;
		node* prev;
		node* next;
	};

public:

	class const_iterator {
		node* current_ptr;
		const list* o_list;
	public:
		using difference_type = std::ptrdiff_t;
		using iterator_category = std::bidirectional_iterator_tag;
		using value_type = const T;
		using reference = const T&;
		using pointer = const T*;

		const_iterator() {
			current_ptr = nullptr;
			o_list = nullptr;
		}
		const_iterator(node* ptr, const list* gen) {
			current_ptr = ptr;
			o_list = gen;
		}
		const_iterator(const const_iterator& rhs) {
			current_ptr = rhs.current_ptr;
			o_list = rhs.o_list;
		}
		const_iterator& operator=(const const_iterator& rhs) {
			current_ptr = rhs.current_ptr;
			o_list = rhs.o_list;
			return *this;
		}

		const_iterator& operator++() {
			current_ptr = current_ptr->next;
			return *this;
		}
		const_iterator operator++(int) {
			const_iterator tmp(*this);
			current_ptr = current_ptr->next;
			return tmp;
		}

		const_iterator& operator--() {
			if (current_ptr == nullptr) {
				current_ptr = o_list->tail;
			}
			else {
				current_ptr = current_ptr->prev;
			}
			return *this;
		}
		const_iterator operator--(int) {
			const_iterator tmp(*this);
			if (current_ptr == nullptr) {
				current_ptr = o_list->tail;
			}
			else {
				current_ptr = current_ptr->prev;
			}
			return tmp;
		}

		reference operator*() const {
			return current_ptr->val;
		}
		pointer operator->() const {
			return &current_ptr->val;
		}

		bool operator==(const const_iterator& rhs) const {
			return current_ptr == rhs.current_ptr && o_list == rhs.o_list;
		}
		bool operator!=(const const_iterator& rhs) const {
			return !(*this == rhs);
		}

		friend class list;
	};

	class iterator {
		node* current_ptr;
		const list* o_list;
	public:
		using difference_type = std::ptrdiff_t;
		using iterator_category = std::bidirectional_iterator_tag;
		using value_type = T;
		using reference = T&;
		using pointer = T*;

		iterator() {
			current_ptr = nullptr;
			o_list = nullptr;
		}
		iterator(node* ptr, const list* gen) {
			current_ptr = ptr;
			o_list = gen;
		}
		iterator(const iterator& rhs) {
			current_ptr = rhs.current_ptr;
			o_list = rhs.o_list;
		}
		iterator& operator=(const iterator& rhs) {
			current_ptr = rhs.current_ptr;
			o_list = rhs.o_list;
			return *this;
		}

		iterator& operator++() {
			current_ptr = current_ptr->next;
			return *this;
		}
		iterator operator++(int) {
			iterator tmp(*this);
			current_ptr = current_ptr->next;
			return tmp;
		}
		iterator& operator--() {
			if (current_ptr == nullptr) {
				current_ptr = o_list->tail;
			}
			else {
				current_ptr = current_ptr->prev;
			}
			return *this;
		}
		iterator operator--(int) {
			iterator tmp(*this);
			if (current_ptr == nullptr) {
				current_ptr = o_list->tail;
			}
			else {
				current_ptr = current_ptr->prev;
			}
			return tmp;
		}

		reference operator*() const {
			return current_ptr->val;
		}
		pointer operator->() const {
			return &current_ptr->val;
		}

		operator const_iterator() const {
			return const_iterator(current_ptr, o_list);
		}

		bool operator==(const iterator& rhs) const {
			return current_ptr == rhs.current_ptr && o_list == rhs.o_list;
		}
		bool operator!=(const iterator& rhs) const {
			return !(*this == rhs);
		}

		friend class list;
	};

	list() {
		head = tail = nullptr;
		num_elements = 0;
	}
	list(const std::vector<T>& vec) : list() {
		for (T elem : vec) {
			push_back(elem);
		}
	}
	~list() {
		list::node *next_node;
		while (head != nullptr)
		{
			next_node = head->next;
			delete head;
			head = next_node;
		}
	}

	void push_back(const T& elem) {
		node *new_tail = new node{ elem, tail, nullptr };
		++num_elements;
		if (tail == nullptr) {
			head = tail = new_tail;
			return;
		}
		tail->next = new_tail;
		tail = new_tail;
	}

	void pop_back() {
		if (tail == nullptr) {
			return;
		}
		list::node *new_tail = tail->prev;
		--num_elements;
		new_tail != nullptr ? new_tail->next = nullptr : head = nullptr;
		delete tail;
		tail = new_tail;
	}

	T& back() {
		if (tail == nullptr) {
			throw std::invalid_argument("Tail of the list is null");
		}
		return tail->val;
	}
	const T& back() const {
		if (tail == nullptr) {
			throw std::invalid_argument("Tail of the list is null");
		}
		return tail->val;
	}

	void push_front(const T& elem) {
		list::node *new_head = new list::node{ elem, nullptr, head };
		++num_elements;
		if (head == nullptr) {
			head = tail = new_head;
			return;
		}
		head->prev = new_head;
		head = new_head;
	}

	void pop_front() {
		if (head == nullptr) {
			return;
		}
		list::node *new_head = head->next;
		--num_elements;
		new_head != nullptr ? new_head->prev = nullptr : tail = nullptr;
		delete head;
		head = new_head;
	}

	T& front() {
		if (head == nullptr) {
			throw std::invalid_argument("Head of the list is null");
		}
		return head->val;
	}
	const T& front() const {
		if (head == nullptr) {
			throw std::invalid_argument("Head of the list is null");
		}
		return head->val;
	}

	void join(list& r) {
		if (r.head == nullptr) {
			return;
		}
		if (tail == nullptr) {
			swap(r);
			return;
		}
		tail->next = r.head;
		r.head->prev = tail;
		tail = r.tail;
		num_elements += r.num_elements;
		r.head = nullptr;
		r.tail = nullptr;
		r.num_elements = 0;
	}

	void reverse() {
		if (head == tail) {
			return;
		}
		list::node *cur_node = head, *next_node;
		while (cur_node != nullptr)
		{
			next_node = cur_node->next;
			std::swap(cur_node->prev, cur_node->next);
			cur_node = next_node;
		}
		std::swap(head, tail);
	}

	void unique() {
		if (head == nullptr) {
			return;
		}
		list::node *from = head, *cur_node = head->next, *next_node;
		while (cur_node != tail)
		{
			next_node = cur_node->next;
			if (from->val == cur_node->val) {
				delete cur_node;
				--num_elements;
			}
			else
			{
				from->next = cur_node;
				cur_node->prev = from;
				from = cur_node;
			}
			cur_node = next_node;
		}
		if (from->val == tail->val) {
			delete tail;
			from->next = nullptr;
			tail = from;
			--num_elements;
		}
	}
	template <typename BinPredicate>
	void unique(BinPredicate pred) {
		if (head == nullptr) {
			return;
		}
		list::node *from = head, *cur_node = head->next, *next_node;
		while (cur_node != tail)
		{
			next_node = cur_node->next;
			if (pred(from->val, cur_node->val)) {
				delete cur_node;
				--num_elements;
			}
			else
			{
				from->next = cur_node;
				cur_node->prev = from;
				from = cur_node;
			}
			cur_node = next_node;
		}
		if (pred(from->val, tail->val)) {
			delete tail;
			from->next = nullptr;
			tail = from;
			--num_elements;
		}
	}

	size_t size() const {
		return num_elements;
	}

	bool empty() const { return num_elements == 0; }

	void remove(const T& value) {
		if (head == nullptr) {
			return;
		}
		list::node *cur_node = head, *next_node, *previous_node = nullptr;
		bool first = true;
		while (cur_node != nullptr)
		{
			while (cur_node != nullptr && cur_node->val == value)
			{
				next_node = cur_node->next;
				delete cur_node;
				--num_elements;
				cur_node = next_node;
			}
			if (first) {
				head = cur_node;
				first = false;
			}
			else
			{
				previous_node->next = cur_node;
			}
			if (cur_node == nullptr) {
				break;
			}
			cur_node->prev = previous_node;
			previous_node = cur_node;
			cur_node = cur_node->next;
		}
		tail = previous_node;
	}

	template <typename Predicate>
	void remove_if(Predicate p) {
		if (head == nullptr) {
			return;
		}
		list::node *cur_node = head, *next_node, *previous_node = nullptr;
		bool first = true;
		while (cur_node != nullptr)
		{
			while (cur_node != nullptr && p(cur_node->val))
			{
				next_node = cur_node->next;
				delete cur_node;
				--num_elements;
				cur_node = next_node;
			}
			if (first) {
				head = cur_node;
				first = false;
			}
			else
			{
				previous_node->next = cur_node;
			}
			if (cur_node == nullptr) {
				break;
			}
			cur_node->prev = previous_node;
			previous_node = cur_node;
			cur_node = cur_node->next;
		}
		tail = previous_node;
	}

	void merge(list& rhs) {
		if (rhs.head == nullptr) {
			return;
		}
		if (head == nullptr) {
			swap(rhs);
			return;
		}
		list::node *this_node = head->next, *rhs_node = rhs.head, *prev_node;
		if (rhs.head->val < head->val) {
			this_node = head;
			head = rhs.head;
			rhs_node = head->next;
		}
		prev_node = head;
		while (rhs_node != nullptr && this_node != nullptr)
		{
			if (this_node->val < rhs_node->val) {
				prev_node->next = this_node;
				this_node->prev = prev_node;
				prev_node = this_node;
				this_node = this_node->next;
			}
			else
			{
				prev_node->next = rhs_node;
				rhs_node->prev = prev_node;
				prev_node = rhs_node;
				rhs_node = rhs_node->next;
			}
		}
		if (rhs_node == nullptr) {
			prev_node->next = this_node;
			this_node->prev = prev_node;
		}
		else
		{
			prev_node->next = rhs_node;
			rhs_node->prev = prev_node;
			tail = rhs.tail;
		}

		num_elements += rhs.num_elements;
		rhs.head = rhs.tail = nullptr;
		rhs.num_elements = 0;
	}

	template <typename Comparator>
	void merge(list& rhs, Comparator cmp) {
		if (rhs.head == nullptr) {
			return;
		}
		if (head == nullptr) {
			swap(rhs);
			return;
		}
		list::node *this_node = head->next, *rhs_node = rhs.head, *prev_node;
		if (cmp(rhs.head->val, head->val)) {
			this_node = head;
			head = rhs.head;
			rhs_node = head->next;
		}
		prev_node = head;
		while (rhs_node != nullptr && this_node != nullptr)
		{
			if (!cmp(this_node->val, rhs_node->val)) {
				prev_node->next = this_node;
				this_node->prev = prev_node;
				prev_node = this_node;
				this_node = this_node->next;
			}
			else
			{
				prev_node->next = rhs_node;
				rhs_node->prev = prev_node;
				prev_node = rhs_node;
				rhs_node = rhs_node->next;
			}
		}
		if (rhs_node == nullptr) {
			prev_node->next = this_node;
			this_node->prev = prev_node;
		}
		else
		{
			prev_node->next = rhs_node;
			rhs_node->prev = prev_node;
			tail = rhs.tail;
		}

		num_elements += rhs.num_elements;
		rhs.head = rhs.tail = nullptr;
		rhs.num_elements = 0;
	}

	// Standard copy operations.
	list(const list& rhs) :list() {
		for (auto node_value : rhs) {
			push_back(node_value);
		}
	}
	list& operator=(const list& rhs) {
		if (this == &rhs) {
			return *this;
		}
		while (num_elements > 0)
		{
			pop_back();
		}
		for (auto node_value : rhs) {
			push_back(node_value);
		}
		return *this;
	}

	// Move operations
	// After the move is performed, the moved from list should be assignable and destructible.
	list(list&& rhs) :list() {
		swap(rhs);
	}
	list& operator=(list&& rhs) {
		if (this == &rhs) {
			return *this;
		}
		while (num_elements > 0)
		{
			pop_back();
		}
		swap(rhs);
		return *this;
	}

	void swap(list& rhs) {
		std::swap(head, rhs.head);
		std::swap(tail, rhs.tail);
		std::swap(num_elements, rhs.num_elements);
	}

	// Iterator creation -- reverse iterator overloads are intentionally missing
	iterator begin() {
		return iterator(head, this);
	}
	iterator end() {
		return iterator(nullptr, this);
	}
	const_iterator begin() const {
		return const_iterator(head, this);
	}
	const_iterator end() const {
		return const_iterator(nullptr, this);
	}
	const_iterator cbegin() const {
		return const_iterator(head, this);
	}
	const_iterator cend() const {
		return const_iterator(nullptr, this);
	}


	bool operator==(const list& rhs) const {
		if (num_elements != rhs.num_elements) {
			return false;
		}
		return std::equal(begin(), end(), rhs.begin());
	}
	bool operator<(const list& rhs) const {
		if (rhs.num_elements == 0 && num_elements == 0) {
			return false;
		}
		if (rhs.num_elements > num_elements) {
			return true;
		}

		auto diff = std::mismatch(begin(), end(), rhs.begin(), rhs.end());

		if (diff.first == end()) {
			return false;
		}

		auto a = *diff.first;
		auto b = *diff.second;
		return a < b;
	}
	bool operator<=(const list& rhs) const {
		return !(*this > rhs);
	}
	bool operator>(const list& rhs) const {
		return !(rhs >= *this);
	}
	bool operator>=(const list& rhs) const {
		return !(*this < rhs);
	}

	std::pair<list, list> split(const_iterator place) {
		list first_list{}, second_list{};
		if (head == nullptr) {
			goto returning;
		}
		if (head == place.current_ptr) {
			swap(second_list);
			goto returning;
		}
		if (place.current_ptr == nullptr) {
			swap(first_list);
			goto returning;
		}
		first_list.head = head;
		first_list.num_elements = std::distance(cbegin(), place);
		first_list.tail = place.current_ptr->prev;

		second_list.head = place.current_ptr;
		if (second_list.head == nullptr) {
			second_list.tail = nullptr;
		}
		else
		{
			second_list.head->prev = nullptr;
			second_list.tail = tail;
		}
		second_list.num_elements = num_elements - first_list.num_elements;
		first_list.tail->next = nullptr;

		tail = head = nullptr;
		num_elements = 0;

	returning:
		return{ std::move(first_list), std::move(second_list) };
	}

	void sort() {
		if (head == tail) {
			return;
		}
		const_iterator it = begin();
		std::advance(it, num_elements / 2);
		std::pair<list, list> halves = split(it);
		halves.first.sort();
		halves.second.sort();
		halves.first.merge(halves.second);
		halves.first.swap(*this);
	}

	template <typename Comparator>
	void sort(Comparator cmp) {
		if (head == tail) {
			return;
		}
		const_iterator it = begin();
		std::advance(it, num_elements / 2);
		std::pair<list, list> halves = split(it);
		halves.first.sort(cmp);
		halves.second.sort(cmp);
		halves.first.merge(halves.second, cmp);
		halves.first.swap(*this);
		return;
	}


private:
	node* head;
	node* tail;
	size_t num_elements;
};

/*
* Two lists are not equal iff any of their elements differ.
*/
template <typename T>
bool operator!=(const list<T>& lhs, const list<T>& rhs) {
	return !(lhs == rhs);
}

/*
* ADL customization point for std::swap.
* Should do the same as calling lhs.swap(rhs)
*/
template <typename T>
void swap(list<T>& lhs, list<T>& rhs) {
	lhs.swap(rhs);
}

#endif
