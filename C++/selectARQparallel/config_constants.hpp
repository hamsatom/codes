/*
Contains constants used all over the program
*/
#pragma once

/*
Print messages*/
#define NO_ARGS_MSG "Port number must be provided as argument."
#define HELP_SERVER_MSG "\r\n Options:\r\n   -h            Show this help\r\n   -b <address>  Bind to a specific address\r\n   -v            Enable verbose output\r\n  -t            Enable testing mode\r\n"
#define FRAME_LONG_MSG "Received frame is too long "
#define FRAME_SHORT_MSG "Received frame is too short "
#define INVALID_SEQ_MSG "Received invalid sequence "
#define FRAME_SEQ "Received frame with sequence: "
#define HELP_CLIENT_MSG "\r\n Options:\r\n   -h            Show this help\r\n   -t<amount>            Send <amount> test packages then exit, default alue is 100'000\r\n   -i<amount>            size of package interval between each time measuring"
#define MISSING_CLIENT_ARGS_MSG "Address and port number must be provided as arguments "
#define CLIENT_SELECT_ERROR_MSG "Error while using select in client "
#define CLIENT_READ_ERROR_MSG "Error while using read in client"
#define CLIENT_WRITE_ERROR_MSG "Error while using write in client"

#define REPORT_FORMAT ".txt"
#define INTERVAL_UNITS std::chrono::microseconds
#define INTERVAL_UNITS_MSG "Microseconds for "
#define TOTAL_TIME_UNITS std::chrono::seconds
#define TOTAL_TIME_UNITS_MSG "Seconds for whole transmission: "

/*
Configuration constants
*/
#define SEQ_MODULO 0x20000
#define HALF_SEQ 0x8000

#define SEQ_NUMBER_SIZE 2
#define MESSAGE_SIZE 255
#define CHECKSUM_SIZE 1
#define MSG_LENGTH_SIZE 1
#define TIMEOUT 1000

#define WINDOW_SIZE 8
#define PROBABILITY 90