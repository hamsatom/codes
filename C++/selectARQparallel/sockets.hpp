/*
This class creates appropriate socket
*/
#pragma once
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <string>
/*
Gets socket for server*/
extern int get_server_socket(char* &host, const std::string &port);
/*
Gets socket for client*/
extern int get_client_socket(const std::string &host, const  std::string &port);
/*
Creates socket
*/
void create_socket(const char* host, const std::string &port, addrinfo &socket_config, addrinfo *&open_socket);
/*
* Returns host and port from given socket
*
* If error occurres, prints error message to standard error output and
* returns empty string
*/
extern std::string get_name_from_addr(const sockaddr *address, socklen_t baddress_length);