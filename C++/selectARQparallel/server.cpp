#include <cstdio>
#include <ctype.h>
#include <cstdlib>
#include <unistd.h>
#include <iostream>
#include <string>
#include <sys/socket.h>
#include <sys/types.h>
#include <cstring>
#include <unordered_map>
#include <thread>
#include <cerrno>

#include "server.hpp"
#include "config_constants.hpp"
#include "sockets.hpp"
#include "util.hpp"
#include "window.hpp"

int main(int argc, char *argv[]) {
	program_name = argv[0];
	auto options = resolve_args(argc, argv);
	int socket = get_server_socket(options.first, options.second);

	std::thread t1(start_server, socket);
	std::thread t2(receive_data, socket);
	t1.join();
	t2.join();
}

static std::pair<char*, std::string> resolve_args(int &argc, char *argv[])
{
	int curr_char;
	char* host = NULL;
	while ((curr_char = getopt(argc, argv, "hb:vt")) != EOF) {
		switch (curr_char) {
		case 'h':
			print_help();
			exit(EXIT_SUCCESS);
			break;
		case 'b':
			host = optarg;
			break;
		case 'v':
			verbose = true;
			break;
		case 't':
			testing = true;
			break;
		case '?':
		default:
			print_usage(stderr);
			std::cerr << "Run " << program_name << " -h for help" << std::endl;
			exit(EXIT_FAILURE);
		}
	}

	if (optind != argc - 1) {
		std::cerr << NO_ARGS_MSG << std::endl;
		print_usage(stderr);
		exit(EXIT_FAILURE);
	}

	std::string port = argv[optind];

	return{ std::move(host), std::move(port) };
}


static void print_usage(FILE *location)
{
	int result = std::fprintf(location, "Usage: %s[<options>] <port>\r\n", program_name.c_str());
	if (result < EXIT_SUCCESS) {
		std::cerr << "Printf error in server.cpp" << std::endl << strerror(errno) << std::endl;
		exit(EXIT_FAILURE);
	}
}

static void print_help()
{
	print_usage(stdout);
	std::cout << HELP_SERVER_MSG << std::endl;
}

void receive_data(int listen_socket) {
	ssize_t bytes_received;
	std::string received_transmition;
	received_transmition.resize(SEQ_NUMBER_SIZE + MSG_LENGTH_SIZE + MESSAGE_SIZE + CHECKSUM_SIZE + 1);
	sockaddr_storage addr;
	socklen_t addr_len = sizeof(addr);
	
	while (true) {

		bytes_received = recvfrom(listen_socket, &received_transmition[0], received_transmition.size(), 0, (sockaddr*)&addr, &addr_len);

		if (bytes_received == -1) {
			std::cerr << strerror(errno) << std::endl;
			continue;
		}
		if (bytes_received < SEQ_NUMBER_SIZE) {
			std::cerr << FRAME_SHORT_MSG << std::endl;
			continue;
		}
		if (bytes_received >= (signed)received_transmition.size()) {
			std::cerr << FRAME_LONG_MSG << std::endl;
			continue;
		}

		queue.add({ bytes_received, received_transmition, addr, addr_len });
	}
}

void start_server(int listen_socket) {
	int wait_seq = 0, seq;

	std::string received_transmition;
	received_transmition.resize(SEQ_NUMBER_SIZE + MSG_LENGTH_SIZE + MESSAGE_SIZE + CHECKSUM_SIZE + 1);
	ssize_t bytes_received;
	std::unordered_map < std::string, std::pair<int, Window> > client_seq;
	std::string client_id;
	Window window{};
	sockaddr_storage addr;
    socklen_t addr_len;
	std::tuple<ssize_t, std::string, sockaddr_storage, socklen_t> transmission_to_process;
	
	while (true)
	{
		seq = 0;

		if (!queue.take(transmission_to_process)) {
			continue;
		}
		bytes_received = std::get<0>(transmission_to_process);
		received_transmition = std::get<1>(transmission_to_process);
		addr = std::get<2>(transmission_to_process);
		addr_len = std::get<3>(transmission_to_process);

		client_id = get_name_from_addr((sockaddr*)&addr, addr_len);

		if (verbose) {
			std::cerr << "Received " << get_hex_representation(received_transmition, bytes_received) << " from " << client_id << std::endl;
		}

		seq = read_seq(received_transmition);
		auto msg_length = read_msg_length(received_transmition);
		std::string msg = read_msg(received_transmition, msg_length);

		if (!lengths_equal(msg, msg_length)) {
			if (verbose) {
				std::cerr << "Received message differ in length with received length " << msg.length() << " != " << msg_length << std::endl;
			}
			continue;
		}

		uint8_t checksum = read_chksum8(received_transmition, bytes_received);

		try {
			if (!msg_equals_checksum(msg, checksum)) {
				if (verbose) {
					std::cerr << "Received message and checksum aren't equal - received checksum: " << static_cast<unsigned>(unsigned(checksum)) << std::endl;
				}
				continue;
			}
		}
		catch (std::bad_alloc& ba) {
			if (verbose) {
				std::cerr << "Error while comparing lengths " << ba.what() << std::endl;
			}
			continue;
		}
		
		auto key_found = client_seq.find(client_id);
		if (key_found == client_seq.end()) {
			client_seq.insert({ client_id, {0,Window{}} });
			wait_seq = 0;
			window = Window{};
		}
		else {
			wait_seq = key_found->second.first;
			window = key_found->second.second;
		}

		if (seq_is_equal_or_greater(seq, wait_seq) && seq_is_less(seq, wait_seq + WINDOW_SIZE) && !window.has_sequence(seq)) {
			if (verbose) {
				std::cerr << "Checksum received: " << static_cast<unsigned>(unsigned(checksum)) << std::endl;
				std::cerr << "Message length received: " << msg_length << std::endl;
			}
			window.store(seq, checksum, msg, msg_length);
		}

		while (window.has_sequence(wait_seq)) {
			std::cout << client_id << " ";
			window.print_message(wait_seq, stdout);
			if (testing) {
				std::string file_name = "server-" + client_id + REPORT_FORMAT;
				FILE *report_file = fopen(file_name.c_str(), "a+");
				window.print_message(wait_seq, report_file);
				fclose(report_file);
			}
			if (window.get_message_length(wait_seq) == 0) {
				fclose(stdout);
			}
			wait_seq = seq_increase(wait_seq);
		}
		fflush(stdout);
		client_seq.at(client_id) = { wait_seq, window };


		std::string acknowledge_msg;
		acknowledge_msg.resize(SEQ_NUMBER_SIZE);

		write_seq(acknowledge_msg, seq);

		if (random_chance()) {
			bytes_received = sendto(listen_socket, acknowledge_msg.c_str(), acknowledge_msg.size(), 0, (sockaddr*)&addr, addr_len);
			if (verbose) {
				std::cerr << "Sent acknowledgment for " << msg << std::endl;
			}
		}
		else if (verbose) {
			std::cerr << "Received message dropped" << std::endl;
		}
	}
}
