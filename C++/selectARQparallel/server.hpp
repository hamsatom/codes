/**
This class is resposible for runnig server and accepting sockets
*/
#pragma once
#include <string>
#include <unistd.h>
#include <sys/socket.h>

#include "window.hpp"
#include "blocking_queue.hpp"


blocking_queue<std::tuple<ssize_t, std::string, sockaddr_storage, socklen_t>> queue;

/*
If testing is enabled the server created transmission report in txt named after client
*/
static bool testing = false;

/*
Contains name of this program more specificly it'S executable. It's similar to argv[0] of main method
*/
static std::string program_name;

/*
If true programm produces more output*/
static bool verbose = true;

/*
Prints message about usage*/
static void print_usage(FILE *location);


/*Prints list of supported arguments*/
static void print_help();

/*
Returns host and port from passed arguments. Host is char* because string can't be NULL and host can be empty.
*/
static std::pair<char*, std::string> resolve_args(int &argc, char *argv[]);

/*
Runs the server*/
void start_server(int listen_socket);

void receive_data(int listen_socket);
