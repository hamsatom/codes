#pragma once
#include <queue>
#include <mutex>
#include <condition_variable>
#include <iostream>

template <typename T>
class blocking_queue {
private:
	std::queue<T> queue;
	std::mutex mutex;
	std::condition_variable take_variable;
	bool closing = false;

public:
	bool add(T const &value) {
		std::unique_lock<std::mutex> lock(mutex);
		if (closing) {
			return false;
		}
		queue.push(value);
		take_variable.notify_one();
		return true;
	}

	bool take(T &out) {
		std::unique_lock<std::mutex> lock(mutex);
		take_variable.wait(lock, [&]() -> bool { return !queue.empty() || closing; });
		if (queue.empty()) {
			return false;
		}
		out = queue.front();
		queue.pop();
		return true;
	}

	void close() {
		std::unique_lock<std::mutex> lock(mutex);
		closing = true;
		take_variable.notify_all();
	}

	bool is_closed() {
		std::unique_lock<std::mutex> lock(mutex);
		return closing;
	}

	~blocking_queue() {
		close();
	}
};