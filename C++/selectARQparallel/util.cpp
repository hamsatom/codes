#include <string>
#include <iostream>
#include <cstring>
#include "util.hpp"
#include "config_constants.hpp"


int read_seq(const std::string &data) {
	int i, seq = 0;
	for (i = 0; i < SEQ_NUMBER_SIZE; ++i) {
		seq <<= 8;
		seq += (unsigned char)data[i];
	}
	return seq;
}

uint8_t read_chksum8(const std::string &data, size_t len) {
	return static_cast<unsigned>(data[len - CHECKSUM_SIZE]);
}

size_t read_msg_length(const std::string &data) {
	return (unsigned char)data[SEQ_NUMBER_SIZE];
}

std::string read_msg(const std::string &data, size_t len) {
	return data.substr(SEQ_NUMBER_SIZE + MSG_LENGTH_SIZE, len);
}

void write_seq(std::string &data, int seq) {
	int i;
	for (i = SEQ_NUMBER_SIZE - 1; i >= 0; --i) {
		data[i] = seq & 0xFF;
		seq >>= 8;
	}
}

bool seq_is_less(const int &a, const int &b) {
	int difference = a - b;
	if ((difference < 0 && difference > -HALF_SEQ) || (difference > HALF_SEQ)) {
		return true;
	}
	else {
		return false;
	}
}

bool seq_is_greater(const int &a, const int &b) {
	int difference = a - b;
	if ((difference > 0 && difference < HALF_SEQ) || (difference < -HALF_SEQ)) {
		return true;
	}
	else {
		return false;
	}
}

bool seq_is_equal_or_greater(const int &a, const int &b) {
	if (a == b) {
		return true;
	}
	return seq_is_greater(a, b);
}

int seq_increase(const int &seq) {
	return (seq + 1) % SEQ_MODULO;
}

bool random_chance() {
	return random_generator() % 100 < PROBABILITY;
}

size_t get_random_from_range(const size_t &start, const size_t &end) {
	return (random_generator() % (end - start - 1)) + start;
}

uint8_t generate_checksum8(const std::string &msg) {
	if (msg.empty()) {
		return 0;
	}
	unsigned int sum = msg.front();
	for (auto c : msg.substr(1)) {
		sum ^= c;
	}
	return (uint8_t)sum;
}

bool msg_equals_checksum(const std::string &msg, const uint8_t &checksum) {
	return checksum == generate_checksum8(msg);
}

bool lengths_equal(const std::string &msg, const size_t &length) {
	return msg.length() == length;
}

std::string generate_random_string() {
	auto len = get_random_from_range(1, 40);
	std::string s;
	s.reserve(len);
	const std::string alphanum = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

	for (size_t i = 0; i < len; ++i) {
		s.push_back(alphanum.at(get_random_from_range(0, alphanum.length())));
	}
	return s;
}

char* get_hex_representation(const std::string &ptr, const size_t &size) {
	static char buf[1024];
	unsigned int pos = 0;
	std::memset(&buf, 0, sizeof(buf));
	for (size_t i = 0; i < size; ++i) {
		int n;
		if (pos > sizeof(buf) - 10) {
			buf[pos++] = '.';
			buf[pos++] = '.';
			buf[pos++] = '.';
			break;
		}
		if (i > 0) {
			buf[pos++] = ' ';
		}
		n = snprintf(buf + pos, sizeof(buf) - pos, "%02x", (unsigned char)ptr[i]);
		pos += n;
	}
	buf[pos] = '\0';
	return buf;
}