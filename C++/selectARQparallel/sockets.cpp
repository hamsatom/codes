#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>
#include <string>
#include <cstdio>
#include <iostream>
#include <cerrno>
#include <cstdlib>
#include <cstring>

void create_socket(const char* host, const std::string &port, addrinfo &socket_config, addrinfo *&open_socket) {
	int return_code;
	socket_config.ai_family = AF_UNSPEC;  /* allow IPv4 or IPv6 */
	socket_config.ai_socktype = SOCK_DGRAM;
	socket_config.ai_protocol = 0;
	socket_config.ai_canonname = NULL;
	socket_config.ai_addr = NULL;
	socket_config.ai_next = NULL;

	return_code = getaddrinfo(host, port.c_str(), &socket_config, &open_socket);
	if (return_code != EXIT_SUCCESS) {
		std::cerr << strerror(errno) << std::endl << gai_strerror(return_code) << std::endl;
		exit(EXIT_FAILURE);
	}
}

std::string get_name_from_addr(const sockaddr *server_address, socklen_t server_address_length) {
	char host_buf[NI_MAXHOST], port_buf[NI_MAXSERV];
	int open_socket = getnameinfo(
		server_address, server_address_length,
		host_buf, sizeof(host_buf),
		port_buf, sizeof(port_buf),
		NI_NUMERICHOST | NI_NUMERICSERV);
	if (open_socket != EXIT_SUCCESS) {
		std::cerr << "sockets.cpp getnameinfo failed: " << strerror(errno) << std::endl << gai_strerror(open_socket) << std::endl;
		return "";
	}
	std::string ret(host_buf);
	ret += "-";
	ret += port_buf;
	return ret;
}

int get_server_socket(char* &host, const std::string &port) {
	addrinfo socket_config;
	addrinfo *open_socket, *curr_socket;

	std::memset(&socket_config, 0, sizeof(socket_config));
	socket_config.ai_flags = AI_PASSIVE;
	create_socket(host, port, socket_config, open_socket);
	
	int socket_return_code, close_result;
	for (curr_socket = open_socket; curr_socket != NULL; curr_socket = curr_socket->ai_next) {
		socket_return_code = socket(curr_socket->ai_family, curr_socket->ai_socktype, curr_socket->ai_protocol);
		if (socket_return_code == -1) {
			continue;
		}

		if (bind(socket_return_code, curr_socket->ai_addr, curr_socket->ai_addrlen) != EXIT_SUCCESS) {
			close_result = close(socket_return_code);
			if (close_result < EXIT_SUCCESS) {
				std::cerr << "Close server error in socket.cpp" << std::endl << strerror(errno) << std::endl;
			}
			continue;
		}
		std::cerr << "Established server socket: " << std::endl;
		std::cerr << "Host: " << get_name_from_addr((sockaddr *)&curr_socket, sizeof(curr_socket)) << std::endl;
		std::cerr << "Port: " << port << std::endl;
		freeaddrinfo(open_socket);
		return socket_return_code;
	}

	freeaddrinfo(open_socket);
	std::cerr << "Couldn't get server socket" << strerror(errno) << std::endl;
	exit(EXIT_FAILURE);
}

int get_client_socket(const std::string &host, const std::string &port) {
	addrinfo socket_config;
	addrinfo *open_socket, *curr_socket;

	std::memset(&socket_config, 0, sizeof(socket_config));
	socket_config.ai_flags = 0;
	create_socket(host.c_str(), port, socket_config, open_socket);

	int socket_return_code, close_result;
	for (curr_socket = open_socket; curr_socket != NULL; curr_socket = curr_socket->ai_next) {
		socket_return_code = socket(curr_socket->ai_family, curr_socket->ai_socktype, curr_socket->ai_protocol);
		if (socket_return_code == -1) {
			continue;
		}

		if (connect(socket_return_code, curr_socket->ai_addr, curr_socket->ai_addrlen) != EXIT_SUCCESS) {
			close_result = close(socket_return_code);
			if (close_result < EXIT_SUCCESS) {
				std::cerr << "Close client error in scoket.cpp" << std::endl << strerror(errno) << std::endl;
			}
			continue;
		}
		std::cerr << "Established client socket: " << std::endl;
		std::cerr << "Host: " << host << std::endl;
		std::cerr << "Port: " << port << std::endl;
		freeaddrinfo(open_socket);
		return socket_return_code;
	}

	freeaddrinfo(open_socket);
	std::cerr << "Couldn't get client socket" << strerror(errno) << std::endl;
	exit(EXIT_FAILURE);
}