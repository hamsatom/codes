#include <cstdio>
#include <iostream>
#include <cstring>
#include <cerrno>
#include <chrono>

#include "window.hpp"
#include "config_constants.hpp"


bool Window::has_sequence(const int & sequence)
{
	return windows[sequence % WINDOW_SIZE].sequence == sequence;
}

void Window::store(const int & sequence, const uint8_t & checksum, const std::string & message, const size_t & msg_length)
{
	WindowItem *curr_window = &windows[sequence % WINDOW_SIZE];
	curr_window->sequence = sequence;
	curr_window->checksum = checksum;
	curr_window->length = msg_length;
	curr_window->message = message;
	curr_window->sent = false;
	curr_window->acknowledged = false;
}

void Window::print_message(const int & sequence, FILE * out_location)
{
	WindowItem *curr_window = &windows[sequence % WINDOW_SIZE];
	std::fwrite(curr_window->message.c_str(), curr_window->length, 1, out_location);
	if (ferror(out_location)) {
		std::cerr << "window.cpp: print_message: fwrite failed" << std::endl << strerror(errno) << std::endl;
	}
}

std::chrono::time_point<std::chrono::high_resolution_clock> Window::get_start_time(const int &sequence) {
	WindowItem *curr_window = &windows[sequence % WINDOW_SIZE];
	if (curr_window->sequence != sequence) {
		return std::chrono::high_resolution_clock::now();
	}
	return curr_window->start_time;
}

bool Window::is_sent(const int &sequence) {
	WindowItem *curr_window = &windows[sequence % WINDOW_SIZE];
	if (curr_window->sequence == sequence) {
		return curr_window->sent;
	}
	return false;
}

void Window::set_sent(const int &sequence) {
	WindowItem *curr_window = &windows[sequence % WINDOW_SIZE];
	if (curr_window->sequence == sequence) {
		curr_window->sent = true;
	}
}

void Window::set_time(const int &sequence, const std::chrono::time_point<std::chrono::high_resolution_clock> &time) {
	WindowItem *curr_window = &windows[sequence % WINDOW_SIZE];
	if (curr_window->sequence == sequence) {
		curr_window->start_time = time;
	}
}

void Window::acknowledge(const int & sequence) {
	WindowItem *curr_window = &windows[sequence % WINDOW_SIZE];
	if (curr_window->sequence == sequence) {
		curr_window->acknowledged = true;
	}
}

std::string Window::get_message(const int & sequence)
{
	WindowItem *curr_window = &windows[sequence % WINDOW_SIZE];
	if (curr_window->sequence != sequence) {
		return "";
	}
	return curr_window->message;
}

bool Window::is_acknowledged(const int & sequence)
{
	WindowItem *curr_window = &windows[sequence % WINDOW_SIZE];
	if (curr_window->sequence != sequence) {
		return false;
	}
	return curr_window->acknowledged;
}

size_t Window::get_message_length(const int & sequence)
{
	WindowItem *curr_window = &windows[sequence % WINDOW_SIZE];
	if (curr_window->sequence != sequence) {
		return 0;
	}
	return curr_window->length;
}

uint8_t Window::get_checksum(const int &sequence) {
	WindowItem *curr_window = &windows[sequence % WINDOW_SIZE];
	if (curr_window->sequence != sequence) {
		return 0;
	}
	return curr_window->checksum;
}