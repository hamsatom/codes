/*
Thsi is a client that connects to provided server
*/
#pragma once
#include <string>
#include "window.hpp"
#include "blocking_queue.hpp"


blocking_queue<int> queue;

/*
If true client runs in testing mode and writes message to file
*/
static bool testing = false;

/*
Amount of packages to be sent to server
*/
static long test_amount = 100'000;

/*
Interval between number of packages where to measure time
*/
static long package_interval = 100;

/*
Contains name of this program more specificly it'S executable. It's similar to argv[0] of main method
*/
static std::string program_name;

/*
Entered host + entered port 
*/
static std::string client_name;

/*
If true program produces debuging report*/
static bool verbose = true;

/*
Prints message about usage*/
static void print_usage(FILE *location);


/*Prints list of supported arguments*/
static void print_help();

/*
Returns host and port from passed arguments*/
static std::pair< std::string, std::string> resolve_args(int &argc, char *argv[]);

/*
Runs the client*/
void start_client(Window &window, int send_socket);

/*
Check if return code doesn't equal -1 which means failure. If it does than print error msg and exits program.
*/
void handle_error(const std::string &msg, const int &code);

/*
Sends one frame to server more precisely one message stored in window
*/
static void send_frame(Window &window, const int send_socket);
