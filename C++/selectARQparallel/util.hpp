/*
This class contains basic function for operations with transmition.
*/
#pragma once
#include <string>
#include <random>

/*
Seeded random generator
*/
static std::random_device random_generator("/dev/random");

/**
* Read sequence number from received frame.
*/
extern int read_seq(const std::string &data);


/**
* Write sequence number to the given buffer.
*/
extern void write_seq(std::string &data, int seq);


/**
* Return true if sequence number a < b, else false.
*/
extern bool seq_is_less(const int &a, const int &b);

/**
* Return  if sequence number a > b, else false.
*/
extern bool seq_is_greater(const int &a, const int &b);

/**
* Return true if sequence number a >= b, else false.
*/
extern bool seq_is_equal_or_greater(const int &a, const int &b);

/**
* Return sequence number incremented by 1.
*/
extern int seq_increase(const int &seq);

/*
Return true if random roll is within specified propability range
*/
extern bool random_chance();

/*
* Return created sum of message using 8bit XOR rule. If msg is empty returns 0. If creating substring fails then bad_alloc is thrown.
*/
extern uint8_t generate_checksum8(const std::string &msg);


/*
* Read checksum from received frame - we suppose that it's last element in frame
*/
extern uint8_t read_chksum8(const std::string &data, size_t len);


/*
* Read checksum from received frame - we suppose that it's after two sequence chars
*/
extern size_t read_msg_length(const std::string &data);

/*
Return true if msg equals true, otherwise false. Throws bad_alloc if out of memory happens.
*/
extern bool msg_equals_checksum(const std::string &msg, const uint8_t &checksum);

/*
Return message from data
*/
extern std::string read_msg(const std::string &data, size_t len);

/*
Return random number from range like <start,end]
*/
extern size_t get_random_from_range(const size_t &start, const size_t &end);

/*
Returns true if length of the message and and length equals.
*/
extern bool lengths_equal(const std::string &msg, const size_t &length);

/*
Generates random string with random length within length limit defined by MESSAGE_SIZE
*/
extern std::string generate_random_string();

/*
Return hex representation of string
*/
extern char* get_hex_representation(const std::string &ptr, const size_t &size);