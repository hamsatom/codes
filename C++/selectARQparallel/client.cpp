#include <iostream>
#include <unistd.h>
#include <sys/select.h>
#include <sys/time.h>
#include <algorithm>
#include <cerrno>
#include <cstring>
#include <string>
#include <chrono>
#include <thread>

#include "util.hpp"
#include "client.hpp"
#include "sockets.hpp"
#include "config_constants.hpp"

int main(int argc, char *argv[]) {
    Window window{};
    
	program_name = argv[0];
	auto options = resolve_args(argc, argv);

	std::cerr << "Connecting to " << options.first << ":" << options.second << "..." << std::endl;

	int socket = get_client_socket(options.first, options.second);

	std::cerr << "connection successful" << std::endl;
	client_name = "client-" + options.first + "-" + options.second + "-" + generate_random_string().substr(0, 40);

	auto transmission_start_time = std::chrono::high_resolution_clock::now();
	std::thread t1(start_client, std::ref(window), socket);
	std::thread t2(send_frame, std::ref(window), socket);
	t1.join();
	t2.join();
	if (testing) {
		auto transmission_end_time = std::chrono::high_resolution_clock::now();
		auto time_diff = transmission_end_time - transmission_start_time;
		std::cout << TOTAL_TIME_UNITS_MSG << std::chrono::duration_cast<TOTAL_TIME_UNITS>(time_diff).count() << std::endl;
	}
}

static std::pair<std::string, std::string> resolve_args(int &argc, char *argv[])
{
	int curr_char;
	while ((curr_char = getopt(argc, argv, "hvt::i::")) != EOF) {
		switch (curr_char) {
		case 'h':
			print_help();
			exit(EXIT_SUCCESS);
			break;
		case 'v':
			verbose = true;
			break;
		case 't':
			testing = true;
			if (optarg) {
				test_amount = std::stol(optarg);
			}
			break;
		case 'i':
			if (optarg) {
				package_interval = std::stol(optarg);
			}
			break;
		case '?':
		default:
			print_usage(stderr);
			std::cerr << "Run " << program_name << " -h for help" << std::endl;
			exit(EXIT_FAILURE);
		}
	}

	if (optind != argc - 2) {
		std::cerr << MISSING_CLIENT_ARGS_MSG << std::endl;
		print_usage(stderr);
		exit(EXIT_FAILURE);
	}

	std::string host = argv[optind];
	std::string port = argv[optind + 1];

	return{ std::move(host), std::move(port) };
}

static void print_usage(FILE *out) {
	int result = fprintf(out, "Usage: %s [<options>] <host> <port>\n", program_name.c_str());
	handle_error("fprintf error in client.cpp", result);
}

static void print_help() {
	print_usage(stdout);
	std::cout << HELP_CLIENT_MSG << std::endl;
}

void start_client(Window &window, int send_socket)
{
	int server_wait_seq = 0;
	int seq_to_fill = 0;
	bool eof_received = false;
	

	int select_result;
	int max_fd = 0;
	fd_set read_set, write_set;
	timeval select_timeout;
	auto last_packet_time = std::chrono::high_resolution_clock::now();

	while (true)
	{
		FD_ZERO(&read_set);
		FD_ZERO(&write_set);

		/* budeme cekat na data ze site */
		FD_SET(send_socket, &read_set);
		max_fd = std::max(max_fd, send_socket);

		/* cekame na standardni vstup, pokud neni okno pro odeslani plne */
		if (!eof_received && seq_is_less(seq_to_fill, server_wait_seq + WINDOW_SIZE)) {
			FD_SET(0, &read_set);
			max_fd = std::max(max_fd, 0);
		}

		select_timeout.tv_sec = TIMEOUT / 1000;
		select_timeout.tv_usec = (TIMEOUT % 1000) * 1000;
		select_result = select(max_fd + 1, &read_set, &write_set, NULL, &select_timeout);

		handle_error(CLIENT_SELECT_ERROR_MSG, select_result);

		if (FD_ISSET(0, &read_set)) {
			/* precteme data ze standardniho vstupu a ulozime do okna a odesleme je */
			std::string stdin_msg;
			stdin_msg.reserve(MESSAGE_SIZE);
			if (testing) {
				if (seq_to_fill == 0) {
					last_packet_time = std::chrono::high_resolution_clock::now();
				}
				if (seq_to_fill != 0 && seq_to_fill % package_interval == 0) {
					auto time_now = std::chrono::high_resolution_clock::now();
					auto time_diff = time_now - last_packet_time;
					std::cout << INTERVAL_UNITS_MSG << seq_to_fill - package_interval << "-" << seq_to_fill << " packets: " << std::chrono::duration_cast<INTERVAL_UNITS>(time_diff).count() << std::endl;
					last_packet_time = std::chrono::high_resolution_clock::now();
				}
				if (seq_to_fill >= test_amount) {
					eof_received = true;
				}
				stdin_msg = generate_random_string();
			}
			else
			{
				std::getline(std::cin, stdin_msg);
				if (std::cin.eof()) {
					eof_received = true;
				}
				else if (!std::cin)
				{
					handle_error(CLIENT_READ_ERROR_MSG, -1);
				}
			}

			if (stdin_msg.length() > MESSAGE_SIZE - 1) {
				stdin_msg.resize(MESSAGE_SIZE - 1);
			}
			stdin_msg.append("\r\n");

			uint8_t checksum;
			try {
				checksum = generate_checksum8(stdin_msg);
			}
			catch (std::bad_alloc& ba) {
				if (verbose) {
					std::cerr << "Error while generating checksum" << ba.what() << std::endl;
				}
				continue;
			}


			if (verbose) {
				std::cerr << "Msg from stdin " << stdin_msg;
			}
			
			window.store(seq_to_fill, checksum, stdin_msg, stdin_msg.length());

			if (testing) {
				std::string file_name = client_name + REPORT_FORMAT;
				FILE *report_file = fopen(file_name.c_str(), "a+");
				window.print_message(seq_to_fill, report_file);
				fclose(report_file);
			}

			queue.add(seq_to_fill);
			seq_to_fill = seq_increase(seq_to_fill);
		}

		if (FD_ISSET(send_socket, &read_set)) {
			/* prijmuti potvrzeni ze site*/
			std::string received_msg;
			received_msg.resize(SEQ_NUMBER_SIZE);
			int read_received_result = read(send_socket, &received_msg[0], received_msg.size());
			handle_error(CLIENT_READ_ERROR_MSG, read_received_result);

			if (read_received_result == SEQ_NUMBER_SIZE) {
				/* je to datagram spravne delky */
				int seq = read_seq(received_msg);
				window.acknowledge(seq);
				if (verbose) {
					std::cerr << "Received acknowledgment for " << window.get_message(seq);
				}
				if (seq_is_equal_or_greater(seq, server_wait_seq)) {
					while (window.is_acknowledged(server_wait_seq) && seq_is_less(server_wait_seq, seq_to_fill))
					{
						server_wait_seq = seq_increase(server_wait_seq);
					}

					if (eof_received  && server_wait_seq == seq_to_fill) {
						/* transfer finished successfully */
						queue.close();
						break;
					}

				}
				else if (seq == server_wait_seq) {
					/* server potvrzuje neco, co uz potvrdil; posleme znovu prvni
					ramec z okna */
					queue.add(server_wait_seq);
				}
			}
		}

		if (server_wait_seq != seq_to_fill) {  /* je neco k odeslani */
			auto time_now = std::chrono::high_resolution_clock::now();
			for (int curr_seq = server_wait_seq; curr_seq < seq_to_fill; ++curr_seq)
			{
				auto time_difference = time_now - window.get_start_time(curr_seq);
				auto difference_in_microsecond = std::chrono::duration_cast<std::chrono::microseconds>(time_difference).count();

				if (difference_in_microsecond > TIMEOUT && !window.is_acknowledged(curr_seq)) {
					queue.add(curr_seq);
				}
			}
		}
		if (verbose) {
			std::cout << "----------Curr window-------------" << std::endl;
			for (int curr_seq = server_wait_seq; curr_seq < seq_to_fill; ++curr_seq)
			{
				std::cout << std::boolalpha << "----Acknowledged: " << window.is_acknowledged(curr_seq) << "Sent " << window.is_sent(curr_seq);
				std::cout << "--Message: " << window.get_message(curr_seq);
			}
			std::cout << "---------------------------------" << std::endl;
		}
	}
}

void handle_error(const std::string &msg, const int &code) {
	if (code == -1) {
		std::cerr << msg << std::endl << strerror(errno) << std::endl;
		exit(EXIT_FAILURE);
	}
}

static void send_frame(Window &window, const int send_socket)
{
	int seq;
	int seq_to_send;
	while (!queue.is_closed()) {
		if (!queue.take(seq_to_send)) {
			std::cout << "Skipped" << std::endl;
			continue;
		}
        seq = seq_to_send;
				
		if (!window.has_sequence(seq)) {
			continue;
		}
		
		int write_result;
		std::string msg_to_send;
		msg_to_send.resize(SEQ_NUMBER_SIZE + MSG_LENGTH_SIZE + CHECKSUM_SIZE + window.get_message_length(seq));

		//we have to convert message from const char* to const unsigned char* to avoid problems with another using of checksum
		unsigned char checksum_8b = unsigned(window.get_checksum(seq));
		unsigned char msg_length = unsigned(window.get_message_length(seq));

		if (verbose) {
			std::cerr << "Checksum send: " << static_cast<unsigned>(checksum_8b) << std::endl;
		}


		write_seq(msg_to_send, seq);

		std::memcpy(&msg_to_send[SEQ_NUMBER_SIZE], &msg_length, MSG_LENGTH_SIZE);
		std::memcpy(&msg_to_send[SEQ_NUMBER_SIZE + MSG_LENGTH_SIZE], window.get_message(seq).c_str(), window.get_message_length(seq));
		std::memcpy(&msg_to_send[SEQ_NUMBER_SIZE + MSG_LENGTH_SIZE + window.get_message_length(seq)], &checksum_8b, CHECKSUM_SIZE);

		//break message with given propability
			//when we change some letter with given propability the string has to be greater than 4([seq,seq,msg_length,checksum])
		if (msg_to_send.length() > 4 && !random_chance()) {
			if (verbose) {
				std::cerr << "Msg before byte change " << msg_to_send << std::endl;
			}
			size_t index_to_change = get_random_from_range(SEQ_NUMBER_SIZE + MSG_LENGTH_SIZE, msg_to_send.length() - CHECKSUM_SIZE);
			++msg_to_send[index_to_change];
			if (verbose) {
				std::cerr << "Msg after byte change " << msg_to_send << std::endl;
			}
		}

		if (random_chance()) {
			if (verbose) {
				std::cerr << "Sending msg: " << msg_to_send << std::endl;
			}
			write_result = write(send_socket, msg_to_send.c_str(), SEQ_NUMBER_SIZE + MSG_LENGTH_SIZE + window.get_message_length(seq) + CHECKSUM_SIZE);
			handle_error(CLIENT_WRITE_ERROR_MSG, write_result);
		}
		else if (verbose)
		{
			std::cerr << "Message dropped" << std::endl;
		}
		window.set_time(seq, std::chrono::high_resolution_clock::now());
	}
}
