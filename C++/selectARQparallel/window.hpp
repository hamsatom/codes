#pragma once
#include <string>
#include <chrono>
#include <memory>
#include "config_constants.hpp"
/*
This class is used for storing messages
*/
class Window {
private:
	struct WindowItem {
		/*
		Number of message
		*/
		int sequence = -1;
		/*
		Contol sum of content
		*/
		uint8_t checksum;
		/*
		Length of message
		*/
		size_t length;
		/*
		Message ended with extra \0 so it prints automatically new line
		*/
		std::string message;
		/*
		Point of time when we started waiting for acknowledgment
		*/
		std::chrono::time_point<std::chrono::high_resolution_clock> start_time;
		/*
		True if server acknowledged receiving this msg
		*/
		bool acknowledged = false;	
		/*
		True if item was sent falsse othwerwise
		*/
		bool sent = false;
	};
	WindowItem windows[WINDOW_SIZE];
public:
	/*
	Return true if window sequence equals given sequence
	*/
	bool has_sequence(const int &sequence);
	/*
	Stores given content into window
	*/
	void store(const int & sequence, const uint8_t & checksum, const std::string & message, const size_t & msg_length);
	/*
	Prints stored message to given location vis fwrite. Print message to stderr if failed.
	*/
	void print_message(const int &sequence, FILE *out_location);
	/*
	Return soted message
	*/
	std::string get_message(const int &sequence);
	/*
	Return length stored in window
	*/
	size_t get_message_length(const int &sequence);
	/*
	Returns checksum stored in window
	*/
	uint8_t get_checksum(const int &sequence);
	/*
	Set status acknowledge to true
	*/
	void acknowledge(const int & sequence);
	/*
	Return time when msg was send last time
	*/
	std::chrono::time_point<std::chrono::high_resolution_clock> get_start_time(const int &sequence);
	/*
	Sets start_time of WindowItem
	*/
	void set_time(const int &sequence, const std::chrono::time_point<std::chrono::high_resolution_clock> &time);
	/*
	Return true if window was acknowledge, false if wasn't acknowledged or called sequence doesn't equal stored sequence
	*/
	bool is_acknowledged(const int & sequence);
	/*
	Returns true if window was sent, false if wasn't acknowledged or called sequence doesn't equal stored sequence
	*/
	bool is_sent(const int &sequence);
	/*
	Sets Item'S status to sent
	*/
	void set_sent(const int &sequence);
};