#include "solver.hpp"
#include <sstream>
#include <stack>
#include <cmath>

std::string log_info;
const bool debug_posfix = true;
/*
These 2 must be off in order to get evalution from Upload system
*/
const bool log_input = true;
const bool log_result = true;


solver::solver()
{
}

solver::~solver()
{
}

Operators_Priority get_priority(const char &c)
{
	switch (c)
	{
	case '+':
		return plus;
	case '-':
		return minus;
	case '*':
		return multiply;
	case '/':
		return divide;
	case '^':
		return power;
	case '(':
	case ')':
		return parenthes;
	default:
		log_info.append("Get priority called with unknown operator: \"");
		log_info += c;
		log_info.append("\" \n");
		return number;
	}
}

Associativity get_associvity(const Operators_Priority &priority)
{
	switch (priority)
	{
	case parenthes:
		return Associativity::NONE;
		/*
		Minus has same priority as plus
		*/
	case plus:
		/*
		Multiplication has same priority as division
		*/
	case divide:
		return Associativity::LEFT;
	case power:
		return Associativity::RIGHT;
	default:
		log_info.append("Get associavity called with unknown operator: \"");
		log_info += priority;
		log_info.append("\" \n");
		return Associativity::NONE;
	}
}

std::ostream & operator<<(std::ostream & os, const expr &)
{
	os << log_info;
	log_info.clear();
	return os;
}

std::unique_ptr<expr> create_expression_tree(const std::string & expression)
{
	std::stack<Symbol> stack;
	auto solv_class = std::make_unique<solver>();
	char c;
	std::stringstream ss;
	ss.str(expression);

	if (log_input)
	{
		log_info.append("Create_tree arguments: ");
		log_info.append(expression + "\n");
	}

	while (ss.peek() != EOF)
	{
		c = ss.get();

		if (debug_posfix)
		{
			log_info.append("curr char: \"");
			log_info += c;
			log_info.append("\" \n");
		}

		if (isspace(c))
		{
			continue;
		}
		Symbol current_symbol;
		current_symbol.value = c;
		if (isdigit(c))
		{
			while (isdigit(ss.peek()) || ss.peek() == '.')
			{
				current_symbol.value.push_back(ss.get());
			}
			solv_class->postfix.push_back(current_symbol);
		}
		else if (c == ')')
		{
			while (!stack.empty() && stack.top().value != "(")
			{

				if (debug_posfix)
				{
					log_info.append("stack top while finding: ");
					log_info.append(stack.top().value);
					log_info.append("\n");
				}

				solv_class->postfix.push_back(stack.top());
				stack.pop();
			}
			stack.pop();
		}
		else
		{
			current_symbol.priority = get_priority(c);
			current_symbol.type = Symbol_Type::OPERATOR;
			current_symbol.associavity = get_associvity(current_symbol.priority);
			while (!stack.empty())
			{
				if ((current_symbol.associavity == Associativity::LEFT && current_symbol.priority <= stack.top().priority)
					|| (current_symbol.associavity == Associativity::RIGHT && current_symbol.priority < stack.top().priority))
				{
					solv_class->postfix.push_back(stack.top());
					stack.pop();
				}
				else
				{
					break;
				}
			}
			stack.push(current_symbol);
		}
	}

	while (!stack.empty())
	{
		solv_class->postfix.push_back(stack.top());
		stack.pop();
	}

	log_info.append("Posif result: ");
	for (const Symbol &symb : solv_class->postfix)
	{
		log_info.append(symb.value);
		log_info.append(" ");
	}
	log_info.append("\n");

	return std::move(solv_class);
}

void get_numbers_from_stack(std::stack<double> &stack, double &num1, double &num2)
{
	num1 = stack.top();
	stack.pop();
	num2 = stack.top();
	stack.pop();
}

double solver::evaluate() const
{
	std::stack<double> stack;
	double num1, num2;
	for (const Symbol &symb : postfix)
	{
		if (symb.type == Symbol_Type::NUMBER)
		{
			stack.push(stod(symb.value));
		}
		else
		{
			get_numbers_from_stack(stack, num1, num2);
			if (symb.value == "+")
			{
				stack.push(num2 + num1);
			}
			else if (symb.value == "-")
			{
				stack.push(num2 - num1);
			}
			else if (symb.value == "*")
			{
				stack.push(num2 * num1);
			}
			else if (symb.value == "/")
			{
				stack.push(num2 / num1);
			}
			else if (symb.value == "^")
			{
				stack.push(pow(num2, num1));
			}
			else
			{
				log_info.append("Unknown operation in eval: \"");
				log_info.append(symb.value);
				log_info.append("\" \n");
			}
		}
	}

	if (log_result)
	{
		log_info.append("Result: ");
		log_info = std::to_string(stack.top());
		log_info.append("\n");
	}

	return stack.top();
}

void solver::write(std::ostream &) const
{
}