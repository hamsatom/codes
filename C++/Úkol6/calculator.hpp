#include "expr.hpp"
#include <vector>
#include <math.h>
#pragma once

class calculator : public expr {

private:
	virtual void write(std::ostream& out) const;

public:
	std::vector<std::string> postfix;

	calculator();
	virtual ~calculator();

	double virtual evaluate() const;
};