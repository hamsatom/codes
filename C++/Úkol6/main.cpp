#include <iostream>
#include "calculator.hpp"
#include <sstream>
#include <ctype.h>
#include <stack>

using namespace std;

calculator::calculator() {

}

calculator::~calculator() {

}
double calculator::evaluate() const
{
	std::stack<long double> stack;
	
	for (auto& tmp : postfix) {
		if (isdigit(tmp.at(0))) {
			stack.push(stod(tmp));
		}
		else {
			if (stack.size() == 1) {
				return stack.top();
			}

			long double num2 = stack.top();
			stack.pop();
			long double num1 = stack.top();
			stack.pop();


			switch (tmp.at(0)) {
			case 43:
				stack.push(num1 + num2);
				break;
			case 45:
				stack.push(num1 - num2);
				break;
			case 42:
				stack.push(num1 * num2);
				break;
			case 47:
				stack.push(num1 / num2);
				break;
			case 94:
				long double tmpp = pow(num1, num2);
				stack.push(tmpp);
				break;
			}
		}
	}

	return stack.top();
}

void calculator::write(std::ostream & out) const
{
}

std::ostream& operator<<(std::ostream& out, const expr& expr) {
	out << expr.evaluate();
	return out;
}

std::unique_ptr<expr> create_expression_tree(const std::string& expression) {

	std::stringstream ss;
	std::stack<char> stack;

	auto cal = std::make_unique<calculator>();

	ss.str(expression);
	string tmp2;

	for (auto& tmp : expression) {

		if (tmp == 41) {
			
			if (tmp2 != "") {
				cal->postfix.push_back(tmp2);
				tmp2 = "";
			}

			string tmp3;
			tmp3 += stack.top();
			stack.pop();

			if (tmp3 != "(") {
				cal->postfix.push_back(tmp3);
				if (stack.size() > 0) {
					tmp3 = "";
					tmp3 += stack.top();
					stack.pop();
					while (tmp3 != "(") {
						cal->postfix.push_back(tmp3);
						tmp3 = "";
						tmp3 += stack.top();
						stack.pop();
					}
				}
			}
		}
		else if (stack.size() > 0 && 
			(((stack.top() == 42 && tmp == 47) || (stack.top() == 47 && tmp == 42)) 
				|| ((stack.top() == 45 && tmp == 43) || (stack.top() == 43 && tmp == 45)) 
				|| (stack.top() == 45 && tmp == 45) || (stack.top() == 43 && tmp == 43) 
				|| (stack.top() == 47 && tmp == 47) 
				|| (stack.top() == 42 && tmp == 42)
				|| (stack.top() == 47 && tmp == 43)
				|| (stack.top() == 42 && tmp == 43)
				|| (stack.top() == 47 && tmp == 45)
				|| (stack.top() == 42 && tmp == 45)
				|| (stack.top() == 94 && tmp == 47)
				|| (stack.top() == 94 && tmp == 42)
				|| (stack.top() == 94 && tmp == 43)
				|| (stack.top() == 94 && tmp == 45))) {

			if (tmp2 != "") {
				cal->postfix.push_back(tmp2);
				tmp2 = "";
			}

			string tmp3;
			tmp3 += stack.top();
			stack.pop();

			cal->postfix.push_back(tmp3);
			stack.push(tmp);
		}else {
			if (isdigit(tmp) || tmp == 46) {
				tmp2 += tmp;
			}
			else if (tmp == 43 || tmp == 42 || tmp == 45 || tmp == 47 || tmp == 94 || tmp == 40 || tmp == 41) {

				stack.push(tmp);

				if (tmp2 != "") {
					cal->postfix.push_back(tmp2);
					tmp2 = "";
				}


			}
		}
	}

	if (tmp2 != "") {
		cal->postfix.push_back(tmp2);
	}

	while (!stack.empty()) {
		string tmp3;
		tmp3 += stack.top();
		stack.pop();

		if (tmp3 != "(") {
			cal->postfix.push_back(tmp3);
		}
	}

	return std::move(cal);
}
