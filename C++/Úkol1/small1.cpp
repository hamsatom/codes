#include "small1.hpp"
#include <iostream>
#define ull register unsigned long long

using namespace std;

unsigned long long count_words(string &line) {
	ull count = 1;
	char previous = line.front();
	for (string::iterator it = line.begin(); it != line.end(); ++it) {
		if (isspace(previous) && !isspace(*it))
		{
			++count;
		}
		previous = *it;
	}
	return count;
}

std::pair<int, int> parse_matrix(std::istream& in) {
	register string line;
	if (!getline(in, line)) {
		return{ 0,0 };
	}
	ull row_index = 1;
	ull count;
	ull column_index = count_words(line);

	while (getline(in, line)) {
		count = count_words(line);
		if (count != column_index) {
			throw std::invalid_argument("Column size doesn't match");
		}
		++row_index;
	}
	return{ row_index, column_index };
}

void hline(std::ostream& out, ull W) {
	W = 2 * W + 3;
	for (ull i = 0; i < W; ++i) {
		out << '-';
	}
	out << endl;
}

void print_table(std::ostream& out, const std::vector<std::string>& vec) {
	ull max_length = 0;
	register string s;
	for (string s : vec) {
		if (s.size() > max_length) {
			max_length = s.size();
		}
	}
	++max_length;
	hline(out, max_length);
	for (ull i = 1; i < vec.size(); i += 2) {
		s = vec.at(i - 1);
		s.insert(0, max_length - s.length(), ' ');
		out << s.insert(0, "|");
		s = vec.at(i);
		s.insert(0, max_length - s.length(), ' ');
		out << s.insert(0, "|") << '|' << '\n';
	}
	if (vec.size() % 2 != 0) {
		s = vec.at(vec.size() - 1);
		s.insert(0, max_length - s.length(), ' ');
		out << s.insert(0, "|");
		s = " ";
		s.insert(0, max_length - s.length(), ' ');
		out << s.insert(0, "|") << '|' << '\n';
	}
	hline(out, max_length);
	out.flush();
}

bool validate_line(const std::string& str) {
	if (str.length() < 10 || str.back() == 'x') {
		return false;
	}
	ull i;
	register bool should_contain_value;
	if (!str.compare(0, 4, "true")) {
		should_contain_value = true;
		i = 5;
	}
	else if (!str.compare(0, 5, "false")) {
		{
			should_contain_value = false;
			i = 6;
		}
	}
	else
	{
		return false;
	}
	register string numbers[3];
	register unsigned short numbers_taken = 0;
	ull result;
	register bool valid_numbers;
	ull from = i;

	for (; i < str.length(); ++i) {
		while (!isspace(str[i]) && i < str.length()) {
			++i;
		}
		numbers[numbers_taken] = str.substr(from, i - 1);
		++numbers_taken;
		from = i;
	}

	try {
		result = stoull(numbers[0], NULL, 8);
		valid_numbers = (result == stoull(numbers[1], NULL, 10));
		valid_numbers = valid_numbers && (result == stoull(numbers[2], NULL, 16));
	}
	catch (invalid_argument e) {
		return false;
	}

	return should_contain_value == valid_numbers;
}

std::string max_number(std::istream& in) {
	register string line, buf = "", max = "";
	ull i;
	string::iterator it;
	while (getline(in, line)) {
		for (it = line.begin(); it < line.end(); ++it) {
			while (*it != ',' && it < line.end()) {
				buf += *it;
				++it;
			}
			if (max.length() < buf.length()) {
				max = buf;
			}
			else if (max.length() == buf.length()) {
				for (i = 0; i < max.length(); ++i)
				{
					if (max[i] > buf[i]) {
						break;
					}
					else if (buf[i] > max[i])
					{
						max = buf;
						break;
					}
				}
			}
			buf.clear();
		}
	}
	return max;
}