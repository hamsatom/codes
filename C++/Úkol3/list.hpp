#ifndef PJC_LIST_HPP
#define PJC_LIST_HPP

#include <cstddef>

using std::size_t;

struct list {
    struct node {
        double val;
        node* prev;
        node* next;
    };

    node* head = nullptr;
    node* tail = nullptr;
    size_t size = 0;
};

/*
 * Inserts an element at the end of the list.
 *
 * Should run in in constant time.
 */
void push_back(list& l, double elem);

/*
 * Removes an element from the end of the list.
 *
 * Precondition: Should not be called on an empty list.
 * Should run in in constant time.
 */
void pop_back(list& l);

/*
 * Allows access to the last element of the list.
 *
 * Precondition: Should not be called on an empty list.
 * Should run in in constant time.
 */
double& back(list& l);

/*
 * Inserts an element at the front of the list.
 *
 * Should run in in constant time.
 */
void push_front(list& l, double elem);

/*
 * Removes an element from the front of the list.
 *
 * Precondition: Should not be called on an empty list.
 * Should run in in constant time.
 */
void pop_front(list& l);

/*
 * Allows access to the first element of the list.
 *
 * Precondition: Should not be called on an empty list.
 * Should run in in constant time.
 */
double& front(list& l);

/*
 * After dispose, the list l must be empty and not hold on to any memory.
 * Over the lifetime of a list, dispose can be called any number of times.
 *
 * Should run in linear time.
 */
void dispose(list& l);

/*
 * Destructively appends list r to list l, so that list r ends up empty.
 *
 * Should run in constant time.
 */
void join(list& l, list& r);

/*
 * Reverses order of all elements of list l.
 * Given list containing 1 2 3 4 2 1, the result should be a list containing
 * 1 2 4 3 2 1.
 *
 * Should run in linear time.
 */
void reverse(list& l);

/*
 * Filters elements in the list so that there are no immediate duplicates.
 * Given list containing 1 1 2 2 2 3 4 2 1, the result should be a list
 * containing 1 2 3 4 2 1.
 *
 * Should run in linear time.
 */
void unique(list& l);

/*
 * Returns size of the list l.
 *
 * Should run in constant time.
 */
size_t size(const list& l);

/*
 * Returns true if the list l does not contain any elements.
 *
 * Should run in constant time.
 */
bool empty(const list& l);

#endif
