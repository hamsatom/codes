#include "list.hpp"
#include <stdexcept>  

void push_back(list& l, double elem) {
	list::node *new_tail = new list::node{ elem, l.tail, nullptr };
	l.size += 1;
	if (l.tail == nullptr) {
		l.head = l.tail = new_tail;
		return;
	}
	l.tail->next = new_tail;
	l.tail = new_tail;
}

void pop_back(list& l) {
	if (l.tail == nullptr) {
		return;
	}
	list::node *new_tail = l.tail->prev;
	l.size -= 1;
	new_tail != nullptr ? new_tail->next = nullptr : l.head = nullptr;
	delete l.tail;
	l.tail = new_tail;
}

double& back(list& l) {
	if (l.tail == nullptr) {
		throw std::invalid_argument("Tail of the list is null");
	}
	return l.tail->val;
}

void push_front(list& l, double elem) {
	list::node *new_head = new list::node{ elem, nullptr, l.head };
	l.size += 1;
	if (l.head == nullptr) {
		l.head = l.tail = new_head;
		return;
	}
	l.head->prev = new_head;
	l.head = new_head;
}

void pop_front(list& l) {
	if (l.head == nullptr) {
		return;
	}
	list::node *new_head = l.head->next;
	l.size -= 1;
	new_head != nullptr ? new_head->prev = nullptr : l.tail = nullptr;
	delete l.head;
	l.head = new_head;
}

double& front(list& l) {
	if (l.head == nullptr) {
		throw std::invalid_argument("Head of the list is null");
	}
	return l.head->val;
}

void dispose(list & l) {
	if (l.head == nullptr) {
		return;
	}
	list::node *cur_node = l.head, *next_node;
	while (cur_node != nullptr)
	{
		next_node = cur_node->next;
		delete cur_node;
		cur_node = next_node;
	}
	l.size = 0;
	l.head = l.tail = nullptr;
}

void join(list& l, list& r) {
	if (r.head == nullptr) {
		return;
	}
	list empty_list{};
	if (l.tail == nullptr) {
		l = r;
		r = empty_list;
		return;
	}
	l.tail->next = r.head;
	r.head->prev = l.tail;
	l.size += r.size;
	r = empty_list;
}

void reverse(list& l) {
	if (l.head == nullptr || l.head == l.tail) {
		return;
	}
	list::node *tmp;
	list::node *cur_node = l.head, *next_node;
	while (cur_node != nullptr)
	{
		next_node = cur_node->next;
		tmp = cur_node->prev;
		cur_node->prev = cur_node->next;
		cur_node->next = tmp;
		cur_node = next_node;
	}
	tmp = l.head;
	l.head = l.tail;
	l.tail = tmp;
}

void unique(list& l) {
	if (l.head == nullptr) {
		return;
	}
	list::node *from = l.head, *cur_node = l.head->next, *next_node;
	while (cur_node != l.tail)
	{
		next_node = cur_node->next;
		if (from->val == cur_node->val) {
			delete cur_node;
			l.size -= 1;
		}
		else
		{
			from->next = cur_node;
			cur_node->prev = from;
			from = cur_node;
		}
		cur_node = next_node;
	}
	if (from->val == l.tail->val) {
		delete l.tail;
		from->next = nullptr;
		l.tail = from;
		l.size -= 1;
	}
}

size_t size(const list& l) {
	return l.size;
}

bool empty(const list& l) {
	return l.size == 0;
}
