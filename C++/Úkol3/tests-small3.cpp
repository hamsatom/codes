#include "catch.hpp"

#include <algorithm>
#include <functional>
#include <iterator>

#include "list.hpp"


namespace {
bool list_equal(const list& l, const std::vector<double>& numbers) {
    // This might help to prevent hard crashes in some cases.
    // But its rather unlikely, because its the internal structure that gets people, not incrementing/decrementing sizes.
    if (size(l) != numbers.size()) {
        return false;
    }


    // Check the internal structure
    // Front to back
    {
        auto* cursor = l.head;
        auto expected = begin(numbers);

        while (cursor != nullptr) {
            if (expected == end(numbers)) {
                return false;
            }
            if (cursor->val != *expected) {
                return false;
            }
            cursor = cursor->next;
            ++expected;
        }
    }
    // Back to front
    {
        auto* cursor = l.tail;
        auto expected = rbegin(numbers);

        while (cursor != nullptr) {
            if (expected == rend(numbers)) {
                return false;
            }
            if (cursor->val != *expected) {
                return false;
            }
            cursor = cursor->prev;
            ++expected;
        }
    }
    return true;
}

bool reports_as_empty(const list& l) {
    return empty(l) && (size(l) == 0);
}

class scope_guard {
public:
    scope_guard(std::function<void(void)> func):
        f(std::move(func)) {}

    // Copies aren't meaningful, moves are implicitly disabled for simplicity
    scope_guard(const scope_guard&) = delete;
    scope_guard& operator=(const scope_guard&) = delete;


    ~scope_guard() {
        if (enabled) {
            f();
        }
    }
    void dismiss() {
        enabled = false;
    }
private:
    bool enabled = true;
    std::function<void(void)> f;
};

}

TEST_CASE("Basic functionality -- {pop, push}_{front, back}, size, empty, dispose") {
    list l;
    scope_guard sg([&]() { dispose(l); });
    REQUIRE(reports_as_empty(l));
    SECTION("Series of push_back") {
        for (int i : {0, 2, 5, 9}){ 
            push_back(l, i);
            REQUIRE(back(l) == i);
        }
        REQUIRE(size(l) == 4);
        REQUIRE(list_equal(l, { 0, 2, 5, 9 }));
    }
    SECTION("Series of push_front") {
        for (int i : {0, 3, 7, 2}) {
            push_front(l, i);
            REQUIRE(front(l) == i);
        }
        REQUIRE(size(l) == 4);
        REQUIRE(list_equal(l, { 2, 7, 3, 0 }));
    }
    SECTION("Series of push_back and pop_back") {
        for (int i = 0; i < 30; i += 4) {
            push_back(l, i);
            REQUIRE(back(l) == i);
            REQUIRE(front(l) == i);
            REQUIRE(size(l) == 1);
            pop_back(l);
            REQUIRE(reports_as_empty(l));
        }
        REQUIRE(reports_as_empty(l));
    }
    SECTION("Series of push_back and pop_front") {
        for (int i = 0; i < 30; i += 4) {
            push_back(l, i);
            REQUIRE(back(l) == i);
            REQUIRE(front(l) == i);
            REQUIRE(size(l) == 1);
            pop_front(l);
            REQUIRE(reports_as_empty(l));
        }
        REQUIRE(reports_as_empty(l));
    }
    SECTION("Series of push_front and pop_front") {
        for (int i = 0; i < 30; i += 4) {
            push_front(l, i);
            REQUIRE(back(l) == i);
            REQUIRE(front(l) == i);
            REQUIRE(size(l) == 1);
            pop_front(l);
            REQUIRE(reports_as_empty(l));
        }
        REQUIRE(reports_as_empty(l));
    }
    SECTION("Series of push_front and pop_back") {
        for (int i = 0; i < 30; i += 4) {
            push_front(l, i);
            REQUIRE(back(l) == i);
            REQUIRE(front(l) == i);
            REQUIRE(size(l) == 1);
            pop_back(l);
            REQUIRE(reports_as_empty(l));
        }
        REQUIRE(reports_as_empty(l));
    }
    SECTION("complex 1") {
        for (int i : {0, 2, 4, 6, 8}) {
            push_back(l, i);
        }
        REQUIRE(size(l) == 5);
        pop_back(l);
        pop_front(l);
        REQUIRE(size(l) == 3);
        for (int i : {1, 3, 0}) {
            push_front(l, i);
        }
        REQUIRE(list_equal(l, {0, 3, 1, 2, 4, 6}));
    }
    SECTION("complex 2") {
        for (int i : {0, 2, 4}) {
            push_back(l, i);
            push_front(l, i);
        }
        REQUIRE(list_equal(l, { 4, 2, 0, 0, 2, 4 }));
        pop_front(l);
        pop_back(l);
        pop_back(l);
        REQUIRE(size(l) == 3);
        REQUIRE(front(l) == 2);
        REQUIRE(back(l) == 0);
        for (int i : {1, 2, 3}) {
            push_front(l, i);
            push_back(l, i);
        }
        REQUIRE(list_equal(l, {3, 2, 1, 2, 0, 0, 1, 2, 3}));
    }
    SECTION("complex 3") {
        for (int i : {0, 2, 4}) {
            push_back(l, i);
        }
        for (int i : {1, 3, 5}) {
            push_front(l, i);
        }
        REQUIRE(list_equal(l, {5, 3, 1, 0, 2, 4}));
        dispose(l);
        for (int i : {1, 2}) {
            push_front(l, i);
        }
        for (int i : {3, 5}) {
            pop_back(l);
            push_back(l, i);
        }
        REQUIRE(list_equal(l, { 2, 5 }));
    }
    SECTION("complex 4") {
        for (int i : {1, 2, 3, 4}) {
            push_back(l, i);
        }
        for (int i = 0; i < 2; ++i) {
            pop_back(l);
            pop_front(l);
        }
        REQUIRE(reports_as_empty(l));

        for (int i : {1, 2, 3, 4}) {
            push_back(l, i);
        }
        pop_back(l);
        pop_back(l);
        for (int i : {5, 6}) {
            push_back(l, i);
        }
        pop_front(l);
        pop_front(l);
        pop_front(l);
        REQUIRE(size(l) == 1);
        REQUIRE(!empty(l));
        for (int i : {7, 8}) {
            push_front(l, i);
        }
        REQUIRE(list_equal(l, { 8, 7, 6 }));
    }
    SECTION("Dispose can be called multiple times") {
        dispose(l);
        dispose(l);
        REQUIRE(reports_as_empty(l));
        dispose(l);
        for (int i : {1, 2, 3, 4, 5}) {
            push_back(l, i);
        }
        dispose(l);
        dispose(l);
        REQUIRE(reports_as_empty(l));
    }
}


TEST_CASE("join") {
    list lhs, rhs;
    scope_guard lsg([&]() { dispose(lhs); });
    scope_guard rsg([&]() { dispose(rhs); });
    auto elems = { 0., 2., 4. };
    SECTION("empty lists") {
        join(lhs, rhs);
        REQUIRE(reports_as_empty(lhs));
        REQUIRE(reports_as_empty(rhs));
    }
    SECTION("rhs is empty") {
        for (auto e : elems) {
            push_back(lhs, e);
        }
        join(lhs, rhs);
        REQUIRE(list_equal(lhs, elems));
        REQUIRE(reports_as_empty(rhs));
    }
    SECTION("lhs is empty") {
        for (auto e : elems) {
            push_back(rhs, e);
        }
        join(lhs, rhs);
        REQUIRE(list_equal(lhs, elems));
        REQUIRE(reports_as_empty(rhs));
    }
    SECTION("both lists have elements in them") {
        for (auto e : elems) {
            push_back(lhs, e);
            push_back(rhs, e);
        }
        join(lhs, rhs);
        REQUIRE(list_equal(lhs, { 0, 2, 4, 0, 2, 4 }));
        REQUIRE(reports_as_empty(rhs));
    }
}


TEST_CASE("reverse") {
    list l;
    scope_guard sg([&]() { dispose(l); });
    SECTION("Empty list") {
        reverse(l);
        REQUIRE(reports_as_empty(l));
    }
    SECTION("List with single element") {
        push_back(l, 10);
        reverse(l);
        REQUIRE(back(l) == 10);
        SECTION("List with two elements") {
            push_front(l, 20);
            reverse(l);
            REQUIRE(list_equal(l, {10, 20}));
        }
    }
    SECTION("List with many elements") {
        auto elems = { 0., 1., 2., 3., 4., 5., 6., 7., 8., 9. };
        for (auto e : elems) {
            push_back(l, e);
        }
        SECTION("One reverse") {
            reverse(l);
            REQUIRE(list_equal(l, {9, 8, 7, 6, 5, 4, 3, 2, 1, 0}));
            SECTION("Two reverses cancel each other out") {
                reverse(l);
                REQUIRE(list_equal(l, elems));
            }
        }
    }
}


TEST_CASE("unique") {
    list l;
    scope_guard sg([&]() { dispose(l); });
    SECTION("Empty list") {
        unique(l);
        REQUIRE(reports_as_empty(l));
    }
    SECTION("Degenerated case 1") {
        for (int i : {1, 1, 1, 1, 1, 1, 1, 1, 1}) {
            push_back(l, i);
        }
        unique(l);
        REQUIRE(list_equal(l, { 1 }));
    }
    SECTION("Unique elements") {
        for (int i : {1, 2, 3, 4, 5}) {
            push_back(l, i);
        }
        unique(l);
        REQUIRE(list_equal(l, { 1, 2, 3, 4, 5 }));
    }
    SECTION("Sorted duplicate elements") {
        for (int i : {1, 2, 3, 5, 5, 5}) {
            push_back(l, i);
        }
        unique(l);
        REQUIRE(list_equal(l, { 1, 2, 3, 5 }));
    }
    SECTION("Unsorted duplicate elements") {
        for (int i : {1, 2, 2, 3, 3, 3, 4, 1, 2, 5, 5, 5}) {
            push_back(l, i);
        }
        unique(l);
        REQUIRE(list_equal(l, {1, 2, 3, 4, 1, 2, 5}));
    }
}
