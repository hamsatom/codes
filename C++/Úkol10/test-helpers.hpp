#ifndef PJC_HELPERS_HPP
#define PJC_HELPERS_HPP

#include <iosfwd>
#include <iterator>

struct counter {
    counter() = default;
    counter(int dc, int cc, int ca, int mc, int ma, int d, int o):default_constructors(dc),
        copy_constructors(cc), copy_assignments(ca), move_constructors(mc), move_assignments(ma),
        destructors(d), other(o) {}
    int default_constructors = 0;
    int copy_constructors = 0;
    int copy_assignments = 0;
    int move_constructors = 0;
    int move_assignments = 0;
    int destructors = 0;
    int other = 0;
};

bool operator==(const counter& c1, const counter& c2);
counter operator-(counter c1, const counter& c2);
std::ostream& operator<<(std::ostream& out, const counter& cnt);

struct tracker {
    tracker();
    tracker(double val);
    tracker(const tracker&);
    tracker& operator=(const tracker&);
    tracker(tracker&&);
    tracker& operator=(tracker&&);
    ~tracker();

    double value;
    static counter cnt;
};

bool operator==(const tracker& c1, const tracker& c2);


template <bool Copy, bool Move>
struct ttracker {
    ttracker():value(0) {
        cnt.default_constructors++;
    }

    ttracker(int val):value(val) {
        cnt.other++;
    }

    ttracker(const ttracker& rhs) noexcept(Copy):value(rhs.value) {
        cnt.copy_constructors++;
    }

    ttracker& operator=(const ttracker& rhs) noexcept(Copy) {
        value = rhs.value;
        cnt.copy_assignments++;
        return *this;
    }

    ttracker(ttracker&& rhs)noexcept(Move):value(rhs.value) {
        cnt.move_constructors++;
    }

    ttracker& operator=(ttracker&& rhs) noexcept(Move) {
        value = rhs.value;
        cnt.move_assignments++;
        return *this;
    }

    ~ttracker() {
        cnt.destructors++;
    }


    int value;
    static counter cnt;
};
template <bool Copy, bool Move> counter ttracker<Copy, Move>::cnt = {};

template <typename Iterator, bool ForceInvalidate>
struct fake_input_iterator {
    using difference_type = typename Iterator::difference_type;
    using value_type = typename Iterator::value_type;
    using reference = typename Iterator::reference;
    using pointer = typename Iterator::pointer;
    using iterator_category = std::input_iterator_tag;

    fake_input_iterator() = default;
    fake_input_iterator(Iterator it):place(it) {}

    bool operator==(const fake_input_iterator& rhs) const {
        return place == rhs.place;
    }
    bool operator!=(const fake_input_iterator& rhs) const {
        return place != rhs.place;
    }
    reference operator*() {
        return *place;
    }
    fake_input_iterator& operator++() {
        if (ForceInvalidate) {
            *place = value_type{};
        }
        ++place;
        return *this;
    }
    fake_input_iterator operator++(int) {
        if (ForceInvalidate) {
            *place = value_type{};
        }
        auto tmp(*this);
        ++(*this);
        return tmp;
    }
    pointer operator->() {
        return place;
    }
    
    Iterator place;
};

struct ordered {
    ordered() = default;
    ordered(int sortby, size_t order):sortby(sortby), order(order) {}

    int sortby;
    size_t order;
};

bool operator==(const ordered& o1, const ordered& o2);
bool operator<(const ordered& o1, const ordered& o2);

#endif