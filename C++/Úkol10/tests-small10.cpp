#include "catch.hpp"
#include "test-helpers.hpp"

#include "adjacent_pair.hpp"

#include <vector>
#include <forward_list>
#include <sstream>
#include <string>
#include <iterator>

TEST_CASE("Forward+ iterators") {
    SECTION("RA Iterators") {
        std::vector<int> vec;
        SECTION("Empty vector") {
            int res = 0;
            adjacent_pair(begin(vec), end(vec), [&](int l, int r) { res += l * r; });
            REQUIRE(res == 0);
            SECTION("Added an element") {
                vec.push_back(1);
                adjacent_pair(begin(vec), end(vec), [&](int l, int r) { res += l * r; });
                REQUIRE(res == 0);
            }
        }
        SECTION("Reasonably filled vector") {
            vec = { 1, 2, 3, 4, 5, 6 };
            int res = 0;
            adjacent_pair(begin(vec), end(vec), [&](int l, int r) { res += l * r; });
            REQUIRE(res == 70);
        }
    }
    SECTION("Forward Iterators") {
        std::forward_list<int> list;
        SECTION("Empty list") {
            int res = 0;
            adjacent_pair(begin(list), end(list), [&](int l, int r) { res += l + r; });
            REQUIRE(res == 0);
            SECTION("Added an element") {
                list.push_front(1);
                adjacent_pair(begin(list), end(list), [&](int l, int r) { res += l + r; });
                REQUIRE(res == 0);
            }
        }
        SECTION("Filled list") {
            list = { 1, 2, 3, 4, 5 };
            int res = 0;
            adjacent_pair(begin(list), end(list), [&](int l, int r) { res += l + r; });
            REQUIRE(res == 24);
        }
    }
    SECTION("Tracking types") {
        std::vector<tracker> vec;
        for (int i = 0; i < 11; ++i) {
            vec.push_back(i);
        }
        auto temp = tracker::cnt;
        double res = 0;
        adjacent_pair(begin(vec), end(vec), [&](const auto& l, const auto& r) { res += l.value + r.value; });
        REQUIRE(res == 100);
        REQUIRE(tracker::cnt == temp);
    }
}

template <typename T>
using diter = fake_input_iterator<typename std::vector<T>::iterator, true>;

TEST_CASE("Input iterators") {
    SECTION("Empty stream") {
        std::stringstream sstr;
        std::string res;
        adjacent_pair(std::istream_iterator<char>(sstr), std::istream_iterator<char>(), [&](char l, char r) {res += r; res += l; });
        REQUIRE(res == "");
    }
    SECTION("One character stream") {
        std::stringstream sstr("a");
        std::string res;
        adjacent_pair(std::istream_iterator<char>(sstr), std::istream_iterator<char>(), [&](char l, char r) {res += r; res += l; });
        REQUIRE(res == "");
    }
    SECTION("Non-empty stream") {
        std::stringstream sstr("abcdefg");
        std::string res;
        adjacent_pair(std::istream_iterator<char>(sstr), std::istream_iterator<char>(), [&](char l, char r) {res += r; res += l; });
        REQUIRE(res == "bacbdcedfegf");
    }
    SECTION("Properly destructive input iterator") {
        std::vector<int> vec{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        diter<int> first(vec.begin()), last(vec.end());
        int res = 0;
        adjacent_pair(first, last, [&](int l, int r) {res += l * r; });
        REQUIRE(res == 240);
    }
}

using ex = ttracker<false, false>;
using cnex = ttracker<true, false>;
using mnex = ttracker<false, true>;
using bnex = ttracker<false, true>;
template <typename T>
using iter = fake_input_iterator<typename std::vector<T>::iterator, false>;

TEST_CASE("Bonus") {
    SECTION("Nothing is noexcept -- moves") {
        std::vector<ex> vec; vec.reserve(12);
        for (int i = 0; i < 10; ++i) {
            vec.emplace_back(i);
        }
        iter<ex> first(vec.begin()), last(vec.end());
        auto cnt = ex::cnt;
        int res = 0;
        adjacent_pair(first, last, [&](const ex& l, const ex& r) { res += l.value * r.value; });
        REQUIRE(res == 240);
        auto diff = ex::cnt - cnt;
        REQUIRE(diff.copy_assignments == 0);
        REQUIRE(diff.copy_constructors == 0);
        REQUIRE(diff.move_constructors == 1);
        REQUIRE(diff.move_assignments == 9);
    }
    SECTION("Copies are noexcept -- copies") {
        std::vector<cnex> vec; vec.reserve(12);
        for (int i = 0; i < 10; ++i) {
            vec.emplace_back(i);
        }
        iter<cnex> first(vec.begin()), last(vec.end());
        auto cnt = cnex::cnt;
        int res = 0;
        adjacent_pair(first, last, [&](const cnex& l, const cnex& r) { res += l.value * r.value; });
        REQUIRE(res == 240);
        auto diff = cnex::cnt - cnt;
        REQUIRE(diff.copy_assignments == 9);
        REQUIRE(diff.copy_constructors == 1);
        REQUIRE(diff.move_constructors == 0);
        REQUIRE(diff.move_assignments == 0);
    }
    SECTION("Moves are noexcept -- moves") {
        std::vector<mnex> vec; vec.reserve(12);
        for (int i = 0; i < 10; ++i) {
            vec.emplace_back(i);
        }
        iter<mnex> first(vec.begin()), last(vec.end());
        auto cnt = mnex::cnt;
        int res = 0;
        adjacent_pair(first, last, [&](const mnex& l, const mnex& r) { res += l.value * r.value; });
        REQUIRE(res == 240);
        auto diff = mnex::cnt - cnt;
        REQUIRE(diff.copy_assignments == 0);
        REQUIRE(diff.copy_constructors == 0);
        REQUIRE(diff.move_constructors == 1);
        REQUIRE(diff.move_assignments == 9);
    }
    SECTION("Both are noexcept -- moves") {
        std::vector<bnex> vec; vec.reserve(12);
        for (int i = 0; i < 10; ++i) {
            vec.emplace_back(i);
        }
        iter<bnex> first(vec.begin()), last(vec.end());
        auto cnt = bnex::cnt;
        int res = 0;
        adjacent_pair(first, last, [&](const bnex& l, const bnex& r) { res += l.value * r.value; });
        REQUIRE(res == 240);
        auto diff = bnex::cnt - cnt;
        REQUIRE(diff.copy_assignments == 0);
        REQUIRE(diff.copy_constructors == 0);
        REQUIRE(diff.move_constructors == 1);
        REQUIRE(diff.move_assignments == 9);
    }
}
