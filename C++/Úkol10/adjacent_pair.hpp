#include <iterator>
#include <type_traits>

template <typename ForwIter, typename Func>
void adjacent_pair(ForwIter first, ForwIter last, Func f, std::forward_iterator_tag) {
	if (first == last) {
		return;
	}
	auto one = first;
	auto two = ++first;
	if (two == last) {
		return;
	}
	for (; two != last; ++one, ++two) {
		f(*one, *two);
	}
}

template <typename InputIter, typename Func>
void adjacent_pair(InputIter first, InputIter last, Func f, std::input_iterator_tag) {
	if (first == last) {
		return;
	}
	auto one = first;
	auto two = ++first;
	if (two == last) {
		return;
	}
	auto a = *one;
	auto b = *two;
	++two;
	for (; two != last; ++two) {
		f(a, b);
		a = b;
		b = *two;
	}
	f(a, b);
}

template <typename ForwardIter, typename Func>
void adjacent_pair(ForwardIter first, ForwardIter last, Func f) {
	adjacent_pair(first, last, f, typename std::iterator_traits<ForwardIter>::iterator_category());
}
