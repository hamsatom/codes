#ifndef PJC_EXPR_HPP
#define PJC_EXPR_HPP 

#include <iosfwd>
#include <string>
#include <memory>

class expr {
private:
    virtual void write(std::ostream& out) const = 0;
    friend std::ostream& operator<<(std::ostream&, const expr&);

public:
    double virtual evaluate() const = 0;
    virtual ~expr() = default;
};

class math_exception : public std::exception {
public:
    math_exception(const char* c):text(c) {}
    math_exception(const std::string& s):text(s) {}
    virtual const char* what() const noexcept override {
        return text.c_str();
    }
protected:
    std::string text;
};

class domain_exception : public math_exception {
public:
    using math_exception::math_exception;
};

class unknown_function_exception : public math_exception {
public:
    using math_exception::math_exception;
};

class invalid_expression_exception : public math_exception {
public:
    using math_exception::math_exception;
};

/*
* Creates expression tree from mathematical expression.
*
* Must respect the usual mathematical ordering of operators and parenthesis.
* For details see https://cw.fel.cvut.cz/wiki/courses/a7b36pjc/ukoly/ukol_6
*/
std::unique_ptr<expr> create_expression_tree(const std::string& expression);

#endif