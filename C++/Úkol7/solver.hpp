#include "expr.hpp"
#include <list>
#pragma once

enum class Associativity : char
{
	RIGHT,
	LEFT,
	NONE
};

enum Operators_Priority : char
{
	number = 0,
	plus = 1,
	minus = 1,
	multiply = 2,
	divide = 2,
	power = 3,
	/*
	Although parenthesis have one of the highest priorities in real world they are omitted while making postfix notation so their priority is lowest possible
	*/
	parenthes = 0
};

enum class Symbol_Type
{
	OPERATOR,
	NUMBER,
	LEFT_PARENTHES,
	RIGHT_PARENTHES,
	FUNCTION
};

struct Symbol
{
	std::string value;
	Operators_Priority priority = number;
	Symbol_Type type = Symbol_Type::NUMBER;
	Associativity associavity = Associativity::NONE;
};

class solver : public expr {

private:
	virtual void write(std::ostream& out) const;

public:
	std::list<Symbol> postfix;

	solver();
	virtual ~solver();

	double virtual evaluate() const;
};