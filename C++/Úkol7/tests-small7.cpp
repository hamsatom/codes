#include "catch.hpp"
#include "expr.hpp"
#include "tests-small7-data.hpp"
#include <tuple>


TEST_CASE("Basic functionality") {
    SECTION("Functions") {
        SECTION("sin") {
            auto orig = create_expression_tree("sin(21)");
            REQUIRE(orig->evaluate() == Approx(0.83666));
            REQUIRE(create_expression_tree("sin(20 + 1)")->evaluate() == Approx(orig->evaluate()));
            REQUIRE(create_expression_tree("sin(0)")->evaluate() == Approx(0));
            REQUIRE(create_expression_tree("sin(0.5 * 3.14159)")->evaluate() == Approx(1));
            REQUIRE(create_expression_tree("sin(1.5 * 3.14159)")->evaluate() == Approx(-1));
            REQUIRE(create_expression_tree("2 * sin(2)")->evaluate() == Approx(1.8185948537));
            REQUIRE(create_expression_tree("sin(3)")->evaluate() == Approx(0.1411200081));
            REQUIRE(create_expression_tree("1-sin(2)")->evaluate() == Approx(0.0907025732));
            REQUIRE(create_expression_tree("1+sin(2)+2")->evaluate() == Approx(3.9092974268));
        }
        SECTION("cos") {
            REQUIRE(create_expression_tree("cos(0)")->evaluate() == Approx(1));
            REQUIRE(create_expression_tree("cos(3.14159)")->evaluate() == Approx(-1));
            REQUIRE(create_expression_tree("cos(0.5 * 3.14159)")->evaluate() == Approx(0));
            REQUIRE(create_expression_tree("cos(1.5 * 3.14159)")->evaluate() == Approx(0));
            REQUIRE(create_expression_tree("cos(27 * 213 - 12314)")->evaluate() == Approx(-0.9774124111));
            REQUIRE(create_expression_tree("cos(0.5 * 3.14159 * 12 - 2 + 12.2)")->evaluate() == Approx(-0.714276795));
        }
        SECTION("log") {
            REQUIRE(create_expression_tree("log(1)")->evaluate() == Approx(0));
            REQUIRE(create_expression_tree("log(2.71828)")->evaluate() == Approx(1));
            REQUIRE(create_expression_tree("log(2.71828 ^ 1)")->evaluate() == Approx(1));
            REQUIRE(create_expression_tree("log(2.71828 ^ 2)")->evaluate() == Approx(2));
            REQUIRE(create_expression_tree("log(2.71828 ^ 3)")->evaluate() == Approx(3));
            REQUIRE(create_expression_tree("log(2.71828 ^ 4)")->evaluate() == Approx(4));
            REQUIRE(create_expression_tree("log(73 ^ 3)")->evaluate() == Approx(12.8713783234));
        }
    }

    SECTION("Error reporting") {
        SECTION("Unknown function") {
            REQUIRE_THROWS_AS(create_expression_tree("coz(0)")->evaluate(), unknown_function_exception);
            REQUIRE_THROWS_AS(create_expression_tree("f11(0)")->evaluate(), unknown_function_exception);
            REQUIRE_THROWS_AS(create_expression_tree("tan(2)")->evaluate(), unknown_function_exception);
            REQUIRE_THROWS_AS(create_expression_tree("a35(1)")->evaluate(), unknown_function_exception);
        }
        SECTION("Weird symbols") {
            REQUIRE_THROWS_AS(create_expression_tree("1 % sin(0)")->evaluate(), invalid_expression_exception);
            REQUIRE_THROWS_AS(create_expression_tree("1 $ sin(0)")->evaluate(), invalid_expression_exception);
            REQUIRE_THROWS_AS(create_expression_tree("1 # sin(0)")->evaluate(), invalid_expression_exception);
            REQUIRE_THROWS_AS(create_expression_tree("sins")->evaluate(), invalid_expression_exception);
        }

        SECTION("Unmatched braces") {
            REQUIRE_THROWS_AS(create_expression_tree("(")->evaluate(), invalid_expression_exception);
            REQUIRE_THROWS_AS(create_expression_tree("(()))")->evaluate(), invalid_expression_exception);
            REQUIRE_THROWS_AS(create_expression_tree("()()()(()")->evaluate(), invalid_expression_exception);
            REQUIRE_THROWS_AS(create_expression_tree(")")->evaluate(), invalid_expression_exception);
        }

        SECTION("Misc") {
            REQUIRE_THROWS_AS(create_expression_tree("log(0)")->evaluate(), domain_exception);
            REQUIRE_THROWS_AS(create_expression_tree("log(0-1)")->evaluate(), domain_exception);
            REQUIRE_THROWS_AS(create_expression_tree("+")->evaluate(), invalid_expression_exception);
            REQUIRE_THROWS_AS(create_expression_tree("+-+-")->evaluate(), invalid_expression_exception);

            REQUIRE_THROWS_AS(create_expression_tree("a35")->evaluate(), invalid_expression_exception);
            REQUIRE_THROWS_AS(create_expression_tree("35a")->evaluate(), invalid_expression_exception);
            REQUIRE_THROWS_AS(create_expression_tree("sin(1, 2)")->evaluate(), invalid_expression_exception);

            REQUIRE_THROWS_AS(create_expression_tree("")->evaluate(), invalid_expression_exception);
            REQUIRE_THROWS_AS(create_expression_tree(" \t ")->evaluate(), invalid_expression_exception);

            REQUIRE_THROWS_AS(create_expression_tree("3 33")->evaluate(), invalid_expression_exception);
            REQUIRE_THROWS_AS(create_expression_tree("3 + 3 3 +")->evaluate(), invalid_expression_exception);
            REQUIRE_THROWS_AS(create_expression_tree("3 33 +")->evaluate(), invalid_expression_exception);
            REQUIRE_THROWS_AS(create_expression_tree("2sin(2)")->evaluate(), invalid_expression_exception);
            REQUIRE_THROWS_AS(create_expression_tree("*sin(2)")->evaluate(), invalid_expression_exception);
            REQUIRE_THROWS_AS(create_expression_tree("2 * 2 ** sin(2)")->evaluate(), invalid_expression_exception);
            REQUIRE_THROWS_AS(create_expression_tree("3 / sin(2) ++ 1")->evaluate(), invalid_expression_exception);
            REQUIRE_THROWS_AS(create_expression_tree("sin(2)+")->evaluate(), invalid_expression_exception);
        }
    }
}


TEST_CASE("Valid inputs") {
    std::string input;
    double expected;
    for (const auto& c : valid_inputs) {
        std::tie(input, expected) = c;
        INFO("For input: " << input);
        auto e = create_expression_tree(input);
        INFO("Got expression: " << *e);
        REQUIRE(e->evaluate() == Approx(expected));
    }
}


TEST_CASE("Bad syntax") {
    for (const auto& input : bad_syntax) {
        INFO("For input: " << input);
        REQUIRE_THROWS_AS(create_expression_tree(input)->evaluate(), invalid_expression_exception);
    }
}


TEST_CASE("Bad func") {
    for (const auto& input : bad_func) {
        INFO("For input: " << input);
        REQUIRE_THROWS_AS(create_expression_tree(input)->evaluate(), unknown_function_exception);
    }
}


TEST_CASE("Bad domain") {
    for (const auto& input : bad_domain) {
        INFO("For input: " << input);
        REQUIRE_THROWS_AS(create_expression_tree(input)->evaluate(), domain_exception);
    }
}
