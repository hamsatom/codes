#!/usr/bin/env bash
#Deription:	Finds all files in a given directory (and subdirectories) having the suffix provided as an argument and performs one of the following actions: Copies them and changes their suffix, or renames them to use another suffix, or deletes them.                                                                                                               Script has 4 mandatory parameters (3 in case of file deletion): suffix_handle.sh [ -c | -m | -r ] ext1 ext2, where extensions ext1 and ext2 are strings that might containing a dot.                                                            Parameter -c stands for copy operation, -m for rename (move) and -r for file deletion.                                  In case of copy/rename script outputs a line for each file in the following format: old_filename  new_filename, in case of file deletion, the script lists the files being deleted.                                                             When the provided directory does not exist or in case of insufficient permissions for copying (read perm.) or moving (read and write) outputs an error message.
#Author: Tomas Hamsa
#Email: hamsatom@fel.cvut.cz
#Term: B161

#Task version
VERSION=3

#Operation masks
copy_code="-c"
rename_code="-m"
remove_code="-r"

#Error codes
ERR_WITHOUT_PERMISSION=1
ERR_INVALID_LOCATION=2
ERR_INVALID_ARGS=3

#Error messages
USAGE_MSG="Usage: $0 [ directory path ] [ -c | -m | -r ] [ ext1 ] [ ext2 ] 
-c stands for copy operation, -m for rename (move) -r for file deletion
[OPTIONS] -h Help -v Version" 
HELP_MSG="Finds all files in a given directory (and subdirectories) having the suffix provided as an argument and performs one of the actions"
INVALID_ARGS_MSG="Ivalid arguments given"
INVALID_PERMISSION_DIR_MSG="Cannot write or read in directory because of missing permissions"
INVALID_PERMISSION_FILE_MSG="Could operate with file because needed permissions are missing"
INVALID_PATH_MSG="Path to directory is not correct"

#Argument variables
dirPath=""
operation=""
originalName=""
newName=""

#Prints description and usage help. After that end program with code 0
function help {
	echo "${HELP_MSG}"
        echo "${USAGE_MSG}"
        exit
}

#Prints error output if ivalid arguments were given
function invalid_argument_print {
	echo "${INVALID_ARGS_MSG}" >&2
	echo "${USAGE_MSG}" >&2
	exit "${ERR_INVALID_ARGS}"
}

#Prints error output and exits with specific exit code if user does not have read or write permission in directory
function handle_permission_dir { 
	echo "${INVALID_PERMISSION_DIR_MSG}" >&2
	echo "${dirPath}" >&2
	exit "${ERR_WITHOUT_PERMISSION}"
}

#Prints error message if files does not have needed permission
function print_file_permission_error {
	echo "${INVALID_PERMISSION_FILE_MSG}" >&2
}

#Prints error message and exits with specific error code if path is not directory
function handle_invalid_path {
	echo "${INVALID_PATH_MSG}" >&2
	exit "${ERR_INVALID_LOCATION}"
}

#Check if any argument was given
if [ $# -eq 0 ] ; then
        invalid_argument_print
fi

#Process given arguments
while [ -n "$1" ]  ; do
        case "$1" in
                "-h"|"--help" )
                        help
                        ;;
                "-v"|"--version" )
                        echo "${VERSION}"
                        exit
                        ;;
	       *)
		dirPath="$1"
		shift
		while [[ "$1" != -* && $# -gt 0 ]] ; do
			dirPath+=" $1"
			shift
		done
		if [ $# -gt 0 ] ; then
			operation="$1"
			shift
			if [ $# -gt 0 ]; then
				originalName="$1"
				shift
				if [ $# -gt 0 ] ; then
					newName="$1"
					shift
				fi
			fi
		fi
	esac
        shift
done

#Verify that all needed arguments were given
if [[ "$operation" == "" || "$originalName" == "" || "${dirPath}" == "" ]] ; then
	invalid_argument_print
fi
if [[ "$operation" != "${remove_code}" && "$newName" == "" ]] ; then
	invalid_argument_print
fi

#Check if path is a directory
if [ ! -d "${dirPath}" ]; then
	handle_invalid_path
fi

#Check if needed permissions are present
if [[ ! -r "${dirPath}" && "${operation}" != "${remove_code}" ]]; then
	handle_permission_dir
fi
if [[ ! -w "${dirPath}" && "${operation}" == "${rename_code}" ]]; then
	handle_permission_dir
fi

#Enable recursive wildcard search on **
shopt -s globstar

#Execute operation specified in received argument
case "${operation}" in
	#Remove
	"${remove_code}")
		for file in "${dirPath}"/**/*"${originalName}";do
                        if [ ! -f "${file}" ]; then
                                continue
                        fi
                        echo "${file}"
                        rm -f  "${file}"
                done
		;;
	#Copy
	"${copy_code}")
		for file in "${dirPath}"/**/*"${originalName}";do
			if [ ! -f "${file}" ]; then
				continue
			fi
			if [ ! -r "${file}" ]; then
				print_file_permission_error
				echo "${file}" >&2
				continue
			fi
			echo "${file}  => ${file/%${originalName}/${newName}}"
			cp  "${file}" "${file/%${originalName}/${newName}}"
		done
		;;
	#Rename
	"${rename_code}")
		for file in "${dirPath}"/**/*"${originalName}";do
			if [ ! -f "${file}" ]; then
				continue
			fi			
			echo "${file} => ${file/%${originalName}/${newName}}"
			mv  "${file}" "${file/%${originalName}/${newName}}"
		done
		;;
	#This case happens when not supported operation mask was given
	*)
		invalid_argument_print		
esac
