#!/usr/bin/env bash
find . -name ".git" -type d -exec git --git-dir={} --work-tree="$PWD"/{} pull --all -p \; -exec git --git-dir={} --work-tree="$PWD"/{} prune \;
