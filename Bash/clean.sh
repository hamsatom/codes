#!/usr/bin/env bash
find -name target -type d -exec rm -r "{}" +
find -name out -type d -exec rm -r "{}" +
find -name build -type d -exec rm -r "{}" +

find -name *.toc -type f -exec rm -r "{}" +
find -name *.aux -type f -exec rm -r "{}" +
find -name *.gz -type f -exec rm -r "{}" +
find -name *.bak -type f -exec rm -r "{}" +
find -name *.o -type f -exec rm -r "{}" +
find -name *.log -type f -exec rm -r "{}" +
find -name *.lof -type f -exec rm -r "{}" +
find -name *.lot -type f -exec rm -r "{}" +
find -name *.rdg -type f -exec rm -r "{}" +
find -name dependency-reduced-pom.xml -type f -exec rm -r "{}" +
find -name *.dvi -type f -exec rm -r "{}" +
find -name *.class -type f -exec rm -r "{}" +

shopt -s globstar
for f in **/Makefile; do make -C "${f%/*}" clean -n; done

find . -type d -empty -delete
