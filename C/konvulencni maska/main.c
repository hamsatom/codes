#include<stdio.h>
#include<stdlib.h>

/*
Code for operations with PPM file - saving and reading to array was copied from
http://stackoverflow.com/questions/2693631/read-ppm-file-and-store-it-in-an-array-coded-with-c
*/
typedef struct {
    unsigned char red, green, blue;
} Pixel;

typedef struct {
    Pixel *data;
} Image;

int main(const int argc, const char** argv){
    FILE *input = fopen(argv[1], "rb");
    char buff[4];
    if (!fgets(buff, sizeof(buff), input)) {
        perror("File");
        exit(1);
    }
    unsigned short int x, y;
    //read image size information
    if (fscanf(input, "%hu %hu", &x, &y) != 2) {
        fprintf(stderr, "Invalid image size (error loading)\n");
        exit(1);
    }
    //read rgb component
    unsigned char rgb_comp_color;
    if (fscanf(input, "%cu", &rgb_comp_color) != 1) {
        fprintf(stderr, "Invalid rgb component (error loading)\n");
        exit(1);
    }
    for(;fgetc(input) != '\n';);
    //memory allocation for pixel data
    Image img;
    img.data = malloc(x * y * sizeof(Pixel));
    //read pixel data from file
    if (fread(img.data, 3 * x, y, input) != y) {
        fprintf(stderr, "Error loading image\n");
        exit(1);
    }
    fclose(input);

   Image conv_img;
   conv_img.data = malloc(x * y  * sizeof(Pixel));
   const unsigned int s1 = (y-1)*x, s2 = x*y;
   unsigned int heatmap[6] = {0};
   unsigned int i = 0;
   unsigned char value;
   for(; i < x; ++i){
          conv_img.data[i] = img.data[i];
          value = 0.2126 * img.data[i].red + 0.7152 * img.data[i].green + 0.0722 * img.data[i].blue + 0.5;
           ++heatmap[value/51];
       }
       // Body
       for(unsigned int u = 2; i < s1; ++i, ++u){
           // Copy first pixel
           conv_img.data[i] = img.data[i];
           value = 0.2126 * img.data[i].red + 0.7152 * img.data[i].green + 0.0722 * img.data[i].blue + 0.5;
           ++heatmap[value/51];
   		   ++i;
           // Copy body
           for(; i < x*u-1; ++i){
                const unsigned int i3 = i - x;
                const unsigned int i4 = i + x;
                const unsigned int i1 = i-1;
                const unsigned int i2 = i+1;
                short r = 5*img.data[i].red - img.data[i1].red  - img.data[i2].red - img.data[i3].red  - img.data[i4].red;
                r = r > 0 ? (r < 256 ? r : 255) : 0;
                conv_img.data[i].red = r;

                short g = 5*img.data[i].green  - img.data[i1].green  - img.data[i2].green - img.data[i3].green - img.data[i4].green;
                g = g > 0 ? (g < 256 ? g : 255) : 0;
                conv_img.data[i].green = g;

                short b = 5*img.data[i].blue  - img.data[i1].blue  - img.data[i2].blue - img.data[i3].blue - img.data[i4].blue;
                b = b > 0 ? (b < 256 ? b : 255) : 0;
                conv_img.data[i].blue = b;

                value = 0.2126 * r + 0.7152 * g + 0.0722 * b + 0.5;
                ++heatmap[value/51];
           }
           // Copy last pixel
            conv_img.data[i] = img.data[i];
            value = 0.2126 * img.data[i].red + 0.7152 * img.data[i].green + 0.0722 * img.data[i].blue + 0.5;
            ++heatmap[value/51];
       }
       // Last row
       for(; i < s2; ++i){
            conv_img.data[i] = img.data[i];
            value = 0.2126 * img.data[i].red + 0.7152 * img.data[i].green + 0.0722 * img.data[i].blue + 0.5;
            ++heatmap[value/51];
       }

	free(img.data);

    FILE *img_out = fopen("output.ppm", "wb");
    //image header
    fprintf(img_out, "P6\n%d\n%d\n%d\n", x, y, 255);
    // pixel data
    fwrite(conv_img.data, 3 * x, y, img_out);
    free(conv_img.data);
    fclose(img_out);

    FILE *txt_out = fopen("output.txt", "wb");
    fprintf(txt_out, "%d %d %d %d %d ", heatmap[0], heatmap[1], heatmap[2], heatmap[3], heatmap[4] + heatmap[5]);
    fclose(txt_out);

	return (0);
}