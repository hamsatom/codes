#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*
Program jsem napsal s�m s radami ze Stackoverflow, kde jsem pri�el na funkce memcpy, fscanf, fopen a perror
http://en.cppreference.com/w/c/string/byte/memcpy
http://stackoverflow.com/questions/16645583/how-to-copy-char-array-to-another-char-array-in-c
http://stackoverflow.com/questions/1746510/unable-to-open-a-file-with-fopen
http://stackoverflow.com/questions/3647331/how-to-swap-two-numbers-without-using-temp-variables-or-arithmetic-operations
*/


//nazev souboru s uplnou cestou k nemu, ve windows nutno oddelovat pomoci "\\"
#define CESTA_K_TXT "D:\\Tomas\\ownCloud\\C;C++\\tomasSorty\\input.txt"

/*
Vytiskne pole predane v argumentu
*/
void print_array(int *array, int length)
{
    for(int i = 0; i < length; ++i)
    {
        printf("%d, ", array[i]);
    }
    puts("\n");
}

/*
seradi kopii pole v argumentu insert sortem od nejmansiho po nejvetsi
*/
void insert_sort(int *input, int length)
{
    int array[length];

    //zkop�rov�n� vstupn�ho pole do docasneho
    memcpy(array, input, length*sizeof(int));

    //tisk pole na zacatku
    puts("Insert sort - vstup:");
    print_array(array, length);

    //zarazovany prvek
    int temp;
    //index na ktery zarazuji
    int j;
    //index na kterem porovnavam, tedy predtim nez na ktery zaazuji
    int j_minus;
    for(int i = 0; i < length-1; ++i)
    {
        //nastaveni zarazovacich indexu
        j = i+1;
        j_minus = i;
        //nastaveni prvku ktery zarazuji
        temp = array[j];
        //prehazuji prvky v poli dokud nejsem na zacatku a soucasny prvek neni vetsi
        while(j>0 && temp < array[j_minus])
        {
            //zarazeni do pole
            array[j] = array[j_minus];
            //zmenseni indexu
            --j;
            --j_minus;
        }
        //vlozeni zarazovaneho prvku
        array[j] = temp;
    }

    //tisk pole po insert sortu
    puts("Insert sort - vysledek:");
    print_array(array, length);
}


/*
seradi kopii pole v argumentu selection sortem od nejmansiho po nejvetsi
*/
void select_sort(int *input, int length)
{
    int array[length];

    //zkop�rov�n� vstupn�ho pole do docasneho
    memcpy(array, input, length*sizeof(int));

    //tisk pole na zacatku
    puts("Selection sort - vstup:");
    print_array(array, length);

    //prochazeci index
    int i;
    //index minima
    int i_min;
    for(int j = 0; j < length-1; ++j)
    {
        //docasne obsazeni indexu minima
        i_min = j;

        //projeti polek k nalezeni nejmensiho
        for(i = j + 1; i < length; ++i)
        {
            //vymena soucasneho indexu minima s indexem mensiho prvku
            if (array[i] < array[i_min])
            {
                i_min = i;
            }
        }
        //vymena obsahu na soucasnem indexu s nalezenym minimem
        //prohozenim bez pouziti pomocne promene
        if (i_min != j)
        {
            array[j] = array[j] + array[i_min];
            array[i_min] = array[j] - array[i_min];
            array[j] = array[j] - array[i_min];
        }
    }

    //tisk vysledku
    puts("Selection sort - vysledek:");
    print_array(array, length);
}

/*
seradi kopii pole vzestupne pomoci bubble sortu
*/
void bubble_sort(int *input, int length)
{
    int array[length];

    //zkop�rov�n� vstupn�ho pole do docasneho
    memcpy(array, input, length*sizeof(int));

    //tisk pole na zacatku
    puts("Bubble sort - vstup:");
    print_array(array, length);

    --length;

    //index pro neserazenou cast pole
    int j;
    //index prohazovani
    int j_plus;
    for(int i = 0; i < length; ++i)
    {
        //pruchod neserazenou casti pole
        for(j = 0; j < length - i; ++j)
        {
            //nastaveni prehazovaciho indexu
            j_plus = j + 1;

            //prohozeni, bez pomocne promene, kdyz neni serazeno
            if(array[j_plus] < array[j])
            {
                array[j] = array[j] + array[j_plus];
                array[j_plus] = array[j] - array[j_plus];
                array[j] = array[j] - array[j_plus];
            }
        }
    }

    //vytisknuti vysledku
    puts("Bubble sort - vysledek:");
    print_array(array, ++length);
}

/*
main musi byt deklarovan jako posledni funkce, protoze kompilace probiha shora
kdyby byl main nahore, volal by funkce co jeste nejsou deklarovane
*/
int main()
{
    //Otevre txt soubor pro rezim cteni
    FILE *file = fopen(CESTA_K_TXT, "r");

    int pocet_znaku;

    //nacte prvni znak ze souboru a ulozi ho do pocet_znaku
    //neocekaveme, ze v souboru nic neni nebo to neni cele cislo, takze zadna kontrola
    fscanf (file, "%d", &pocet_znaku);

    //deklaruje pole staticke delky, protoze pocet cisel by mel byt znam
    //neocekavema cisla vetsi jak 2,1 milardy nebo mensi jak -2,1 miliardy, takze pouzijeme int jinak bych pouzil treba long long
    int vstupni_pole[pocet_znaku];

    //nacte postupne vsechny cisla do pole
    //omezeno poctem cisel - pocet_znaku
    for(int i = 0; i<pocet_znaku; ++i)
    {
        //nacte konkretni znak na i-ty index v vstupni_pole
        //nekontroluji spravnost vstupu
        fscanf (file, "%d", &vstupni_pole[i]);
    }

    //zavre txt soubor
    fclose(file);

    //pro kontrolu vytiskne vstup
    puts("Vstup:");
    print_array(vstupni_pole, pocet_znaku);

    //seradi vstupni pole pomoci insert sortu
    insert_sort(vstupni_pole, pocet_znaku);

    //razeni selection sortem
    select_sort(vstupni_pole, pocet_znaku);

    //razeni bubble sortem
    bubble_sort(vstupni_pole, pocet_znaku);

    //tisk vstupniho pole
    //melo by zustat stejne, protoze jsem radil kopie
    puts("Vstup po vsech sortech:");
    print_array(vstupni_pole, pocet_znaku);

    //Pokud se vyskytne nejaka chyba jako file not found tak perror vytiskne pricinu
    perror("");
    return 0;
}
