#include <stdio.h>
#include <stdlib.h>
#include "nd.h"
#include "nsd.h"
#define PRIME_MSG "prime\r"
#define FINISH_MSG "DONE\r\n"
#define INVALID_INPUT_MSG "Invalid input\r\n"

const char prime_result = 1;

int main(int argc, char** argv)
{
	int first, second, result = 0;
	while ((result = scanf("%i %i", &first, &second)) != EOF) {
		if (result != 2) {
			fprintf(stderr, INVALID_INPUT_MSG);
			break;
		}

		fprintf(stderr, "%s input: %i, %i\r\n", argv[0], first, second);

		if (first != 1 && second != 1 && nd(first) == prime_result && nd(second) == prime_result) {
			puts(PRIME_MSG);
		}
		else {
			printf("%i\r\n", nsd(first, second));
		}
	}
	fprintf(stderr, FINISH_MSG);
	return EXIT_SUCCESS;
}