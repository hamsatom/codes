#define _POSIX_SOURCE
#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <unistd.h>
#include <fcntl.h>
#include <signal.h>
#include <sys/stat.h>
#include <time.h>

#define PIPE_OPEN_ERROR_CODE  1
#define FORK_ERROR_CODE 2
#define OUTPUT_FILE_ERROR_CODE 3
#define PROCCESS_ERROR_CODE 4
#define KILL_ERROR_CODE 5
#define DUP_ERROR_CODE 6
#define CLOSE_ERROR_CODE 7
#define PRINTF_ERROR_CODE 8
#define FLUSH_ERROR_CODE 9
#define SIGNAL_ERROR_CODE 10
#define EXEC_ERROR_CODE 11
#define WAIT_ERROR_CODE 12

#define END_MSG "TERMINATED\n"
#define PIPE_OPEN_ERROR_MSG "Error while opening pipe"
#define FORK_ERROR_MSG "Error while calling fork"
#define OUTPUT_FILE_ERROR_MSG "Error while creating output file "
#define PROCCESS_ERROR_MSG "Error while creating proccess"
#define KILL_ERROR_MSG "Error while stopping the proccess"
#define DUP_ERROR_MSG "Error while redirecting data"
#define CLOSE_ERROR_MSG  "Error while closing"
#define PRINTF_ERROR_MSG "Error while writing to pipe in printf"
#define FLUSH_ERROR_MSG "Error while flushingto stdout"
#define SIGNAL_ERROR_MSG "Error while getting signal from signal()"
#define EXEC_ERROR_MSG "Error while executing C1 in execl"
#define WAIT_ERROR_MSG "Error while calling wait()"

#define OUTPUT_FILE "out.txt"
#define C1EXEC_LOCATION "c1folder/findGCD"

const int READ_SIDE = 1;
const int WRITE_SIDE = 0;

void signal_handler(int signal_number)
{
	fprintf(stderr, END_MSG);
	exit(EXIT_SUCCESS);
}

void handle_failure(const int method_result, const char *msg, const char code)
{
	if (method_result < EXIT_SUCCESS)
	{
		perror(msg);
		exit(code);
	}
}

int main()
{
	int pipe_line[2];
	int last_result = pipe(pipe_line);
	handle_failure(last_result, PIPE_OPEN_ERROR_MSG, PIPE_OPEN_ERROR_CODE);

	pid_t child1_pid, child2_pid;
	child1_pid = fork();
	handle_failure(child1_pid, FORK_ERROR_MSG, FORK_ERROR_CODE);
	
	if (child1_pid == EXIT_SUCCESS)
	{
		if (signal(SIGUSR1, signal_handler) == SIG_ERR)
		{
			handle_failure(-1, SIGNAL_ERROR_MSG, SIGNAL_ERROR_CODE);
		}

		last_result = close(pipe_line[WRITE_SIDE]);
		handle_failure(last_result, CLOSE_ERROR_MSG, CLOSE_ERROR_CODE);

		last_result = dup2(pipe_line[READ_SIDE], STDOUT_FILENO);
		handle_failure(last_result, DUP_ERROR_MSG, DUP_ERROR_CODE);		

		srand(time(NULL));
		while (1)
		{
			sleep(1);
			last_result = printf("%d %d\n", rand() % 1000, rand() % 1000);
			handle_failure(last_result, PRINTF_ERROR_MSG, PRINTF_ERROR_CODE);			
			if (fflush(stdout) == EOF)
			{
				handle_failure(-1, FLUSH_ERROR_MSG, FLUSH_ERROR_CODE);
			}
		}

		last_result = close(pipe_line[READ_SIDE]);
		handle_failure(last_result, CLOSE_ERROR_MSG, CLOSE_ERROR_CODE);
	}
	else
	{
		child2_pid = fork();
		handle_failure(child2_pid, FORK_ERROR_MSG, FORK_ERROR_CODE);
		
		if (child2_pid == EXIT_SUCCESS)
		{		
			last_result = close(pipe_line[READ_SIDE]);
			handle_failure(last_result, CLOSE_ERROR_MSG, CLOSE_ERROR_CODE);

			last_result = dup2(pipe_line[WRITE_SIDE], STDIN_FILENO);
			handle_failure(last_result, DUP_ERROR_MSG, DUP_ERROR_CODE);	

			int file = open(OUTPUT_FILE, O_CREAT | O_WRONLY, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);			
			handle_failure(file, OUTPUT_FILE_ERROR_MSG, OUTPUT_FILE_ERROR_CODE);

			last_result = dup2(file, STDOUT_FILENO);
			handle_failure(last_result, DUP_ERROR_MSG, DUP_ERROR_CODE);	

			last_result = execl(C1EXEC_LOCATION, C1EXEC_LOCATION, NULL);
			handle_failure(last_result, EXEC_ERROR_MSG, EXEC_ERROR_CODE);	

			last_result = close(pipe_line[WRITE_SIDE]);
			handle_failure(last_result, CLOSE_ERROR_MSG, CLOSE_ERROR_CODE);		

			last_result = close(file);
			handle_failure(last_result, CLOSE_ERROR_MSG, CLOSE_ERROR_CODE);
		}
		else
		{
			last_result = close(pipe_line[READ_SIDE]);
			handle_failure(last_result, CLOSE_ERROR_MSG, CLOSE_ERROR_CODE);

			last_result = close(pipe_line[WRITE_SIDE]);
			handle_failure(last_result, CLOSE_ERROR_MSG, CLOSE_ERROR_CODE);	

			sleep(5);
			last_result = kill(child1_pid, SIGUSR1);
			handle_failure(last_result, KILL_ERROR_MSG, KILL_ERROR_CODE);

			last_result = wait(NULL);
			handle_failure(last_result, WAIT_ERROR_MSG, WAIT_ERROR_CODE);

			last_result = wait(NULL);
			handle_failure(last_result, WAIT_ERROR_MSG, WAIT_ERROR_CODE);
			exit(EXIT_SUCCESS);
		}
	}
	return EXIT_SUCCESS;
}