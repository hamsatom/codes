#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define OUTPUT0 "Delka retezce: "
#define OUTPUT1 "Vstupni retezec: "
#define OUTPUT2 "Serazeno: "
#define OUTPUT3 "Pocet permutaci: "
#define ullong unsigned long long
#define ushort unsigned short

/*
Nacte retezec vytiskne abecdnene vsechny permutace vcetne opakujicich se pismen a
*/

ushort MAX_LENGTH = 8;
ushort my_index;
ushort same = 1;
char *string;

void print_empty()
{
    printf(OUTPUT0"%d\n", 0);
    puts(OUTPUT1);
    puts(OUTPUT2);
    printf(OUTPUT3"%d\n", 0);
}

int compare(const void * a, const void * b)
{
    return (*(char*)a - *(char*)b);
}

ullong count_factorial(int limit)
{
    unsigned short multiplayer = 2;
    ullong result = 1;
    for(; multiplayer <= limit; ++multiplayer)
    {
        result *= multiplayer;
    }
    return result;
}
void findSame()
{
    ushort i;
    ushort sequence_legth = 1;
    ullong multiplier = 0;
    for(i =0; i < my_index; ++i)
    {
        if(i+1 < my_index && string[i] == string[i+1])
        {
            ++sequence_legth;
        }
        else
        {
            if(sequence_legth > 1)
            {
                multiplier += count_factorial(sequence_legth);
            }
            sequence_legth = 1;
        }
    }
    if(multiplier > same) {
    same = multiplier;
    }
}

void permutate()
{
    ushort largerPermFound = 1;
    char tmp;

    int i;
    ushort j,k, l;
    do
    {
        for(l=0; l<same; ++l)
        {
            printf("\"%s\"\n", string);
        }
        for(i = my_index - 2; i >= 0 && string[i] >= string[i+1]; --i) {}
        if(i > -1)
        {
            j = i+1;
            for(k=j; k<my_index && string[k]; ++k)
            {
                if(string[k] > string[i] && string[k] < string[j])
                { j = k; }
            }
            tmp = string[i];
            string[i] = string[j];
            string[j] = tmp;
            qsort(string+i+1, my_index-i-1, sizeof(char), compare);
        }
        else
        {
            largerPermFound = 0;
        }
    }
    while(largerPermFound);
}

int main()
{
    char current_letter;
    scanf("%c", &current_letter);
    if(current_letter != EOF && current_letter != '\n')
    {
        string = calloc(MAX_LENGTH+1, sizeof(char));
        string[0] = current_letter;
    }
    else
    {
        print_empty();
        return 0;
    }

    for(my_index = 1; my_index < MAX_LENGTH; ++my_index)
    {
        scanf("%c", &current_letter);
        if(current_letter == EOF || current_letter == '\n')
        {
            break;
        }
        string[my_index] = current_letter;
    }

    printf(OUTPUT0"%d\n", my_index);
    printf(OUTPUT1"\"%s\"\n", string);
    qsort(string, my_index, sizeof(char), compare);
    printf(OUTPUT2"\"%s\"\n", string);
    printf(OUTPUT3"%llu\n", count_factorial(my_index));
    findSame();
    permutate();

    free(string);
#undef MAX_LENGTH
#undef OUTPUT0
#undef OUTPUT1
#undef OUTPUT2
#undef OUTPUT3
    return 0;
}
