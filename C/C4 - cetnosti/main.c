#include <stdio.h>
#include <stdlib.h>
#define NO_WORDS_OUTPUT "Nebyla zadana zadna slova."
#define SLOVA "Slova:"
#define CETNOSTI "Cetnosti pismen:"

char* result_string;
int result_length = 0;

void add_to_result(char c)
{
    result_string = realloc(result_string, (result_length+1)*sizeof(char));
    result_string[result_length++] = c;
}

void sort_result()
{
    int i, j;
    char tmp;
    for(i = 0; i < result_length - 1; ++i)
    {
        j = i + 1;
        tmp = result_string[j];
        while(j > 0 && tmp < result_string[j-1])
        {
            result_string[j] = result_string[j-1];
            j--;
        }
        result_string[j] = tmp;
    }
}

void count_frequency()
{
    char current_char;
    int start = 0, end = 0;
    float frequency;
    puts("");
    puts(CETNOSTI);
    while (start < result_length)
    {
        current_char = result_string[start];
        while (++end < result_length)
        {
            if (result_string[end] != current_char)
            {
                break;
            }
        }
        printf("%c:", current_char);
        printf("%5.dx ", end - start);
        frequency = (float) ((float) (end - start) / (float) result_length)*100 ;
        printf("(%5.1f%%) ", frequency);
        while(frequency> 0)
        {
            frequency -= 10.0f;
            printf("*");
        }
        if (start < result_length-1)
        {
            puts("");
        }
        else
        {
            puts("");
            break;
        }

        start = end;
    }

}

void printWords(char* string, int length)
{
    puts(SLOVA);
    int i, end = length;
    while(--end >= 0)
    {
        if (string[end] != ' ' && string[end] != '\n' && string[end] != '\0')
        {
            break;
        }
    }
    int start = end;
    while(end >= 0 && start >= 0)
    {
        while(--start >= 0)
        {
            if (string[start] == ' ' || string[start] == '\n' || string[start] == '\0')
            {
                break;
            }
        }
        printf("\"");
        for(i = start+1; i <= end; ++i)
        {
            printf("%c", string[i]);
            add_to_result(string[i]);
        }
        printf("\"");
        if (start > 0)
        {
            puts("");
        }
        else
        {
            break;
        }
        while(--start >= 0)
        {
            if (string[start] != ' ' && string[start] != '\n' && string[start] != '\0')
            {
                break;
            }
        }
        end = start;
    }
}

int main()
{
    int letters_count = 0, i, j, broke;
    char end_check[] = {'k', 'o', 'n', 'e', 'c', '\0'};
    char *long_string = malloc(sizeof(char));
    while(1)
    {
        long_string = realloc(long_string, (letters_count+1)*sizeof(char));
        long_string[letters_count] = getchar();
        if (letters_count > 3 && long_string[letters_count] == 'c')
        {
            j = 0;
            broke = 0;
            for(i = letters_count - 4; i <= letters_count; ++i)
            {
                if (long_string[i] != end_check[j])
                {
                    broke = 1;
                    break;
                }
                ++j;
            }
            if (!broke)
            {
                if (letters_count < 5)
                {
                    puts(NO_WORDS_OUTPUT);
                    break;
                }
                if (long_string[letters_count - 5] == ' ' || long_string[letters_count - 5] == '\n')
                {
                    for(i = letters_count - 4; i <= letters_count; ++i)
                    {
                        long_string[i] = '\0';
                    }
                    result_string = malloc(sizeof(char));
                    printWords(long_string, letters_count-5);
                    sort_result();
                    count_frequency();
                    break;
                }
            }
        }
        ++letters_count;
    }
    free(result_string);
    free(long_string);
    return 0;
}
