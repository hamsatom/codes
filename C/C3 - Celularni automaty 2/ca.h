#ifndef CA_H_INCLUDED
#define CA_H_INCLUDED

typedef struct {
    int colors;
    long long rule;
} ca_cfg_t;

/*
Inicializace knihovny "ca" na z�klad� po�tu barev a ��sla automatu
Vrac� ukazatel na inicializovanou datovou struktury ca_cfg_t
*/
ca_cfg_t* ca_init(int colors, long long rule);

/*
Uvoln�n� datov� struktury ca_cfg_t z pam�ti
*/
void ca_done(ca_cfg_t* cfg);

/*
Simulace jednoho �asov�ho kroku automatu - v p��pad� 1D automatu tedy vyhodnocen� jednoho "��dku".
Parametry: konfigurace, stav v �ase t, d�lka ��dku
N�vratov� hodnota: stav automatu v �ase t+1
*/
int* ca_step(ca_cfg_t *cfg, int* input, int len);

/*
Vyhodnocen� pr�v� jedn� bu�ky 2 barevn�ho automatu s okol�m 1
Parametry: konfigurace, lev� bu�ka, aktu�ln� bu�ka v �ase t, prav� bu�ka
N�vratov� hodnota: aktu�ln� bu�ka v �ase t+1
*/
int  ca_eval_2c(ca_cfg_t *cfg, int left, int center, int right);

/*
Vyhodnocen� pr�v� jedn� bu�ky 3 barevn�ho automatu s okol�m 1
Parametry: konfigurace, lev� bu�ka, aktu�ln� bu�ka v �ase t, prav� bu�ka
N�vratov� hodnota: aktu�ln� bu�ka v �ase t+1
*/
int  ca_eval_3c(ca_cfg_t *cfg, int left, int center, int right);


#endif // CA_H_INCLUDED
