#include"ca.h"
#include <stdio.h>
#include <stdlib.h>

int *in;
int length;
ca_cfg_t *obj;

int get_result(ca_cfg_t *cfg, int left, int center, int right, int dimension)
{
    int limit, i;
    long long rule = cfg->rule;
    limit = (2*dimension) * left + dimension * center + right;
    for(i = 0; i < limit; ++i)
    {
        rule = rule / dimension;
    }
    limit = rule % dimension;
    return limit;
}


int ca_eval_2c(ca_cfg_t *cfg, int left, int center, int right)
{
    return get_result(cfg, left, center, right, 2);
}



int ca_eval_3c(ca_cfg_t *cfg, int left, int center, int right)
{
    return get_result(cfg, left, center, right, 3);
}

void deal_with_2D(int *ret)
{
    ret[0] = ca_eval_2c(obj, 0, in[0],in[1]);
    int i;
    for(i = 1; i < (length-1); ++i)
    {
        ret[i] = ca_eval_2c(obj, in[i-1], in[i],in[i+1]);
    }
    ret[i] = ca_eval_2c(obj, in[i-1], in[i],0);
}

void deal_with_3D(int *ret)
{
    ret[0] = ca_eval_3c(obj, 0, in[0],in[1]);
    int i;
    for(i = 1; i < (length-1); ++i)
    {
        ret[i] = ca_eval_3c(obj, in[i-1], in[i],in[i+1]);
    }
    ret[i] = ca_eval_3c(obj, in[i-1], in[i],0);
}

int* ca_step(ca_cfg_t *cfg, int* input, int len)
{
    in = input;
    length = len;
    obj = cfg;
    int *result = calloc(len+1, sizeof(int));
    switch(cfg->colors)
    {
        case 2:
            if(len == 1)
            {
                result[0] = ca_eval_2c(cfg, 0, input[0], 0);
                return result;
            }
            deal_with_2D(result);
            break;
        case 3:
            if(len == 1)
            {
                result[0] = ca_eval_3c(cfg, 0, input[0], 0);
                return result;
            }
            deal_with_3D(result);
            break;
    }
    return result;
}

ca_cfg_t* ca_init(int colors, long long rule)
{
    ca_cfg_t* ret = malloc(sizeof(ca_cfg_t));
    ret->colors = colors;
    ret->rule = rule;
    return ret;
}

void ca_done(ca_cfg_t* cfg)
{
    free(cfg);
}
