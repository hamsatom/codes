#include <stdio.h>
#include <stdlib.h>
#include "ca.h"
#define wrong_colour_count 100
#define wrong_automat_numbar 101
#define wrong_simulation_moves 102
#define wrong_input_length 103
#define wrong_input 104
#define unkown_error 105
#define llong long long
#define OUTPUT_ERROR "Chybny vstup."
#define ZEROES_OUTPUT "Celkovy pocet nul je "
#define maxOneRule 256ull
#define maxTwoRule 7625597484987ull

llong length;

int main()
{
    int colours_count;
    llong steps, rule;

    scanf("%d", &colours_count);

    if(colours_count != 2 && colours_count != 3)
    {
        puts(OUTPUT_ERROR);
        return wrong_colour_count;
    }

    scanf("%lld", &rule);

    if(rule < 0 || (colours_count == 2 && rule >= maxOneRule) || (colours_count == 3 && rule >= maxTwoRule))
    {
        puts(OUTPUT_ERROR);
        return wrong_automat_numbar;
    }

    scanf("%lld", &steps);

    if(steps < 0)
    {
        puts(OUTPUT_ERROR);
        return wrong_simulation_moves;
    }

    scanf("%lld", &length);

    if(length < 1)
    {
        puts(OUTPUT_ERROR);
        return wrong_input_length;
    }

    int i = 0;
    int *sequence = calloc(length+1, sizeof(int));
    if(sequence == NULL)
    {
        return unkown_error;
    }
    for(; i < length; ++i)
    {
        scanf("%d", &sequence[i]);
        if(sequence[i] < 0 || sequence[i] > (colours_count -1))
        {

            free(sequence);
            puts(OUTPUT_ERROR);
            return wrong_input;
        }
    }

    ++steps;
    ca_cfg_t *ca = ca_init(colours_count, rule);
    int **results = calloc(steps+1, sizeof(int*));
    results[0] = sequence;
    int j;
    llong zeroes = 0;
    for(i=1; i<steps; ++i)
    {
        results[i] = ca_step(ca,results[i-1], length);
    }

    llong *row_value = calloc(steps+1, sizeof(llong));

    for(i=0; i<steps; ++i)
    {
        for(j=0; j< length; ++j)
        {
            if(results[i][j] == 0)
            {
                printf(".");
                ++row_value[i];
                ++zeroes;
            }
            else
            {
                printf("%d", results[i][j]);
            }
        }
        puts("");
    }
    printf(ZEROES_OUTPUT "%llu\n", zeroes);

    llong tmp;
    int *tmp_row;
    for(i = 0; i < steps - 1; ++i)
    {
        j = i + 1;
        tmp = row_value[j];;
        tmp_row = results[j];
        while(j > 0 && tmp > row_value[j-1])
        {
            results[j] = results[j-1];
            row_value[j] = row_value[j-1];
            j--;
        }
        results[j] = tmp_row;
        row_value[j] = tmp;
    }

    for(i=0; i<steps; ++i)
    {
        for(j=0; j< length; ++j)
        {
            if(results[i][j] == 0)
            {
                printf(".");
            }
            else
            {
                printf("%d", results[i][j]);
            }
        }
        puts("");
    }

    for(i=0; i<steps; ++i)
    {
        int* currentIntPtr = results[i];
        free(currentIntPtr);
    }
    free(row_value);
    free(results);
    ca_done(ca);
    return 0;
}
