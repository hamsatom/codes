#include <stdio.h>
#include <stdlib.h>
#define succes 0
#define wrong_size 101
#define wrong_number 102
#define wrong_size_string "Chybna velikost okoli!"
#define wrong_number_string "Chybne cislo pravidla!"
#define max_rule_1 256ull
#define max_rule_2 4294967296ull
#define max_size 2
#define ullong unsigned long long
#define length1 8
#define length2 32

int main()
{
    short size;
    scanf("%hd", &size);
    if(size > max_size || size < 0)
    {
        puts(wrong_size_string);
        return wrong_size;
    }
    ullong rule;
    scanf("%llu", &rule);
    ullong limit = 4ull;
    short length = 2;
    switch(size)
    {
        case 1:
            limit = max_rule_1;
            length = length1;
            break;
        case 2:
            limit = max_rule_2;
            length = length2;
            break;
    }
    if(rule >= limit)
    {
        puts(wrong_number_string);
        return wrong_number;
    }
    short i = 0;
    ullong machineCount = 1ull;
    for(; i < (2*rule)+1; ++i)
    {
        if((machineCount + rule) > limit)
        {
            machineCount = limit;
            break;
        }
        else
        {
            machineCount += rule;
        }
    }
    if(rule ==0)
    {
        machineCount = limit;
    }
    printf("Velikost okoli: %hd\n", size);
    printf("Pocet automatu: %llu\n", machineCount);
    printf("Cislo automatu: %llu\n\n", rule);

    char binary[length];
    short j = 0, kokotina = (2*size)+1;
    char possibilities[kokotina];
    for(i = 0; i<length; ++i)
    {
        if(rule%2==0)
        {
            binary[i] = '.';
        }
        else
        {
            binary[i] = '*';
            --rule;
        }
        rule=rule/2;
    }
    for(j = 0; j<kokotina; ++j)
    {
        possibilities[j] = '.';
    }
    for(i = 0; i<length; ++i)
    {
        for(j = 0; j<kokotina; ++j)
        {
            printf("%c", possibilities[j]);
        }
        puts("");
        for(j = 0; j<size; ++j)
        {
            printf(" ");
        }
        printf("%c", binary[i]);
        puts("");
        if(i != length -1)
        {
            puts("");
        }
        short boze = kokotina-1;
        for(; boze>-1; boze--)
        {
            if(possibilities[boze] == '.')
            {
                possibilities[boze] = '*';
                break;
            }
            if(possibilities[boze] == '*' && (boze-1) > -1)
            {
                if(possibilities[boze-1] != '*')
                {
                    short k = boze;
                    for(; k<kokotina; ++k)
                    {
                        possibilities[k] = '.';
                    }
                    possibilities[boze-1] = '*';
                    break;
                }
            }
        }
    }
    return succes;
}
