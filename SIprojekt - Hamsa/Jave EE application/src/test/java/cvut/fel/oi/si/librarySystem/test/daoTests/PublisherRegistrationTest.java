/*
 * JBoss, Home of Professional Open Source
 * Copyright 2015, Red Hat, Inc. and/or its affiliates, and individual
 * contributors by the @authors tag. See the copyright.txt in the
 * distribution for a full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cvut.fel.oi.si.librarySystem.test.daoTests;

import cvut.fel.oi.si.librarySystem.producer.EntityManagerProducer;
import cvut.fel.oi.si.librarySystem.dao.PublisherDAOImpl;
import cvut.fel.oi.si.librarySystem.dao.daoTemplate.PublisherDAO;
import cvut.fel.oi.si.librarySystem.models.Publisher;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.transaction.api.annotation.TransactionMode;
import org.jboss.arquillian.transaction.api.annotation.Transactional;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.asset.StringAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.ejb.EJBException;
import javax.inject.Inject;

@RunWith(Arquillian.class)
@Transactional
public class PublisherRegistrationTest {

    @Deployment
    public static Archive<?> createTestArchive() {
        return ShrinkWrap.create(WebArchive.class, PublisherRegistrationTest.class.getSimpleName() + ".war")
                .addPackage(EntityManagerProducer.class.getPackage())
                .addPackage(Publisher.class.getPackage())
                .addPackage(PublisherDAOImpl.class.getPackage())
                .addPackage(PublisherDAO.class.getPackage())
                .addAsResource("test-persistence.xml", "META-INF/persistence.xml")
                .addAsWebInfResource("jbossas-ds.xml")
                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml")
                .addAsWebInfResource(
                        new StringAsset("<faces-config version=\"2.0\"/>"),
                        "faces-config.xml");
    }
    
    @Inject PublisherDAO registration;
    
    private Publisher createValidPublisher() {
        Publisher publisher = new Publisher();
        publisher.setName("testName");
        publisher.setAddress("testAddress");
        return publisher;
    }
    
    @Test
    @Transactional(TransactionMode.ROLLBACK)
    public void testRegister() {
        Publisher publisher = createValidPublisher();
        registration.save(publisher);
        Assert.assertNotNull(publisher.getId());
    }
    
    @Test
    @Transactional(TransactionMode.ROLLBACK)
    public void testDelete() {
        Publisher publisher = createValidPublisher();
            registration.save(publisher);
            registration.delete(publisher.getId());
            Assert.assertNull(registration.find(publisher.getId()));
    }
    
    @Test
    @Transactional(TransactionMode.ROLLBACK)
    public void testFind() {
        Publisher publisher = createValidPublisher();
            registration.save(publisher);
            Assert.assertEquals(publisher, registration.find(publisher.getId()));
    }
    
    @Test
    @Transactional(TransactionMode.ROLLBACK)
    public void testList() {
        Publisher publisher = createValidPublisher();
            registration.save(publisher);
            Assert.assertTrue(registration.list().contains(publisher));
    }
    
    @Test
    @Transactional(TransactionMode.ROLLBACK)
    public void testUpdate() throws Exception {
        Publisher publisher = createValidPublisher();
            registration.save(publisher);
            long oldID = publisher.getId();
            publisher.setName("newName");
            registration.update(publisher);
            Assert.assertEquals(oldID, publisher.getId());
    }
    
    @Test(expected = EJBException.class)
    @Transactional(TransactionMode.ROLLBACK)
    public void testRegisterWithoutRequiredData() throws Exception {
        Publisher newPublisher = new Publisher();
        registration.save(newPublisher);
    }
}