package cvut.fel.oi.si.librarySystem.test.serviceTests;

import cvut.fel.oi.si.librarySystem.producer.EntityManagerProducer;
import cvut.fel.oi.si.librarySystem.dao.AuthorDAOImpl;
import cvut.fel.oi.si.librarySystem.dao.PublisherDAOImpl;
import cvut.fel.oi.si.librarySystem.dao.daoTemplate.AuthorDAO;
import cvut.fel.oi.si.librarySystem.dao.daoTemplate.PublisherDAO;
import cvut.fel.oi.si.librarySystem.models.Author;
import cvut.fel.oi.si.librarySystem.models.Publisher;
import cvut.fel.oi.si.librarySystem.services.ContractWithPublisherImpl;
import cvut.fel.oi.si.librarySystem.services.serviceTemplate.ContractWithPublisher;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.transaction.api.annotation.TransactionMode;
import org.jboss.arquillian.transaction.api.annotation.Transactional;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.asset.StringAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;

/**
 * Created by GreenGreen on 22.11.2016.
 */
@RunWith(Arquillian.class)
@Transactional
public class ContractWithPublisherTest {
    
    @Deployment
    public static Archive<?> createTestArchive() {
        return ShrinkWrap.create(WebArchive.class, ContractWithPublisherTest.class.getSimpleName() + ".war")
                         .addPackage(EntityManagerProducer.class.getPackage())
                         .addPackage(Author.class.getPackage()).addPackage(AuthorDAOImpl.class.getPackage()).addPackage(AuthorDAO.class.getPackage())
                         .addPackage(Publisher.class.getPackage()).addPackage(PublisherDAOImpl.class.getPackage()).addPackage(PublisherDAO.class.getPackage())
                         .addPackage(ContractWithPublisherImpl.class.getPackage()).addPackage(ContractWithPublisher.class.getPackage())
                         .addAsResource("test-persistence.xml", "META-INF/persistence.xml")
                         .addAsWebInfResource("jbossas-ds.xml")
                         .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml")
                         .addAsWebInfResource(new StringAsset("<faces-config version=\"2.0\"/>"), "faces-config.xml");
    }
    
    @Inject ContractWithPublisher  contractAssigner;
        
    @Test
    @Transactional(TransactionMode.ROLLBACK)
    public void testMakeContract() {
        Publisher publisher = new Publisher();
        publisher.setName("testName");
        publisher.setAddress("testAddress");
        Author author = new Author();
        author.setEmail("test@gmail.com");
        author.setFirstName("testFirstName");
        author.setSurname("testSurname");
        contractAssigner.assignContract(author, publisher);
        Assert.assertTrue(publisher.getAuthors().contains(author));
        Assert.assertTrue(author.getPublisherWithContract().contains(publisher));
        Assert.assertNotNull(author.getId());
        Assert.assertNotNull(publisher.getId());
    }
}