package cvut.fel.oi.si.librarySystem.test.serviceTests;

import cvut.fel.oi.si.librarySystem.producer.EntityManagerProducer;
import cvut.fel.oi.si.librarySystem.dao.BookDAOImpl;
import cvut.fel.oi.si.librarySystem.dao.LibraryDAOImpl;
import cvut.fel.oi.si.librarySystem.dao.daoTemplate.BookDAO;
import cvut.fel.oi.si.librarySystem.dao.daoTemplate.LibraryDAO;
import cvut.fel.oi.si.librarySystem.models.Book;
import cvut.fel.oi.si.librarySystem.models.Library;
import cvut.fel.oi.si.librarySystem.services.AddBookToLibraryImpl;
import cvut.fel.oi.si.librarySystem.services.serviceTemplate.AddBookToLibrary;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.transaction.api.annotation.TransactionMode;
import org.jboss.arquillian.transaction.api.annotation.Transactional;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.asset.StringAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;
import java.util.Date;

/**
 * Created by GreenGreen on 22.11.2016.
 */
@RunWith(Arquillian.class)
@Transactional
public class AddBookToLibraryTest {
    @Deployment
    public static Archive<?> createTestArchive() {
        return ShrinkWrap.create(WebArchive.class, AddBookToLibraryTest.class.getSimpleName() + ".war")
                         .addPackage(EntityManagerProducer.class.getPackage())
                         .addPackage(Book.class.getPackage()).addPackage(BookDAOImpl.class.getPackage()).addPackage(BookDAO.class.getPackage())
                         .addPackage(Library.class.getPackage()).addPackage(LibraryDAOImpl.class.getPackage()).addPackage(LibraryDAO.class.getPackage())
                         .addPackage(AddBookToLibraryImpl.class.getPackage()).addPackage(AddBookToLibrary.class.getPackage())
                         .addAsResource("test-persistence.xml", "META-INF/persistence.xml")
                         .addAsWebInfResource("jbossas-ds.xml").addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml")
                         .addAsWebInfResource(new StringAsset("<faces-config version=\"2.0\"/>"), "faces-config.xml");
    }
    
    @Inject AddBookToLibrary bookToLibrary;
        
    private Library createdValidLibrary() {
        Library library = new Library();
        library.setName("testName");
        library.setAddress("testAddress");
        return library;
    }
    
    private Book createdValidBook() {
        Book book = new Book();
        book.setTitle("testTitle");
        book.setGenre("testGenre");
        book.setDateOfPublishing(new Date(42L));
        return book;
    }
    
    @Test
    @Transactional(TransactionMode.ROLLBACK)
    public void testAddtoLibrary() {
        Book book = createdValidBook();
        Library library = createdValidLibrary();
        bookToLibrary.addBookToLibrary(book, library);
        Assert.assertFalse(!library.getAvailableBooks().contains(book));
        Assert.assertNotNull(library.getId());
        Assert.assertNotNull(book.getISBN());
        bookToLibrary.addBookToLibrary(book, library);
        Assert.assertFalse(!library.getAvailableBooks().contains(book));
        Assert.assertNotNull(library.getId());
        Assert.assertNotNull(book.getISBN());
    }
    
    @Test
    @Transactional(TransactionMode.ROLLBACK)
    public void testAddManySameBooktoLibrary() {
        Book book = createdValidBook();
        Library library = createdValidLibrary();
        for (int i = 0; i < 10; i++) {
            bookToLibrary.addBookToLibrary(book, library);
        }
        Assert.assertFalse(!library.getAvailableBooks().contains(book));
        Assert.assertFalse(library.getAvailableBooks().parallelStream().filter(e -> e.equals(book)).count() > 5);
        Assert.assertNotNull(library.getId());
        Assert.assertNotNull(book.getISBN());
    }
}