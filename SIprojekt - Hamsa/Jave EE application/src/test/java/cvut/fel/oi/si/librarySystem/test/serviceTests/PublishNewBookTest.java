package cvut.fel.oi.si.librarySystem.test.serviceTests;

import cvut.fel.oi.si.librarySystem.producer.EntityManagerProducer;
import cvut.fel.oi.si.librarySystem.dao.BookDAOImpl;
import cvut.fel.oi.si.librarySystem.dao.PublisherDAOImpl;
import cvut.fel.oi.si.librarySystem.dao.daoTemplate.BookDAO;
import cvut.fel.oi.si.librarySystem.dao.daoTemplate.PublisherDAO;
import cvut.fel.oi.si.librarySystem.models.Book;
import cvut.fel.oi.si.librarySystem.models.Publisher;
import cvut.fel.oi.si.librarySystem.services.PublishNewBookImpl;
import cvut.fel.oi.si.librarySystem.services.serviceTemplate.PublishNewBook;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.transaction.api.annotation.TransactionMode;
import org.jboss.arquillian.transaction.api.annotation.Transactional;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.asset.StringAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;
import java.util.Date;

/**
 * Created by GreenGreen on 22.11.2016.
 */
@RunWith(Arquillian.class)
@Transactional
public class PublishNewBookTest {
    @Deployment
    public static Archive<?> createTestArchive() {
        return ShrinkWrap.create(WebArchive.class, PublishNewBookTest.class.getSimpleName() + ".war")
                         .addPackage(EntityManagerProducer.class.getPackage())
                         .addPackage(Book.class.getPackage()).addPackage(BookDAOImpl.class.getPackage()).addPackage(BookDAO.class.getPackage())
                         .addPackage(Publisher.class.getPackage()).addPackage(PublisherDAOImpl.class.getPackage()).addPackage(PublisherDAO.class.getPackage())
                         .addPackage(PublishNewBook.class.getPackage()).addPackage(PublishNewBookImpl.class.getPackage())
                         .addAsResource("test-persistence.xml", "META-INF/persistence.xml")
                         .addAsWebInfResource("jbossas-ds.xml").addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml")
                         .addAsWebInfResource(new StringAsset("<faces-config version=\"2.0\"/>"), "faces-config.xml");
    }
    
    @Inject PublishNewBook  publishment;
    
    @Test
    @Transactional(TransactionMode.ROLLBACK)
    public void testPublish() {
        Publisher publisher = new Publisher();
        publisher.setName("testName");
        publisher.setAddress("testAddress");
        Book book = new Book();
        book.setTitle("testTitle");
        book.setGenre("testGenre");
        book.setPublisher(publisher);
        book.setDateOfPublishing(new Date(42L));
        publishment.publishNewBook(publisher, book);
        Assert.assertTrue(publisher.getPublishedBooks().contains(book));
        Assert.assertEquals(book.getPublisher(), publisher);
        Assert.assertNotNull(publisher.getId());
        Assert.assertNotNull(book.getISBN());
    }
}