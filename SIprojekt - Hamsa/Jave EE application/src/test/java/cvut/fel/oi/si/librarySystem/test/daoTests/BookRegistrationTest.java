/*
 * JBoss, Home of Professional Open Source
 * Copyright 2015, Red Hat, Inc. and/or its affiliates, and individual
 * contributors by the @authors tag. See the copyright.txt in the
 * distribution for a full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cvut.fel.oi.si.librarySystem.test.daoTests;

import cvut.fel.oi.si.librarySystem.producer.EntityManagerProducer;
import cvut.fel.oi.si.librarySystem.dao.BookDAOImpl;
import cvut.fel.oi.si.librarySystem.dao.daoTemplate.BookDAO;
import cvut.fel.oi.si.librarySystem.models.Book;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.transaction.api.annotation.TransactionMode;
import org.jboss.arquillian.transaction.api.annotation.Transactional;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.asset.StringAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.ejb.EJBException;
import javax.inject.Inject;
import java.util.Date;

@RunWith(Arquillian.class)
@Transactional
public class BookRegistrationTest {
    
    @Deployment
    public static Archive<?> createTestArchive() {
        return ShrinkWrap.create(WebArchive.class, BookRegistrationTest.class.getSimpleName() + ".war")
                         .addPackage(EntityManagerProducer.class.getPackage())
                         .addPackage(Book.class.getPackage())
                         .addPackage(BookDAOImpl.class.getPackage())
                         .addPackage(BookDAO.class.getPackage())
                         .addAsResource("test-persistence.xml", "META-INF/persistence.xml")
                         .addAsWebInfResource("jbossas-ds.xml")
                         .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml")
                         .addAsWebInfResource(
                                 new StringAsset("<faces-config version=\"2.0\"/>"),
                                 "faces-config.xml");
    }
    
    @Inject BookDAO registration;
    
    private Book createValidBook() {
        Book book = new Book();
        book.setTitle("testTitle");
        book.setGenre("testGenre");
        book.setDateOfPublishing(new Date(42L));
        return book;
    }
    
    @Test
    @Transactional(TransactionMode.ROLLBACK)
    public void testRegister() {
        Book book = createValidBook();
        registration.save(book);
        Assert.assertNotNull(book.getISBN());
    }
    
    @Test
    @Transactional(TransactionMode.ROLLBACK)
    public void testDelete() {
        Book book = createValidBook();
        registration.save(book);
        registration.delete(book.getISBN());
        Assert.assertNull(registration.find(book.getISBN()));
    }
    
    @Test
    @Transactional(TransactionMode.ROLLBACK)
    public void testFind() {
        Book book = createValidBook();
        registration.save(book);
        Assert.assertEquals(book, registration.find(book.getISBN()));
    }
    
    @Test
    @Transactional(TransactionMode.ROLLBACK)
    public void testList() {
        Book book = createValidBook();
        registration.save(book);
        Assert.assertTrue(registration.list().contains(book));
    }
    
    @Test
    @Transactional(TransactionMode.ROLLBACK)
    public void testUpdate() throws Exception {
        Book book = createValidBook();
        registration.save(book);
        long oldID = book.getISBN();
        book.setTitle("newName");
        registration.update(book);
        Assert.assertEquals(oldID, book.getISBN());
    }
    
    @Test(expected = EJBException.class)
    @Transactional(TransactionMode.ROLLBACK)
    public void testRegisterWithoutRequiredData() throws Exception {
        Book newBook = new Book();
        registration.save(newBook);
    }
}
