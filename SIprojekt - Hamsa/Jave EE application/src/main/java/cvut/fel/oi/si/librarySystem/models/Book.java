package cvut.fel.oi.si.librarySystem.models;

import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by Tomáš Hamsa on 01.11.2016.
 */
@Entity
public class Book implements Serializable {
    
    public static final long serialVersionUID = 2L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long ISBN;
    
    @ManyToMany(fetch = FetchType.LAZY) private List<Author> authors;
    
    @NotNull private Date dateOfPublishing;
    
    @ManyToOne private Publisher publisher;
    
    @Basic
    @NotBlank
    private String title;
    
    @Basic
    @NotBlank
    @Pattern(regexp = "[^0-9]*", message = "Must not contain numbers")
    private String genre;
    
    public long getISBN() {
        return ISBN;
    }
    
    public void setISBN(long ISBN) {
        this.ISBN = ISBN;
    }
    
    public List<Author> getAuthors() {
        return authors;
    }
    
    public void setAuthors(List<Author> autori) {
        this.authors = autori;
    }
    
    @NotNull
    public Date getDateOfPublishing() {
        if (dateOfPublishing == null) {
            return new Date();
        }
        return (Date) dateOfPublishing.clone();
    }
    
    public void setDateOfPublishing(@NotNull Date datumVydani) {
        this.dateOfPublishing = (Date) datumVydani.clone();
    }
    
    public Publisher getPublisher() {
        return publisher;
    }
    
    public void setPublisher(Publisher publisher) {
        this.publisher = publisher;
    }
    
    public String getTitle() {
        return title;
    }
    
    public void setTitle(String nazev) {
        this.title = nazev;
    }
    
    public String getGenre() {
        return genre;
    }
    
    public void setGenre(String zanr) {
        this.genre = zanr;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Book)) {
            return false;
        }
        
        Book book = (Book) o;
        
        if (authors != null ? !authors.equals(book.authors) : book.authors != null) {
            return false;
        }
        if (dateOfPublishing != null ? !dateOfPublishing.equals(book.dateOfPublishing) : book.dateOfPublishing != null) {
            return false;
        }
        if (publisher != null ? !publisher.equals(book.publisher) : book.publisher != null) {
            return false;
        }
        if (title != null ? !title.equals(book.title) : book.title != null) {
            return false;
        }
        return genre != null ? genre.equals(book.genre) : book.genre == null;
    }
    
    @Override
    public int hashCode() {
        int result = dateOfPublishing != null ? dateOfPublishing.hashCode() : 0;
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (genre != null ? genre.hashCode() : 0);
        return result;
    }
    
    @Override
    public String toString() {
        return "Book{" + "ISBN=" + ISBN + ", dateOfPublishing=" + dateOfPublishing + ", title='" + title + '\'' + ", genre='" + genre + '\'' + '}';
    }
}
