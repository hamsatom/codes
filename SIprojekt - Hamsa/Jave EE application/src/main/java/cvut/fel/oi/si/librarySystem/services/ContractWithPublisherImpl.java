/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cvut.fel.oi.si.librarySystem.services;

import cvut.fel.oi.si.librarySystem.dao.daoTemplate.AuthorDAO;
import cvut.fel.oi.si.librarySystem.dao.daoTemplate.PublisherDAO;
import cvut.fel.oi.si.librarySystem.models.Author;
import cvut.fel.oi.si.librarySystem.models.Publisher;
import cvut.fel.oi.si.librarySystem.services.serviceTemplate.ContractWithPublisher;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Vector;
import java.util.logging.Logger;

/**
 * @author hamsatom
 */
@Stateless
public class ContractWithPublisherImpl implements ContractWithPublisher {
    
    @Inject AuthorDAO adi;
    
    @Inject PublisherDAO pdi;
    
    @Inject Logger log;
    
    @Override
    public void assignContract(@NotNull Author author, @NotNull Publisher publisher) {
        List<Publisher> currentContracts = author.getPublisherWithContract();
        List<Author> currentAuthors = publisher.getAuthors();
        if (currentContracts == null) {
            currentContracts = new Vector<>(1);
        }
        if (currentAuthors == null) {
            currentAuthors = new Vector<>(1);
        }
        
        if (!currentContracts.contains(publisher)) {
            currentContracts.add(publisher);
            author.setPublisherWithContract(currentContracts);
            log.info("Adding " + publisher.toString()+ " to have contract with " + author.toString());
        }
        if (!currentAuthors.contains(author)) {
            currentAuthors.add(author);
            publisher.setAuthors(currentAuthors);
            log.info("Adding " + author.toString() + " to authors under " + publisher.toString());
        }
        if (adi.find(author.getId()) == null) {
            log.info("Created new " + author.toString());
            adi.save(author);
        } else {
            adi.update(author);
            log.info("Updated " + author.toString());
        }
        if (pdi.find(publisher.getId()) == null) {
            log.info("Created new " + publisher.toString());
            pdi.save(publisher);
        } else {
            pdi.update(publisher);
            log.info("Updated " + publisher.toString());
        }
    }
}
