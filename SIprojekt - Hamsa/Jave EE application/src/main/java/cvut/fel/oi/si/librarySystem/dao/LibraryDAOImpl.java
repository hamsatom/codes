package cvut.fel.oi.si.librarySystem.dao;

import cvut.fel.oi.si.librarySystem.dao.daoTemplate.LibraryDAO;
import cvut.fel.oi.si.librarySystem.models.Library;

/**
 *
 * @author hamsatom
 */
public class LibraryDAOImpl extends GenericDAOImpl<Library> implements LibraryDAO {

}
