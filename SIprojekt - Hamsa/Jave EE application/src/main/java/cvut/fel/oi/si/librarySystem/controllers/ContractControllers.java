package cvut.fel.oi.si.librarySystem.controllers;

import cvut.fel.oi.si.librarySystem.models.Author;
import cvut.fel.oi.si.librarySystem.models.Publisher;
import cvut.fel.oi.si.librarySystem.services.serviceTemplate.ContractWithPublisher;

import javax.annotation.PostConstruct;
import javax.enterprise.inject.Model;
import javax.enterprise.inject.Produces;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

/**
 * Created by Tomáš Hamsa on 23.12.2016.
 */
@Model
public class ContractControllers {
    @Inject private ContractWithPublisher contract;
    
    @Produces
    @Named
    private Publisher newPublisherToMakeContract;
    
    @Produces
    @Named
    private Author newAuthorToSignContract;
    
    @Inject private FacesContext facesContext;
    
    @PostConstruct
    public void initNewAuthorAndPublisher() {
        newAuthorToSignContract = new Author();
        newPublisherToMakeContract = new Publisher();
    }
        
    public void signContractWithPublisher() {
        contract.assignContract(newAuthorToSignContract, newPublisherToMakeContract);
        FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_INFO, "Contract signed!", "Assigning contract successful");
        facesContext.addMessage(null, m);
        initNewAuthorAndPublisher();
    }
}
