package cvut.fel.oi.si.librarySystem.services;

import cvut.fel.oi.si.librarySystem.dao.daoTemplate.BookDAO;
import cvut.fel.oi.si.librarySystem.dao.daoTemplate.LibraryDAO;
import cvut.fel.oi.si.librarySystem.models.Book;
import cvut.fel.oi.si.librarySystem.models.Library;
import cvut.fel.oi.si.librarySystem.services.serviceTemplate.AddBookToLibrary;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Vector;
import java.util.logging.Logger;

/**
 * Created by GreenGreen on 22.11.2016.
 */

@Stateless
public class AddBookToLibraryImpl implements AddBookToLibrary {
    
    private static final byte MAX_SAME_BOOKS_COUNT = 5;
    @Inject BookDAO bdi;
    
    @Inject LibraryDAO ldi;
    
    @Inject Logger log;
    
    public void addBookToLibrary(@NotNull Book book, @NotNull Library library) {
        List<Book> currentBooks = library.getAvailableBooks();
        if (currentBooks == null) {
            currentBooks = new Vector<>(1);
        } else {
            long numberOfSameBooksInLibrary = currentBooks.parallelStream().filter(e -> e.equals(book)).count();
            if (numberOfSameBooksInLibrary >= MAX_SAME_BOOKS_COUNT) {
                log.info("Nothing added - " + library.toString() + "contains " + numberOfSameBooksInLibrary + " of "
                         + book.toString() + "that's more than limit " + MAX_SAME_BOOKS_COUNT);
                return;
            }
        }
        currentBooks.add(book);
        library.setAvailableBooks(currentBooks);
        if (ldi.find(library.getId()) != null) {
            ldi.update(library);
            log.info("Updating " + library.toString());
        } else {
            ldi.save(library);
            log.info("Persisting " + library.toString());
        }
        if (bdi.find(book.getISBN()) != null) {
            bdi.update(book);
            log.info("Updating " + book.toString());
        } else {
            bdi.save(book);
            log.info("Persisting " + book.toString());
        }
    }
}
