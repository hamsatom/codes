package cvut.fel.oi.si.librarySystem.services.serviceTemplate;

import cvut.fel.oi.si.librarySystem.models.Book;
import cvut.fel.oi.si.librarySystem.models.Library;

import javax.validation.constraints.NotNull;

/**
 * Created by GreenGreen on 22.11.2016.
 */

public interface AddBookToLibrary {
    public void addBookToLibrary(@NotNull Book book, @NotNull Library library);
}
