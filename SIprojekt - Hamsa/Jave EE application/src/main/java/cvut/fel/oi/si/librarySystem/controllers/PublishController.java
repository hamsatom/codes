package cvut.fel.oi.si.librarySystem.controllers;

import cvut.fel.oi.si.librarySystem.models.Book;
import cvut.fel.oi.si.librarySystem.models.Publisher;
import cvut.fel.oi.si.librarySystem.services.serviceTemplate.PublishNewBook;

import javax.annotation.PostConstruct;
import javax.enterprise.inject.Model;
import javax.enterprise.inject.Produces;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

/**
 * Created by Tomáš Hamsa on 23.12.2016.
 */
@Model
public class PublishController {
    @Inject private PublishNewBook publish;
    
    @Produces
    @Named
    private Book newBookToPublished;
    
    @Produces
    @Named
    private Publisher newPublisherToPublish;
    
    @Inject private FacesContext facesContext;
    
    @PostConstruct
    public void initNewPublisherAndBook() {
        newPublisherToPublish = new Publisher();
        newBookToPublished = new Book();
    }
    
    public void publishNewBook() {
        publish.publishNewBook(newPublisherToPublish, newBookToPublished);
        FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_INFO, "Book published!", "Publication successful");
        facesContext.addMessage(null, m);
        initNewPublisherAndBook();
    }
}
