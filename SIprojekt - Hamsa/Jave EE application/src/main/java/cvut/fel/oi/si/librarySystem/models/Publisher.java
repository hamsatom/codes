package cvut.fel.oi.si.librarySystem.models;

import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;

/**
 * Created by GreenGreen on 01.11.2016.
 */
@Entity
public class Publisher implements Serializable {
    public static final long serialVersionUID = 4L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    
    @Basic
    @Size(min = 8)
    @NotBlank
    private String address;
    
    @ManyToMany(fetch = FetchType.LAZY) private List<Author> authors;
    
    @Basic
    @NotBlank
    private String name;
    
    @OneToMany(fetch = FetchType.LAZY) private List<Book> publishedBooks;
    
    public long getId() {
        return id;
    }
    
    public void setId(long id) {
        this.id = id;
    }
    
    public String getAddress() {
        return address;
    }
    
    public void setAddress(String address) {
        this.address = address;
    }
    
    public List<Author> getAuthors() {
        return authors;
    }
    
    public void setAuthors(List<Author> authors) {
        this.authors = authors;
    }
    
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public List<Book> getPublishedBooks() {
        return publishedBooks;
    }
    
    public void setPublishedBooks(List<Book> publishedBooks) {
        this.publishedBooks = publishedBooks;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Publisher)) {
            return false;
        }
        
        Publisher publisher = (Publisher) o;
        
        if (address != null ? !address.equals(publisher.address) : publisher.address != null) {
            return false;
        }
        if (authors != null ? !authors.equals(publisher.authors) : publisher.authors != null) {
            return false;
        }
        if (name != null ? !name.equals(publisher.name) : publisher.name != null) {
            return false;
        }
        return publishedBooks != null ? publishedBooks.equals(publisher.publishedBooks) : publisher.publishedBooks == null;
    }
    
    @Override
    public int hashCode() {
        int result = address != null ? address.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }
    
    @Override
    public String toString() {
        return "Publisher{" + "id=" + id + ", address='" + address + '\'' + ", name='" + name + '\'' + '}';
    }
}
