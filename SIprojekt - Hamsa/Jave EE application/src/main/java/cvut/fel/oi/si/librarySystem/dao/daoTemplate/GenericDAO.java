/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cvut.fel.oi.si.librarySystem.dao.daoTemplate;

import java.util.List;

/**
 *
 * @author hamsatom
 */
public interface GenericDAO<T>{

    List<T> list();

    T save(T entity);

    void delete(Object id);

    T find(Object id);

    T update(T entity);
}
