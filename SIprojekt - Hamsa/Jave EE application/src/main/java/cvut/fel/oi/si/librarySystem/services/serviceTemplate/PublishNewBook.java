package cvut.fel.oi.si.librarySystem.services.serviceTemplate;

import cvut.fel.oi.si.librarySystem.models.Book;
import cvut.fel.oi.si.librarySystem.models.Publisher;

import javax.validation.constraints.NotNull;

/**
 * Created by GreenGreen on 22.11.2016.
 */
public interface PublishNewBook {
    public void publishNewBook(@NotNull Publisher publisher, @NotNull Book book);
}
