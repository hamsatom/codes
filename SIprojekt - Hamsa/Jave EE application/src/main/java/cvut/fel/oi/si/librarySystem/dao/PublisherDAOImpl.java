package cvut.fel.oi.si.librarySystem.dao;

import cvut.fel.oi.si.librarySystem.dao.daoTemplate.PublisherDAO;
import cvut.fel.oi.si.librarySystem.models.Publisher;

/**
 *
 * @author hamsatom
 */
public class PublisherDAOImpl extends GenericDAOImpl<Publisher> implements PublisherDAO {

}
