package cvut.fel.oi.si.librarySystem.dao;

import cvut.fel.oi.si.librarySystem.dao.daoTemplate.GenericDAO;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;
import java.util.logging.Logger;

/**
 * @author hamsatom
 */
@SuppressWarnings("unchecked")
abstract class GenericDAOImpl<T> implements GenericDAO<T> {
    
    
    @Inject private Logger log;
    
    @Inject protected EntityManager em;
    
    private Class<T> type;
    
    GenericDAOImpl() {
        Type t = getClass().getGenericSuperclass();
        ParameterizedType pt = (ParameterizedType) t;
        type = (Class) pt.getActualTypeArguments()[0];
    }
    
    @Override
    public List<T> list() {
        log.info("Listing");
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<T> cq = cb.createQuery(type);
        Root<T> rootEntry = cq.from(type);
        CriteriaQuery<T> all = cq.select(rootEntry);
        TypedQuery<T> allQuery = em.createQuery(all);
        return allQuery.getResultList();
    }
    
    @Override
    public T save(final T entity) {
        log.info("Registering " + entity.toString());
        this.em.persist(entity);
        return entity;
    }
    
    @Override
    public void delete(final Object id) {
        log.info("Deleting " + id.toString());
        this.em.remove(this.em.getReference(type, id));
    }
    
    @Override
    public T find(final Object id) {
        log.info("Looking for " + id.toString());
        return (T) this.em.find(type, id);
    }
    
    @Override
    public T update(final T entity) {
        log.info("Updating " + entity.toString());
        this.em.merge(entity);
        return entity;
    }
    
}
