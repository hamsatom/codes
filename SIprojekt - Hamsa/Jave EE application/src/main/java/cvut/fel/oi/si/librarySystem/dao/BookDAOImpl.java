package cvut.fel.oi.si.librarySystem.dao;

import cvut.fel.oi.si.librarySystem.dao.daoTemplate.BookDAO;
import cvut.fel.oi.si.librarySystem.models.Book;

/**
 *
 * @author hamsatom
 */
public class BookDAOImpl extends GenericDAOImpl<Book> implements BookDAO {

}
