package cvut.fel.oi.si.librarySystem.dao;

import cvut.fel.oi.si.librarySystem.dao.daoTemplate.AuthorDAO;
import cvut.fel.oi.si.librarySystem.models.Author;

/**
 *
 * @author hamsatom
 */
public class AuthorDAOImpl extends GenericDAOImpl<Author> implements AuthorDAO {
}
