/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cvut.fel.oi.si.librarySystem.dao.daoTemplate;

import cvut.fel.oi.si.librarySystem.models.Author;

/**
 *
 * @author hamsatom
 */
public interface AuthorDAO extends GenericDAO<Author>{
    
}
