package cvut.fel.oi.si.librarySystem.services.serviceTemplate;

import cvut.fel.oi.si.librarySystem.models.Author;
import cvut.fel.oi.si.librarySystem.models.Publisher;

import javax.validation.constraints.NotNull;

/**
 * Created by GreenGreen on 22.11.2016.
 */
public interface ContractWithPublisher {
    public void assignContract(@NotNull Author author, @NotNull Publisher publisher);
}
