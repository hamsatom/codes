package cvut.fel.oi.si.librarySystem.controllers;

import cvut.fel.oi.si.librarySystem.models.Book;
import cvut.fel.oi.si.librarySystem.models.Library;
import cvut.fel.oi.si.librarySystem.services.serviceTemplate.AddBookToLibrary;

import javax.annotation.PostConstruct;
import javax.enterprise.inject.Model;
import javax.enterprise.inject.Produces;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

/**
 * Created by Tomáš Hamsa on 23.12.2016.
 */
@Model
public class BookToLibraryController {
    @Inject private AddBookToLibrary libraryAddition;
    
    @Produces
    @Named
    private Library newLibraryToReceive;
    
    @Produces
    @Named
    private Book newBookToAdd;
    
    @Inject private FacesContext facesContext;
    
    @PostConstruct
    public void initNewBookAndLibrary() {
        newBookToAdd = new Book();
        newLibraryToReceive = new Library();
    }
    
    public void addBookToTheLibrary() {
        libraryAddition.addBookToLibrary(newBookToAdd, newLibraryToReceive);
        FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_INFO, "Book added!", "Adding book to the library successful");
        facesContext.addMessage(null, m);
        initNewBookAndLibrary();
    }
}
