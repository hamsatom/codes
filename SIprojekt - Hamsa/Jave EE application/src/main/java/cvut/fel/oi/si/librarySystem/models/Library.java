package cvut.fel.oi.si.librarySystem.models;

import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;

/**
 * Created by GreenGreen on 01.11.2016.
 */
@Entity
public class Library implements Serializable {
    public static final long serialVersionUID = 3L;
    
    @Basic
    @Size(min = 8)
    @NotBlank
    private String address;
    
    @Basic
    @NotBlank
    private String name;
    
    @ManyToMany(fetch = FetchType.LAZY) private List<Book> availableBooks;
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    
    public long getId() {
        return id;
    }
    
    
    public void setId(long id) {
        this.id = id;
    }
    
    public String getAddress() {
        return address;
    }
    
    public void setAddress(String address) {
        this.address = address;
    }
    
    public String getName() {
        return name;
    }
    
    public void setName(String nazev) {
        this.name = nazev;
    }
    
    public List<Book> getAvailableBooks() {
        return availableBooks;
    }
    
    public void setAvailableBooks(List<Book> availableBooks) {
        this.availableBooks = availableBooks;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Library)) {
            return false;
        }
        
        Library library = (Library) o;
        
        if (address != null ? !address.equals(library.address) : library.address != null) {
            return false;
        }
        if (name != null ? !name.equals(library.name) : library.name != null) {
            return false;
        }
        return availableBooks != null ? availableBooks.equals(library.availableBooks) : library.availableBooks == null;
    }
    
    @Override
    public int hashCode() {
        int result = address != null ? address.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }
    
    @Override
    public String toString() {
        return "Library{" + "address='" + address + '\'' + ", name='" + name + '\'' + ", id=" + id + '}';
    }
}
