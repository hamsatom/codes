package cvut.fel.oi.si.librarySystem.models;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.util.List;


/**
 * Created by Tomáš Hamsa on 01.11.2016.
 */
@Entity
public class Author implements Serializable {
    public static final long serialVersionUID = 1L;
    
    @Basic
    @Email
    private String email;
    
    @Basic
    @NotBlank
    @Pattern(regexp = "[^0-9]*", message = "Must not contain numbers")
    private String firstName;
    
    @Basic
    @NotBlank
    @Pattern(regexp = "[^0-9]*", message = "Must not contain numbers")
    private String surname;
        
    @ManyToMany(fetch = FetchType.LAZY)
    private List<Publisher> publisherWithContract;
        
    @ManyToMany(fetch = FetchType.LAZY)
    private List<Book> wroteBooks;
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    
    public String getEmail() {
        return email;
    }
    
    public void setEmail(String email) {
        this.email = email;
    }
    
    public String getFirstName() {
        return firstName;
    }
    
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    
    public String getSurname() {
        return surname;
    }
    
    public void setSurname(String surname) {
        this.surname = surname;
    }
    
    public List<Publisher> getPublisherWithContract() {
        return publisherWithContract;
    }
    
    public void setPublisherWithContract(List<Publisher> publisherWithContract) {
        this.publisherWithContract = publisherWithContract;
    }
    
    public List<Book> getWroteBooks() {
        return wroteBooks;
    }
    
    public void setWroteBooks(List<Book> wroteBooks) {
        this.wroteBooks = wroteBooks;
    }
    
    public long getId() {
        return id;
    }
    
    public void setId(long id) {
        this.id = id;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Author)) {
            return false;
        }
        
        Author author = (Author) o;
        
        if (email != null ? !email.equals(author.email) : author.email != null) {
            return false;
        }
        if (firstName != null ? !firstName.equals(author.firstName) : author.firstName != null) {
            return false;
        }
        if (surname != null ? !surname.equals(author.surname) : author.surname != null) {
            return false;
        }
        if (publisherWithContract != null ? !publisherWithContract.equals(author.publisherWithContract) : author.publisherWithContract != null) {
            return false;
        }
        return wroteBooks != null ? wroteBooks.equals(author.wroteBooks) : author.wroteBooks == null;
    }
    
    @Override
    public int hashCode() {
        int result = email != null ? email.hashCode() : 0;
        result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
        result = 31 * result + (surname != null ? surname.hashCode() : 0);
        return result;
    }
    
    @Override
    public String toString() {
        return "Author{" + "email='" + email + '\'' + ", firstName='" + firstName + '\'' + ", surname='" + surname + '\'' + ", id=" + id + '}';
    }
}
