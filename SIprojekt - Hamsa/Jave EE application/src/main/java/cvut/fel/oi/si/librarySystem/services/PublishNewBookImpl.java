/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cvut.fel.oi.si.librarySystem.services;

import cvut.fel.oi.si.librarySystem.dao.daoTemplate.BookDAO;
import cvut.fel.oi.si.librarySystem.dao.daoTemplate.PublisherDAO;
import cvut.fel.oi.si.librarySystem.models.Book;
import cvut.fel.oi.si.librarySystem.models.Publisher;
import cvut.fel.oi.si.librarySystem.services.serviceTemplate.PublishNewBook;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Vector;
import java.util.logging.Logger;

/**
 * @author hamsatom
 */
@Stateless
public class PublishNewBookImpl implements PublishNewBook {
    
    @Inject PublisherDAO pdi;
    
    @Inject BookDAO bdi;
    
    @Inject Logger log;
    
    @Override
    public void publishNewBook(@NotNull Publisher publisher, @NotNull Book book) {
        List<Book> publishedBooks = publisher.getPublishedBooks();
        if (publishedBooks == null) {
            publishedBooks = new Vector<>(1);
        }
        if (!publishedBooks.contains(book)) {
            publishedBooks.add(book);
            publisher.setPublishedBooks(publishedBooks);
            log.info("Adding " + book.toString() + " to be published under " + publisher.toString());
        }
        book.setPublisher(publisher);
        if (pdi.find(publisher.getId()) != null) {
            pdi.update(publisher);
            log.info("Updating " + publisher.toString());
        } else {
            pdi.save(publisher);
            log.info("Persisting " + publisher.toString());
        }
        if (bdi.find(book.getISBN()) != null) {
            bdi.update(book);
            log.info("Updating " + book.toString());
        } else {
            bdi.save(book);
            log.info("Persisting " + book.toString());
        }
    }
}
