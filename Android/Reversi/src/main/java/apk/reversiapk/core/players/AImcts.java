package apk.reversiapk.core.players;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import apk.reversiapk.core.ReversiMove;

import java.io.Serializable;
import java.util.ArrayDeque;
import java.util.Collections;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.StreamHandler;

/**
 * Uses Monte Carlo tree search with Upper Confidence Bound
 * @see <a href="https://chessprogramming.wikispaces.com/Monte-Carlo+Tree+Search/">Monte-Carlo Tree Search</a>
 * @see <a href="https://chessprogramming.wikispaces.com/UCT/">Upper Confidence bounds</a>
 */
public class AImcts extends AIwithMemory {
/**
 * Name of this bot
 */
private static final String NAME = "MCTS BOT";
private static final Logger LOG = Logger.getLogger(AImcts.class.getName());
/**
 * Multiplication constant for multiplying visited count in UCB
 */
private static final byte EXPLORATION_VALUE = 2;
/**
 * Multiplication constant for multiplying used in UCB
 */
private static final int MUTIPLIER = 8;
/**
 * Root of Monte-Carlo tree. It's the game state from which I'm searching moves.
 */
private MCTnode root;
private MCTnode curNode, newNode, bestChild, pickedNode;
/**
 * Maximum number of {@code plays} in one {@link MCTnode}
 */
private int mostPlays;
/**
 * Maximum number of {@code wins} in one {@link MCTnode}
 */
private int mostWins;
private byte opColour, tmp, nodeColor, opStones, x, y, myStones;
private byte[][] curState;
private ReversiMove move, possibleMove;


public AImcts() {
    super.transpositionTable = new HashMap<>(120, 1.0f);
    LOG.addHandler(new StreamHandler());
}

/**
 * {@inheritDoc}
 */
@NonNull
@Override
public String getName() {
    return NAME;
}

/**
 * Compares all of my possible moves.
 *
 * @param board
 *         current state of the board
 * @return best of available moves
 */
@Override
public ReversiMove makeNextMove(@NonNull byte[][] board) {
    timeStart = System.nanoTime();
    moves = getPossibleMoves();
    if (moves.size() == 1) {
        return moves.remove(0);
    }
    myColor = getMyColor();
    opponentColor = getOpponentColor();
    mostPlays = Integer.MIN_VALUE;
    mostWins = Integer.MIN_VALUE;
    ID = boardId(board);
    desc = comp.board = board;
    comp.adjustBoardRating();
    Collections.sort(moves, comp.comparator);
    bestMove = moves.get(0);

    if (transpositionTable.containsKey(ID) && (root = (MCTnode) transpositionTable.get(ID)).colour == myColor) {
        root.parent = null;
    } else {
        root = new MCTnode(board, myColor);
        transpositionTable.put(ID, root);
    }
    while (System.nanoTime() - timeStart < TIME_LIMIT) {
        pickedNode = treePolicy();
        backPropagate(pickedNode, simulate());
    }

    for (MCTnode child : root.children) {
        if (child.plays > mostPlays) {
            mostPlays = child.plays;
            mostWins = child.wins;
            bestMove = child.move;
        } else if (child.plays == mostPlays && child.wins > mostWins) {
            mostWins = child.wins;
            bestMove = child.move;
        }
    }

    LOG.info(NAME + " plays " + root.plays);

    moves.clear();
    possibleMoves.clear();
    comp.flippedDiscs.clear();
    return bestMove;
}

/**
 * Increases wins and plays in all upper node from {@code pickedNode}. One {@link MCTnode} must have number of playss same as sum of all plays in his children the same goes for wins.
 *
 * @param pickedNode
 *         node from which I'm increasing values
 * @param result
 *         result of last simulation played from {@code pickedNode}. Equals 1 if game is won, 0 if not.
 */
private void backPropagate(@NonNull MCTnode pickedNode, byte result) {
    while (pickedNode.parent != null) {
        ++pickedNode.plays;
        pickedNode.wins += result;
        pickedNode = pickedNode.parent;
        assert pickedNode != null;
    }
    ++root.plays;
    root.wins += result;
}

/**
 * Plays game of Reversi untill no more moves are possible
 *
 * @return 1 if game is won by me, 0 if not
 */
private byte simulate() {
    nodeColor = pickedNode.colour;
    curState = pickedNode.state;
    myStones = 0;
    opStones = 0;
    opColour = nodeColor == myColor ? opponentColor : myColor;
    while (true) {
        while (!(possibleMoves = comp.getAllPossibleTurns(curState, nodeColor)).isEmpty()) {
            move = possibleMoves.get(0);
            curState = comp.flipDiscs(curState, move.x, move.y, nodeColor, opColour);
            tmp = nodeColor;
            nodeColor = opColour;
            opColour = tmp;
        }
        if ((possibleMoves = comp.getAllPossibleTurns(curState, opColour)).isEmpty()) {
            break;
        } else {
            move = possibleMoves.get(0);
            curState = comp.flipDiscs(curState, move.x, move.y, opColour, nodeColor);
        }
    }
    for (x = 0; x < comp.boardWidth; ++x) {
        for (y = 0; y < comp.boardWidth; ++y) {
            if (curState[x][y] == myColor) {
                ++myStones;
            } else if (curState[x][y] == opponentColor) {
                ++opStones;
            }
        }
    }
    if (myStones > opStones) {
        return 1;
    } else {
        return 0;
    }
}

/**
 * Pick the best node from which game will be simulated
 * @return best node for simulation
 */
private MCTnode treePolicy() {
    curNode = root;
    possibleMoves = moves;
    opColour = opponentColor;
    while (true) {
        if (possibleMoves.isEmpty()) {
            if ((possibleMoves = comp.getAllPossibleTurns(curNode.state, opColour)).isEmpty()) {
                return curNode;
            } else {
                possibleMove = possibleMoves.get(0);
                desc = comp.flipDiscs(curNode.state, possibleMove.x, possibleMove.y, opColour, curNode.colour);
                ID = boardId(desc);
                if (transpositionTable.containsKey(ID) && (newNode = (MCTnode) transpositionTable.get(ID)).colour == curNode.colour) {
                    newNode.parent = curNode;
                    newNode.move = possibleMove;
                    bubbleDataToTheRoot(newNode);
                } else {
                    newNode = new MCTnode(possibleMove, curNode, desc, curNode.colour);
                    transpositionTable.put(ID, newNode);
                }
                curNode.children.addLast(newNode);
                curNode = newNode;
            }
        } else if (curNode.children.size() < possibleMoves.size()) {
            possibleMove = possibleMoves.get(0);
outerLoop:
            for (ReversiMove move : possibleMoves) {
                for (MCTnode child : curNode.children) {
                    if (move.x != child.move.x || move.y != child.move.y) {
                        possibleMove = move;
                        break outerLoop;
                    }
                }
            }
            desc = comp.flipDiscs(curNode.state, possibleMove.x, possibleMove.y, curNode.colour, opColour);
            ID = boardId(desc);
            if (transpositionTable.containsKey(ID) && (newNode = (MCTnode) transpositionTable.get(ID)).colour == opColour) {
                newNode.parent = curNode;
                newNode.move = possibleMove;
                bubbleDataToTheRoot(newNode);
            } else {
                newNode = new MCTnode(possibleMove, curNode, desc, opColour);
                transpositionTable.put(ID, newNode);
            }
            curNode.children.addLast(newNode);
            return newNode;

        } else {
            bestChild();
        }
        opColour = curNode.colour == myColor ? opponentColor : myColor;
        possibleMoves = comp.getAllPossibleTurns(curNode.state, curNode.colour);
    }
}

private void bubbleDataToTheRoot(@NonNull MCTnode maxingNode) {
    assert maxingNode.parent != null;
    while (maxingNode.parent.plays < maxingNode.plays || maxingNode.parent.wins < maxingNode.wins) {
        maxingNode.parent.plays = Math.max(maxingNode.parent.plays, maxingNode.plays);
        maxingNode.parent.wins = Math.max(maxingNode.parent.wins, maxingNode.wins);
        if (maxingNode.parent == root) {
            root.plays = Math.max(root.plays, maxingNode.plays);
            root.wins = Math.max(root.wins, maxingNode.wins);
            break;
        }
        maxingNode = maxingNode.parent;
        assert maxingNode != null;
        assert maxingNode.parent != null;
    }
}

/**
 * Picks best node according to Upper Confidence bounds value
 * @see <a href="https://chessprogramming.wikispaces.com/UCT/">Upper Confidence bounds</a>
 */
private void bestChild() {
    double value;
    bestValue = Double.NEGATIVE_INFINITY;
    bestChild = curNode.children.getFirst();
    for (MCTnode child : curNode.children) {
        value = (child.wins / child.plays) + EXPLORATION_VALUE * Math.sqrt(MUTIPLIER * Math.log(curNode.plays) / child.plays);
        if (value > bestValue) {
            bestValue = value;
            bestChild = child;
        }
    }
    curNode = bestChild;
}

/**
 * One node in Monte-Carlo tree. Equals one possible game state
 */
private class MCTnode implements Serializable {
    /**
     * Color of player on move
     */
    final byte colour;
    /**
     * Game states to I can get from this one
     */
    final ArrayDeque<MCTnode> children = new ArrayDeque<>(34);
    /**
     * Current state of the game
     */
    final byte[][] state;
    /**
     * Node from which I got to this state
     */
    @Nullable MCTnode parent;
    /**
     * Number of wins simulated from this node
     */
    int wins = 0;
    /**
     * Number of plays played from this node
     */
    int plays = 0;
    /**
     * Move which lead to this game {@code state}
     */
    ReversiMove move;

    MCTnode(ReversiMove move, @Nullable MCTnode parent, byte[][] state, byte colour) {
        this.parent = parent;
        this.state = state;
        this.colour = colour;
        this.move = move;
    }

    MCTnode(byte[][] board, byte myColor) {
        this.state = board;
        this.colour = myColor;
    }
}
}