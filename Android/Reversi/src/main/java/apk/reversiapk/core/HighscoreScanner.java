package apk.reversiapk.core;

import android.content.Context;
import android.support.annotation.NonNull;
import apk.reversiapk.core.enums.Constants;
import apk.reversiapk.graphic.activities.Highscore;

import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Scans highscore file and set what in it
 */
public class HighscoreScanner {
private static final Logger LOG = Logger.getLogger(Highscore.class.getName());
private static final String FILE_NAME = "Reversi.score";

/**
 * Writes to file defined number of highest scores
 * Creates new higscore files if there is none
 *
 * @param context
 *         context of activity, which called this function
 * @param winnersName
 *         name of player who won last game on this device
 * @param winnersScore
 *         their score
 */
public void writeToFile(@NonNull Context context, String winnersName, int winnersScore) {
    try {
        File file = new File(context.getExternalFilesDir(null), FILE_NAME);
        FileInputStream fileIn = new FileInputStream(file);
        BufferedInputStream bfIn = new BufferedInputStream(fileIn);
        BufferedReader br = new BufferedReader(new InputStreamReader(bfIn));
        String lastName;
        int lastScore;
        StringBuilder data = new StringBuilder();

        // Adds names and scores to one string in descending order
        for (int i = 0; i < Constants.MAX_HIGHSCORE_LINES.getValue(); ++i) {
            lastName = br.readLine();
            if (lastName == null) {
                data.append(winnersName).append("\n").append(winnersScore).append("\n");
                break;
            }
            lastScore = Integer.parseInt(br.readLine());
            if (winnersScore > lastScore) {
                data.append(winnersName).append("\n").append(winnersScore).append("\n");
                winnersName = lastName;
                winnersScore = lastScore;
            } else {
                data.append(lastName).append("\n").append(lastScore).append("\n");
            }
        }

        br.close();
        bfIn.close();
        fileIn.close();

        //Writes new highscore ordere in file
        writeData(context, data.toString());

    } catch (FileNotFoundException e) {
        // Happens if file was not found
        // File must be empty then and player score the highest ever achieved
        writeData(context, "" + winnersName + "\n" + winnersScore + "\n");

    } catch (IOException e) {
        LOG.log(Level.SEVERE, "Couldn't rearange highscore", e);
    }
}

/**
 * Creates file with highscore and writes into in
 *
 * @param context
 *         context of activity, which called this function
 * @param data
 *         String to be written in the file
 */
private void writeData(@NonNull Context context, @NonNull final String data) {
    try {
        File file = new File(context.getExternalFilesDir(null), FILE_NAME);
        if (!file.exists() && !file.createNewFile()) {
            return;
        }
        FileOutputStream stream = new FileOutputStream(file);
        BufferedOutputStream out = new BufferedOutputStream(stream);

        out.write(data.getBytes());
        out.flush();

        out.close();
        stream.close();

    } catch (IOException e) {
        LOG.log(Level.SEVERE, e.getMessage(), e);
    }
}

/**
 * Reads whole {@code FILE_NAME}
 *
 * @param context
 *         context of activity, which called this function
 * @return returns whole content of file in one string and desired format
 */
@NonNull
public String readWholeFile(@NonNull Context context) {
    String ret = "";
    try {
        File file = new File(context.getExternalFilesDir(null), FILE_NAME);
        FileInputStream fileIn = new FileInputStream(file);
        InputStreamReader inputStreamReader = new InputStreamReader(new BufferedInputStream(fileIn));
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
        String loadedName;
        String loadedScore;
        StringBuilder stringBuilder = new StringBuilder();

        for (int i = 1; i <= Constants.MAX_HIGHSCORE_LINES.getValue(); ++i) {
            if ((loadedName = bufferedReader.readLine()) == null) {
                break;
            }

            loadedScore = bufferedReader.readLine();
            stringBuilder.append(i).append(".").append(" ").append(loadedName).append("\n").append(loadedScore).append("\n\n");
        }

        bufferedReader.close();
        inputStreamReader.close();
        fileIn.close();

        return stringBuilder.toString();
    } catch (IOException e) {
        LOG.log(Level.SEVERE, "Couldn't read highscore", e);
    }
    return ret;
}
}