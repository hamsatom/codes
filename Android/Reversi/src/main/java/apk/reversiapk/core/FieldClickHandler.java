package apk.reversiapk.core;

import android.support.annotation.Nullable;
import android.view.View;
import apk.reversiapk.graphic.Field;

import java.io.Serializable;
import java.util.logging.Logger;
import java.util.logging.StreamHandler;

/**
 * Custom on click listener that remember position of last pressed field
 */
public final class FieldClickHandler implements View.OnClickListener, Serializable {
private static final Logger LOG = Logger.getLogger(FieldClickHandler.class.getName());
/**
 * Last move of user
 */
@Nullable private static volatile ReversiMove lastMove;

public FieldClickHandler() {
    LOG.addHandler(new StreamHandler());
}

/**
 * {@inheritDoc}
 */
@Override
public void onClick(View v) {
    Field field = (Field) v;
    LOG.info(field.getPositionX() + " " + field.getPositionY());
    lastMove = new ReversiMove(field.getPositionX(), field.getPositionY());
}

/**
 * @return last move of user
 */
@Nullable
public ReversiMove getLastMove() {
    return lastMove;
}

/**
 * Erases last move from user. Come in handy when user clicked on possible place before it's move so this move shouldn't be taken
 */
public void nullLastMve() {
    lastMove = null;
}
}