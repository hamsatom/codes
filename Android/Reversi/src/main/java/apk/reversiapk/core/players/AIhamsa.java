package apk.reversiapk.core.players;

import android.support.annotation.NonNull;
import apk.reversiapk.core.ReversiMove;
import apk.reversiapk.core.enums.Colour;
import apk.reversiapk.core.enums.Constants;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.logging.Logger;
import java.util.logging.StreamHandler;

public class AIhamsa extends AI {
private static final Logger LOG = Logger.getLogger(AIhamsa.class.getName());
private static final String NAME = "Hamsa BOT";
/**
 * Mozne tahy oponenta
 */
private final ArrayDeque<ReversiMove> oppontsPossibleMoves = new ArrayDeque<>(34);
/**
 * Hodnoceni dulezitosti poli na desce
 */
private final byte[][] fieldRating = {{99, -8, 8, 6, 6, 8, -8, 99}, {-8, -24, -4, -3, -3, -4, -24, -8}, {8, -4, 7, 4, 4, 7,
        -4, 8}, {6, -3, 4, 0, 0, 4, -3, 6}, {6, -3, 4, 0, 0, 4, -3, 6}, {8, -4, 7, 4, 4, 7, -4, 8}, {-8, -24, -4, -3,
        -3, -4, -24, -8}, {99, -8, 8, 6, 6, 8, -8, 99}};
/**
 * moje barva
 */
private byte thisColour;
/**
 * barva opponenta
 */
private byte oppponentsColour;
/**
 * Hraci deska vytvorena z (@param board) Slouzi k simulaci tahu
 */
private byte[][] fields;
/**
 * List s platnymi tahy Postupne zmensen na nejlepsi tah
 */
private ArrayList<ReversiMove> validMoves;

public AIhamsa() {
    LOG.addHandler(new StreamHandler());
}

/**
 * @return jmeno tohoto hrace
 */

@NonNull
@Override
public String getName() {
    return NAME;
}

/**
 * Funkce nastavuje lokalni promene. Nachazi muj kamen ten predava dalsim
 * metoda. Vola metody na vybrani nejlepsiho tahu.
 *
 * @param board
 * @return muj dalse tah
 */
@Override
public ReversiMove makeNextMove(@NonNull byte[][] board) {
    thisColour = getMyColor();
    oppponentsColour = getOpponentColor();
    fields = board;
    oppontsPossibleMoves.clear();
    validMoves = getPossibleMoves();

    adjustBoardRating();
    chooseByFieldRating();
    numberOfOpponentsMoves(board);
    stableEnemyDiscs(board);
    myStableDiscs(board);
    evalFrontier(board);

    //returns last stored move
    //last move is usually better for unknown reasons
    return validMoves.get(validMoves.size() - 1);
}

/**
 * Dynamically changes position rating if corner is occupied
 */
private void adjustBoardRating() {
    changeValuesAroundCorner(0, Constants.BOARD_ROWS.getValue() - 1, 1, -1);
    changeValuesAroundCorner(Constants.BOARD_COLUMNS.getValue() - 1, Constants.BOARD_ROWS.getValue() - 1, -1, -1);
    changeValuesAroundCorner(0, 0, 1, 1);
    changeValuesAroundCorner(Constants.BOARD_COLUMNS.getValue() - 1, 0, -1, 1);
}

/**
 * Changes values aroung one corner
 *
 * @param cornerX
 *         corner X
 * @param cornerY
 *         corner Y
 * @param x
 *         direction x
 * @param y
 *         direction y
 */
private void changeValuesAroundCorner(int cornerX, int cornerY, int x, int y) {
    if (fields[cornerX][cornerY] == Colour.EMPTY_SQUARE.getValue()) {
        return;
    }
    fieldRating[cornerX + x][cornerY] = (byte) 7;
    fieldRating[cornerX][cornerY + y] = (byte) 7;
    fieldRating[cornerX + x][cornerY + y] = (byte) -7;
}

/**
 * Nachazi mujo kamen na desce
 *
 * @param board
 */
private void findMyStone(@NonNull byte[][] board) {
    byte y;
    for (byte x = 0; x < board.length; ++x) {
        for (y = 0; y < board.length; ++y) {
            if (board[x][y] == thisColour) {
                findEnemyStone(x, y, board);
            }
        }
    }
}

/**
 * Hleda oponentovi kameny na desce. Sestavuje vektor (nepriteluv kamen -
 * muj kamen). Vola metodu na hledani prazdneho mista.
 *
 * @param x
 *         souradnice meho kamene
 * @param y
 *         souradnice meho kamene
 * @param board
 *         hraci deska na ktere se zrovna pohybujeme
 */
private void findEnemyStone(byte x, byte y, @NonNull byte[][] board) {
    byte vectorX, vectorY;
    for (byte i = (byte) (x - 1); i <= x + 1; ++i) {
        if (i < 0 || i >= board.length) {
            continue;
        }
        for (byte j = (byte) (y - 1); j <= y + 1; ++j) {
            if (j < 0 || j >= board.length) {
                continue;
            }
            if (board[i][j] == oppponentsColour) {
                vectorX = (byte) (i - x);
                vectorY = (byte) (j - y);
                findEmptyPlace(x, y, vectorX, vectorY, board);
            }
        }
    }
}

/**
 * Pomoci smeroveho vektoru hleda prazdne misto na desce. Prohlizene pole je
 * x krat vektor + souradnice meho kamene. Pokud je prohlizene pole prazdne
 * jsou prohlizene souradnice pridany do moznych tahu.
 *
 * @param myStoneX
 *         x souradnice meho kamene
 * @param myStoneY
 *         y souradnice meho kamene
 * @param vectorX
 *         vektor pro x souradnici
 * @param vectorY
 *         vektor pro y souradnici
 * @param board
 *         Hraci deska ne ktere hledam
 */
private void findEmptyPlace(byte myStoneX, byte myStoneY, byte vectorX, byte vectorY, @NonNull byte[][] board) {
    byte observingX;
    byte observingY;

    for (byte i = 1; i < board.length; ++i) {

        observingX = (byte) (myStoneX + i * vectorX);
        observingY = (byte) (myStoneY + i * vectorY);

        if (observingX >= board.length || observingX < 0 || observingY >= board.length || observingY < 0) {
            return;
        }
        if (board[observingX][observingY] == thisColour) {
            return;
        }
        if (board[observingX][observingY] == Colour.EMPTY_SQUARE.getValue()) {
                oppontsPossibleMoves.add(new ReversiMove(observingX, observingY));
            return;
        }
    }
}

/**
 * Vybira tah na pole s nejvetsim hodnocenim.
 */
private void chooseByFieldRating() {
    byte bigggestFieldRating = Byte.MIN_VALUE;
    ArrayList<ReversiMove> moveCopy = (ArrayList<ReversiMove>) validMoves.clone();

    for (ReversiMove move : moveCopy) {
        if (fieldRating[move.getX()][move.getY()] == bigggestFieldRating) {
            validMoves.add(move);
        }
        if (fieldRating[move.getX()][move.getY()] > bigggestFieldRating) {
            bigggestFieldRating = fieldRating[move.getX()][move.getY()];
            validMoves.clear();
            validMoves.add(move);
        }
    }
}

    /**
     * Vybira tahy podle nejmensiho poctu prazdnych poli okolo.
     *
     * @param board Hraci deska ne ktere hledam
     */
    private void evalFrontier(@NonNull byte[][] board) {
        byte maxEmptyPlaces = Byte.MAX_VALUE;
        byte maxOddCount = Byte.MAX_VALUE;
        byte emptyPlaces, x, y;
        int i, j;

        ArrayList<ReversiMove> moveCopy = (ArrayList<ReversiMove>) validMoves.clone();

        for (ReversiMove move : moveCopy) {
            emptyPlaces = 0;
            x = move.getX();
            y = move.getY();

            for (i = x - 1; i <= x + 1; ++i) {
                if (i < 0 || i >= board.length) {
                    continue;
                }
                for (j = y - 1; j <= y + 1; ++j) {
                    if (j < 0 || j >= board.length) {
                        continue;
                    }
                    if (board[i][j] == Colour.EMPTY_SQUARE.getValue()) {
                        ++emptyPlaces;
                    }
                }
            }
            if (emptyPlaces == maxEmptyPlaces) {
                validMoves.add(move);
            }

            if (emptyPlaces < maxEmptyPlaces) {
                maxEmptyPlaces = emptyPlaces;
                validMoves.clear();
                validMoves.add(move);

            }
            if (emptyPlaces % 2 != 0 && emptyPlaces < maxOddCount && emptyPlaces < 10) {
                validMoves.clear();
                validMoves.add(move);

                maxOddCount = emptyPlaces;
                if (maxEmptyPlaces > maxOddCount) {
                    maxEmptyPlaces = (byte) (maxOddCount - 2);
                } else {
                    maxEmptyPlaces -= 2;
                }
            }
        }
    }

/**
 * Pocita kolik stabilnich disku bych mel po zahrani moznych tahu. Stabilni
 * disk nejde dale otocit.
 *
 * @param board
 */
private void myStableDiscs(@NonNull byte[][] board) {
    ArrayList<ReversiMove> moveCopy = (ArrayList<ReversiMove>) validMoves.clone();
    byte maxStableDiscs = Byte.MIN_VALUE;
    byte stableDiscsCount;
    byte x;
    byte y;
    for (ReversiMove move : moveCopy) {
        stableDiscsCount = 0;
        mapUpdate(move.getX(), move.getY(), getMyColor());
        for (x = 0; x < board.length; ++x) {
            for (y = 0; y < board.length; ++y) {
                if (board[x][y] != getMyColor()) {
                    continue;
                }
                if (areStableDiscsAround(x, fields, y, getMyColor()) || areFilledAllDirections(x, y) || isAtEdge(move.getX(), move.getY())) {
                    ++stableDiscsCount;
                }
            }
        }
        if (stableDiscsCount == maxStableDiscs) {
            validMoves.add(move);
        }
        if (stableDiscsCount > maxStableDiscs) {
            maxStableDiscs = stableDiscsCount;
            validMoves.clear();
            validMoves.add(move);
        }
        fields = board;
    }
}

/**
 * Pocita kolik stabilnich disku bude mit souper po zahrani urciteho tahu.
 *
 * @param board
 */
private void stableEnemyDiscs(@NonNull byte[][] board) {
    ArrayList<ReversiMove> moveCopy = (ArrayList<ReversiMove>) validMoves.clone();
    byte leastOpponentsStablesCount = Byte.MAX_VALUE;
    byte stableDiscsCount;
    byte x;
    byte y;
    for (ReversiMove move : moveCopy) {
        stableDiscsCount = 0;
        mapUpdate(move.getX(), move.getY(), getMyColor());
        for (x = 0; x < board.length; ++x) {
            for (y = 0; y < board.length; ++y) {
                if (board[x][y] == getOpponentColor() && areStableDiscsAround(x, fields, y, getOpponentColor()) ||
                        areFilledAllDirections(x, y) || isAtEdge(move.getX(), move.getY())) {
                    ++stableDiscsCount;
                }
            }
        }
        if (stableDiscsCount == leastOpponentsStablesCount) {
            validMoves.add(move);
        }
        if (stableDiscsCount < leastOpponentsStablesCount) {
            leastOpponentsStablesCount = stableDiscsCount;
            validMoves.clear();
            validMoves.add(move);
        }
        fields = board;
    }
}


    /**
 * Hodnoti jestli se jedna o stabilni disk podle disku okolu.
 *
 * @param x
 *         x souradnice tahu.
 * @param board
 *         hraci deska na ktere hledam.
 * @param y
 *         y souradnice tahu.
 * @param colour
 *         colour toho ci stabilni hledam.
 * @return false kdyz neni vedle stabilnich disku.
 */
private boolean areStableDiscsAround(byte x, @NonNull byte[][] board, byte y, byte colour) {
    byte j;
    for (byte i = (byte) (x - 1); i <= x + 1; ++i) {
        if (i < 0 || i >= board.length) {
            continue;
        }
        for (j = (byte) (y - 1); j <= y + 1; ++j) {
            if (j < 0 || j >= board.length || (i == x && j == y)) {
                continue;
            }
            if (board[i][j] == colour && !isAtEdge(i, j) && !areFilledAllDirections(i, j)) {
                return false;
            }
        }
    }
    return true;
}


/**
 * Hleda jestli jsou pro dany kamen zaplnene vsechny hraci smery.
 *
 * @param i
 *         x souradnice kamene.
 * @param j
 *         y souradnice kamene.
 * @return true kdyz jsou vsechny smery zaplnene.
 */
private boolean areFilledAllDirections(byte i, byte j) {
    return filledDirection(1, 0, i, j) &&
           filledDirection(1, 1, i, j) &&
           filledDirection(0, 1, i, j) &&
           filledDirection(0, -1, i, j) &&
           filledDirection(-1, -1, i, j) &&
           filledDirection(-1, 0, i, j) &&
           filledDirection(-1, 1, i, j);
}

/**
 * Hodnoti jestli je disk u kraje.
 *
 * @return true kdyj je u kraje.
 */
private boolean isAtEdge(byte x, byte y) {
    return x == 0 || x == fields.length - 1 || y == 0 || y == fields.length - 1;
}

/**
 * Kontroluje jestli je prava strana radku zaplena.
 *
 * @return true kdy je je prava strana radku zaplena.
 */
private boolean filledDirection(int dirX, int dirY, int x, int y) {
    int currentX;
    int currentY;

    for (byte i = 1; i < fields.length; ++i) {
        currentX = x + dirX;
        currentY = y + dirY;
        if (currentX >= fields.length || currentX < 0) {
            return true;
        }
        if (currentY >= fields.length || currentY < 0) {
            return true;
        }
        if (fields[currentX][currentY] == Colour.EMPTY_SQUARE.getValue()) {
            return false;
        }
    }
    return false;
}

/**
 * Hodnoti moje tahy podle poctu moznych tahu oponenta.
 *
 * @param board
 *         deska na ktere hledam.
 */
private void numberOfOpponentsMoves(byte[][] board) {
    ArrayList<ReversiMove> moveCopy = (ArrayList<ReversiMove>) validMoves.clone();

    byte maxiAmountOfEnemyStones = Byte.MAX_VALUE;

    for (ReversiMove moznyTah : moveCopy) {
        byte size = 0;
        fields = board;
        thisColour = getMyColor();
        oppponentsColour = getOpponentColor();
        mapUpdate(moznyTah.getX(), moznyTah.getY(), thisColour);

        changeColour();
        findMyStone(fields);

        for (ReversiMove element : oppontsPossibleMoves) {
            if (element != null) {
                ++size;
            }
        }

        if (size == maxiAmountOfEnemyStones) {
            validMoves.add(moznyTah);
        }

        if (size < maxiAmountOfEnemyStones) {
            maxiAmountOfEnemyStones = size;

            validMoves.clear();
            validMoves.add(moznyTah);
        }
        oppontsPossibleMoves.clear();
    }
}

/**
 * Nasimuluje mapu po zahrani urciteho tahu.
 *
 * @param coordinateX
 *         x souradnice tahu.
 * @param coordinateY
 *         y souradnice tahu.
 * @param colour
 *         colour toho kdo dava kamen.
 */
private void mapUpdate(int coordinateX, int coordinateY, byte colour) {
    fields[coordinateX][coordinateY] = colour;
    byte vectorX, vectorY, j;

    for (byte a = (byte) (coordinateX - 1); a <= coordinateX + 1; ++a) {
        if (a < 0 || a >= fields.length) {
            continue;
        }
        for (j = (byte) (coordinateY - 1); j <= coordinateY + 1; ++j) {
            if (j < 0 || j >= fields.length || fields[a][j] != oppponentsColour) {
                continue;
            }
            vectorX = (byte) (a - coordinateX);
            vectorY = (byte) (j - coordinateY);
            updateDirection(coordinateX, coordinateY, colour, vectorX, vectorY);
        }
    }
}

private void updateDirection(int coordinateX, int coordinateY, byte colour, byte vectorX, byte vectorY) {
    byte currentX;
    byte currentY;
    byte newX;
    byte newY;
    for (byte i = 1; i < fields.length; ++i) {

        currentX = (byte) (coordinateX + i * vectorX);
        currentY = (byte) (coordinateY + i * vectorY);

        if (currentX >= fields.length || currentX < 0 || currentY >= fields.length || currentY < 0) {
            break;
        }

        if (fields[currentX][currentY] == Colour.EMPTY_SQUARE.getValue()) {
            break;
        }
        if (fields[currentX][currentY] != colour) {
            continue;
        }
        for (byte b = 1; b < fields.length; ++b) {
            newX = (byte) (coordinateX + b * vectorX);
            newY = (byte) (coordinateY + b * vectorY);

            if (newX >= fields.length || newX < 0 || newY >= fields.length || newY < 0) {
                break;
            }

            if (fields[newX][newY] == oppponentsColour) {
                fields[newX][newY] = colour;
            }
        }
    }
}

/**
 * Zmeni barvu hrace
 */
private void changeColour() {
    byte tmp = thisColour;
    thisColour = oppponentsColour;
    oppponentsColour = tmp;
}

}

