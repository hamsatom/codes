package apk.reversiapk.core.players;

import android.support.annotation.NonNull;
import apk.reversiapk.core.ReversiMove;
import apk.reversiapk.core.enums.Colour;
import apk.reversiapk.core.enums.Constants;

import java.io.Serializable;
import java.util.*;

/**
 * Contains variables and methods for board evaluate. getting possible moves and turning stones on the board
 */
public abstract class AIwithMemory extends AI {
static final long TIME_LIMIT = 1000000000L;
/**
 * Instance of computer class capable of computing everything concerning
 * turns.
 * This means making and unmaking move on board, evaluating game state and getting all possible moves
 */
final TurnComputer comp = new TurnComputer(Constants.BOARD_COLUMNS.getValue(), Colour.EMPTY_SQUARE.getValue());
/**
 * Sorts {@link ReversiMove} by scores from highest to lowest
 */
final MaxComparator maxComparator = new MaxComparator();
/**
 * Sorts {@link ReversiMove} by scores from lowest to highest
 */
final MinComparator minComparator = new MinComparator();
/**
 * Transposition table used to store all searched game states so they don't need to be searched again. Needs to be cleared after each turn in {@link apk.reversiapk.core.ReversiController} as it'S
 * size can grow to 50 000 entries. If table weren't cleared after game, {@link AIwithMemory} that won the game couldn't be serialized and passed to
 * {@link apk.reversiapk.graphic.activities.GameActivity}
 */
public HashMap<String, Object> transpositionTable;
/**
 * Colour of current player
 */
byte myColor;
/**
 * Colour of opponent
 */
byte opponentColor;
/**
 * Best move found
 */
ReversiMove bestMove;
/**
 * Possible moves from initial game state. {@code makeNextMove} is called to playe from this game state
 */
ArrayList<ReversiMove> moves;
/**
 * Time when {@code makeNextMove} was called
 */
long timeStart;
/**
 * ID of game state
 */
String ID;
/**
 * Value of the best move
 */
double bestValue;
/**
 * Moves possible from initial game state
 */
ArrayList<ReversiMove> possibleMoves;
/**
 * game state
 */
byte[][] desc;

private StringBuilder builder;
private byte idX, idY;

/**
 * Turns values on board to String of length {@code boardSize*boardSoze}
 *
 * @param board
 *         game state
 * @return ID of this game state
 */
@NonNull
String boardId(@NonNull byte[][] board) {
    builder = new StringBuilder(64);
    for (idX = 0; idX < board.length; ++idX) {
        for (idY = 0; idY < board.length; ++idY) {
            builder.append(board[idX][idY]);
        }
    }
    return builder.toString();
}

/**
 * Possible statuses of discs while evaluating turn. Initial status should be {@code TBD}
 */
private enum DiscStatus {
    YET_TO_PROCESS,
    STABLE,
    TBD,
    UNSTABLE
}

/**
 * One game state of game
 */
class Node implements Serializable {
    /**
     * Moves that can be played from this position
     */
    final ArrayList<ReversiMove> possibleMoves;
    /**
     * Depth from which I got score
     */
    int depth;
    /**
     * Value of the move
     */
    double score;

    Node(int depth, double score, ArrayList<ReversiMove> possibleMoves) {
        this.depth = depth;
        this.score = score;
        this.possibleMoves = possibleMoves;
    }
}

/**
 * Sorts {@link ReversiMove} by scores from highest to lowest
 */
private class MaxComparator implements Comparator<ReversiMove>, Serializable {

    @Override
    /**
     * {@inheritDoc}
     */ public int compare(@NonNull ReversiMove reversiMove, @NonNull ReversiMove t1) {
        return (int) (t1.score - reversiMove.score);
    }
}

/**
 * Sorts {@link ReversiMove} by scores from lowest to highest
 */
private class MinComparator implements Comparator<ReversiMove>, Serializable {

    @Override
    /**
     * {@inheritDoc}
     */ public int compare(@NonNull ReversiMove reversiMove, @NonNull ReversiMove t1) {
        return (int) (reversiMove.score - t1.score);
    }
}

/**
 * The instance of this class complements every game. It is responsible for
 * computing values, game states, etc..
 */
class TurnComputer implements Serializable {
    /**
     * Length and width of {@code board} because {@code board} should by symmetrical
     **/
    final int boardWidth;
    /**
     * Stores all discs flipped when called {@code flipDiscs}
     */
    final ArrayDeque<Integer> flippedDiscs = new ArrayDeque<>(485);
    /**
     * Compares moves according to their {@code dynamicWorth}
     */
    final TurnComparator comparator = new TurnComparator();
    // Constants used to evaluate board
    // inspired in this article https://kartikkukreja.wordpress.com/2013/03/30/heuristic-function-for-reversiothello/
    private static final double CORNER_DISCS_MULTIPLIER = 20043.1;
    private static final double STABLE_DISCS_MULTIPLIER = 382.026;
    private static final double MOBILITY_MULTIPLIER = 78.922;
    private static final double FRONTIERS_MULTIPLIER = 74.936;
    private static final double COIN_PARITY_MULTIPLIER = 10;
    private static final double STATIC_DIFFERENCE_MULTIPLIER = 10;
    /**
     * Values used to statically evaluate position
     */
    private final byte[] STATIC_WORTH = {99, -8, 8, 6, 6, 8, -8, 99, -8, -24, -4, -3, -3, -4, -24, -8, 8, -4, 7, 4, 4, 7, -4, 8, 6, -3, 4, 0, 0, 4, -3, 6, 6, -3, 4, 0, 0, 4, -3, 6, 8, -4, 7, 4, 4,
            7, -4, 8, -8, -24, -4, -3, -3, -4, -24, -8, 99, -8, 8, 6, 6, 8, -8, 99};
    // Used to loop through possible directions
    private final byte[] OFFSET_A = {-1, -1, 0, 1, 1, 1, 0, -1};
    private final byte[] OFFSET_B = {0, 1, 1, 1, 0, -1, -1, -1};
    /**
     * Colour of empty field on board
     */
    private final byte emptyColor;
    @NonNull private final DiscStatus[] myStableDiscs, opponentsStableDiscs;
    /**
     * Coordinates of corners on the {@code board}
     */
    @NonNull private final int[] corners;
    /**
     * Game state
     */
    byte[][] board;
    /**
     * Dynamically changed board rating
     */
    private byte[] dynamicWorth = {99, -8, 8, 6, 6, 8, -8, 99, -8, -24, -4, -3, -3, -4, -24, -8, 8, -4, 7, 4, 4, 7, -4, 8, 6, -3, 4, 0, 0, 4, -3, 6, 6, -3, 4, 0, 0, 4, -3, 6, 8, -4, 7, 4, 4, 7, -4,
            8, -8, -24, -4, -3, -3, -4, -24, -8, 99, -8, 8, 6, 6, 8, -8, 99};
    /**
     * Colour of my stones
     */
    private byte myColor;
    /**
     * Colour of opponent stones
     */
    private byte opponentColor;
    /**
     * Variables for each turn (set to initial value whenever another evaluation is needed)
     */
    private double numberOfMyDiscs, numberOfMyStableDiscs, numberOfMyCornerDiscs, numberOfMyFrontierDiscs, numberOfOpponentDiscs, numberOfOpponentStableDiscs, numberOfOpponentCornerDiscs,
            numberOfOpponentFrontierDiscs;
    private int staticValue;
    private ArrayList<ReversiMove> moves;
    private byte offsetXTurns, offsetYTurns;
    private byte yTurns, iTurns, xTurns;
    private byte offsetAFlip;
    private byte offsetBFlip;
    private byte countFlip;
    private int newXFlip;
    private int newYFlip;
    private byte iFlip;
    private byte[][] actualBoard;
    private byte actualColorValid;
    private byte countValid;
    private int aValid;
    private int bValid;
    private byte colorTurns;
    private int xUnflip;
    private int yUnflip;
    private boolean firstUnflip;
    private double valueEval;
    private int opponentPossibleTurnsMobility;
    private int myPossibleTurnsMobility;
    private int iAsses;
    private int jAsses;
    private int maxAsses;
    private byte lAsses;
    private boolean assesAll;
    private int newXAsses;
    private int newYAsses;
    private byte colorAsses, iAssesDisc;
    private int offsetXImport, offsetYImport;
    private byte colImport;
    private boolean retImport;
    private int newYfindY, addressFindY;
    private boolean ret;
    private int addressFindX, newXfindX;
    private boolean retFindX;
    private int addressStable;
    private DiscStatus discStable;
    private byte offsetXExamine, offsetYExamine, iExamine;
    private int aXExamine, aYExamine, bXExamine, bYExamine;
    private int startXLine, startYLine;


    /**
     * Sets important values whenever the game starts
     *
     * @param boardLength
     *         the length of the board
     * @param emptyColor
     *         the mask for empty color
     */
    TurnComputer(int boardLength, byte emptyColor) {
        this.boardWidth = boardLength;
        this.emptyColor = emptyColor;
        this.myStableDiscs = new DiscStatus[boardWidth * boardWidth];
        this.opponentsStableDiscs = new DiscStatus[boardWidth * boardWidth];
        corners = new int[]{0, boardWidth - 1};
    }

    /**
     * Returns a list of possible moves available to the player of given color
     *
     * @param actualBoard
     *         the current game state
     * @param color
     *         the color of player we want to consider
     * @return list of all possible moves
     */
    ArrayList<ReversiMove> getAllPossibleTurns(byte[][] actualBoard, byte color) {
        moves = new ArrayList<>(34);
        this.actualBoard = actualBoard;
        this.colorTurns = color;

        for (xTurns = 0; xTurns < boardWidth; ++xTurns) {
            for (yTurns = 0; yTurns < boardWidth; ++yTurns) {
                if (actualBoard[xTurns][yTurns] != emptyColor) {
                    continue;
                }
                for (iTurns = 0; iTurns < boardWidth; ++iTurns) {
                    offsetXTurns = OFFSET_A[iTurns];
                    offsetYTurns = OFFSET_B[iTurns];
                    if (isLineValid()) {
                        moves.add(new ReversiMove(xTurns, yTurns));
                    }
                }
            }
        }
        this.board = actualBoard;
        adjustBoardRating();
        Collections.sort(moves, comparator);
        return moves;
    }

    /**
     * Checks the validity of the line, whether it is possible to play there
     *
     * @return recovered board
     */
    private boolean isLineValid() {
        countValid = 1;
        aValid = xTurns + offsetXTurns;
        bValid = yTurns + offsetYTurns;

        while (conforms(aValid, bValid)) {
            actualColorValid = actualBoard[aValid][bValid];
            if (actualColorValid == emptyColor) {
                return false;
            } else if (actualColorValid == colorTurns) {
                return countValid != 1;
            }
            aValid += offsetXTurns;
            bValid += offsetYTurns;
            ++countValid;
        }
        return false;
    }

    /**
     * Returns the board to its previous state
     *
     * @param toUnflip
     *         a board to recover
     * @param color
     *         a player's color (who played)
     * @return recovered board
     */
    byte[][] unflipDiscs(byte[][] toUnflip, byte color) {
        firstUnflip = true;

        while (true) {
            yUnflip = flippedDiscs.removeLast();
            if (yUnflip == -1) {
                return toUnflip;
            }
            xUnflip = flippedDiscs.removeLast();
            if (firstUnflip) {
                toUnflip[xUnflip][yUnflip] = emptyColor;
                firstUnflip = false;
            } else {
                toUnflip[xUnflip][yUnflip] = color;
            }
        }
    }

    /**
     * Returns the state of board after given move
     *
     * @param toFlip
     *         a board to flip
     * @param x
     *         x coordinates of the move
     * @param y
     *         y coordinates of the move
     * @param color1
     *         color of the player, whose turn it is
     * @param color2
     *         color of the other player
     * @return game state after move
     */
    byte[][] flipDiscs(byte[][] toFlip, int x, int y, byte color1, int color2) {
        this.flippedDiscs.add(-1);
        toFlip[x][y] = color1;
        for (iFlip = 0; iFlip < boardWidth; ++iFlip) {
            offsetAFlip = OFFSET_A[iFlip];
            offsetBFlip = OFFSET_B[iFlip];
            countFlip = 1;
            newXFlip = x + offsetAFlip;
            newYFlip = y + offsetBFlip;
            while (conforms(newXFlip, newYFlip) && toFlip[newXFlip][newYFlip] == color2) {
                ++countFlip;
                newXFlip += offsetAFlip;
                newYFlip += offsetBFlip;
            }

            if (conforms(newXFlip, newYFlip) && toFlip[newXFlip][newYFlip] == color1) {
                while (countFlip - 1 > 0) {
                    --countFlip;
                    newXFlip -= offsetAFlip;
                    newYFlip -= offsetBFlip;
                    toFlip[newXFlip][newYFlip] = color1;
                    flippedDiscs.add(newXFlip);
                    flippedDiscs.add(newYFlip);
                }

            }
        }
        flippedDiscs.add(x);
        flippedDiscs.add(y);

        return toFlip;
    }

    /**
     * Returns the turn value for the complete evaluation. I was heavily inspired there:
     *
     * @param played
     *         the game state
     * @param myColor
     *         the color of the player whose turn it is
     * @param opponentColor
     *         the color of the other player
     * @return turn value
     * @see <a href="https://kartikkukreja.wordpress.com/2013/03/30/heuristic-function-for-reversiothello/">Function</a>
     */
    double evaluateTurn(byte[][] played, byte myColor, byte opponentColor) {
        this.myColor = myColor;
        this.opponentColor = opponentColor;
        numberOfMyStableDiscs = numberOfOpponentStableDiscs = numberOfMyDiscs = numberOfOpponentDiscs = numberOfMyCornerDiscs = numberOfOpponentCornerDiscs = numberOfMyFrontierDiscs =
        numberOfOpponentFrontierDiscs = staticValue = 0;
        Arrays.fill(this.myStableDiscs, DiscStatus.TBD);
        Arrays.fill(this.opponentsStableDiscs, DiscStatus.TBD);
        this.board = played;
        adjustBoardRating();

        valueEval = 0;

        if (importantEdgeDiscs(myColor, myStableDiscs) | importantEdgeDiscs(opponentColor, opponentsStableDiscs)) {
            assesAll = true;
            assessAllDiscs();
            valueEval += getStabilityScore();
        } else {
            assesAll = false;
            assessAllDiscs();
        }
        valueEval += getParityScore();
        valueEval += getStaticScore();
        valueEval += getFrontiersScore();
        valueEval += getMobilityScore();

        return valueEval;
    }

    /**
     * Return evaluation of frontiers
     *
     * @return evaluation of frontiers
     */
    private double getFrontiersScore() {
        if (numberOfMyFrontierDiscs > numberOfOpponentFrontierDiscs) {
            return numberOfMyFrontierDiscs * (-100) / (numberOfMyFrontierDiscs + numberOfOpponentFrontierDiscs) * FRONTIERS_MULTIPLIER;
        } else if (numberOfOpponentCornerDiscs > numberOfMyDiscs) {
            return numberOfOpponentFrontierDiscs * (100) / (numberOfMyFrontierDiscs + numberOfOpponentFrontierDiscs) * FRONTIERS_MULTIPLIER;
        }
        return 0;
    }

    /**
     * Returns evaluation of static value
     *
     * @return evaluation of static value
     */
    private double getStaticScore() {
        return staticValue * STATIC_DIFFERENCE_MULTIPLIER;
    }

    /**
     * Returns evaluation of stability
     *
     * @return evaluation of stability
     */
    private double getStabilityScore() {
        return ((numberOfMyStableDiscs - numberOfOpponentStableDiscs) * STABLE_DISCS_MULTIPLIER) + ((numberOfMyCornerDiscs - numberOfOpponentCornerDiscs) * CORNER_DISCS_MULTIPLIER);
    }

    /**
     * Returns evaluation of parity
     *
     * @return evaluation of parity
     */
    private double getParityScore() {
        if (numberOfMyDiscs > numberOfOpponentDiscs) {
            return numberOfMyDiscs * (100) / (numberOfMyDiscs + numberOfOpponentDiscs) * COIN_PARITY_MULTIPLIER;
        } else if (numberOfOpponentCornerDiscs > numberOfMyDiscs) {
            return numberOfOpponentDiscs * (-100) / (numberOfMyDiscs + numberOfOpponentDiscs) * COIN_PARITY_MULTIPLIER;
        }
        return 0;
    }

    /**
     * Returns evaluation of mobility
     *
     * @return evaluation of mobility
     */
    private double getMobilityScore() {
        myPossibleTurnsMobility = getAllPossibleTurns(board, myColor).size();
        opponentPossibleTurnsMobility = getAllPossibleTurns(board, opponentColor).size();
        if (myPossibleTurnsMobility > opponentPossibleTurnsMobility) {
            return (myPossibleTurnsMobility) * (100) / (myPossibleTurnsMobility + opponentPossibleTurnsMobility) * MOBILITY_MULTIPLIER;
        } else if (opponentPossibleTurnsMobility > myPossibleTurnsMobility) {
            return (opponentPossibleTurnsMobility) * (-100) / (myPossibleTurnsMobility + opponentPossibleTurnsMobility) * MOBILITY_MULTIPLIER;
        }
        return 0;
    }

    /**
     * Assesses discs in a spiral manner
     */
    private void assessAllDiscs() {
        for (lAsses = 0; lAsses < boardWidth / 2; ++lAsses) {
            maxAsses = boardWidth - 1 - lAsses;
            for (iAsses = lAsses; iAsses < maxAsses; ++iAsses) {
                if (conforms(iAsses, lAsses)) {
                    assessDisc(iAsses, lAsses);
                }
            }
            for (jAsses = lAsses; jAsses < maxAsses; ++jAsses) {
                if (conforms(maxAsses, jAsses)) {
                    assessDisc(maxAsses, jAsses);
                }
            }
            for (iAsses = maxAsses; iAsses > lAsses; --iAsses) {
                if (conforms(iAsses, maxAsses)) {
                    assessDisc(iAsses, maxAsses);
                }
            }
            for (jAsses = maxAsses; jAsses > lAsses; --jAsses) {
                if (conforms(lAsses, jAsses)) {
                    assessDisc(lAsses, jAsses);
                }
            }
        }
        if ((boardWidth & 1) > 0) {
            assessDisc(boardWidth / 2, boardWidth / 2);
        }
    }

    /**
     * Assesses one disc
     *
     * @param x
     *         x coordinates of the disc
     * @param y
     *         y coordinates of the disc
     */
    private void assessDisc(int x, int y) {

        colorAsses = board[x][y];
        if (colorAsses == myColor) {
            ++numberOfMyDiscs;
            staticValue += dynamicWorth[x * boardWidth + y];
        } else if (colorAsses == opponentColor) {
            ++numberOfOpponentDiscs;
            staticValue -= dynamicWorth[x * boardWidth + y];
        }

        for (iAssesDisc = 0; iAssesDisc < boardWidth; ++iAssesDisc) {
            newXAsses = x + OFFSET_A[iAssesDisc];
            newYAsses = y + OFFSET_B[iAssesDisc];
            if (!conforms(newXAsses, newYAsses) || board[newXAsses][newYAsses] != emptyColor) {
                continue;
            }
            if (colorAsses == myColor) {
                numberOfMyFrontierDiscs++;
            } else if (colorAsses == opponentColor) {
                numberOfOpponentFrontierDiscs++;
            }
            break;
        }

        if (assesAll) {
            if (isDiskStable(x, y, myColor, myStableDiscs)) {
                ++numberOfMyStableDiscs;
            } else if (isDiskStable(x, y, opponentColor, opponentsStableDiscs)) {
                ++numberOfOpponentStableDiscs;
            }
        }
    }

    /**
     * Dynamically changes dynamic field worth
     */
    void adjustBoardRating() {
        dynamicWorth = STATIC_WORTH.clone();
        if (board[0][boardWidth - 1] != emptyColor) {
            changeValuesAroundCorner(0, boardWidth - 1, 1, -1);
        }
        if (board[boardWidth - 1][boardWidth - 1] != emptyColor) {
            changeValuesAroundCorner(boardWidth - 1, boardWidth - 1, -1, -1);
        }
        if (board[0][0] != emptyColor) {
            changeValuesAroundCorner(0, 0, 1, 1);
        }
        if (board[boardWidth - 1][0] != emptyColor) {
            changeValuesAroundCorner(boardWidth - 1, 0, -1, 1);
        }
    }

    /**
     * Changes values around one corner
     *
     * @param cornerX
     *         corner X coordinate
     * @param cornerY
     *         corner Y coordinate
     * @param x
     *         X direction
     * @param y
     *         Y direction
     */
    private void changeValuesAroundCorner(int cornerX, int cornerY, int x, int y) {
        dynamicWorth[(cornerX + x) * boardWidth + cornerY] = 7;
        dynamicWorth[cornerX * boardWidth + cornerY + y] = 7;
        dynamicWorth[(cornerX + x) * boardWidth + cornerY + y] = -7;
    }

    /**
     * Finds discs on the edges
     *
     * @param color
     *         color of the player we are considering
     * @param stableDiscs
     *         an array of player's stable discs
     * @return true if disc is in edge, false if not
     */
    private boolean importantEdgeDiscs(byte color, DiscStatus[] stableDiscs) {
        retImport = false;

        for (int xImportant : corners) {
            for (int yImportant : corners) {
                offsetXImport = (xImportant == 0) ? 1 : -1;
                offsetYImport = (yImportant == 0) ? 1 : -1;
                if (board[xImportant][yImportant] == color) {
                    retImport = true;
                    colImport = color;
                    stableDiscs[xImportant * boardWidth + yImportant] = DiscStatus.STABLE;
                    if (color == myColor) {
                        numberOfMyCornerDiscs++;
                    } else {
                        numberOfOpponentCornerDiscs++;
                    }
                } else {
                    stableDiscs[xImportant * boardWidth + yImportant] = DiscStatus.UNSTABLE;
                    colImport = Byte.MAX_VALUE;
                }
                if (findOtherEdgeDiscsXAxis(xImportant, yImportant, offsetXImport, stableDiscs) | findOtherEdgeDiscsYAxis(xImportant, yImportant, offsetYImport, stableDiscs)) {
                    retImport = true;
                }
            }
        }
        return retImport;
    }

    /**
     * Finds other discs on the x edge
     *
     * @param x
     *         x coordinates of the corner
     * @param y
     *         y coordinates of the corner
     * @param diff
     *         a direction, where the program will continue
     * @param stableDiscs
     *         an array of player's stable discs
     * @return {@code true} if other stable discs were found, {@code false} if not
     */
    private boolean findOtherEdgeDiscsYAxis(int x, int y, int diff, DiscStatus[] stableDiscs) {
        newYfindY = y + diff;
        ret = false;

        while (conforms(newYfindY) && board[x][newYfindY] == colImport) {
            ret = true;
            stableDiscs[x * boardWidth + newYfindY] = DiscStatus.STABLE;
            newYfindY += diff;
        }

        while (conforms(newYfindY + diff)) {
            addressFindY = x * boardWidth + newYfindY;
            if (stableDiscs[addressFindY] == DiscStatus.TBD) {
                stableDiscs[addressFindY] = DiscStatus.UNSTABLE;
            }
            newYfindY += diff;
        }
        return ret;
    }

    /**
     * Finds other discs on the y edge
     *
     * @param x
     *         x coordinates of the corner
     * @param y
     *         y coordinates of the corner
     * @param diff
     *         a direction, where the program will continue
     * @param stableDiscs
     *         an array of player's stable discs
     * @return {@code true} if other stable discs were found, {@code false} if not
     */
    private boolean findOtherEdgeDiscsXAxis(int x, int y, int diff, DiscStatus[] stableDiscs) {
        newXfindX = x + diff;
        retFindX = false;

        while (conforms(newXfindX) && board[newXfindX][y] == colImport) {
            retFindX = true;
            stableDiscs[(newXfindX) * boardWidth + y] = DiscStatus.STABLE;
            newXfindX += diff;
        }

        while (conforms(newXfindX + diff)) {
            addressFindX = (newXfindX) * boardWidth + y;
            if (stableDiscs[addressFindX] == DiscStatus.TBD) {
                stableDiscs[addressFindX] = DiscStatus.UNSTABLE;
            }
            newXfindX += diff;
        }
        return retFindX;
    }

    /**
     * Returns true if x coordinate is present in a board
     *
     * @param x
     *         x coordinate
     * @return true if x coordinate is present in a board otherwise false
     */
    private boolean conforms(int x) {
        return x >= 0 && x < boardWidth;
    }

    /**
     * Returns true if x and y coordinates are present in a board
     *
     * @param x
     *         x coordinate
     * @param y
     *         y coordinate
     * @return true if x and y coordinates are present in a board otherwise false
     */
    private boolean conforms(int x, int y) {
        return conforms(x) && conforms(y);
    }

    /**
     * Returns whether the disc is stable
     *
     * @param x
     *         x coordinate of the disc
     * @param y
     *         y coordinate of the disc
     * @param color
     *         color of the player we consider
     * @param stableDiscs
     *         an array of player's stable discs
     * @return true if the disk is stable, otherwise false
     */
    private boolean isDiskStable(int x, int y, int color, DiscStatus[] stableDiscs) {
        addressStable = x * boardWidth + y;
        discStable = stableDiscs[addressStable];
        if (!conforms(x, y) || discStable == DiscStatus.STABLE) {
            return true;
        }
        if (discStable == DiscStatus.UNSTABLE) {
            return false;
        }
        if (board[x][y] != color) {
            stableDiscs[addressStable] = DiscStatus.UNSTABLE;
            return false;
        }
        stableDiscs[addressStable] = DiscStatus.YET_TO_PROCESS;
        if (examineEnvironment(x, y, color, stableDiscs)) {
            stableDiscs[addressStable] = DiscStatus.STABLE;
            return true;
        } else {
            stableDiscs[addressStable] = DiscStatus.UNSTABLE;
            return false;
        }
    }

    /**
     * Examines the discs environment to find out whether it is stable or not
     *
     * @param x
     *         x coordinate of the disc
     * @param y
     *         y coordinate of the disc
     * @param color
     *         color of the player we consider
     * @param stableDiscs
     *         an array of player's stable discs
     * @return true if environment conforms, otherwise not
     */
    private boolean examineEnvironment(int x, int y, int color, DiscStatus[] stableDiscs) {
        for (iExamine = 0; iExamine < boardWidth; ++iExamine) {
            offsetXExamine = OFFSET_A[iExamine];
            offsetYExamine = OFFSET_B[iExamine];
            if (offsetXExamine + offsetYExamine >= 0 && (offsetXExamine != 1 || offsetYExamine != -1)) {
                continue;
            }
            if (isLineFull(x, y, offsetXExamine * -1, offsetYExamine * -1)) {
                continue;
            }
            aXExamine = x + offsetXExamine * -1;
            aYExamine = y + offsetYExamine * -1;
            bXExamine = x + offsetXExamine;
            bYExamine = y + offsetYExamine;

            if (Math.min(aXExamine, boardWidth - 1 - aXExamine) + Math.min(aYExamine, boardWidth - 1 - aYExamine) > Math.min(bXExamine, boardWidth -
                                                                                                                                        1 - bXExamine) + Math.min(bYExamine, boardWidth -
                                                                                                                                                                             1 - bYExamine)) {
                bXExamine = aXExamine;
                bYExamine = aYExamine;
                aXExamine = x + offsetXExamine;
                aYExamine = y + offsetYExamine;
            }
            if (!(stableDiscs[aXExamine * boardWidth + aYExamine] != DiscStatus.YET_TO_PROCESS && isDiskStable(aXExamine, aYExamine, color, stableDiscs) || (stableDiscs[bXExamine * boardWidth +
                                                                                                                                                                         bYExamine] != DiscStatus.YET_TO_PROCESS && isDiskStable(bXExamine, bYExamine, color, stableDiscs)))) {
                stableDiscs[x * boardWidth + y] = DiscStatus.UNSTABLE;
                return false;
            }
        }
        stableDiscs[x * boardWidth + y] = DiscStatus.STABLE;
        return true;
    }

    /**
     * Finds out whether the line is full
     *
     * @param x
     *         x coordinate of the beginning
     * @param y
     *         y coordinate of the beginning
     * @param offsetX
     *         x direction
     * @param offsetY
     *         y direction
     * @return true if line is full otherwise false
     */
    private boolean isLineFull(int x, int y, int offsetX, int offsetY) {
        startXLine = x + offsetX;
        startYLine = y + offsetY;
        while (conforms(startXLine, startYLine)) {
            startXLine += offsetX;
            startYLine += offsetY;
        }

        startXLine -= offsetX;
        startYLine -= offsetY;

        if (isDirectionFull(startXLine, startYLine, offsetX, offsetY)) {
            startXLine = x;
            startYLine = y;
            offsetX *= -1;
            offsetY *= -1;
            while (conforms(startXLine, startYLine)) {
                startXLine += offsetX;
                startYLine += offsetY;
            }
            startXLine -= offsetX;
            startYLine -= offsetY;
            return isDirectionFull(startXLine, startYLine, offsetX, offsetY);
        } else {
            return false;
        }
    }

    /**
     * Returns whether the direction is full
     *
     * @param x
     *         x coordinate of the beginning
     * @param y
     *         y coordinate of the beginning
     * @param offsetX
     *         x direction
     * @param offsetY
     *         y direction
     * @return true if direction is full otherwise not
     */
    private boolean isDirectionFull(int x, int y, int offsetX, int offsetY) {
        while (conforms(x, y)) {
            if (board[x][y] == emptyColor) {
                return false;
            }
            x = x + offsetX;
            y = y + offsetY;
        }
        return true;
    }

    /**
     * This class instances compare two moves according to their static value
     */
    private class TurnComparator implements Comparator<ReversiMove>, Serializable {
        /**
         * {@inheritDoc}
         */
        @Override
        public int compare(@NonNull ReversiMove o1, @NonNull ReversiMove o2) {
            return dynamicWorth[o2.x * boardWidth + o2.y] - dynamicWorth[o1.x * boardWidth + o1.y];
        }

    }
}
}