package apk.reversiapk.core.exceptions;

/**
 * Exception thrown when Exception happens during initializing player
 */
public class PlayerInitException extends Exception {
/**
 * @param s
 *         Message in /**
 *         {@link PlayerInitException}
 * @param e
 *         {@inheritDoc}
 */
public PlayerInitException(String s, Exception e) {
    /**
     * {@inheritDoc}
     */
    super(s, e);
}
}
