package apk.reversiapk.core.players;


import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import apk.reversiapk.core.ReversiMove;
import apk.reversiapk.core.enums.Colour;
import apk.reversiapk.core.enums.Constants;

import java.io.Serializable;
import java.util.ArrayList;

public class AIhrusa extends AI {
/**
 * Nmae of this bot
 */
private static final String name = "Hrůša BOT";
/**
 * value of corner
 */
    private static final int CORNER_VALUE = 10;
/**
 * Maximal minmax depth
 */
private final int MIN_MAX_DEPTH = 27;
/**
 * All possible moves
 */
private ArrayList<ReversiMove> options;
@Nullable private ReversStrategy rs, rspredicted;
/**
 * Stone gains
 */
private int gain, bestGain;
/**
 * Current move
 */
    private ReversiMove move;

@NonNull
    @Override
/**
 * {@inheritDoc}
 */ public String getName() {
        return name;
    }

    @Override
/**
 * {@inheritDoc}
 */ public ReversiMove makeNextMove(byte[][] board) {
        options = getPossibleMoves();
        bestGain = Integer.MIN_VALUE;

        for (ReversiMove option : options) {
            rs = new ReversStrategy(board, 0, 0, this.getMyColor(), this.getOpponentColor(), this.getMyColor());
            rspredicted = rs.thinkAhead(MIN_MAX_DEPTH);
            if (rspredicted == null) {
                continue;
                }
            gain = rspredicted.getGain();
            if (gain > bestGain) {
                bestGain = gain;
                move = option;
            }
        }

        return move;
    }

    /**
     * Takes in a board, move, and player ID. Returns total gain of that move and update board in a wrapper.
     */
    @NonNull
    private GridPack updateGrid(byte[][] grid, @NonNull ReversiMove move, byte playerID, byte opponentID) {
        GridPack out = new GridPack(grid, 0, playerID, opponentID);
        out.updateGridDir(move, 0, 1);
        out.updateGridDir(move, 1, 0);
        out.updateGridDir(move, 0, -1);
        out.updateGridDir(move, -1, 0);
        out.updateGridDir(move, 1, 1);
        out.updateGridDir(move, -1, -1);
        out.updateGridDir(move, 1, -1);
        out.updateGridDir(move, -1, 1);
        return out;
    }

private boolean isValidMove(@NonNull byte[][] board, int r, int c, int playerColor, int opponentColor) {
        return checkDir(board, r, c, 1, 1, playerColor, opponentColor) ||
                checkDir(board, r, c, -1, 1, playerColor, opponentColor) ||
                checkDir(board, r, c, 1, -1, playerColor, opponentColor) ||
                checkDir(board, r, c, -1, -1, playerColor, opponentColor) ||
                checkDir(board, r, c, 0, 1, playerColor, opponentColor) ||
                checkDir(board, r, c, 1, 0, playerColor, opponentColor) ||
                checkDir(board, r, c, 0, -1, playerColor, opponentColor) ||
                checkDir(board, r, c, -1, 0, playerColor, opponentColor);
    }


private boolean checkDir(@NonNull byte[][] board, int r, int c, int sr, int sc, int playerColor, int opponentColor) {
        int step = 1;
        boolean stepped = false;
        if (board[r][c] != Colour.EMPTY_SQUARE.getValue()) {
            return false;
        }
        while ((r + sr * step) >= 0 && (r + sr * step) < board.length && (c + sc * step) >= 0 && (c + sc * step) <
                board[0].length) {
            if (board[r + sr * step][c + sc * step] != Colour.EMPTY_SQUARE.getValue()) {
                if (stepped)
                //we met enemy rock
                {
                    if (board[r + sr * step][c + sc * step] == playerColor)
                    //and we see ourselves again, VALID MOVE
                    {
                        return true;
                    }
                } else {
                    if (board[r + sr * step][c + sc * step] == opponentColor)
                    //we have at least one enemy rock in the way.
                    {
                        stepped = true;
                    } else {
                        //First item was our stone
                        return false;
                    }
                }
            } else {
                //we hit empty.
                return false;
            }
            ++step;
        }
        return false;
    }

private class ReversStrategy implements Serializable {
        /**
         * update boardstate after the last decision
         */
        private final byte[][] boardstate;
        /**
         * how deep in the tree we are
         */
        private final int depth;
        /**
         * how much the active player gains (substract when opponent plays)
         */
        private final int gain;
        /**
         * active player
         */
        private final byte playerID;
        /**
         * the other player
         */
        private final byte opponentID;
        private final int actualplayerID;

    ReversStrategy(byte[][] boardstate, int depth, int gain, byte playerID, byte opponentID,
                   int actualplayerID) {
            this.boardstate = boardstate;
            this.depth = depth;
            this.gain = gain;
            this.playerID = playerID;
            this.opponentID = opponentID;
            this.actualplayerID = actualplayerID;
        }

    int getPureGain() {
            return gain;
        }

    int getGain() {
            if (playerID == actualplayerID) {
                return gain;
            }
            return -gain;
        }

        /**
         * Returns Subsequent choices of the active player as ReversStrategies for the player labeled as opponentID
         * (the players swap roles)
         */
        @NonNull
        ReversStrategy[] getNextStep() {
            ArrayList<ReversiMove> validMoves = getOptions(boardstate, playerID, opponentID);
            ReversStrategy[] out = new ReversStrategy[validMoves.size()];
            int iter = 0;
            GridPack gp;
            for (ReversiMove rm : validMoves) {
                gp = updateGrid(boardstate, rm, playerID, opponentID);
                out[iter++] = new ReversStrategy(gp.getGrid(), depth + 1, getGain() + gp.getGain(), opponentID, playerID, actualplayerID);
            }
            return out;
        }

    @NonNull
    private ArrayList<ReversiMove> getOptions(@NonNull byte[][] board, int playerColor, int opponentColor) {
            ArrayList<ReversiMove> out = new ArrayList<>();
            for (byte i = 0; i < board.length; ++i) {
                for (byte j = 0; j < board[0].length; ++j) {
                    if (isValidMove(board, i, j, playerColor, opponentColor)) {
                        out.add(new ReversiMove(i, j));
                    }
                }
            }
            return out;
        }

    @Nullable
    ReversStrategy getMostOptimalNextStep() {
            ReversStrategy[] strats = getNextStep();
            int locgain = -50000;
            int index = 0;
            int wantedindex = -1;
            for (ReversStrategy rs : strats) {
                if (rs.getPureGain() > locgain) {
                    locgain = gain;
                    wantedindex = index;
                }
                ++index;
            }
            if (wantedindex != -1) {
                return strats[wantedindex];
            }
            return null;
        }

        /**
         * Does most optimal step as far as possible within steps. Checks null returns.
         */
        @Nullable
        ReversStrategy thinkAhead(int steps) {
            ReversStrategy out = this.getMostOptimalNextStep();
            int i = 0;
            ReversStrategy candidate;
            while (i++ < steps) {
                candidate = out != null ? out.getMostOptimalNextStep() : null;
                if (null != candidate) {
                    out = candidate;
                } else {
                    return out;
                }
            }
            return out;
        }
    }

private class GridPack implements Serializable {
    private final byte[][] grid;
    private final byte playerID;
    private final byte opponentID;
        private int gain;

    GridPack(byte[][] board, int gain, byte playerID, byte opponentID) {
            this.grid = board;
            this.gain = gain;
            this.playerID = playerID;
            this.opponentID = opponentID;
        }

    void updateGridDir(@NonNull ReversiMove move, int r, int c) {
            int step = 1;
            int tempgain = 0;
            int[][] tempcoords = new int[10][2];
            boolean started = false;
            int ro, co;
            while (true) {
                ro = move.getX() + step * r;
                co = move.getY() + step * c;
                if (ro < 0 || co < 0 || ro >= grid.length || co >= grid[0].length) {
                    break;
                }
                if (grid[co][ro] == Colour.EMPTY_SQUARE.getValue()) {
                    break;
                }
                if (!started) {
                    if (grid[ro][co] == opponentID) {
                        started = true;
                    } else {
                        break;
                    }
                }
                if (started) {
                    if (grid[ro][co] == opponentID) {
                        ++tempgain;
                        tempcoords[tempgain][0] = ro;
                        tempcoords[tempgain][1] = co;
                        if ((co == 0 || co == Constants.BOARD_COLUMNS.getValue() - 1) && (ro == 0 || ro == Constants
                                .BOARD_ROWS.getValue() - 1)) {
                            tempgain += CORNER_VALUE;
                        }
                    } else if (grid[ro][co] == playerID) {
                        for (int i = 0; i < step; ++i) {
                            this.grid[tempcoords[i][0]][tempcoords[i][1]] = this.playerID;
                            this.gain += tempgain;
                            break;
                        }
                    } else {
                        break;
                    }
                    ++step;
                }
            }
        }

    int getGain() {
            return gain;
        }

    byte[][] getGrid() {
            return grid;
        }
    }
}

