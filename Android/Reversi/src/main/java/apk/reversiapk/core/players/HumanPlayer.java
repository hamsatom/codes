package apk.reversiapk.core.players;

import android.support.annotation.Nullable;
import apk.reversiapk.core.FieldClickHandler;
import apk.reversiapk.core.ReversiMove;

import java.util.logging.Logger;
import java.util.logging.StreamHandler;

/**
 * Return moves that user player
 */
public class HumanPlayer extends Player {
private static final Logger LOG = Logger.getLogger(HumanPlayer.class.getName());
private final String name;
/**
 * On click handler used on the board
 */
private final FieldClickHandler clicked;


public HumanPlayer(String name, FieldClickHandler clicked) {
    LOG.addHandler(new StreamHandler());
    LOG.info("Created HumanPlayer with name " + name);
    this.name = name;
    this.clicked = clicked;
}

/**
 * {@inheritDoc}
 */
@Override
public String getName() {
    return name;
}

/**
 * {@inheritDoc}
 */
@Nullable
@Override
public ReversiMove makeNextMove(byte[][] board) {
    clicked.nullLastMve();
    ReversiMove nextMove = null;
    ReversiMove checkingMove;
    while (nextMove == null) {
        checkingMove = clicked.getLastMove();
        if (checkingMove == null) {
            continue;
        }
        for (ReversiMove validMove : getPossibleMoves()) {
            if (validMove.getX() != checkingMove.getX() || validMove.getY() != checkingMove.getY()) {
                continue;
            }
            nextMove = checkingMove;
            break;
        }
    }
    return nextMove;
}

}
