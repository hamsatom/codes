package apk.reversiapk.core;

import java.io.Serializable;

/**
 * Object used to store coordinates of move on board
 */
public class ReversiMove implements Serializable {
/**
 * x coordinate
 */
public final byte x;
/**
 * y coordinate
 */
public final byte y;
public static final long serialVersionUID = 69L;
/**
 * Score assigned to this move. Initial value is 0
 */
public double score = 0;

public ReversiMove(byte x, byte y) {
    this.x = x;
    this.y = y;
}

/**
 * Returns x coordinate of this move
 *
 * @return x coordinate of this move
 */
public final byte getX() {
    return x;
}

/**
 * Returns y cordinate of this move
 * @return y cordinate of this move
 */
public final byte getY() {
    return y;
}
}
