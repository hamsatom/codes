package apk.reversiapk.core.players;

import android.support.annotation.NonNull;
import apk.reversiapk.core.ReversiMove;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.StreamHandler;

/**
 * Uses Memory-enhanced Test Driver with node n and value f search algorithm - MTD(f)
 * @see <a href="https://chessprogramming.wikispaces.com/MTD%28f%29/">MTD(f)</a>
 */
public class AImtdf extends AIwithMemory {
/**
 * Name of this bot
 */
private static final String NAME = "MTD(f) BOT";
private static final Logger LOG = Logger.getLogger(AImtdf.class.getName());
/**
 * Bound of current search
 */
private double lowerBound, upperBound, beta;
/**
 * The initial maximum bot this bot goes to. Usually initialized to 2 then increased while is still time by 2.
 */
private byte MAXIMAL_DEPTH;
/**
 * Value of current move
 */
private double value;
private Node node;
private double alpha;
/**
 * Queasiest of difference between last and previous depth used to guess how long is next depth gonna take
 */
private double q;
/**
 * Time spend finding best move since {@code makeNextMove} was called
 */
private long timeSpend;
/**
 * TIme spend on searching in this depth
 */
private long timeForThisDepth;
/**
 * Time spend on searching previous depth, depth is {@code maximalDepth  - 2} if {@code maximalDepth} is greater than 2
 */
private long timePreviousDepth;

public AImtdf() {
    super.transpositionTable = new HashMap<>(40000, 1.0f);
    LOG.addHandler(new StreamHandler());
}

/**
 * {@inheritDoc}
 */
@NonNull
@Override
public String getName() {
    return NAME;
}

/**
 * Compares all of my possible moves.
 *
 * @param board
 *         current state of the board
 * @return best of available moves
 */
@Override
public ReversiMove makeNextMove(byte[][] board) {
    timeStart = System.nanoTime();
    moves = getPossibleMoves();
    myColor = getMyColor();
    opponentColor = getOpponentColor();
    desc = comp.board = board;
    comp.adjustBoardRating();
    Collections.sort(moves, comp.comparator);
    bestMove = moves.get(0);
    MAXIMAL_DEPTH = 2;
    timePreviousDepth = timeSpend = timeForThisDepth = 0;

    while (timeSpend + timeForThisDepth < TIME_LIMIT && MAXIMAL_DEPTH < 60) {
        timeForThisDepth = System.nanoTime();

        mtdf();

        MAXIMAL_DEPTH += 2;
        timeSpend = System.nanoTime() - timeStart;
        timeForThisDepth = System.nanoTime() - timeForThisDepth;
        q = (timeForThisDepth + 0.0) / (timePreviousDepth + 0.0);
        q = q > 1 && q != Double.POSITIVE_INFINITY ? q : 1;
        timePreviousDepth = timeForThisDepth;
        timeForThisDepth *= q;
    }

    LOG.info(NAME + " time spend " + timeSpend + " for depth " + (MAXIMAL_DEPTH - 2));

    moves.clear();
    possibleMoves.clear();
    comp.flippedDiscs.clear();
    return bestMove;
}

/**
 * Searched minimax tree using null window alpha-beta with transposition table calls
 *
 * @see <a href="https://chessprogramming.wikispaces.com/MTD%28f%29/">MTD(f)</a>
 */
private void mtdf() {
    lowerBound = Double.NEGATIVE_INFINITY;
    upperBound = Double.POSITIVE_INFINITY;
    value = bestValue;
    do {
        if (value == lowerBound) {
            beta = value + 1;
        } else {
            beta = value;
        }
        exploreFirstLayer();
        value = bestValue;
        if (value < beta) {
            upperBound = value;
        } else {
            lowerBound = value;
        }
    } while (lowerBound < upperBound);
}

/**
 * Searches first layer of node under initial game position. Either evaluates the state if no other moves are possible or it get value from {@code alphaBetaWithMemory} if started from node. This
 * method is necessary to initialized start variables in
 * {@code alphaBetaWithMemory}.
 */
private void exploreFirstLayer() {
    bestValue = Double.NEGATIVE_INFINITY;
    alpha = beta - 1;
    Collections.sort(moves, maxComparator);
    for (ReversiMove move : moves) {
        desc = comp.flipDiscs(desc, move.x, move.y, myColor, opponentColor);
        ID = boardId(desc);
        node = (Node) transpositionTable.get(ID);
        if (node != null) {
            if (node.depth >= MAXIMAL_DEPTH) {
                value = node.score;
            } else {
                possibleMoves = node.possibleMoves;
                if (possibleMoves.isEmpty()) {
                    value = comp.evaluateTurn(desc, myColor, opponentColor);
                } else {
                    value = alphaBetaWithMemory(possibleMoves, desc, alpha, beta, MAXIMAL_DEPTH - 1, opponentColor, myColor, false);
                }
                node.score = value;
                node.depth = MAXIMAL_DEPTH;
                Collections.sort(possibleMoves, minComparator);
            }
        } else {
            possibleMoves = comp.getAllPossibleTurns(desc, opponentColor);
            if (possibleMoves.isEmpty()) {
                value = comp.evaluateTurn(desc, myColor, opponentColor);
            } else {
                value = alphaBetaWithMemory(possibleMoves, desc, alpha, beta, MAXIMAL_DEPTH - 1, opponentColor, myColor, false);
            }
            Collections.sort(possibleMoves, minComparator);
            transpositionTable.put(ID, new Node(MAXIMAL_DEPTH, value, possibleMoves));
        }

        desc = comp.unflipDiscs(desc, opponentColor);
        move.score = value;

        if (value > bestValue) {
            bestValue = value;
            bestMove = move;
            if (value >= beta) {
                return;
            }
        }
        alpha = Math.max(value, alpha);
    }
}


/**
 * Recursively grows the minmax tree and return the maximal/minimal value the
 * player can obtain.
 *
 * @param moves
 *         list of all possible moves available to player
 * @param board
 *         current game state
 * @param alpha
 *         value of alpha
 * @param beta
 *         value of beta
 * @param depth
 *         remaining recursive depth
 * @param color1
 *         color of the player to whom the board is evaluated
 * @param color2
 *         color of the other player
 * @param maximizing
 *         true if we want the player to maximize his outcome,
 *         otherwise false
 * @return the best possible value
 */
private double alphaBetaWithMemory(@NonNull ArrayList<ReversiMove> moves, byte[][] board, double alpha, double beta, final int depth, final byte color1, final byte color2, final boolean maximizing) {
    /**
     * Value of current move
     */
    double value;
    /**
     * Moves that could be played after {@code move} found in {@code moves}
     */
    ArrayList<ReversiMove> possibleMoves;
    Node node;
    /**
     * ID of current board state
     */
    String ID;
    /**
     * best value found so far
     */
    double bestValue;

    if (maximizing) {
        bestValue = Double.NEGATIVE_INFINITY;
        for (ReversiMove move : moves) {
            board = comp.flipDiscs(board, move.x, move.y, color1, color2);
            ID = boardId(board);
            node = (Node) transpositionTable.get(ID);
            if (node != null) {
                if (node.depth >= depth) {
                    value = node.score;
                } else {
                    possibleMoves = node.possibleMoves;
                    if (possibleMoves.isEmpty() || depth == 1) {
                        value = comp.evaluateTurn(board, myColor, opponentColor);
                    } else {
                        value = alphaBetaWithMemory(possibleMoves, board, alpha, beta, depth - 1, color2, color1, false);
                    }
                    node.score = value;
                    node.depth = depth;
                    Collections.sort(possibleMoves, minComparator);
                }
            } else {
                possibleMoves = comp.getAllPossibleTurns(board, color2);
                if (possibleMoves.isEmpty() || depth == 1) {
                    value = comp.evaluateTurn(board, myColor, opponentColor);
                } else {
                    value = alphaBetaWithMemory(possibleMoves, board, alpha, beta, depth - 1, color2, color1, false);
                }
                Collections.sort(possibleMoves, minComparator);
                transpositionTable.put(ID, new Node(depth, value, possibleMoves));
            }

            board = comp.unflipDiscs(board, color2);
            move.score = value;

            if (value > bestValue) {
                if (value >= beta) {
                    return value;
                }
                bestValue = value;
            }
            alpha = Math.max(value, alpha);
        }
    } else {
        bestValue = Double.POSITIVE_INFINITY;
        for (ReversiMove move : moves) {
            board = comp.flipDiscs(board, move.x, move.y, color1, color2);
            ID = boardId(board);
            node = (Node) transpositionTable.get(ID);
            if (node != null) {
                if (node.depth >= depth) {
                    value = node.score;
                } else {
                    possibleMoves = node.possibleMoves;
                    if (possibleMoves.isEmpty() || depth == 1) {
                        value = comp.evaluateTurn(board, myColor, opponentColor);
                    } else {
                        value = alphaBetaWithMemory(possibleMoves, board, alpha, beta, depth - 1, color2, color1, true);
                    }
                    node.score = value;
                    node.depth = depth;
                    Collections.sort(possibleMoves, maxComparator);
                }
            } else {
                possibleMoves = comp.getAllPossibleTurns(board, color2);
                if (possibleMoves.isEmpty() || depth == 1) {
                    value = comp.evaluateTurn(board, myColor, opponentColor);
                } else {
                    value = alphaBetaWithMemory(possibleMoves, board, alpha, beta, depth - 1, color2, color1, true);
                }
                Collections.sort(possibleMoves, maxComparator);
                transpositionTable.put(ID, new Node(depth, value, possibleMoves));
            }

            board = comp.unflipDiscs(board, color2);
            move.score = value;

            if (value < bestValue) {
                if (value <= alpha) {
                    return value;
                }
                bestValue = value;
            }
            beta = Math.min(value, beta);
        }
    }

    return bestValue;
}
}