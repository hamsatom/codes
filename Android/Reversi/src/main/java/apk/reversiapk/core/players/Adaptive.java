package apk.reversiapk.core.players;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import apk.reversiapk.core.ReversiMove;
import apk.reversiapk.core.enums.Constants;

import java.util.logging.Logger;
import java.util.logging.StreamHandler;

/**
 * Reversi player that first third plays like {@link AImcts} then like {@link AIsimon} and finishes like {@link Greedy}
 * This way I expected player will be able to capture important fields at the begging of the game then play strategically best
 * moves and capture most remaining stones at the end
 */
public class Adaptive extends AI {
private static final Logger LOG = Logger.getLogger(Adaptive.class.getName());
/**
 * Name of this bot
 */
private static final String NAME = "Adaptive Bot";
/**
 * Number of players this {@link Adaptive} player like
 */
private static final byte BOTS_COUNT = 3;
/**
 * Number of remaining stones. When board contains less then this amount {@link Greedy} start playing
 */
private static final byte FINISHING_COUNT = 8;
/**
 * Array of used palyers
 */
@NonNull private final Player[] botPool;
/**
 * Milestones when player changes
 */
private int switchCount = ((Constants.BOARD_COLUMNS.getValue() * Constants.BOARD_ROWS.getValue()) - FINISHING_COUNT)
                          / 2;
/**
 * Position in array of current player
 */
private int currentPlayer = 0;

/**
 * AMount of places stones
 */
private byte count;

private byte row, column;
private int index;

/**
 * Add different bots to array
 */
public Adaptive() {
    LOG.addHandler(new StreamHandler());
    botPool = new Player[BOTS_COUNT];
    botPool[0] = new AImcts();
    botPool[1] = new AIsimon();
    botPool[2] = new Greedy();
}

/**
 * {@inheritDoc}
 */
public ReversiMove makeNextMove(byte[][] board) {
    // First cheks amount of stones on current board
    countStones(board);

    index = count / switchCount;
    if (currentPlayer != index) {
        // Changes bot by increasing index
        if (botPool[currentPlayer] instanceof AIwithMemory) {
            ((AIwithMemory) botPool[currentPlayer]).transpositionTable.clear();
        }
        currentPlayer = index;
        LOG.info("Switched player to " + botPool[currentPlayer].getName());
    }

    return setPlayersAndMove(board);
}

/**
 * Initialises current {@link Player}
 *
 * @param board
 *         Representation of board in byte array
 * @return Reversi move that particular player played
 */
@Nullable
private ReversiMove setPlayersAndMove(byte[][] board) {
    botPool[currentPlayer].setMyColor(getEnumColour());
    botPool[currentPlayer].setOpponentColor(getOpponentEnumColour());
    botPool[currentPlayer].setPossibleMoves(getPossibleMoves());
    return botPool[currentPlayer].makeNextMove(board);
}

/**
 * Count stones on the board
 *
 * @param board
 *         Representation of playground in byte array
 */
private void countStones(byte[][] board) {
    count = 0;
    for (column = 0; column < Constants.BOARD_COLUMNS.getValue(); ++column) {
        for (row = 0; row < Constants.BOARD_ROWS.getValue(); ++row) {
            if (board[column][row] != 0) {
                ++count;
            }
        }
    }
}

/**
 * {@inheritDoc}
 */
@NonNull
public String getName() {
    return NAME;
}
}