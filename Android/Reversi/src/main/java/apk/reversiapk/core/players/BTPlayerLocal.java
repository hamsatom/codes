package apk.reversiapk.core.players;

import android.support.annotation.Nullable;
import apk.reversiapk.core.FieldClickHandler;
import apk.reversiapk.core.ReversiMove;
import apk.reversiapk.core.btconnection.ConnectionHolder;
import apk.reversiapk.core.btconnection.ConnectionThread;
import apk.reversiapk.core.exceptions.SendingException;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Local Bluetooth player
 * Sends played move over Bluetooth
 */
public class BTPlayerLocal extends HumanPlayer {
private static final Logger LOG = Logger.getLogger(BTPlayerLocal.class.getName());

public BTPlayerLocal(String name, FieldClickHandler clicked) {
    super(name, clicked);
}

/**
 * {@inheritDoc}
 */
@Nullable
@Override
public ReversiMove makeNextMove(byte[][] board) {
    // Finds Thread maintaining connection and sends the move
    ConnectionThread connection = ConnectionHolder.getInstance().getConnection();
    ReversiMove nextMove = super.makeNextMove(board);
    try {
        connection.sendMove(nextMove);
    } catch (SendingException e) {
        LOG.log(Level.WARNING, "Couldn't send move", e);
    }
    return nextMove;
}
}
