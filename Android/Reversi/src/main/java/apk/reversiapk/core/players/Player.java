package apk.reversiapk.core.players;

import apk.reversiapk.core.ReversiMove;
import apk.reversiapk.core.enums.Colour;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Base cllas of player
 */
public abstract class Player implements PlayerInterface, Serializable {
/**
 * My colour
 */
private Colour myColor;
/**
 * Colour of the opponent
 */
private Colour opponentColor;
/**
 * Array of possible moves
 */
private ArrayList<ReversiMove> possibleMove;

/**
 * @return all possible moves
 */
final ArrayList<ReversiMove> getPossibleMoves() {
    return possibleMove;
}

/**
 * Sets possible moves for this palyer
 *
 * @param possibleMove
 *         array of all possible moves
 */
public final void setPossibleMoves(ArrayList<ReversiMove> possibleMove) {
    this.possibleMove = possibleMove;
}

/**
 * @return colou of this palyer
 */
final byte getMyColor() {
    return myColor.getValue();
}

/**
 * @param myColor
 *         set current colour
 */
public final void setMyColor(Colour myColor) {
    this.myColor = myColor;
}

/**
 * @return colour of opponent
 */
final byte getOpponentColor() {
    return opponentColor.getValue();
}

/**
 * Set colour of opponent
 *
 * @param opponentColor
 *         oppoonent colour
 */
public final void setOpponentColor(Colour opponentColor) {
    this.opponentColor = opponentColor;
}

/**
 * Enum representaion of colour
 *
 * @return my colour in  {@link Enum}
 */
public final Colour getEnumColour() {
    return myColor;
}

/**
 * Sets enum colour of opponent
 *
 * @return opponent colour in enum representation
 */
final Colour getOpponentEnumColour() {
    return opponentColor;
}
}
