package apk.reversiapk.core.players;

import android.support.annotation.Nullable;
import apk.reversiapk.core.ReversiMove;
import apk.reversiapk.core.btconnection.ConnectionHolder;
import apk.reversiapk.core.btconnection.ConnectionThread;

import java.util.logging.Logger;
import java.util.logging.StreamHandler;

/**
 * Returns moves received via Bluetooth
 */
public class BTPlayerRemote extends Player {
private static final Logger LOG = Logger.getLogger(BTPlayerRemote.class.getName());
/**
 * Received name
 */
private final String name;

public BTPlayerRemote(String name) {
    LOG.addHandler(new StreamHandler());
    this.name = name;
}

/**
 * {@inheritDoc}
 */
@Override
public String getName() {
    return name;
}

/**
 * {@inheritDoc}
 */
@Nullable
public ReversiMove makeNextMove(byte[][] board) {
    ReversiMove nextMove = null;
    ReversiMove checkingMove;
    ConnectionThread connection = ConnectionHolder.getInstance().getConnection();
    // Loops while no valid move is found
    while (nextMove == null) {
        // Obtains received move
        checkingMove = connection.getReceivedMove();
        if (checkingMove == null) {
            continue;
        }
        // Tries if received move is valid and if so then returns it
        for (ReversiMove validMove : getPossibleMoves()) {
            if (validMove.getX() != checkingMove.getX() || validMove.getY() != checkingMove.getY()) {
                continue;
            }
            nextMove = checkingMove;
            break;
        }
    }
    return nextMove;
}
}