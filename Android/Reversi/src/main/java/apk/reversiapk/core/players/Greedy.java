package apk.reversiapk.core.players;

import android.support.annotation.NonNull;
import apk.reversiapk.core.ReversiMove;
import apk.reversiapk.core.enums.Colour;
import apk.reversiapk.core.enums.Constants;

import java.util.ArrayList;

/**
 * Tries to flip most stones during his turn
 */
public class Greedy extends AI {
private static final String NAME = "Greedy BOT";
private int gain;
private int var;
private ArrayList<ReversiMove> moves;
private ReversiMove move;

/**
 * {@inheritDoc}
 */
@NonNull
@Override
public String getName() {
    return NAME;
}

/**
 * {@inheritDoc}
 */
@Override
public ReversiMove makeNextMove(byte[][] board) {
    moves = getPossibleMoves();

    //tries to eat the most pieces each turn
    move = moves.remove(0);
    gain = countGain(board, move);

    for (ReversiMove oneMove : moves) {
        var = countGain(board, oneMove);
        if (var > gain) {
            gain = var;
            move = oneMove;
        }
    }
    return move;
}

/**
 * Calculates total gain
 *
 * @param grid
 *         received board array
 * @param move
 *         one possible move
 * @return gain if this move will be played
 */
private int countGain(byte[][] grid, @NonNull ReversiMove move) {
    int out = 1;
    out += countGainDir(grid, move.getX(), move.getY(), 1, 1);
    out += countGainDir(grid, move.getX(), move.getY(), -1, 1);
    out += countGainDir(grid, move.getX(), move.getY(), 1, -1);
    out += countGainDir(grid, move.getX(), move.getY(), -1, -1);
    out += countGainDir(grid, move.getX(), move.getY(), 0, -1);
    out += countGainDir(grid, move.getX(), move.getY(), 0, 1);
    out += countGainDir(grid, move.getX(), move.getY(), 1, 0);
    out += countGainDir(grid, move.getX(), move.getY(), -1, 0);
    return out;
}

/**
 * Counts gain in one direction
 *
 * @param board
 *         array representation of palyground
 * @param r
 *         x coordinate
 * @param c
 *         y coordinate
 * @param sr
 *         x direction
 * @param sc
 *         y direction
 * @return gain in one direction
 */
private int countGainDir(byte[][] board, int r, int c, int sr, int sc) {
    boolean neighbourOpponent = false;
    boolean endingMyStone = false;
    byte gain = 0;

    // first check if the direction should be turned
    while (!endingMyStone) {
        c += sc;
        r += sr;
        ++gain;

        if (c < 0 || c >= Constants.BOARD_COLUMNS.getValue() || r < 0 || r >= Constants.BOARD_ROWS
                .getValue()) {
            break;
        }
        if (board[c][r] == Colour.EMPTY_SQUARE.getValue()) {
            break;
        }

        if (board[c][r] != getMyColor()) {
            neighbourOpponent = true;
        } else {
            endingMyStone = true;
        }
    }

    // flip the stones if they should be flipped
    if (neighbourOpponent && endingMyStone) {
        return gain;
    } else {
        return 0;
    }
}
}