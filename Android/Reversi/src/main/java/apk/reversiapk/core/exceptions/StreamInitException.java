package apk.reversiapk.core.exceptions;

import java.io.IOException;

/**
 * Exception thrown when opening stream for bluetooth devices fails
 */
public class StreamInitException extends Exception {
/**
 * @param s
 *         Message in {@link StreamInitException}
 * @param e
 *         {@inheritDoc}
 */
public StreamInitException(String s, IOException e) {
    super(s, e);
}
}
