package apk.reversiapk.core.players;

import android.support.annotation.Nullable;
import apk.reversiapk.core.ReversiMove;

/**
 * Common interface for all players
 */
interface PlayerInterface {

/**
 * @return name of this player
 */
String getName();

/**
 * @param board
 *         array representation of playground
 * @return next move of player
 */
@Nullable
ReversiMove makeNextMove(byte[][] board);
}
