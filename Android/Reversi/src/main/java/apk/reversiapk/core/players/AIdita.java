package apk.reversiapk.core.players;

import android.support.annotation.NonNull;
import apk.reversiapk.core.ReversiMove;
import apk.reversiapk.core.enums.Colour;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;


/**
 * This player can see one move to the future and evaluates game board by
 * using some dark magic.
 */
public class AIdita extends AI {
    private final static String name = "Dita BOT";
private ReversiMove bestMove;
private double value;
private double bestValue;
    private ArrayList<ReversiMove> moves;
    private Board board;

@NonNull
    @Override
/**
 * {@inheritDoc}
 */ public String getName() {
        return name;
    }

    @Override
/**
 * {@inheritDoc}
 */ public ReversiMove makeNextMove(byte[][] b) {
        // prepare the board object
        board = new Board(b, getMyColor(), getOpponentColor(), Colour.EMPTY_SQUARE.getValue());

        // get possible moves on this board
        moves = getPossibleMoves();

        // evaluate moves until the best one is found
        bestMove = moves.remove(0);
        bestValue = evalAfterAllOpMoves(board.getCopy().simulateMove(bestMove.getX(), bestMove.getY()));

        // check all of them
        for (ReversiMove move : moves) {

            // evaluate
            value = evalAfterAllOpMoves(board.getCopy().simulateMove(move.getX(), move.getY()));
            // compare to last best
            if (value > bestValue) {
                bestMove = move;
                bestValue = value;
            }
        }
        return bestMove;
    }

    /**
     * Simulates opponent's moves on a board and returns the average outcome.
     *
     * @param b game state
     * @return avarage outcome
     */
    private double evalAfterAllOpMoves(@NonNull Board b) {
        // get all possible opponents move
        ArrayList<ReversiMove> opMoves = b.getPossibleMoves(b.getOpColor());
        double evalSum = 0;
        Board afterMove;

        for (ReversiMove opMove : opMoves) {
            // get board after move
            afterMove = b.getCopy().simulateMove(b.getOpColor(), opMove.getX(), opMove.getY());
            evalSum += afterMove.goodness();
        }

        // return average
        return evalSum / (opMoves.size() + 0.0);
    }
}

class Board implements Serializable{

    private final byte[][] board;
    private final int boardSize;
    private final byte myColor;
    private final byte opColor;
    private final byte emptyColor;
@NonNull private final boolean[][] stables;
    private final int[][] outcomes = {{99, -8, 8, 6, 6, 8, -8, 99}, {-8, -24, -4, -3, -3, -4, -24, -8}, {8, -4, 7, 4, 4,
            7, -4, 8}, {6, -3, 4, 0, 0, 4, -3, 6}, {6, -3, 4, 0, 0, 4, -3, 6}, {8, -4, 7, 4, 4, 7, -4, 8}, {-8, -24, -4,
            -3, -3, -4, -24, -8}, {99, -8, 8, 6, 6, 8, -8, 99}};

Board(byte[][] board, byte myColor, byte opColor, byte emptyColor) {
        this.board = board;
        this.boardSize = this.board.length;
        this.myColor = myColor;
        this.opColor = opColor;
        this.emptyColor = emptyColor;
        this.stables = new boolean[this.boardSize + 2][this.boardSize + 2];
    }

@NonNull
    public Board getCopy() {
        /**
         * gives copy of the object
         */
        return new Board(this.getBoardCopy(), myColor, opColor, emptyColor);
    }


@NonNull
    public ArrayList<ReversiMove> getPossibleMoves(int player) {
        /**
         * returns all posssble moves for a player on this board
         */
    ArrayList<ReversiMove> moves = new ArrayList<>(34);
        for (byte i = 0; i < boardSize; ++i) {
            for (byte j = 0; j < boardSize; ++j) {
                if (this.isValidMove(player, i, j) > 0) {
                    moves.add(new ReversiMove(i, j));
                }
            }
        }
        return moves;
    }

    /**
     * Checks if certain move can be made by a player. Returns number of tiles
     * that will be flipped by that move (0 if move is invalid).
     *
     * @param color
     * @param x
     * @param y
     * @return
     */
    private int isValidMove(int color, int x, int y) {
        int f = this.getField(x, y);

        if (f != this.emptyColor) {
            return 0;
        }

        int sum = 0;

        // vector coordinates to check move in all directions
        int[] difX = {-1, -1, 0, 1, 1, 1, 0, -1};
        int[] difY = {0, 1, 1, 1, 0, -1, -1, -1};

        for (byte i = 0; i < boardSize; ++i) {
            // check direction to flip
            sum += this.checkDirection(color, x, y, difX[i], difY[i]);
        }
        return sum;
    }

private int checkDirection(int color, int srcx, int srcy, int difx, int dify) {
        int me;
        int op;

        // set temporary variables "me" and "op" according to "color"
        if (color == this.myColor) {
            me = myColor;
            op = opColor;
        } else {
            me = opColor;
            op = myColor;
        }

        // x,y are coordinates of points in direction difx,dify from srcx,srcy
        int x = srcx + difx;
        int y = srcy + dify;
        int opStone = 0;

        while (this.contains(x, y)) {
            // count opponents stones
            if (this.getField(x, y) == op) {
                opStone += 1;
            }
            // once own stone is reached, return current opStone status
            else if (this.getField(x, y) == me) {
                return opStone;
            }
            // return 0 if empty slot is reached
            else {
                return 0;
            }
            //change difx,dify
            x += difx;
            y += dify;
        }
        return 0;
    }

private int getField(int x, int y) {
        if (!this.contains(x, y)) {
            return 2;
        }
        return board[x][y];
    }

@NonNull
Board simulateMove(int x, int y) {
        return this.simulateMove(myColor, x, y);
    }

    /**
     * Modifies this board with a certain move of a player. To create a new
     * board and keep this one, use board.getCopy().simulateMove()...
     *
     * @param color
     * @param x
     * @param y
     * @return
     */
    @NonNull
    Board simulateMove(byte color, int x, int y) {
        int[] difX = {-1, -1, 0, 1, 1, 1, 0, -1};
        int[] difY = {0, 1, 1, 1, 0, -1, -1, -1};

        // modify all directions
        for (int i = 0; i < boardSize; ++i) {
            checkFlipDirection(color, x, y, difX[i], difY[i]);
        }

        //flip the place of the move, too
        this.setField(x, y, color);
        return this;
    }

    /**
     * checks if a flip is possible in a direction and performs it if so
     *
     * @param color
     * @param srcx
     * @param srcy
     * @param difx
     * @param dify
     */
    private void checkFlipDirection(int color, int srcx, int srcy, int difx, int dify) {
        int x = srcx + difx;
        int y = srcy + dify;
        boolean flip = false;

        int me;
        int op;

        if (color == this.myColor) {
            me = myColor;
            op = opColor;
        } else {
            me = opColor;
            op = myColor;
        }

        int f = this.getField(x, y);
        int field;
        // flip is only possible when field next to the move is empty
        if (f == op) {
            x += difx;
            y += dify;
            while (this.contains(x, y)) {
                field = this.getField(x, y);
                // flip is possible once my stone is reached
                if (field == me) {
                    flip = true;
                    break;
                }
                // if an empty slot is discovered after op stones, flip i impossible
                else if (field == this.emptyColor) {
                    break;
                }
                x += difx;
                y += dify;
            }
        }
        if (flip) {
            flipDirection(color, srcx, srcy, difx, dify);
        }
    }

    private void flipDirection(int color, int srcx, int srcy, int difx, int dify) {
        int x = srcx + difx;
        int y = srcy + dify;

        byte me;

        if (color == myColor) {
            me = myColor;
        } else {
            me = opColor;
        }

        while (this.contains(x, y) && this.getField(x, y) != me) {
            this.setField(x, y, me);
            x += difx;
            y += dify;
        }
    }

    /**
     * @return a deep copy of Board's board field
     */
    @NonNull
    private byte[][] getBoardCopy() {
        byte[][] copy = new byte[this.boardSize][];
        for (byte i = 0; i < this.boardSize; ++i) {
            copy[i] = Arrays.copyOf(this.board[i], this.boardSize);
        }
        return copy;
    }

private void setField(int x, int y, byte color) {
        if (color == opColor || color == myColor || color == emptyColor) {
            this.board[x][y] = color;
        }
    }

byte getOpColor() {
        return this.opColor;
    }

private boolean contains(int x, int y) {
        return x < this.boardSize && y < this.boardSize && x >= 0 && y >= 0;
    }

    /**
     * checks stability of a stone. A stable stone is a stone that can never be
     * flipped until the end of the game.
     */
    private boolean isStable(int x, int y) {

        boolean result = isStableFrom(x, y, 0, 1) && isStableFrom(x, y, 1, 1) && isStableFrom(x, y, 1, 0) && isStableFrom
                (x,
                        y,
                        1,
                        -1);
        this.stables[x + 1][y + 1] = result;
        return result;
    }

    /**
     * checks stone stability for a specified direction. Basic flip rules:
     * 1) if the row (direction) is full, the stone can not be flipped
     * 2) if a stable stone lies next to the tested one in certain direction,
     * both stones can not be flipped
     * 3) if the row from target stone to the edge of the field is full of "my"
     * stones only, the target stone can not be flipped
     * *) situation of "my" stone surrounded by "op" stones from both sides is
     * too complicated and time consuming to evaluate and such accuracy
     * isn't needed on this level. Such stones are considered unstable.
     *
     * @param srcx
     * @param srcy
     * @param difx
     * @param dify
     * @return
     */
    private boolean isStableFrom(int srcx, int srcy, int difx, int dify) {
        if (this.stables[srcx + difx + 1][srcy + dify + 1] || this.stables[srcx - difx + 1][srcy - dify + 1]) {
            return true;
        }
        int op;

        if (this.getField(srcx, srcy) == this.myColor) {
            op = opColor;
        } else {
            op = myColor;
        }

        boolean firstFull = true;
        boolean firstOp = false;

        // one direction (and I don't mean the band!)
        int x = srcx + difx;
        int y = srcy + dify;
        int field;
        while (this.contains(x, y)) {
            field = this.getField(x, y);
            if (field == op) {
                firstOp = true;
            }
            if (field == this.emptyColor) {
                firstFull = false;
                break;
            }
            x += difx;
            y += dify;
        }

        if (firstFull && !firstOp) {
            return true;
        }

        // the other direction
        x = srcx - difx;
        y = srcy - dify;
        while (this.contains(x, y)) {
            field = this.getField(x, y);
            if (field == op && !firstFull) {
                return false;

            }
            if (field == this.emptyColor) {
                if (firstOp) {
                    return false;
                }
                if (!firstFull) {
                    return false;
                }
            }
            x -= difx;
            y -= dify;
        }
        return true;
    }

    /**
     * returns number of moves each player can make from this board.
     *
     * @return: array, [0] is number of my moves, [1] are op's moves
     */
    @NonNull
    private int[] getMobility() {
        int[] moves = {0, 0};
        for (byte i = 0; i < boardSize; ++i) {
            for (byte j = 0; j < boardSize; ++j) {
                if (isValidMove(myColor, i, j) > 0) {
                    ++moves[0];
                } else if (isValidMove(opColor, i, j) > 0) {
                    ++moves[1];
                }
            }
        }
        return moves;
    }

    /**
     * evaluation method.
     * It is inspired by https://kartikkukreja.wordpress
     * .com/2013/03/30/heuristic-function-for-reversiothello,
     * but I've optimalised it and added some new features (such as stability).
     *
     * @return
     */
    double goodness() {

        int myTilesCount = 0;
        int opTilesCount = 0;
        int myFrontTiles = 0;
        int opFrontTiles = 0;
        int myStableTiles = 0;
        int opStableTiles = 0;
        int x, y;
        double resultFieldOutcomes = 0;

        int[] X1 = {-1, -1, 0, 1, 1, 1, 0, -1};
        int[] Y1 = {0, 1, 1, 1, 0, -1, -1, -1};

        int field;
        // count number of my stones and stable stones
        for (byte i = 0; i < boardSize; ++i) {
            for (byte j = 0; j < boardSize; ++j) {
                field = this.board[i][j];
                if (field == myColor) {
                    resultFieldOutcomes += this.outcomes[i][j];
                    ++myTilesCount;
                    if (isStable(i, j)) {
                        ++myStableTiles;
                    }
                } else if (field == opColor) {
                    resultFieldOutcomes -= this.outcomes[i][j];
                    ++opTilesCount;
                    if (isStable(i, j)) {
                        ++opStableTiles;
                    }
                }
                // also count all fields, that are next to an empty slot
                if (field != emptyColor) {
                    for (byte k = 0; k < boardSize; ++k) {
                        x = i + X1[k];
                        y = j + Y1[k];
                        if (x >= 0 && x < boardSize && y >= 0 && y < boardSize && this.board[x][y] == emptyColor) {
                            if (this.board[i][j] == myColor) {
                                ++myFrontTiles;
                            } else {
                                ++opFrontTiles;
                            }
                            break;
                        }
                    }
                }
            }
        }

        double resultTilesCount, resultCorners, resultStability, resultMobility, resultFrontTiles;

        if (myTilesCount > opTilesCount) {
            resultTilesCount = (100.0 * myTilesCount) / (myTilesCount + opTilesCount);
        } else if (myTilesCount < opTilesCount) {
            resultTilesCount = -(100.0 * opTilesCount) / (myTilesCount + opTilesCount);
        } else {
            resultTilesCount = 0;
        }

        if (myFrontTiles > opFrontTiles) {
            resultFrontTiles = -(100.0 * myFrontTiles) / (myFrontTiles + opFrontTiles);
        } else if (myFrontTiles < opFrontTiles) {
            resultFrontTiles = (100.0 * opFrontTiles) / (myFrontTiles + opFrontTiles);
        } else {
            resultFrontTiles = 0;
        }

        if (myStableTiles > opStableTiles) {
            resultStability = -(100.0 * myStableTiles) / (myStableTiles + opStableTiles);
        } else if (myStableTiles < opStableTiles) {
            resultStability = (100.0 * opStableTiles) / (myStableTiles + opStableTiles);
        } else {
            resultStability = 0;
        }


        //corner coordinates
        int[] cornersX = {0, 0, this.boardSize - 1, this.boardSize - 1};
        int[] cornersY = {0, this.boardSize - 1, this.boardSize - 1, 0};

        int myCorners = 0;
        int opCorners = 0;

        // count corners
        for (byte i = 0; i < 4; ++i) {
            field = this.board[cornersX[i]][cornersY[i]];

            if (field == myColor) {
                ++myCorners;
            } else if (field == opColor) {
                ++opCorners;
            }
        }

        resultCorners = 25 * (myCorners - opCorners);

        // get mobility
        int[] mobilities = this.getMobility();
        int myMobility = mobilities[0];
        int opMobility = mobilities[1];
        if (myMobility > opMobility) {
            resultMobility = (100.0 * myMobility) / (myMobility + opMobility);
        } else if (myMobility < opMobility) {
            resultMobility = -(100.0 * opMobility) / (myMobility + opMobility);
        } else {
            resultMobility = 0;
        }

        //assign priorities to each criteria
        double tiles = 10 * resultTilesCount;
        double frontTiles = 75 * resultFrontTiles;
        double corners = 800 * resultCorners;
        double mobility = 80 * resultMobility;
        double outcome = 10 * resultFieldOutcomes;
        double stability = 30 * resultStability;

        return tiles + frontTiles + corners + +mobility + outcome + stability;
    }
}