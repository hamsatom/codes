package apk.reversiapk.core.btconnection;

/**
 * Class storing ConnectingThread instance so connection can be access by both {@link apk.reversiapk.core.players.BTPlayerLocal} and {@link apk.reversiapk.core.players.BTPlayerRemote}
 */
public class ConnectionHolder {
/**
 * Instance of this class that will be given if called
 */
private static ConnectionHolder instance;

/**
 * Instance of running Connection Thread
 */
private ConnectionThread connection;

private ConnectionHolder() {
}

/**
 * Thread safe method that returns instance of this class
 *
 * @return instance of this class
 */
public static synchronized ConnectionHolder getInstance() {
    if (instance == null) {
        instance = new ConnectionHolder();
    }
    return instance;
}

/**
 * @return Instance of ConnectionThread
 */
public ConnectionThread getConnection() {
    return this.connection;
}

/**
 * Set running instance of Connection Thread
 *
 * @param connection
 *         new Connection Thread
 */
public void setConnection(ConnectionThread connection) {
    this.connection = connection;
}
}
