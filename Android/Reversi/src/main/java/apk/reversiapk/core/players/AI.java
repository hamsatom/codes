package apk.reversiapk.core.players;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import apk.reversiapk.core.ReversiMove;

import java.util.Random;

/**
 * Dummy {@link Player} returns random valid move
 */
public class AI extends Player {
/**
 * Name of this player
 */
private static final String NAME = "Easy BOT";

/**
 * {@inheritDoc}
 */
@NonNull
@Override
public String getName() {
    return NAME;
}

/**
 * {@inheritDoc}
 */
@Nullable
@Override
public ReversiMove makeNextMove(byte[][] board) {
    Random rnd = new Random();
    int i = rnd.nextInt(getPossibleMoves().size());
    return getPossibleMoves().get(i);
}
}