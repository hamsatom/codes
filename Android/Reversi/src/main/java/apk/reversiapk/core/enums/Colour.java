package apk.reversiapk.core.enums;

import java.io.Serializable;

/**
 * Enum holding values of colours on board
 */
public enum Colour implements Serializable {
    /**
     * Colour of first playe's tones
     */
    PLAYER_1_COLOUR((byte) 1),
    /**
     * Colour of second player's stones
     */
    PLAYER_2_COLOUR((byte) 2),
    /**
     * Colour of empty field in board
     */
    EMPTY_SQUARE((byte) 0);

private final byte value;

Colour(byte value) {
    this.value = value;
}

/**
 * @return values of specified colour
 */
public byte getValue() {
    return value;
}
}
