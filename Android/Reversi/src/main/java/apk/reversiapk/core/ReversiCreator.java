package apk.reversiapk.core;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import apk.reversiapk.core.enums.Colour;
import apk.reversiapk.core.exceptions.PlayerInitException;
import apk.reversiapk.core.players.*;
import apk.reversiapk.graphic.activities.GameActivity;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.StreamHandler;

/**
 * Class that adds players to the game. cacthes exception and handles game in separate thread
 */
public class ReversiCreator implements Parcelable {
private static final Logger LOG = Logger.getLogger(ReversiCreator.class.getName());
public static final Parcelable.Creator<ReversiCreator> CREATOR = new Parcelable.Creator<ReversiCreator>() {
    @NonNull
    public ReversiCreator createFromParcel(@NonNull Parcel source) {
        return new ReversiCreator(source);
    }

    @NonNull
    public ReversiCreator[] newArray(int size) {
        return new ReversiCreator[size];
    }
};
/**
 * FIrst player added to the game
 */
@Nullable private Player player1;
/**
 * Second player added to the game
 */
private Player player2;
/**
 * Thread used for game logic
 */
private Thread gameLogicInstance;
/**
 * Controller used for game logic
 */
@Nullable private ReversiController controller;

public ReversiCreator() {
    LOG.addHandler(new StreamHandler());
}

private ReversiCreator(@NonNull Parcel in) throws OutOfMemoryError {
    this.player1 = (Player) in.readSerializable();
    this.player2 = (Player) in.readSerializable();
}

/**
 * Initialize Easy bot
 *
 * @throws PlayerInitException
 *         if initialization fails
 */
public void setPlayerAI() throws PlayerInitException {
    try {
        if (player1 == null) {
            this.player1 = new AI();
        } else {
            this.player2 = new AI();
        }
    } catch (Exception e) {
        LOG.severe("AI wasn't initialized");
        throw new PlayerInitException("AI wasn't initialized", e);
    }
}

/**
 * Initialize AIhamsa bot
 *
 * @throws PlayerInitException
 *         if initialization fails
 */
public void setPlayerAdvancedAI() throws PlayerInitException {
    try {
        if (player1 == null) {
            this.player1 = new AIhamsa();
        } else {
            this.player2 = new AIhamsa();
        }
    } catch (Exception e) {
        LOG.severe("AIhamsa wasn't initialized");
        throw new PlayerInitException("AIhamsa wasn't initialized", e);
    }
}

/**
 * Initialize bot
 *
 * @throws PlayerInitException
 *         if initialization fails
 */
public void setPlayerDita() throws PlayerInitException {
    try {
        if (player1 == null) {
            this.player1 = new AIdita();
        } else {
            this.player2 = new AIdita();
        }
    } catch (Exception e) {
        LOG.severe(e.getMessage());
        throw new PlayerInitException(e.getLocalizedMessage(), e);
    }
}

    /**
     * Initialize bot
     *
     * @throws PlayerInitException if initialization fails
     */
    public void setPlayerHrusa() throws PlayerInitException {
        try {
            if (player1 == null) {
                this.player1 = new AIhrusa();
            } else {
                this.player2 = new AIhrusa();
            }
        } catch (Exception e) {
            LOG.severe(e.getMessage());
            throw new PlayerInitException(e.getLocalizedMessage(), e);
        }
    }

    /**
 * Initialize human player
 *
 * @param name
 *         name of the player
 * @param fieldClickHandler
 *         handler added to it's field
 * @throws PlayerInitException
 *         if initialization fails
 */
public void setPlayerHuman(String name, FieldClickHandler fieldClickHandler) throws PlayerInitException {
    try {
        if (player1 == null) {
            this.player1 = new HumanPlayer(name, fieldClickHandler);
        } else {
            this.player2 = new HumanPlayer(name, fieldClickHandler);
        }
    } catch (Exception e) {
        LOG.severe("HumanPlayer wasn't initialized");
        throw new PlayerInitException("HumanPlayer wasn't initialized", e);
    }
}

/**
 * Initialize minmax bot
 *
 * @throws PlayerInitException
 *         if initialization fails
 */
public void setPlayerSimon() throws PlayerInitException {
    try {
        if (player1 == null) {
            this.player1 = new AIsimon();
        } else {
            this.player2 = new AIsimon();
        }
    } catch (Exception e) {
        LOG.severe("MinMax wasn't initialized");
        throw new PlayerInitException("MinMax wasn't initialized", e);
    }
}

/**
 * Initialize switch mode bot
 *
 * @throws PlayerInitException
 *         if initialization fails
 */
public void setPlayerAdaptive() throws PlayerInitException {
    try {
        if (player1 == null) {
            this.player1 = new Adaptive();
        } else {
            this.player2 = new Adaptive();
        }
    } catch (Exception e) {
        LOG.severe("AdaptivePlayer wasn't initialized");
        throw new PlayerInitException("AdaptivePlayer wasn't initialized", e);
    }
}

/**
 * Initilaiize remote Bluetooth player
 *
 * @param name
 *         name of the player
 * @throws PlayerInitException
 *         if initialization fails
 */
public void setBTRemotePlayer(String name) throws PlayerInitException {
    try {
        if (player1 == null) {
            this.player1 = new BTPlayerRemote(name);
        } else {
            this.player2 = new BTPlayerRemote(name);
        }
    } catch (Exception e) {
        LOG.severe("BTPlayerRemote wasn't initialized");
        throw new PlayerInitException("BTPlayerRemote wasn't wasn't initialized", e);
    }
}

/**
 * Initialize loval Bluetooth player
 *
 * @param name
 *         name of the player
 * @param clickHandler
 *         Handler used on their fields
 * @throws PlayerInitException
 *         if initialization fails
 */
public void setBTLocalPlayer(String name, FieldClickHandler clickHandler) throws PlayerInitException {
    try {
        if (player1 == null) {
            this.player1 = new BTPlayerLocal(name, clickHandler);
        } else {
            this.player2 = new BTPlayerLocal(name, clickHandler);
        }
    } catch (Exception e) {
        LOG.severe("BTPlayerRemote wasn't initialized");
        throw new PlayerInitException("BTPlayerRemote wasn't wasn't initialized", e);
    }
}

/**
 * Initialize new {@code AImtdf} player to empty slot
 *
 * @throws PlayerInitException
 *         if initialization fails
 */
public void setPlayerMtdf() throws PlayerInitException {
    try {
        if (player1 == null) {
            this.player1 = new AImtdf();
        } else {
            this.player2 = new AImtdf();
        }
    } catch (Exception e) {
        LOG.severe("Mtdf wasn't initialized");
        throw new PlayerInitException("Mtdf wasn't initialized", e);
    }
}

/**
 * Initialize new {@code AImcts} player to empty slot
 *
 * @throws PlayerInitException
 *         if initialization fails
 */
public void setPlayerMcts() throws PlayerInitException {
    try {
        if (player1 == null) {
            this.player1 = new AImcts();
        } else {
            this.player2 = new AImcts();
        }
    } catch (Exception e) {
        LOG.severe("Mcts wasn't initialized");
        throw new PlayerInitException("Mcts wasn't initialized", e);
    }
}

/**
 * Initialize greedy bot
 *
 * @throws PlayerInitException
 *         if initialization fails
 */
public void setPlayerGreedy() throws PlayerInitException {
    try {
        if (player1 == null) {
            this.player1 = new Greedy();
        } else {
            this.player2 = new Greedy();
        }
    } catch (Exception e) {
        LOG.severe("GreedyPlayer wasn't initialized");
        throw new PlayerInitException("GreedyPlayer wasn't initialized", e);
    }
}

/**
 * Remove first player so the init method add on first position and rewrites the second
 */
public void removePlayers() {
    this.player1 = null;
}

/**
 * Creates new game controller
 *
 * @param gameActivity
 *         gui of game
 */
public void CreateGame(final GameActivity gameActivity) {
    controller = new ReversiController(player1, player2, gameActivity);
    playInSepareteThread();
}

/**
 * Starts game in separate thread and handles it's exceptions
 */
private void playInSepareteThread() {
    Runnable runGame = new Runnable() {
        @Override
        public void run() {
            try {
                assert controller != null;
                controller.play();
            } catch (InterruptedException e) {
                gameLogicInstance.interrupt();
                LOG.log(Level.INFO, "Thread interrupted", e);
            } catch (Throwable e) {
                LOG.log(Level.SEVERE, "Unknown exception happened", e);
                System.exit(1);
            }
        }
    };
    gameLogicInstance = new Thread(runGame);
    gameLogicInstance.start();
}

/**
 * Sets game for annother game than plays it in new thread
 */
public void restartGame() {
    assert controller != null;
    controller.restart();
    playInSepareteThread();
}

/**
 * Interrupts game thread
 */
public void endGameLogic() {
    gameLogicInstance.interrupt();
    LOG.info("GameActivity logic interrupted");
}

/**
 * Returns possible moves of current player
 */
public final ArrayList<ReversiMove> getCurrentPossible() {
    assert controller != null;
    return controller.curretPossible();
}

/**
 * @return colour of current player
 */
public final Colour getCurrentColour() {
    assert controller != null;
    return controller.getOnMove();
}

@Override
public int describeContents() {
    return 0;
}

@Override
public void writeToParcel(@NonNull Parcel dest, int flags) {
    dest.writeSerializable(this.player1);
    dest.writeSerializable(this.player2);
}
}