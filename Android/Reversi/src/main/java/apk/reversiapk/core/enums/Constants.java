package apk.reversiapk.core.enums;

/**
 * Enum storing constant values used in this application
 * Because most of these constants are used all over the application and there aren't so many of them it'S easire to
 * store them in one Enum than declaring them as static finals
 */
public enum Constants {
    /**
     * Number of rows on reversi board
     */
    BOARD_ROWS((byte) 8),
    /**
     * Number of columns on reversi board
     */
    BOARD_COLUMNS((byte) 8),
    /**
     * Number of stones that each player have on start of the game
     */
    NUMBER_OF_STONES_AT_START((byte) 2),
    /**
     * Number of players in one reversi game
     */
    PLAYER_LIMIT((byte) 2),
    /**
     * String used as ID while getting data from an Intent
     */
    NICK_ID("Nickname"),
    /**
     * Maximum number of characters that can be in user'S name
     */
    NICK_MAX_LENGTH((byte) 12),
    /**
     * Request code for various data while starting activity for result
     */
    REQUEST_CODE((byte) 1),
    /**
     * Message to be put in toaster while user left only whitespace name
     */
    NICK_EMPTY_MSG("Nick is empty, insert nick"),
    /**
     * Message to be put in toaster while user's name was longer than {@code NICK_MAX_LENGTH}
     */
    NICK_LONG_MSG("Nick longer than 12 chars"),
    /**
     * Message to be put in toaster when no bluetooth adapter was found
     */
    NO_BT_MSG("No bluetooth detected"),
    /**
     * Message to be put in toaster when user went back from settings without turning on BT
     */
    BT_OFF_MSG("Bluetooth must be on and visible"),
    /**
     * Number of player that won passed in extra
     */
    EXTRA_PLAYER_COUNT("playerCount"),
    /**
     * Name of the player who won passed in extra
     */
    EXTRA_WINNER_NAME("winnerName"),
    /**
     * Score that the winning player achieved
     */
    EXTRA_SCORE("score"),
    /**
     * Code for a field wher is possible to place a stone. Used as a tah for stone img.
     */
    MARKED_POSSIBLE((byte) 1),
    /**
     * Tag shown with paired bluetooth devices
     */
    PAIRED_TAG(" (Paired)"),
    /**
     * Tag show with found bluetooth devices
     */
    DISCOVERED_TAG(" (Found) "),
    /**
     * Tag shown with both paired and found devices
     */
    DISCOVERED_N_PAIRED_TAG(" (Paired + Visible) "),
    /**
     * Message show while player can't be initialized
     */

    EXCEPTION_PLAYER_INIT_MSG("Exception while adding player. Player wasn't added."),
    /**
     * Bluetooth UUID of this game
     */
    UUID_STRING("00001101-0000-1000-8000-00805F9B34FB"),
    /**
     * Message in connectin exception
     */
    CONNECTION_EXCEPTION("Connection exception"),
    /**
     * Message in exception which occurs while accepting a socket
     */
    SOCKET_ACCEPTING_EXCEPTION("Exception at accepting socket"),
    /**
     * Message shown with exception while closing a socket
     */
    SOCKET_CLOSING_EXCEPTION("Exception while closing socket"),
    /**
     * Message from exception shown while sending date over BT
     */
    SENDING_EXCEPTION("Exception while sending data"),
    /**
     * Message shown while connection can't be establish
     */
    CONNECTION_EXCEPTION_MSG("Couldn't connect, please try again."),
    /**
     * String identifier of click handler passed in extra
     */
    CLICK_HANDLER_ID("Click Handler"),
    /**
     * Number of players stored in highscore file
     */
    MAX_HIGHSCORE_LINES((byte) 10),
    /**
     * IDentifier of Creator passed in extra
     */
    CREATOR_ID("Creator");

private byte value;
private String id;

/**
 * {@inheritDoc}
 */
Constants(byte b) {
    this.value = b;
}

/**
 * {@inheritDoc}
 */
Constants(String id) {
    this.id = id;
}

/**
 * @return return message
 */
public final String getMsg() {
    return id;
}

/**
 * @return value of enum
 */
public final byte getValue() {
    return value;
}
}
