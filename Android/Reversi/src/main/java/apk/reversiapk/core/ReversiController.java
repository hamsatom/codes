package apk.reversiapk.core;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import apk.reversiapk.core.enums.Colour;
import apk.reversiapk.core.enums.Constants;
import apk.reversiapk.core.players.AIwithMemory;
import apk.reversiapk.core.players.Player;
import apk.reversiapk.graphic.activities.GameActivity;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.StreamHandler;

/**
 * Controls logic of Reversi game
 * Calls players when they are on move and updates board
 * Also calls methods in {@link GameActivity} to update shown board
 */
class ReversiController implements Serializable {
private static final Logger LOG = Logger.getLogger(ReversiController.class.getName());
/**
 * Playground grid
 */
private static final Colour[][] placement = new Colour[Constants.BOARD_COLUMNS.getValue()][Constants.BOARD_ROWS.getValue()];
/**
 * Pause time between thread actions
 */
private static final short WAIT_TIME = 250;
    /**
 * Activity showing GUI of the game
 */
private final GameActivity graphicBoard;
/**
 * First player
 */
private Player player1;
/**
 * Second player
 */
private Player player2;
/**
 * Player that is currently on move
 */
private Player onMove;
/**
 * Stores all possible move that {@code onMove} can play
 */
private ArrayList<ReversiMove> possibleMoves;
/**
 * Amount of stones player 1 currently has
 */
private byte player1Stones;
/**
 * AMount of stones second player has
 */
private byte player2Stones;

ReversiController(Player player1, Player player2, GameActivity gameActivity) {
    LOG.addHandler(new StreamHandler());
    this.player1 = player1;
    this.player2 = player2;
    this.graphicBoard = gameActivity;
    initBoard();
}

/**
 * Loop that controls the while game
 * As 1 player can play various number of moves while the second has no moves available this is suitable solution
 * Loop also stops if thread is interrupted
 *
 * @throws InterruptedException
 *         if activity showing the game is closed game is interrupted
 *         otherwise the game would run in menu after pressing back
 */
void play() throws InterruptedException {
    while (!Thread.interrupted()) {
        if (canPlay(onMove.getEnumColour())) {
            updateGraphics();
            playOneMove();
        } else {
            switchPlayers();
            if (onMove == player1) {
                graphicBoard.shownOnMove1();
            } else {
                graphicBoard.shownOnMove2();
            }
            if (!canPlay(onMove.getEnumColour())) {
                LOG.info("End of game");
                graphicBoard.updateBoard(placement);
                graphicBoard.updateStones(player1Stones, player2Stones);
                graphicBoard.showEndingScreen(pickWinner(), evaluate());

                return;
            }
        }
    }
}

/**
 * Returns the winner of game session also clears transposition table if winner is {@link AIwithMemory} so the winner can be passed to {@link apk.reversiapk.graphic.activities.EndingScreen} without
 * memory conflicts
 * @return {@code Player} that won the game, {@code null} if it's a draw
 */
@Nullable
private Player pickWinner() {
    if (player1 instanceof AIwithMemory) {
        ((AIwithMemory) player1).transpositionTable.clear();
    }
    if (player2 instanceof AIwithMemory) {
        ((AIwithMemory) player2).transpositionTable.clear();
    }

    if (player1Stones > player2Stones) {
        return player1;
    } else if (player2Stones > player1Stones) {
        return player2;
    } else {
        return null;
    }
}

/**
 * Counts empty squares and stones each player have and calculates their score according to it
 *
 * @return score of the winning player
 */
private int evaluate() {
    int occupied = 0;
    for (Colour[] places : placement) {
        for (Colour onePlace : places) {
            if (onePlace != Colour.EMPTY_SQUARE) {
                ++occupied;
            }
        }
    }
    LOG.info("Occupied " + occupied);
    LOG.info(String.valueOf("Fields " + Constants.BOARD_COLUMNS.getValue() * Constants.BOARD_ROWS.getValue()));
    LOG.info(String.valueOf("Quotient " + (Constants.BOARD_COLUMNS.getValue() * Constants.BOARD_ROWS.getValue()) /
                                          occupied));
    LOG.info(String.valueOf("Player 2 stones " + player2Stones));
    if (player1Stones > player2Stones) {
        return player1Stones * (Constants.BOARD_COLUMNS.getValue() * Constants.BOARD_ROWS.getValue() / occupied);
    } else if (player2Stones > player1Stones) {
        return player2Stones * (Constants.BOARD_COLUMNS.getValue() * Constants.BOARD_ROWS.getValue() / occupied);
    } else {
        return 0;
    }
}

/**
 * Starts the game again after one have been player
 */
void restart() {
    Player tmp = player1;
    player1 = player2;
    player2 = tmp;
    initBoard();
    LOG.info("Game was restarted");
}


/**
 * Updates {@code placement} after on move was played also switches the players
 *
 * @throws InterruptedException
 *         if game was closed while {@code HumanPlayer} on move
 */
private void playOneMove() throws InterruptedException {
    ReversiMove lastMove = onMove.makeNextMove(boardCopy());
    assert lastMove != null;
    LOG.info(onMove.getName() + " played to: " + lastMove.x + "," + lastMove.y);
    placeTakenMove(onMove.getEnumColour(), lastMove);
    possibleMoves.clear();
    switchPlayers();
}

/**
 * @return deep copy of {@code placement} with bytes values instead of enum
 */
@NonNull
private byte[][] boardCopy() {
    byte[][] copy = new byte[Constants.BOARD_COLUMNS.getValue()][Constants.BOARD_ROWS.getValue()];
    for (byte y = 0; y < Constants.BOARD_COLUMNS.getValue(); ++y) {
        for (byte x = 0; x < Constants.BOARD_ROWS.getValue(); ++x) {
            copy[x][y] = placement[x][y].getValue();
        }
    }
    return copy;
}

/**
 * Manages all changes in {@link GameActivity}
 *
 * @throws InterruptedException
 *         if game was closed while updating graphics
 */
private void updateGraphics() throws InterruptedException {
    Thread.sleep(WAIT_TIME);
    graphicBoard.updateBoard(placement);
    Thread.sleep(WAIT_TIME);
    graphicBoard.updateStones(player1Stones, player2Stones);
    if (onMove == player1) {
        graphicBoard.shownOnMove1();
    } else {
        graphicBoard.shownOnMove2();
    }
    graphicBoard.setPossibleField(onMove.getEnumColour(), (List<ReversiMove>) possibleMoves.clone());
    Thread.sleep(2 * WAIT_TIME);
}

/**
 * Switches who is currently on move
 */
private void switchPlayers() {
    if (onMove == player1) {
        onMove = player2;
        return;
    }
    onMove = player1;
}

/**
 * Sets up the play board
 * Adds start stones to players and place their first stones
 */
private void initBoard() {
    fillBoard();
    graphicBoard.updateNames(player1.getName(), player2.getName());
    placement[Constants.BOARD_COLUMNS.getValue() / 2 - 1][Constants.BOARD_ROWS.getValue() / 2] = placement[Constants.BOARD_COLUMNS.getValue() / 2][Constants.BOARD_ROWS.getValue() / 2 - 1] = Colour.PLAYER_1_COLOUR;
    placement[Constants.BOARD_COLUMNS.getValue() / 2][Constants.BOARD_ROWS.getValue() / 2] = placement[Constants
                                                                                                               .BOARD_COLUMNS.getValue() / 2 - 1][Constants.BOARD_ROWS.getValue() / 2 - 1] = Colour.PLAYER_2_COLOUR;
    player1.setMyColor(Colour.PLAYER_1_COLOUR);
    player1.setOpponentColor(Colour.PLAYER_2_COLOUR);
    player2.setMyColor(Colour.PLAYER_2_COLOUR);
    player2.setOpponentColor(Colour.PLAYER_1_COLOUR);
    player2Stones = player1Stones = Constants.NUMBER_OF_STONES_AT_START.getValue();
    possibleMoves = new ArrayList<>(34);
    onMove = player1;
}

/**
 * Fill board with empty squares
 */
private void fillBoard() {
    for (byte y = 0; y < Constants.BOARD_COLUMNS.getValue(); ++y) {
        for (byte x = 0; x < Constants.BOARD_ROWS.getValue(); ++x) {
            placement[x][y] = Colour.EMPTY_SQUARE;
        }
    }
}

/**
 * Finds if player can play, add their possible moves to {@code possibleMoves}
 *
 * @param playersColour
 *         colour of player that is controlled
 * @return true if player has some moves, false if not
 */
private boolean canPlay(Colour playersColour) {
    player1Stones = 0;
    player2Stones = 0;
    ReversiMove move;
    boolean canPlay = false;
    byte row;

    for (byte column = 0; column < Constants.BOARD_COLUMNS.getValue(); ++column) {
        for (row = 0; row < Constants.BOARD_ROWS.getValue(); ++row) {
            if (placement[column][row] == Colour.PLAYER_1_COLOUR) {
                ++player1Stones;
                continue;
            }
            if (placement[column][row] == Colour.PLAYER_2_COLOUR) {
                ++player2Stones;
                continue;
            }
            move = new ReversiMove(column, row);
            if (isCorrectMove(playersColour, move)) {
                canPlay = true;
                possibleMoves.add(move);

            }
        }
    }

    onMove.setPossibleMoves((ArrayList<ReversiMove>) possibleMoves.clone());
    return canPlay;
}

/**
 * Check different directions if {@code move} turns stones in any of them
 *
 * @param playersColour
 *         colour of player that is to play
 * @param move
 *         one move that should be valid
 * @return true is it'S a valid move, false if not
 */
private boolean isCorrectMove(Colour playersColour, @NonNull ReversiMove move) {
    byte column = move.x;
    byte row = move.y;

    return isCorrectDirection(playersColour, 1, 0, column, row) ||
           isCorrectDirection(playersColour, 1, 1, column, row) ||
           isCorrectDirection(playersColour, 0, 1, column, row) ||
           isCorrectDirection(playersColour, -1, 1, column, row) ||
           isCorrectDirection(playersColour, -1, 0, column, row) ||
           isCorrectDirection(playersColour, -1, -1, column, row) ||
           isCorrectDirection(playersColour, 0, -1, column, row) ||
           isCorrectDirection(playersColour, 1, -1, column, row);
}

/**
 * Check if stone is valid in specified direction
 *
 * @param playersColour
 *         colour of player that is to play
 * @param dirX
 *         moves in x direction
 * @param dirY
 *         moves in y direction
 * @param column
 *         column where the move should be placed
 * @param row
 *         row where the move should be placed
 * @return true if move is valid is this direction, false if not
 */
private boolean isCorrectDirection(Colour playersColour, int dirX, int dirY, byte column, byte row) {
    boolean foundOpponent = false;
    boolean endingMyStone = false;

    while (!endingMyStone) {
        column += dirX;
        row += dirY;
        if (column < 0 || column >= Constants.BOARD_COLUMNS.getValue() || row < 0 || row >= Constants.BOARD_ROWS
                                                                                                    .getValue()) {
            return false;
        }
        if (placement[column][row] == Colour.EMPTY_SQUARE) {
            return false;
        }
        if (placement[column][row] != playersColour) {
            foundOpponent = true;
        } else {
            endingMyStone = true;
        }
    }
    return foundOpponent;
}

/**
 * Flip stones on board after played move
 *
 * @param playersColour
 *         colour of player that moved
 * @param move
 *         move that the player made
 * @throws InterruptedException
 *         if application closed while updating
 */
private void placeTakenMove(Colour playersColour, @NonNull ReversiMove move) throws InterruptedException {
    byte column = move.x;
    byte row = move.y;

    placement[column][row] = playersColour;
    graphicBoard.updateBoard(placement);

    flipDirection(playersColour, 1, 0, column, row);
    flipDirection(playersColour, 1, 1, column, row);
    flipDirection(playersColour, 1, -1, column, row);
    flipDirection(playersColour, -1, 1, column, row);
    flipDirection(playersColour, -1, 0, column, row);
    flipDirection(playersColour, -1, -1, column, row);
    flipDirection(playersColour, 0, -1, column, row);
    flipDirection(playersColour, 0, 1, column, row);
}


/**
 * Flip all stones in 1 direction if they should be flipped
 *
 * @param playersColour
 *         colour of player that played
 * @param dirX
 *         moves in x direction
 * @param dirY
 *         moves in y direction
 * @param column
 *         column where the move was placed
 * @param row
 *         row of it
 * @throws InterruptedException
 *         if application closed while updating
 */
private void flipDirection(Colour playersColour, int dirX, int dirY, byte column, byte row) throws
                                                                                            InterruptedException {
    boolean neighbourOpponent = false;
    boolean endingMyStone = false;
    byte steps = 0;

    // first check if the direction should be turned
    while (!endingMyStone) {
        column += dirX;
        row += dirY;
        ++steps;

        if (column < 0 || column >= Constants.BOARD_COLUMNS.getValue() || row < 0 || row >= Constants.BOARD_ROWS
                                                                                                    .getValue()) {
            break;
        }
        if (placement[column][row] == Colour.EMPTY_SQUARE) {
            break;
        }

        if (placement[column][row] != playersColour) {
            neighbourOpponent = true;
        } else {
            endingMyStone = true;
        }
    }

    // flip the stones if they should be flipped
    if (neighbourOpponent && endingMyStone) {
        dirX *= -1;
        dirY *= -1;
        for (byte cell = 1; cell < steps; ++cell) {
            Thread.sleep(WAIT_TIME / 2);
            column += dirX;
            row += dirY;
            placement[column][row] = playersColour;
            graphicBoard.showTurned(column, row);
        }
    }
}

/**
 * @return possible moves of player currently on move
 */
final ArrayList<ReversiMove> curretPossible() {
    return possibleMoves;
}

/**
 * @return colour of player currently on move
 */
final Colour getOnMove() {
    return onMove.getEnumColour();
}
}