package apk.reversiapk.core.exceptions;

import java.io.IOException;

/**
 * Exception thrown while sending over Bluetooth fails
 */
public class SendingException extends Exception {
/**
 * @param s
 *         Message in {@link SendingException}
 * @param e
 *         {@inheritDoc}
 */
public SendingException(String s, IOException e) {
    super(s, e);
}
}
