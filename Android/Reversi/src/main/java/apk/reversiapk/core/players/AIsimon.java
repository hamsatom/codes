package apk.reversiapk.core.players;

import android.support.annotation.NonNull;
import apk.reversiapk.core.ReversiMove;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.StreamHandler;


/**
 * Uses Principal Variation Search with Aspiration Windows and Enhanced Forward Pruning to find the best move. Also uses Principal Variation and move score ordering to search
 * more promising moves first. Depth is variant and is determined according to time needed for last depth. Certain depths are visited in iterative deepening that increases depth by 2 to avoid very
 * different odd and even depth scores. Because this bot uses Transposition table, visited board combination are stored and not searched again.
 * @see <a href="https://chessprogramming.wikispaces.com/Principal+Variation+Search/">Principal Variation Search</a>
 * @see <a href="https://chessprogramming.wikispaces.com/Enhanced+Forward+Pruning/">Enhanced Forward Pruning</a>
 * @see <a href="https://chessprogramming.wikispaces.com/Aspiration+Windows/">Aspiration Windows</a>
 * @see <a href="https://chessprogramming.wikispaces.com/Principal+variation/">Principal variation</a>
 */
public class AIsimon extends AIwithMemory {
/**
 * Name of this bot
 */
private static final String NAME = "DarkUnpacker";
private static final Logger LOG = Logger.getLogger(AIsimon.class.getName());
/**
 * Number of nodes searched in reduced depth to cause cut-off
 */
private static final byte M = 6;
/**
 * Number of cut node children to cut-off current node
 *
 * @see <a href="https://chessprogramming.wikispaces.com/Node+Types/">Node types</a>
 */
private static final byte C = 3;
/**
 * Node type masks. If masks were {@code Enum} operation like {@code -type} wouldn't be possible thus is more convenient declare them this way
 */
private static final byte CUT_NODE = 1, PV_NODE = 0, ALL_NODE = -1;
/**
 * Stored Principal Variation found in previous iteration. Cleared after each {@code makeNextMove} call.
 */
private final ArrayDeque<ReversiMove> pvMoves = new ArrayDeque<>(130);
/**
 * The initial maximum bot this bot goes to. Usually initialized to 2 then increased while is still time by 2.
 */
private byte maximalDepth;
/**
 * Value of current move
 */
private double value;
private Node node;
/**
 * Time spend finding best move since {@code makeNextMove} was called
 */
private long timeSpend;
/**
 * TIme spend on searching in this depth
 */
private long timeForThisDepth;
/**
 * Time spend on searching previous depth, depth is {@code maximalDepth  - 2} if {@code maximalDepth} is greater than 2
 */
private long timePreviousDepth;
/**
 * Queasiest of difference between last and previous depth used to guess how long is next depth gonna take
 */
private double q;
/**
 * Searching first children
 */
private boolean first;

public AIsimon() {
    super.transpositionTable = new HashMap<>(25000, 1.0f);
    LOG.addHandler(new StreamHandler());
}

/**
 * {@inheritDoc}
 */
@NonNull
@Override
public String getName() {
    return NAME;
}

/**
 * Compares all of my possible moves.
 *
 * @param board
 *         current state of the board
 * @return best of available moves
 */
@Override
public ReversiMove makeNextMove(byte[][] board) {
    timeStart = System.nanoTime();
    moves = getPossibleMoves();
    myColor = getMyColor();
    opponentColor = getOpponentColor();
    maximalDepth = 2;
    timePreviousDepth = timeSpend = timeForThisDepth = 0;
    desc = comp.board = board;
    comp.adjustBoardRating();
    Collections.sort(moves, comp.comparator);
    bestMove = moves.get(0);

    while (timeSpend + timeForThisDepth < TIME_LIMIT && maximalDepth < 60) {
        timeForThisDepth = System.nanoTime();
        bestValue = Double.NEGATIVE_INFINITY;
        first = true;
        Collections.sort(moves, maxComparator);

        for (ReversiMove move : moves) {
            desc = comp.flipDiscs(desc, move.x, move.y, myColor, opponentColor);
            ID = boardId(desc);
            node = (Node) transpositionTable.get(ID);

            if (node != null) {
                if (node.depth >= maximalDepth) {
                    value = node.score;
                } else {
                    possibleMoves = node.possibleMoves;
                    getValueFromPVS();
                    node.score = value;
                    node.depth = maximalDepth;
                    Collections.sort(possibleMoves, minComparator);
                }
            } else {
                possibleMoves = comp.getAllPossibleTurns(desc, opponentColor);
                getValueFromPVS();
                Collections.sort(possibleMoves, minComparator);
                transpositionTable.put(ID, new Node(maximalDepth, value, possibleMoves));
            }
            desc = comp.unflipDiscs(desc, opponentColor);
            move.score = value;

            if (value > bestValue) {
                bestValue = value;
                bestMove = move;
                if (value >= Double.POSITIVE_INFINITY) {
                    break;
                }
            }
        }
        maximalDepth += 2;
        timeSpend = System.nanoTime() - timeStart;
        timeForThisDepth = System.nanoTime() - timeForThisDepth;
        q = (timeForThisDepth + 0.0) / (timePreviousDepth + 0.0);
        q = q > 1 && q != Double.POSITIVE_INFINITY ? q : 1;
        timePreviousDepth = timeForThisDepth;
        timeForThisDepth *= q;
    }

    LOG.info(NAME + " time spend " + timeSpend + " for depth " + (maximalDepth - 2));

    pvMoves.clear();
    moves.clear();
    possibleMoves.clear();
    comp.flippedDiscs.clear();
    return bestMove;
}

/**
 * Either evaluates the state if no other moves are possible or it get value from {@code principalVariationSearch} if started from node. This method is necessary to initialized start variables in
 * {@code principalVariationSearch}.
 */
private void getValueFromPVS() {
    if (possibleMoves.isEmpty()) {
        value = comp.evaluateTurn(desc, myColor, opponentColor);
    } else if (first) {
        value = principalVariationSearch(possibleMoves, desc, bestValue, Double.POSITIVE_INFINITY, maximalDepth - 1, opponentColor, myColor, false, PV_NODE);
        first = false;
    } else {
        value = principalVariationSearch(possibleMoves, desc, bestValue, bestValue + 1, maximalDepth - 1, opponentColor, myColor, false, CUT_NODE);
        if (value > bestValue && value < Double.POSITIVE_INFINITY || value == Double.POSITIVE_INFINITY && bestValue + 1 == Double.POSITIVE_INFINITY) {
            if (value == bestValue + 1) {
                value = bestValue;
            }
            value = principalVariationSearch(possibleMoves, desc, value, Double.POSITIVE_INFINITY, maximalDepth - 1, opponentColor, myColor, false, PV_NODE);
        }
    }
}

/**
 * Recursively grows the minnmax tree and return the maximal/minimal value the player can obtain.
 * Principal Variation Search (PVS),
 * an enhancement to Alpha-Beta, based on null- or zero window searches of none PV-nodes, to prove a move is worse or not than an already safe score from the principal variation.

 * @param moves
 *         list of all possible moves available to player
 * @param board
 *         current game state
 * @param alpha
 *         value of alpha
 * @param beta
 *         value of beta
 * @param depth
 *         remaining recursive depth
 * @param color1
 *         color of the player to whom the board is evaluated
 * @param color2
 *         color of the other player
 * @param maximizing
 *         true if we want the player to maximize his outcome,
 *         otherwise false
 * @param type type of the current node
 * @return the best possible value
 * @see <a href="https://chessprogramming.wikispaces.com/Principal+Variation+Search/">Principal Variation Search</a>
 */
private double principalVariationSearch(@NonNull ArrayList<ReversiMove> moves, byte[][] board, double alpha, double beta, int depth, final byte color1, final byte color2, final boolean maximizing,
                                        int type) {
    /**
     * Value of current move
     */
    double value;
    /**
     * Moves that could be played after {@code move} found in {@code moves}
     */
    ArrayList<ReversiMove> possibleMoves;
    Node node;
    /**
     * ID of current board state
     */
    String ID;

    if (!pvMoves.isEmpty() && moves.contains(pvMoves.peekFirst())) {
        type = PV_NODE;
        moves.remove(pvMoves.peekFirst());
        moves.add(0, pvMoves.pollFirst());
    }

    if (type == CUT_NODE && depth > 2) {
        /**
         * Number of cut nodes found under this node
         */
        byte c = 0;
        /**
         * Number of nodes searched
         */
        byte m = 0;
        /**
         * Depth in which I'm searching
         */
        int reducedDepth = depth - 1;
        for (ReversiMove move : moves) {
            board = comp.flipDiscs(board, move.x, move.y, color1, color2);
            ID = boardId(board);
            node = (Node) transpositionTable.get(ID);
            if (node != null) {
                if (node.depth >= reducedDepth) {
                    value = node.score;
                } else {
                    possibleMoves = node.possibleMoves;
                    if (possibleMoves.isEmpty()) {
                        value = comp.evaluateTurn(board, myColor, opponentColor);
                    } else {
                        value = principalVariationSearch(possibleMoves, board, alpha, beta, reducedDepth - 1, color2, color1, !maximizing, ALL_NODE);
                    }
                    node.score = value;
                    node.depth = reducedDepth;

                    if (maximizing) {
                        Collections.sort(possibleMoves, minComparator);
                    } else {
                        Collections.sort(possibleMoves, maxComparator);
                    }
                }
            } else {
                possibleMoves = comp.getAllPossibleTurns(board, color2);
                if (possibleMoves.isEmpty()) {
                    value = comp.evaluateTurn(board, myColor, opponentColor);
                } else {
                    value = principalVariationSearch(possibleMoves, board, alpha, beta, reducedDepth - 1, color2, color1, !maximizing, ALL_NODE);
                }

                if (maximizing) {
                    Collections.sort(possibleMoves, minComparator);
                } else {
                    Collections.sort(possibleMoves, maxComparator);
                }
                transpositionTable.put(ID, new Node(reducedDepth, value, possibleMoves));
            }
            board = comp.unflipDiscs(board, color2);
            move.score = value;

            if (value >= beta || value <= alpha) {
                ++c;
                if (c >= C) {
                    return beta;
                }
            }
            if (++m >= M) {
                break;
            }
        }
    }

    /**
     * {@code true} if it's first move search from this node, {@code falsee} if not
     */
    boolean first = true;
    /**
     * best value found so far
     */
    double bestValue = maximizing ? Double.NEGATIVE_INFINITY : Double.POSITIVE_INFINITY;
    for (ReversiMove move : moves) {
        board = comp.flipDiscs(board, move.x, move.y, color1, color2);
        ID = boardId(board);
        node = (Node) transpositionTable.get(ID);
        if (node != null) {
            if (node.depth >= depth) {
                value = node.score;
            } else {
                possibleMoves = node.possibleMoves;
                if (possibleMoves.isEmpty() || depth == 1) {
                    value = comp.evaluateTurn(board, myColor, opponentColor);
                } else if (first) {
                    value = principalVariationSearch(possibleMoves, board, alpha, beta, depth - 1, color2, color1, !maximizing, -type);
                    first = false;
                } else {
                    value = principalVariationSearch(possibleMoves, board, alpha, alpha + 1, depth - 1, color2, color1, !maximizing, type == CUT_NODE ? ALL_NODE : CUT_NODE);
                    if (value > alpha && value < beta || type == PV_NODE && value == beta && beta == alpha + 1) {
                        if (value == alpha + 1) {
                            value = alpha;
                        }
                        value = principalVariationSearch(possibleMoves, board, value, beta, depth - 1, color2, color1, !maximizing, type);
                    }
                }
                node.score = value;
                node.depth = depth;

                if (maximizing) {
                    Collections.sort(possibleMoves, minComparator);
                } else {
                    Collections.sort(possibleMoves, maxComparator);
                }
            }
        } else {
            possibleMoves = comp.getAllPossibleTurns(board, color2);
            if (possibleMoves.isEmpty() || depth == 1) {
                value = comp.evaluateTurn(board, myColor, opponentColor);
            } else if (first) {
                value = principalVariationSearch(possibleMoves, board, alpha, beta, depth - 1, color2, color1, !maximizing, -type);
                first = false;
            } else {
                value = principalVariationSearch(possibleMoves, board, alpha, alpha + 1, depth - 1, color2, color1, !maximizing, type == CUT_NODE ? ALL_NODE : CUT_NODE);
                if (value > alpha && value < beta || type == PV_NODE && value == beta && beta == alpha + 1) {
                    if (value == alpha + 1) {
                        value = alpha;
                    }
                    value = principalVariationSearch(possibleMoves, board, value, beta, depth - 1, color2, color1, !maximizing, type);
                }
            }
            if (maximizing) {
                Collections.sort(possibleMoves, minComparator);
            } else {
                Collections.sort(possibleMoves, maxComparator);
            }
            transpositionTable.put(ID, new Node(depth, value, possibleMoves));
        }
        board = comp.unflipDiscs(board, color2);
        move.score = value;

        if (maximizing) {
            if (value > bestValue) {
                if (value >= beta) {
                    return value;
                }
                bestValue = value;
            }
            if (value > alpha) {
                alpha = value;
                pvMoves.addFirst(move);
            }
        } else {
            if (value < bestValue) {
                if (value <= alpha) {
                    return value;
                }
                bestValue = value;
            }
            if (value < beta) {
                beta = value;
                pvMoves.addFirst(move);
            }
        }
    }
    return bestValue;
}
}