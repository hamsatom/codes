package apk.reversiapk.core.btconnection;

import android.bluetooth.BluetoothSocket;
import android.support.annotation.NonNull;
import apk.reversiapk.core.ReversiMove;
import apk.reversiapk.core.exceptions.SendingException;
import apk.reversiapk.core.exceptions.StreamInitException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.StreamHandler;
import org.apache.commons.lang3.SerializationUtils;

/**
 * Maintains connection between two devices
 */
public class ConnectionThread extends Thread {
private static final Logger LOG = Logger.getLogger(ConnectionThread.class.getName());
/**
 * Connected socked obtained earlier viw rf  channel
 */
@NonNull private final BluetoothSocket socket;
/**
 * Sttream of input data
 */
private final InputStream inStream;
/**
 * Stream of ouput data
 */
private final OutputStream outStream;
/**
 * Name of enemy Bluetooth player that was received
 */
private volatile String receivedName;
/**
 * Last recevied enemy move
 */
private volatile ReversiMove receivedMove;

/**
 * Opens communication stream
 *
 * @param socket
 *         Socket shared between devices
 * @throws StreamInitException
 *         if stream failed to open
 */
public ConnectionThread(@NonNull BluetoothSocket socket) throws StreamInitException {
    LOG.addHandler(new StreamHandler());
    this.socket = socket;
    InputStream tmpIn;
    OutputStream tmpOut;
    try {
        tmpIn = socket.getInputStream();
        tmpOut = socket.getOutputStream();
    } catch (IOException e) {
        throw new StreamInitException("Exception while initializing streams", e);
    }
    inStream = tmpIn;
    outStream = tmpOut;
}

/**
 * Maintans connection in new Thread
 */
@Override
public void run() {
    byte[] buffer = new byte[1024];  // buffer store for the stream
    int bytes; // bytes returned from read()

    // Keep listening to the InputStream until an exception occurs
    while (true) {
        try {
            // Read from the InputStream
            bytes = inStream.read(buffer);
            // Saves received data
            // First transmission is always the name
            if (receivedName != null) {
                receivedMove = (ReversiMove) SerializationUtils.deserialize(buffer);
                LOG.info(receivedMove.getX() + " " + receivedMove.getY());
            } else {
                receivedName = new String(buffer, 0, bytes);
            }
        } catch (IOException e) {
            LOG.log(Level.FINE, "End of transaction", e);
            break;
        }
    }
}

/**
 * Send Reversimove to remote device
 *
 * @param move
 *         played ReversiMove
 * @throws SendingException
 *         when sending wasn't successful
 */
public void sendMove(ReversiMove move) throws SendingException {
    try {
        outStream.write(SerializationUtils.serialize(move));
    } catch (IOException e) {
        throw new SendingException("Exceptiong while sending stream", e);
    }
}

/**
 * Sends name to remote device
 *
 * @param name
 *         local player name
 * @throws SendingException
 *         when sending was not successful
 */
public void sendName(@NonNull String name) throws SendingException {
    LOG.info(name);
    try {
        outStream.write(name.getBytes());
    } catch (IOException e) {
        throw new SendingException("Exceptiong while sending stream", e);
    }
}

/**
 * Stops connection between devices. Closes socket, streams and interrupts this (@link Thread)
 */
public void stopConnection() {
    try {
        inStream.close();
        outStream.close();
        socket.close();
    } catch (IOException e) {
        LOG.log(Level.INFO, "Exception while ending connection", e);
    } catch (NullPointerException e) {
        LOG.log(Level.INFO, "Connection stopped before initialization", e);
    } finally {
        Thread.currentThread().interrupt();
    }
}

/**
 * @return received Name
 */
public String getReceivedName() {
    return receivedName;
}

/**
 * @return last received ReversiMove
 */
public ReversiMove getReceivedMove() {
    return receivedMove;
}
}

