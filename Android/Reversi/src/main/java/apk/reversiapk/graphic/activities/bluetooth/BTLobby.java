package apk.reversiapk.graphic.activities.bluetooth;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;
import apk.reversiapk.R;
import apk.reversiapk.core.FieldClickHandler;
import apk.reversiapk.core.ReversiCreator;
import apk.reversiapk.core.btconnection.ConnectionHolder;
import apk.reversiapk.core.btconnection.ConnectionThread;
import apk.reversiapk.core.enums.Constants;
import apk.reversiapk.core.exceptions.PlayerInitException;
import apk.reversiapk.core.exceptions.SendingException;
import apk.reversiapk.core.exceptions.StreamInitException;
import apk.reversiapk.graphic.activities.GameActivity;
import apk.reversiapk.graphic.activities.NameSelection;

import java.io.IOException;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

public class BTLobby extends AppCompatActivity {
private static final Logger LOG = Logger.getLogger(BTLobby.class.getName());
private ServerThread serverThread;
private BluetoothAdapter btAdapter;
private ConnectionThread connectionThread;
private ProgressBar loadingCircle;

@Override
protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_btobby);
    startWaiting();
}

@Override
public void onBackPressed() {
    endConnectionThreads();
    super.onBackPressed();
}

private void endConnectionThreads() {
    if (connectionThread != null) {
        connectionThread.stopConnection();
    } else if (serverThread != null) {
        serverThread.closeConnection();
        serverThread.interrupt();
    }
}

@Override
protected void onDestroy() {
    endConnectionThreads();
    super.onDestroy();
}

@Override
protected void onStop() {
    super.onStop();
    if (btAdapter != null) {
        btAdapter.cancelDiscovery();
    }
    if (serverThread != null) {
        serverThread.interrupt();
    }
}

private void startWaiting() {
    loadingCircle = (ProgressBar) findViewById(R.id.loadingPanel);
    btAdapter = BluetoothAdapter.getDefaultAdapter();
    serverThread = new ServerThread();
    serverThread.start();
}

private void manageConnectedSocket(@NonNull BluetoothSocket socket) {
    LOG.info(socket.toString());
    if (loadingCircle != null) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                loadingCircle.setVisibility(View.GONE);
            }
        });
    }
    try {
        connectionThread = new ConnectionThread(socket);
        ConnectionHolder.getInstance().setConnection(connectionThread);
        connectionThread.start();
    } catch (StreamInitException e) {
        LOG.log(Level.WARNING, Constants.CONNECTION_EXCEPTION.getMsg(), e);
        onBackPressed();
        Toast.makeText(getApplicationContext(), Constants.CONNECTION_EXCEPTION_MSG.getMsg(), Toast.LENGTH_SHORT).show();
    }
    Intent nameSelectIntent = new Intent(this, NameSelection.class);
    nameSelectIntent.putExtra(Constants.EXTRA_PLAYER_COUNT.getMsg(), (byte) 2);
    startActivityForResult(nameSelectIntent, Constants.REQUEST_CODE.getValue());
}

@Override
protected void onActivityResult(int requestCode, int resultCode, @NonNull Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    if (resultCode == RESULT_OK && requestCode == Constants.REQUEST_CODE.getValue()) {
        prepareBTGame(data.getStringExtra(Constants.NICK_ID.getMsg()));
    } else {
        Toast.makeText(getApplicationContext(), "Ending BT session", Toast.LENGTH_SHORT).show();
        onBackPressed();
    }
}

private void prepareBTGame(@NonNull String localPLayerName) {
    try {
        connectionThread.sendName(localPLayerName);
    } catch (SendingException e) {
        LOG.log(Level.WARNING, Constants.SENDING_EXCEPTION.getMsg(), e);
        Toast.makeText(getApplicationContext(), Constants.CONNECTION_EXCEPTION_MSG.getMsg(), Toast.LENGTH_SHORT).show();
        onBackPressed();
        return;
    }
    ReversiCreator btCreator = new ReversiCreator();
    FieldClickHandler clickHandler = new FieldClickHandler();
    try {
        btCreator.setBTLocalPlayer(localPLayerName, clickHandler);
    } catch (PlayerInitException e) {
        LOG.log(Level.SEVERE, "Local BluetoothPlayer initialization exception", e);
        Toast.makeText(getApplicationContext(), Constants.EXCEPTION_PLAYER_INIT_MSG.getMsg(), Toast.LENGTH_SHORT)
             .show();
        onBackPressed();
        return;
    }
    startBTGame(btCreator, clickHandler);
}

private void startBTGame(@NonNull ReversiCreator btCreator, FieldClickHandler clickHandler) {
    String remotePlayerName = null;
    remotePlayerName = getString(remotePlayerName);
    if (DFG(btCreator, remotePlayerName)) {
        return;
    }
    Intent btGameIntent = new Intent(this, GameActivity.class);
    btGameIntent.putExtra(Constants.CREATOR_ID.getMsg(), btCreator);
    btGameIntent.putExtra(Constants.CLICK_HANDLER_ID.getMsg(), clickHandler);
    startActivity(btGameIntent);
}

private boolean DFG(@NonNull ReversiCreator btCreator, String remotePlayerName) {
    try {
        btCreator.setBTRemotePlayer(remotePlayerName);
    } catch (PlayerInitException e) {
        LOG.log(Level.SEVERE, "BTPlayerRemote initialization exception", e);
        Toast.makeText(getApplicationContext(), Constants.EXCEPTION_PLAYER_INIT_MSG.getMsg(), Toast.LENGTH_SHORT)
             .show();
        onBackPressed();
        return true;
    }
    return false;
}

private String getString(String remotePlayerName) {
    remotePlayerName = sdf(remotePlayerName);
    return remotePlayerName;
}

@Nullable
private String sdf(@Nullable String remotePlayerName) {
    String checkingName;
    while (remotePlayerName == null) {
        checkingName = connectionThread.getReceivedName();
        if (checkingName != null && !checkingName.isEmpty()) {
            remotePlayerName = checkingName;
        } else {
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                LOG.log(Level.INFO, "Thread interrupted", e);
            }
        }
    }
    return remotePlayerName;
}

private class ServerThread extends Thread {
    @Nullable private final BluetoothServerSocket serverSocket;

    ServerThread() {
        // Use a temporary object that is later assigned to serverSocket,
        // because serverSocket is final
        BluetoothServerSocket tmp = null;
        try {
            // MY_UUID is the app's UUID string, also used by the client code
            tmp = btAdapter.listenUsingRfcommWithServiceRecord("Reversi", UUID.fromString(Constants.UUID_STRING
                                                                                                  .getMsg()));
        } catch (IOException e) {
            LOG.log(Level.WARNING, Constants.CONNECTION_EXCEPTION.getMsg(), e);
        }
        serverSocket = tmp;
    }

    @Override
    public void run() {
        LOG.info("Thread started");
        BluetoothSocket acceptingSocket;
        // Keep listening until exception occurs or a socket is returned
        while (true) {
            LOG.info("Hosting");
            try {
                acceptingSocket = getSocket();
            } catch (IOException e) {
                LOG.log(Level.WARNING, Constants.SOCKET_ACCEPTING_EXCEPTION.getMsg(), e);
                break;
            }
            // If a connection was accepted
            if (acceptingSocket != null) {
                // Do work to manage the connection (in a separate thread)
                manageConnectedSocket(acceptingSocket);
            }
        }
    }

    private BluetoothSocket getSocket() throws IOException {
        BluetoothSocket acceptingSocket;
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            LOG.log(Level.INFO, "Server Thread interrupted", e);
        }
        acceptingSocket = serverSocket.accept();
        return acceptingSocket;
    }

    /**
     * Will closeConnection the listening socket, and cause the thread to finish
     */
    void closeConnection() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            LOG.log(Level.INFO, "Interrupted wile closing connection");
        }
        try {
            serverSocket.close();
        } catch (IOException e) {
            LOG.log(Level.WARNING, Constants.SOCKET_CLOSING_EXCEPTION.getMsg(), e);
        }
    }
}
}
