package apk.reversiapk.graphic;

import android.app.Application;

public class Hints extends Application {
private String hint;

public String getHint() {
    return this.hint;
}

public void setHint(String newHint) {
    this.hint = newHint;
}
}
