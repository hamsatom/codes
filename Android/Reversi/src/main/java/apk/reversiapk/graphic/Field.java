package apk.reversiapk.graphic;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;

public class Field extends AppCompatImageView {
private byte positionX;
private byte positionY;

public Field(Context context) {
    super(context);
}

public Field(Context context, AttributeSet attrs) {
    super(context, attrs);
}

public Field(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
}

public byte getPositionX() {
    return positionX;
}

public void setPositionX(byte positionX) {
    this.positionX = positionX;
}

public byte getPositionY() {
    return positionY;
}

public void setPositionY(byte positionY) {
    this.positionY = positionY;
}

}
