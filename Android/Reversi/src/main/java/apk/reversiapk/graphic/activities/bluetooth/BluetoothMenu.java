package apk.reversiapk.graphic.activities.bluetooth;

import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;
import apk.reversiapk.R;
import apk.reversiapk.core.enums.Constants;

public class BluetoothMenu extends AppCompatActivity {
private BluetoothAdapter btAdapter;

@Override
protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_bluetooth_game);
    initAndCheckBT();
}

private void initAndCheckBT() {
    btAdapter = BluetoothAdapter.getDefaultAdapter();
    if (btAdapter == null) {
        Toast.makeText(getApplicationContext(), Constants.NO_BT_MSG.getMsg(), Toast.LENGTH_SHORT).show();
        finish();
    }
}

public void onClickLobby(View view) {
    if (checkEnabled()) {
        startActivity(new Intent(this, BTLobby.class));
    }
}


public void onCLickJoin(View view) {
    if (checkEnabled()) {
        startActivity(new Intent(this, BTQueue.class));
    }
}

private boolean checkEnabled() {
    if (btAdapter.isEnabled()) {
        return true;
    }
    Intent setupBT = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
    startActivityForResult(setupBT, Constants.REQUEST_CODE.getValue());
    return btAdapter.isEnabled() || btAdapter.getState() == BluetoothAdapter.STATE_TURNING_ON;
}


@Override
protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    if (resultCode != RESULT_OK && requestCode == Constants.REQUEST_CODE.getValue()) {
        Toast.makeText(getApplicationContext(), Constants.BT_OFF_MSG.getMsg(), Toast.LENGTH_SHORT).show();
    }
}
}
