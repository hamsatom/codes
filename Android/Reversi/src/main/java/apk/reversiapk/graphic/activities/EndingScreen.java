package apk.reversiapk.graphic.activities;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.TextView;
import apk.reversiapk.R;
import apk.reversiapk.core.HighscoreScanner;
import apk.reversiapk.core.enums.Constants;
import apk.reversiapk.core.players.HumanPlayer;
import apk.reversiapk.core.players.Player;

public class EndingScreen extends AppCompatActivity {
    private MediaPlayer mpplayer;

@Override
protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_ending_screen);
    soundGameOver();
    adjustSize();
    showResult();
}

private void soundGameOver() {
    mpplayer = MediaPlayer.create(this, R.raw.sound_game_over);
    mpplayer.start();
}

private void showResult() {
    TextView shownWinnerName = (TextView) findViewById(R.id.textViewWinnerName);
    TextView shownScore = (TextView) findViewById(R.id.textViewWinnerScore);
    TextView wonText = (TextView) findViewById(R.id.textViewWonText);
    Intent usedIntent = getIntent();
    if (shownScore == null || shownWinnerName == null || wonText == null) {
        return;
    }
    Player winner = (Player) usedIntent.getSerializableExtra(Constants.EXTRA_WINNER_NAME.getMsg());
    int score = usedIntent.getIntExtra(Constants.EXTRA_SCORE.getMsg(), 0);
    if (winner == null) {
        shownWinnerName.setText(String.valueOf("TIE"));
        wonText.setVisibility(View.GONE);
        shownScore.setVisibility(View.GONE);
    } else {
        shownWinnerName.setText(winner.getName());
        shownScore.setVisibility(View.VISIBLE);
        wonText.setVisibility(View.VISIBLE);
        shownScore.setText(String.valueOf(score));
        if (winner instanceof HumanPlayer) {
            HighscoreScanner save = new HighscoreScanner();
            save.writeToFile(getApplicationContext(), winner.getName(), score);
        }
    }
}

private void adjustSize() {
    DisplayMetrics screen = new DisplayMetrics();
    getWindowManager().getDefaultDisplay().getMetrics(screen);
    int width = (int) (0.6 * screen.widthPixels);
    int height = (int) (0.3 * screen.heightPixels);
    getWindow().setLayout(width, height);
}


public void onClickRestart(View endScreen) {
    setResult(RESULT_OK);
    mpplayer.release();
    finish();
}

public void onCLickBack(View endScreen) {
    setResult(RESULT_CANCELED);
    mpplayer.release();
    finish();
}
}
