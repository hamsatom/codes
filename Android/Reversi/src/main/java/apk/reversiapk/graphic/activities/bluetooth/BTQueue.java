package apk.reversiapk.graphic.activities.bluetooth;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;
import apk.reversiapk.R;
import apk.reversiapk.core.FieldClickHandler;
import apk.reversiapk.core.ReversiCreator;
import apk.reversiapk.core.btconnection.ConnectionHolder;
import apk.reversiapk.core.btconnection.ConnectionThread;
import apk.reversiapk.core.enums.Constants;
import apk.reversiapk.core.exceptions.PlayerInitException;
import apk.reversiapk.core.exceptions.SendingException;
import apk.reversiapk.core.exceptions.StreamInitException;
import apk.reversiapk.graphic.activities.GameActivity;
import apk.reversiapk.graphic.activities.NameSelection;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Set;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.StreamHandler;

public class BTQueue extends AppCompatActivity implements AdapterView.OnItemClickListener {
private static final Logger LOG = Logger.getLogger(BTQueue.class.getName());
private ArrayList<BluetoothDevice> foundDevices;
private ArrayAdapter<String> listAdapter;
private BluetoothAdapter btAdapter;
private Set<BluetoothDevice> pairedDevices;
// Create a BroadcastReceiver for ACTION_FOUND
private final BroadcastReceiver receiver = new BroadcastReceiver() {
    public void onReceive(Context context, @NonNull Intent intent) {
        String action = intent.getAction();
        // When discovery finds a device
        if (BluetoothDevice.ACTION_FOUND.equals(action)) {
            // Get the BluetoothDevice object from the Intent
            BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
            LOG.info("Found " + device.getName());
            // Add the name and address to an array adapter to show in a ListView
            String tag = Constants.DISCOVERED_TAG.getMsg();
            for (BluetoothDevice pairedDevice : pairedDevices) {
                if (pairedDevice.getName().equals(device.getName())) {
                    tag = Constants.DISCOVERED_N_PAIRED_TAG.getMsg();
                    listAdapter.remove(pairedDevice.getName() + Constants.PAIRED_TAG.getMsg() + "\n" + pairedDevice
                                                                                                               .getAddress());
                    break;
                }
            }
            listAdapter.add(device.getName() + tag + "\n" + device.getAddress());
            foundDevices.add(device);
        }
    }
};
private ConnectionThread connectionThread;
private JoiningThread connect;

@Override
protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_btqueue);
    LOG.addHandler(new StreamHandler());
    init();
}

private void init() {
    foundDevices = new ArrayList<>();
    btAdapter = BluetoothAdapter.getDefaultAdapter();
    ListView listView = (ListView) findViewById(R.id.listView);
    if (listView == null) {
        return;
    }
    listView.setOnItemClickListener(this);
    listAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1);
    listView.setAdapter(listAdapter);
    getPairedDevices();

    // Register the BroadcastReceiver
    registerReceiver(receiver, new IntentFilter(BluetoothDevice.ACTION_FOUND));
    startDiscovery();
}

private void getPairedDevices() {
    pairedDevices = btAdapter.getBondedDevices();
    for (BluetoothDevice pairedDevice : pairedDevices) {
        listAdapter.add(pairedDevice.getName() + Constants.PAIRED_TAG.getMsg() + "\n" + pairedDevice.getAddress());
        foundDevices.add(pairedDevice);
    }
}

@Override
protected void onDestroy() {
    endConnectionThreads();
    super.onDestroy();
}

private void endConnectionThreads() {
    if (connectionThread != null) {
        connectionThread.stopConnection();
    } else if (connect != null) {
        connect.closeConnection();
        connect.interrupt();
    }
}

@Override
protected void onStop() {
    try {
        this.unregisterReceiver(receiver);
    } catch (IllegalArgumentException e) {
        LOG.log(Level.WARNING, "Receiver disappeared", e);
    }
    if (btAdapter != null) {
        btAdapter.cancelDiscovery();
    }
    if (connect != null) {
        connect.interrupt();
    }
    super.onStop();
}

private void startDiscovery() {
    btAdapter.cancelDiscovery();
    btAdapter.startDiscovery();
}

@Override
public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
    BluetoothDevice selectedDevice = foundDevices.get(position);
    LOG.info(selectedDevice.getName());
    LOG.info("Position " + position);
    connect = new JoiningThread(selectedDevice);
    connect.start();
}

@Override
public void onBackPressed() {
    endConnectionThreads();
    super.onBackPressed();
}


private void manageConnectedSocket(@NonNull BluetoothSocket socket) {
    LOG.info(socket.toString());
    try {
        connectionThread = new ConnectionThread(socket);
        ConnectionHolder.getInstance().setConnection(connectionThread);
        connectionThread.start();
    } catch (StreamInitException e) {
        LOG.log(Level.WARNING, Constants.CONNECTION_EXCEPTION.getMsg(), e);
        onBackPressed();
        Toast.makeText(getApplicationContext(), Constants.CONNECTION_EXCEPTION_MSG.getMsg(), Toast.LENGTH_SHORT).show();
    }
    Intent nameSelectIntent = new Intent(this, NameSelection.class);
    nameSelectIntent.putExtra(Constants.EXTRA_PLAYER_COUNT.getMsg(), (byte) 2);
    startActivityForResult(nameSelectIntent, Constants.REQUEST_CODE.getValue());
}

@Override
protected void onActivityResult(int requestCode, int resultCode, @NonNull Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    if (resultCode == RESULT_OK && requestCode == Constants.REQUEST_CODE.getValue()) {
        prepareAndStartBTGame(data.getStringExtra(Constants.NICK_ID.getMsg()));
    } else {
        Toast.makeText(getApplicationContext(), "Ending BT session", Toast.LENGTH_SHORT).show();
        onBackPressed();
    }
}

private void prepareAndStartBTGame(@NonNull String localPLayerName) {
    try {
        connectionThread.sendName(localPLayerName);
    } catch (SendingException e) {
        LOG.log(Level.WARNING, Constants.SENDING_EXCEPTION.getMsg(), e);
        Toast.makeText(getApplicationContext(), Constants.CONNECTION_EXCEPTION_MSG.getMsg(), Toast.LENGTH_SHORT).show();
        onBackPressed();
        return;
    }
    ReversiCreator btCreator = new ReversiCreator();
    String remotePlayerName = null;
    String checkingName;

    while (remotePlayerName == null) {
        checkingName = connectionThread.getReceivedName();
        if (checkingName != null && !checkingName.isEmpty()) {
            remotePlayerName = checkingName;
        } else {
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                LOG.log(Level.INFO, "Thread interrupted", e);
            }
        }
    }
    try {
        btCreator.setBTRemotePlayer(remotePlayerName);
    } catch (PlayerInitException e) {
        LOG.log(Level.SEVERE, "BTPlayerRemote initialization exception", e);
        Toast.makeText(getApplicationContext(), Constants.EXCEPTION_PLAYER_INIT_MSG.getMsg(), Toast.LENGTH_SHORT)
             .show();
        onBackPressed();
        return;
    }
    FieldClickHandler clickHandler = new FieldClickHandler();
    try {
        btCreator.setBTLocalPlayer(localPLayerName, clickHandler);
    } catch (PlayerInitException e) {
        LOG.log(Level.SEVERE, "Local BluetoothPlayer initialization exception", e);
        Toast.makeText(getApplicationContext(), Constants.EXCEPTION_PLAYER_INIT_MSG.getMsg(), Toast.LENGTH_SHORT)
             .show();
        onBackPressed();
        return;
    }
    Intent btGameIntent = new Intent(this, GameActivity.class);
    btGameIntent.putExtra(Constants.CREATOR_ID.getMsg(), btCreator);
    btGameIntent.putExtra(Constants.CLICK_HANDLER_ID.getMsg(), clickHandler);
    startActivity(btGameIntent);
}

private class JoiningThread extends Thread {
    @Nullable private final BluetoothSocket joiningSocket;

    JoiningThread(@NonNull BluetoothDevice device) {
        // Use a temporary object that is later assigned to joiningSocket,
        // because joiningSocket is final
        BluetoothSocket tmp = null;

        // Get a BluetoothSocket to connect with the given BluetoothDevice
        try {
            // MY_UUID is the app's UUID string, also used by the server code
            tmp = device.createRfcommSocketToServiceRecord(UUID.fromString(Constants.UUID_STRING.getMsg()));
        } catch (IOException e) {
            LOG.log(Level.WARNING, Constants.CONNECTION_EXCEPTION.getMsg(), e);
        }
        joiningSocket = tmp;
    }

    @Override
    public void run() {
        // Cancel discovery because it will slow down the connection
        btAdapter.cancelDiscovery();

        try {
            tryToSleepThread();
            // Connect the device through the socket. This will block
            // until it succeeds or throws an exception
            LOG.info("Connecting");
            joiningSocket.connect();
        } catch (IOException connectException) {
            LOG.log(Level.WARNING, Constants.CONNECTION_EXCEPTION.getMsg(), connectException);
            // Unable to connect; close the socket and get out
            try {
                joiningSocket.close();
            } catch (IOException closeException) {
                LOG.log(Level.WARNING, Constants.SOCKET_CLOSING_EXCEPTION.getMsg(), closeException);
            }
            return;
        }

        // Do work to manage the connection (in a separate thread)
        manageConnectedSocket(joiningSocket);
    }

    private void tryToSleepThread() {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            LOG.log(Level.INFO, "Joining thread interrupted", e);
        }
    }

    /**
     * Will closeConnection an in-progress connection, and close the socket
     */
    void closeConnection() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            LOG.log(Level.INFO, "Interrupted wile closing connection");
        }
        try {
            joiningSocket.close();
        } catch (IOException e) {
            LOG.log(Level.WARNING, Constants.SOCKET_CLOSING_EXCEPTION.getMsg(), e);
        }
    }
}
}
