package apk.reversiapk.graphic.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import apk.reversiapk.R;
import apk.reversiapk.core.FieldClickHandler;
import apk.reversiapk.core.ReversiCreator;
import apk.reversiapk.core.enums.Constants;
import apk.reversiapk.core.exceptions.PlayerInitException;
import apk.reversiapk.graphic.Hints;

import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.StreamHandler;

public class PlayerSelection extends AppCompatActivity {
private static final Logger LOG = Logger.getLogger(PlayerSelection.class.getName());
private final ReversiCreator creator = new ReversiCreator();
private final FieldClickHandler clickHandler = new FieldClickHandler();
private Intent gameIntent;
@Nullable private String lastName;
private byte playerCount = 1;

@Override
protected void onCreate(Bundle savedInstanceState) {
    LOG.addHandler(new StreamHandler());
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_player_selection);
    gameIntent = new Intent(this, GameActivity.class);
    updateButtons();
}

private void updateButtons() {
    Button lastPlayer = (Button) findViewById(R.id.btnLastPlr);
    Button newPplayer = (Button) findViewById(R.id.btnNewPlr);
    newPplayer.setText(R.string.userPlayer);
    lastPlayer.setVisibility(View.GONE);
    Hints hint = (Hints) getApplication();
    lastName = hint.getHint();
    if (lastName == null) {
        return;
    }
    lastPlayer.setText(lastName);
    lastPlayer.setVisibility(View.VISIBLE);
    newPplayer.setText(R.string.newUser);
}

@Override
protected void onResume() {
    updateButtons();
    super.onResume();
}

private void updateNumber() {
    ++playerCount;
    if (playerCount > Constants.PLAYER_LIMIT.getValue()) {
        gameIntent.putExtra(Constants.CREATOR_ID.getMsg(), creator);
        startActivity(gameIntent);
        playerCount = 1;
        creator.removePlayers();
    }
    TextView playerSelectionScreen = (TextView) findViewById(R.id.textViewPlayerNumber);
    if (playerSelectionScreen == null) {
        return;
    }
    playerSelectionScreen.setText(String.valueOf(playerCount));
}

public final void onClickLastPlr(View playerSelection) {
    try {
        creator.setPlayerHuman(lastName, clickHandler);
        gameIntent.putExtra(Constants.CLICK_HANDLER_ID.getMsg(), clickHandler);
        updateNumber();
    } catch (PlayerInitException e) {
        recordException(e, lastName + " initialization exception");
    }
}

public final void onClickDummy(View playerSelection) {
    try {
        creator.setPlayerAI();
        updateNumber();
    } catch (PlayerInitException e) {
        recordException(e, "DummyPlayer initialization exception");
    }
}

    public final void onClickDita(View playerSelection) {
        try {
            creator.setPlayerDita();
            updateNumber();
        } catch (PlayerInitException e) {
            recordException(e, "DitaBot initialization exception");
        }
    }

    public final void onClickHrusa(View playerSelection) {
        try {
            creator.setPlayerHrusa();
            updateNumber();
        } catch (PlayerInitException e) {
            recordException(e, "HrusaBot initialization exception");
        }
    }

public final void onClickAdaptive(View playerSelection) {
    try {
        creator.setPlayerAdaptive();
        updateNumber();
    } catch (PlayerInitException e) {
        recordException(e, "AdaptivePlayer initialization exception");
    }
}

    private void recordException(PlayerInitException e, String msg) {
        LOG.log(Level.SEVERE, msg, e);
        Toast.makeText(getApplicationContext(), Constants.EXCEPTION_PLAYER_INIT_MSG.getMsg(), Toast.LENGTH_SHORT).show();
    }


public final void onClickMinMax(View playerSelection) {
    try {
        creator.setPlayerSimon();
        updateNumber();
    } catch (PlayerInitException e) {
        recordException(e, "MinMaxPlayer initialization exception");
    }
}

public final void onClickGreedy(View playerSelection) {
    try {
        creator.setPlayerGreedy();
        updateNumber();
    } catch (PlayerInitException e) {
        recordException(e, "GreedyPlayer initialization exception");
    }
}

public final void onClickAdvanced(View playerSelection) {
    try {
        creator.setPlayerAdvancedAI();
        updateNumber();
    } catch (PlayerInitException e) {
        recordException(e, "AdvancedPlayer initialization exception");
    }
}

public final void onClickMcts(View playerSelection) {
    try {
        creator.setPlayerMcts();
        updateNumber();
    } catch (PlayerInitException e) {
        recordException(e, "Mcts initialization exception");
    }
}

public final void onClickHuman(View playerSelection) {
    Intent nameScreen = new Intent(this, NameSelection.class);
    nameScreen.putExtra(Constants.EXTRA_PLAYER_COUNT.getMsg(), playerCount);
    startActivityForResult(nameScreen, Constants.REQUEST_CODE.getValue());
}

public final void onClickMtdf(View playerSelection) {
    try {
        creator.setPlayerMtdf();
        updateNumber();
    } catch (PlayerInitException e) {
        recordException(e, e.getMessage());
    }
}

@Override
protected void onActivityResult(int requestCode, int resultCode, @NonNull Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    if (requestCode == Constants.REQUEST_CODE.getValue() && resultCode == RESULT_OK) {
        try {
            creator.setPlayerHuman(data.getStringExtra(Constants.NICK_ID.getMsg()), clickHandler);
            gameIntent.putExtra(Constants.CLICK_HANDLER_ID.getMsg(), clickHandler);
            updateNumber();
        } catch (PlayerInitException e) {
            recordException(e, "HumanPlayer initialization exception");
        }
    } else {
        LOG.warning("Name selection ended with different request code or with wrong result");
    }
}
}