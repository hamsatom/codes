package apk.reversiapk.graphic.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import apk.reversiapk.R;
import apk.reversiapk.core.HighscoreScanner;

import java.util.logging.Logger;
import java.util.logging.StreamHandler;

public class Highscore extends AppCompatActivity {
private static final Logger LOG = Logger.getLogger(Highscore.class.getName());

@Override
protected void onCreate(Bundle savedInstanceState) {
    LOG.addHandler(new StreamHandler());
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_highscore);
    showHighscore();
}

/**
 * Gets highscore from file and ads it to list view via list adapter
 */
private void showHighscore() {
    ListView listView = (ListView) findViewById(R.id.listViewHighScore);
    if (listView == null) {
        return;
    }
    ArrayAdapter<String> listAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1);
    listView.setAdapter(listAdapter);
    HighscoreScanner scores = new HighscoreScanner();

    //splits data got from file so each string contains name and score so it's showing properly in ListView
    String[] score = scores.readWholeFile(getApplicationContext()).split("\n" + "\n");
    for (String aScore : score) {
        listAdapter.add(aScore);
    }
}
}
