package apk.reversiapk.graphic.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.MultiAutoCompleteTextView;
import android.widget.Toast;
import apk.reversiapk.R;
import apk.reversiapk.core.enums.Constants;
import apk.reversiapk.graphic.Hints;

import java.util.logging.Logger;
import java.util.logging.StreamHandler;

public class NameSelection extends AppCompatActivity {
private static final Logger LOG = Logger.getLogger(NameSelection.class.getName());
private Hints hints;
private MultiAutoCompleteTextView nickField;

@Override
protected void onCreate(Bundle savedInstanceState) {
    LOG.addHandler(new StreamHandler());
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_name_selection);
    nickField = (MultiAutoCompleteTextView) findViewById(R.id.editChooseNick);
    updateHint();
    setButton();
}


private void updateHint() {
    hints = (Hints) getApplication();
    if (nickField == null) {
        LOG.severe("Nickfield was null");
        return;
    }
    String newHint = hints.getHint();
    if (newHint != null) {
        nickField.setHint(newHint);
    }
}

private void setButton() {
    Button selecetNext = (Button) findViewById(R.id.buttonSelectNext);
    Button startGame = (Button) findViewById(R.id.buttonStartGame);

    if (selecetNext == null || startGame == null) {
        LOG.severe("Button in nameselection was null");
        return;
    }

    if (getIntent().getByteExtra(Constants.EXTRA_PLAYER_COUNT.getMsg(), (byte) 1) > 1) {
        selecetNext.setVisibility(View.GONE);
        startGame.setVisibility(View.VISIBLE);
    } else {
        startGame.setVisibility(View.GONE);
        selecetNext.setVisibility(View.VISIBLE);
    }
}

public final void OnClickConfirmNick(View nickScreen) {
    if (nickField == null) {
        LOG.severe("Nickfield was null");
        return;
    }
    
    String nickName = nickField.getText().toString().trim();
    if (nickName.length() == 0) {
        Toast.makeText(getApplicationContext(), Constants.NICK_EMPTY_MSG.getMsg(), Toast.LENGTH_SHORT).show();
        return;
    } else if (nickName.length() > Constants.NICK_MAX_LENGTH.getValue()) {
        Toast.makeText(getApplicationContext(), Constants.NICK_LONG_MSG.getMsg(), Toast.LENGTH_SHORT).show();
        LOG.info("Nick too long: " + nickName.length());
        return;
    }
    LOG.info("Nick taken is: " + nickName);
    hints.setHint(nickName);
    setResult(RESULT_OK, new Intent().putExtra(Constants.NICK_ID.getMsg(), nickName));
    finish();
}
}
