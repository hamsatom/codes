package apk.reversiapk.graphic.activities;

import android.content.Intent;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayout;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;
import apk.reversiapk.R;
import apk.reversiapk.core.FieldClickHandler;
import apk.reversiapk.core.ReversiCreator;
import apk.reversiapk.core.ReversiMove;
import apk.reversiapk.core.enums.Colour;
import apk.reversiapk.core.enums.Constants;
import apk.reversiapk.core.players.Player;
import apk.reversiapk.graphic.Field;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.StreamHandler;

public class GameActivity extends AppCompatActivity {
private static final Logger LOG = Logger.getLogger(GameActivity.class.getName());
private static final int STONE_WHITE = R.drawable.stone_white;
private static final int STONE_BLACK = R.drawable.stone_black;
private static final int STONE_GREY = R.drawable.stone_grey;
private static final int EMPTY_FIELD = R.drawable.stone_empty;
private static final int AVAILABLE_WHITE = R.drawable.stone_available_white;
private static final int AVAILABLE_BLACK = R.drawable.stone_available_black;
private final Field[][] fields = new Field[Constants.BOARD_ROWS.getValue()][Constants.BOARD_COLUMNS.getValue()];
private ReversiCreator usedCreator;
private boolean showPossible = true;
private TextView shownName1;
private TextView shownName2;
    private MediaPlayer backgroundMusic;
    private MediaPlayer musicStoneFlip;
    private MediaPlayer
            soundFight;
private int positionWhereTrackStopped = 0;


@Override
protected void onCreate(Bundle savedInstanceState) {
    LOG.addHandler(new StreamHandler());
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_game);
    shownName1 = (TextView) findViewById(R.id.textViewPlayer1Name);
    shownName2 = (TextView) findViewById(R.id.textViewPlayer2Name);
    showFields();
    musicStoneFlip = MediaPlayer.create(this, R.raw.sound_flip);
    musicStoneFlip.setVolume(50, 50);
    startGameLogic();
}

private void soundFight() {
    soundFight = MediaPlayer.create(this, R.raw.sound_fight);
    soundFight.start();
}

public void updateNames(final String player1Name, final String player2Name) {
    if (shownName1 == null || shownName2 == null) {
        LOG.severe("One of shown names was null");
        return;
    }
    this.runOnUiThread(new Runnable() {
        @Override
        public void run() {
            shownName1.setText(player1Name);
            shownName2.setText(player2Name);
        }
    });
}

private void soundBackGroundTrack() {
    backgroundMusic = MediaPlayer.create(this, R.raw.sound_background_track);
    backgroundMusic.setLooping(true);
    backgroundMusic.seekTo(positionWhereTrackStopped);
    CheckBox box = (CheckBox) findViewById(R.id.checkBox3);
    if (box.isChecked()) {
        backgroundMusic.start();
    }
}

public void showTurned(final byte column, final byte row) {
    this.runOnUiThread(new Runnable() {
        @Override
        public void run() {
            try {
                musicStoneFlip.start();
            } catch (Exception e) {
                LOG.log(Level.INFO, e.getLocalizedMessage(), e);
            }
            fields[column][row].setBackgroundResource(STONE_GREY);
        }
    });
}

@Override
protected void onStop() {
    endMusic();
    super.onStop();
}

public void onClickchangeStateOFBackgroundMusic(View view) {
    if (backgroundMusic.isPlaying()) {
        positionWhereTrackStopped = backgroundMusic.getCurrentPosition();
        backgroundMusic.pause();
    } else {
        backgroundMusic.seekTo(positionWhereTrackStopped);
        backgroundMusic.start();
    }
}

public void updateStones(final byte player1Stones, final byte player2Stones) {
    this.runOnUiThread(new Runnable() {
        @Override
        public void run() {
            TextView shownStones1 = (TextView) findViewById(R.id.textViewPlayer1Stones);
            TextView shownStones2 = (TextView) findViewById(R.id.textViewPlayer2Stones);
            if (shownStones1 == null || shownStones2 == null) {
                LOG.severe("One of stone counters was null");
                return;
            }
            shownStones1.setText(String.valueOf(player1Stones));
            shownStones2.setText(String.valueOf(player2Stones));
        }
    });
}

private void startGameLogic() {
    try {
        usedCreator = getIntent().getParcelableExtra(Constants.CREATOR_ID.getMsg());
        usedCreator.CreateGame(this);
    } catch (Throwable e) {
        LOG.log(Level.SEVERE, "Controller exception", e);
        Toast.makeText(getApplicationContext(), "Controller exception occurred", Toast.LENGTH_SHORT).show();
        backgroundMusic.stop();
        endMusic();
        finish();
    }
    soundFight();
}

@Override
public void onBackPressed() {
    super.onBackPressed();
    usedCreator.endGameLogic();
    this.finish();
    endMusic();
    musicStoneFlip.release();
}

private void showFields() {
    Intent thisIntent = getIntent();
    GridLayout gridLayout = (GridLayout) findViewById(R.id.gameBoard);
    FieldClickHandler onClickField = null;
    Field field;

    if (gridLayout == null) {
        LOG.severe("Gridlayout was null");
        return;
    }

    if (thisIntent.hasExtra(Constants.CLICK_HANDLER_ID.getMsg())) {
        LOG.info("Found clickHandler");
        onClickField = (FieldClickHandler) thisIntent.getSerializableExtra(Constants.CLICK_HANDLER_ID.getMsg());
    }

    for (byte y = 0; y < Constants.BOARD_ROWS.getValue(); ++y) {
        for (byte x = 0; x < Constants.BOARD_COLUMNS.getValue(); ++x) {
            field = new Field(this);
            field.setPositionX(x);
            field.setPositionY(y);
            field.setClickable(true);
            field.setOnClickListener(onClickField);
            field.setBackgroundResource(EMPTY_FIELD);
            fields[x][y] = field;
            gridLayout.addView(field);
        }
    }
}

public final void updateBoard(final Colour[][] placement) {
    this.runOnUiThread(new Runnable() {
        @Override
        public void run() {
            showPlacement(placement);
        }
    });
}

private void showPlacement(final Colour[][] placement) {
    for (byte y = 0; y < Constants.BOARD_ROWS.getValue(); ++y) {
        for (byte x = 0; x < Constants.BOARD_COLUMNS.getValue(); ++x) {
            switch (placement[x][y]) {
                case PLAYER_1_COLOUR:
                    fields[x][y].setBackgroundResource(STONE_BLACK);
                    fields[x][y].setTag(null);
                    break;
                case PLAYER_2_COLOUR:
                    fields[x][y].setBackgroundResource(STONE_WHITE);
                    fields[x][y].setTag(null);
                    break;
                default:
                    fields[x][y].setBackgroundResource(EMPTY_FIELD);
                    fields[x][y].setTag(null);
                    break;
            }
        }
    }
}

public final void setPossibleField(final Colour colour, @NonNull final List<ReversiMove> possibleMoves) {
    if (!showPossible) {
        return;
    }
    this.runOnUiThread(new Runnable() {
        @Override
        public void run() {
            showAvalibleMoves(colour, possibleMoves);
        }
    });
}

private void showAvalibleMoves(Colour colour, @NonNull List<ReversiMove> possibleMoves) {
    for (ReversiMove availableMove : possibleMoves) {
        if (colour == Colour.PLAYER_1_COLOUR) {
            fields[availableMove.getX()][availableMove.getY()].setBackgroundResource(AVAILABLE_BLACK);
            fields[availableMove.getX()][availableMove.getY()].setTag(Constants.MARKED_POSSIBLE);
        } else {
            fields[availableMove.getX()][availableMove.getY()].setBackgroundResource(AVAILABLE_WHITE);
            fields[availableMove.getX()][availableMove.getY()].setTag(Constants.MARKED_POSSIBLE);
        }
    }
}

public void onClickHints(View hintBtn) {
    showPossible = !showPossible;
    if (showPossible) {
        setPossibleField(usedCreator.getCurrentColour(), usedCreator.getCurrentPossible());
        return;
    }
    for (byte y = 0; y < Constants.BOARD_ROWS.getValue(); ++y) {
        for (byte x = 0; x < Constants.BOARD_COLUMNS.getValue(); ++x) {
            if (fields[x][y].getTag() == Constants.MARKED_POSSIBLE) {
                fields[x][y].setBackgroundResource(EMPTY_FIELD);
                fields[x][y].setTag(null);
            }
        }
    }
}

public void shownOnMove1() {
    this.runOnUiThread(new Runnable() {
        @Override
        public void run() {
            shownName1.setBackgroundColor(Color.rgb(23, 88, 23));
            shownName2.setBackgroundColor(Color.TRANSPARENT);
        }
    });
}

public void shownOnMove2() {
    this.runOnUiThread(new Runnable() {
        @Override
        public void run() {
            shownName1.setBackgroundColor(Color.TRANSPARENT);
            shownName2.setBackgroundColor(Color.rgb(23, 88, 23));
        }
    });
}

public void showEndingScreen(Player winnerPlayer, int score) {
    Intent endScreenIntent = new Intent(this, apk.reversiapk.graphic.activities.EndingScreen.class);
    endScreenIntent.putExtra(Constants.EXTRA_WINNER_NAME.getMsg(), winnerPlayer);
    endScreenIntent.putExtra(Constants.EXTRA_SCORE.getMsg(), score);
    startActivityForResult(endScreenIntent, Constants.REQUEST_CODE.getValue());
    endMusic();
}

    private void endMusic() {
        try {
            positionWhereTrackStopped = backgroundMusic.getCurrentPosition();
        } catch (Exception e) {
            LOG.log(Level.INFO, e.getMessage(), e);
        }
        backgroundMusic.release();
        soundFight.release();
    }

    @Override
    protected void onResume() {
        super.onResume();
        soundBackGroundTrack();
    }

    @Override
protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    if (resultCode == RESULT_OK) {
        soundFight();
        usedCreator.restartGame();
    } else {
        onBackPressed();
    }
}
}


