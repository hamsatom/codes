package apk.reversiapk.graphic.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import apk.reversiapk.R;
import apk.reversiapk.graphic.activities.bluetooth.BluetoothMenu;

public class Menu extends AppCompatActivity {

@Override
protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_menu);
}

public final void OnClickRules(View menu) {
    startActivity(new Intent(this, Rules.class));
}

public final void OnClickLocal(View menu) {
    startActivity(new Intent(this, PlayerSelection.class));
}

public final void onClickExit(View menu) {
    finish();
}

public final void onClickHighScore(View menu) {
    startActivity(new Intent(this, Highscore.class));
}

public final void onClickBluettoth(View menu) {
    startActivity(new Intent(this, BluetoothMenu.class));
}
}
